const electron = require('electron');
// const { app } = electron;
const { BrowserWindow} = electron;
const { autoUpdater } = require("electron-updater");
const { appInfo } = require('./backend/services/t32XrayStudioAppInfoHandler');
const { app } = appInfo;
const { os } = appInfo;

const homeDir = appInfo.getAppHome(),
// db = require('./backend/db')
path = require('path'),
logDir = path.join(homeDir, 'logs/'),
logPath = path.join(logDir, 'XrayStudioBackend.log'),
url = require('url'),
fs = require('fs-extra'),
logger = require('electron-log'),
appVersion = require('./package.json').version,
t32XrayStudioMain = require('./backend/t32XrayStudioMain'),
t32XrayStudioUploadQueue = require('./backend/services/t32XrayStudioUploadQueue'),
xrayStudioFs = require('./backend/services/t32XrayStudioFs'),
t32XrayStudioSettingService = require('./backend/services/t32XrayStudioSettingsService'),
// t32XrayStudioBrowserWindowHandler = require('./backend/services/t32XrayStudioBrowserWindowHandler');
t32XrayStudioAppWindowLauncher = require('./backend/services/t32XrayStudioAppWindowLauncher'),
t32XrayStudioAppUpdater = require("./backend/services/t32XrayStudioAppUpdater");

// Keep reference of main window because of GC
// mainWindow = null,
// frameFinished = false,
// feedUrlSet = false,
// autoUpdateRan = false,
var isDevelopment = process.env.NODE_ENV === 'development',
updateFeed = 'https://images.tab32.com/imagingStudio/updates/latest/',
frontEndFilePath = path.join(__dirname, '/frontend/xrayStudioV1/index.html'),
studioSetting = null,
studioContext = null;

logger.transports.file.level = 'info';
// logger.transports.file.resolvePath = (variables) => {
//   return logPath;
// }
logger.transports.file.file = logPath;
logger.transports.maxSize = 10048576;

xrayStudioFs.ensureDirSync(logDir);

/**
 * Updated 2/15/19 Jagvir Sarai
 * Load settings first, then start the app window, if settings loading fails, kill app.
 */
app.on('ready', function () {
  logger.info("testing delete");

  // db.testDelete('TreatmentQuickKey', '01901041')
  // .then(() => {
  //   logger.info('test deleclearte complete')
  //   app.quit()
  // })
  // .catch((e) => {
  //   logger.error("error testing delete ", e)
  // })

  logger.info('here are the args passed to us ', process.argv)
  var appArgs = process.argv;

  initializeApp(appArgs)
  .then(() => {
    logger.info("------APP INIT COMPLETE------")
  })
  .catch((err) => {
    if (err.commsError) {
      logger.info('---ERROR WITH DRIVER COMMS---');
      logger.info(err);
    } else if (err.initErr) {
      logger.info("----ERROR INTIALIZING APP-----", err.initErr);
      logger.info("-----QUITING APP-----");
      studioContext = err.context;
      studioContext.browerWindow.exitAppWindow();
    } else {
      logger.info("----ERROR WITH POST INIT STEP-----", err);
    }
  })
});

// Quit when all windows are closed
app.on('window-all-closed', function () {
  logger.info("------SAVING STUDIO SETTINGS BEFORE QUIT----");
  t32XrayStudioSettingService.storeSettingsInJsonImmediate()
    .then(() => {
      logger.info("-----QUITING APP-----");
      app.quit();
    })
    .catch((err) => {
      logger.info("-----FAILED TO SAVE STUDIO SETTINGS----");
      logger.error(err);
      logger.info("-----QUITING APP-----");
      app.quit();
    })
});


async function initializeApp(appArgs) {
  try {
    let setting = await t32XrayStudioSettingService.getSettings();

    logger.info("-----Initializing App------");

    updateFeed = setting.appUpdateUrl || updateFeed;

    setting.viewMode = null;
    studioSetting = setting;

    logger.info('Setting up main window');

    let browserWindow = await t32XrayStudioAppWindowLauncher.initialize(frontEndFilePath, isDevelopment);

    logger.info("main window loaded frame");
    studioContext = await t32XrayStudioMain.buildStudioContext(appInfo, browserWindow, studioSetting, appArgs);

    logger.info("-----Studio Main Initialize complete-----");
    // logger.info(studioContext)
    logger.info("----STARTING DRIVER COMMS SETUP-----");
    await t32XrayStudioMain.startDriverComms(studioContext);
    // logger.info("------DRIVER MSG SENT-----");
    // logger.info("------STARTING DB INIT-----");
    // await t32XrayStudioMain.startDbConnection(studioContext);
    // logger.info("------ DB INIT COMPLETE-----");
    logger.info("----CHECKING FOR APP UPDATES-----");
    await t32XrayStudioAppUpdater.initialize(studioContext.theApp.app.getVersion(), updateFeed, appUpdaterResponse)

  } catch (e) {
    throw e;
  }
}

const appUpdaterResponse = function (actionObj) {
  try {
    if (actionObj.action == 'notifyClient') {
      studioContext.browerWindow.notifyClient(actionObj.msg);
    } else if (actionObj.action == 'removeAppUrl') {
      studioSetting.appUpdateUrl = '';
      let msg = {};
      msg.keyValToChange = {
        appUpdateUrl: ""
      };
      msg.msgType = 'updateXrayStudioSettingsProperty'

      studioContext.browserWindow.notifyClient(msg);
    } else if (actionObj.action == 'error') {
      throw actionObj.msg
    }
  } catch (e) {
    logger.error("error performing updater callback ", e)
  }
}

// /**
//  * Checks to see if we have a minor or major update
//  * @param {*} trimmedApVer 
//  * @param {*} trimmedUpdateVer 
//  */
// function isDiffMinorVersions(trimmedApVer, trimmedUpdateVer) {
//   return trimmedApVer !== trimmedUpdateVer ? true : false;
// }

// function checkForUpdates() {
//   logger.info("checking for updates");
//   if (!autoUpdateRan) {
//     if (feedUrlSet && frameFinished) {
//       logger.info("update checker ready");
//       logger.info('feedUrlSet: ' + feedUrlSet);
//       logger.info('frameFinised: ' + frameFinished);
//       autoUpdater.checkForUpdates();
//       autoUpdateRan = true
//     } else {
//       logger.info("update checker not ready");
//       logger.info('feedUrlSet: ' + feedUrlSet);
//       logger.info('frameFinised: ' + frameFinished);
//     }
//   } else {
//     logger.info("update checker already ran");
//   }
// }

// function trimVersion(appVersion) {
//   var reg = /\d\.\d/;
//   var appVerMatch = appVersion.match(reg);
//   logger.info(appVerMatch);
//   var trimVer = appVerMatch && appVerMatch.length ? appVerMatch[0] : null;

//   logger.info("The trimmed app version")
//   logger.info(trimVer);
//   return trimVer;
// }

// function initializeAppUpdater() {
//   try {
//     var trimmedAppVersion = trimVersion(appVersion);
  
//     if (os.platform() === 'darwin') {
//       updateFeed = updateFeed + 'osx';
//     } else if (os.platform() === 'win32') {
//       updateFeed = updateFeed + 'win/' + os.arch();
//     }
  
//     autoUpdater.on("update-available", (evt, info) => {
//       logger.info("A new update is available");
//       var message = 'A new version of the app is avaliable and will be downloaded automatically.';
//       setTimeout(function () {
//         mainWindow.webContents.send('BackendPushNotification', {
//           msgType: "softwareUpdate",
//           msg: message
//         });
//       }, 5000)
//     });
  
//     autoUpdater.on("update-downloaded", (updateInfo) => {
//       var msg = "A new update is ready to install, version " + updateInfo.version + " is downloaded and will be automatically installed on restart ";
//       logger.info(msg);
  
//       logger.info("The update info", updateInfo);
  
//       /**
//        * Updated 2.27.19 - Jagvir Sarai
//        * New logic for controlled releasese. 
//        * When minor update(not patch fix), we remove the app update urls
//        */
//       var trimmedUpdateVersion = trimVersion(updateInfo.version);
  
//       if (isDiffMinorVersions(trimmedAppVersion, trimmedUpdateVersion)) {
//         //remove app update url, so any machine that has manual update url will be moved over and point to latest world code.
//         t32XrayStudioSettingService.getSettings()
//           .then(function (setting) {
//             setting.appUpdateUrl = "";
//             var msg = {};
//             msg.keyValToChange = {
//               appUpdateUrl: ""
//             };
//             msg.msgType = 'updateXrayStudioSettingsProperty'
  
//             studioContext.browserWindow.notifyClient(msg);
//           })
//       };
//       var message = `Software update v${updateInfo.version} downloaded and will be installed upon restart.`;
  
//       mainWindow.webContents.send('BackendPushNotification', {
//         msgType: "softwareUpdate",
//         msg: message
//       });
//     });
  
//     autoUpdater.on("error", (error) => {
//       logger.info('error with auto update ----->', error);
//       // var message = 'There was an error during the software update.';
//       // mainWindow.webContents.send("BackendPushNotification", { msgType: "softwareUpdate", msg: message });
//     });
  
//     autoUpdater.on("checking-for-update", (event) => {
//       logger.info("--------Checking for update---------");
//     });
  
//     autoUpdater.on("update-not-available", () => {
//       logger.info("----------Update not available--------");
//     });
  
//     // const feedURL = updateFeed + '?v=' + appVersion;
//     // logger.info('Setting feed update url : ' + feedURL);
//     var feedOptions = {
//       provider: "generic",
//       url: updateFeed,
//       publishAutoUpdate: true,
//       updaterCacheDirName: 'tab32-imaging-studio-updater',
//     };
//     autoUpdater.setFeedURL(feedOptions);
//     feedUrlSet = true;
//     logger.info("going into checking update");
//     checkForUpdates();
//   } catch (e) {
//     throw e
//   }
// }

// function initializeMainWindow() {

//   try {
//     // Create main window
//     // Other options available at:
//     // http://electron.atom.io/docs/latest/api/browser-window/#new-browserwindow-options
//     let windowOpts = {
//       name: "tab32XrayStudio",
//       width: 1150,
//       height: 850,
//       toolbar: false,
//       webPreferences: {
//         nodeIntegration: true
//       }
//     }
  
//     mainWindow = t32XrayStudioBrowserWindowHandler.initialize(windowOpts).mainWindow;
  
//     // mainWindow = new BrowserWindow({
//     //   name: "tab32XrayStudio",
//     //   width: 1150,
//     //   height: 850,
//     //   toolbar: false,
//     //   webPreferences: {
//     //     nodeIntegration: true
//     //   }
//     // });
  
//     console.log(mainWindow)
  
//     if (!isDevelopment) {
//       mainWindow.setMenu(null);
//     }
  
//     // Target HTML file which will be opened in window
//     logger.info("loading index.html");
//     logger.info("here is the dirname ", __dirname)
//     logger.info(path.join(__dirname, "frontend/xrayStudioV1/index.html"))
//   // 'file://' + __dirname + "/frontend/index.html"
//     mainWindow.loadURL(
//       // url.format({pathname: path.join(__dirname, "/frontend/imagingStudio/index.html"), protocol: 'file:', slashes: true})
//       url.format({pathname: path.join(__dirname, "/frontend/xrayStudioV1/index.html"), protocol: 'file:', slashes: true})
//     );
  
//     // Uncomment to use Chrome developer tools
//     // if (isDevelopment) {
//       mainWindow.webContents.openDevTools({
//         mode: 'right'
//       });
//     // }
  
//     // Cleanup when main window is closed
//     mainWindow.on('close', (e) => {
//       if (t32XrayStudioUploadQueue.getQueueCount() <= 0) {
//         /* the user tried to quit the app */
//         logger.info("----EXITING-------");
//         mainWindow = null;
//       } else {
//         /* the user only tried to close the window */
//         e.preventDefault();
//         mainWindow.webContents.send('BackendPushNotification', {
//           msgType: "uploadsPending"
//         });
//       }
//     })
  
//     mainWindow.webContents.on('did-frame-finish-load', function () {
//       // logger.info("Checking for updates: ");
//       frameFinished = true;
//       checkForUpdates();
//     });

//   } catch (e) {
//     throw e
//   }

// }

// t32XrayStudioSettingService.getSettings()
//   .then((setting) => {

//     logger.info("-----Initializing App------");

//     updateFeed = setting.appUpdateUrl || updateFeed;

//     setting.viewMode = null;
//     studioSetting = setting;

//     logger.info('Setting up main window');

//     return t32XrayStudioAppWindowLauncher.initialize(frontEndFilePath, isDevelopment)
//   })
//   .then((browserWindow) => {
//     logger.info("main window loaded frame");
//     return t32XrayStudioMain.buildStudioContext(appInfo, browserWindow, studioSetting, appArgs)
//   })
//   .then((theContext) => {
//     studioContext = theContext;
//     logger.info("-----Studio Main Initialize complete-----");
//     logger.info("----CHECKING FOR APP UPDATES-----");
//     return t32XrayStudioAppUpdater.initialize(studioContext, updateFeed)
//   })
//   .then((theContext) => {
//     logger.info("-----Studio Main Initialize complete-----");
//     logger.info("----STARTING DRIVER COMMS SETUP-----");
//     return t32XrayStudioMain.startDriverComms(studioContext)
//   })
//   .then(function () {
//     logger.info("------DRIVER MSG SENT-----");
//     logger.info("------STARTING DB INIT-----");
//     return t32XrayStudioMain.startDbConnection(studioContext)
//   })
//   .then(function () {
//     logger.info("------DB INIT COMPLETE------")
//   })
//   .catch((err) => {
//     if (err.commsError) {
//       logger.info('---ERROR WITH DRIVER COMMS---');
//       logger.info(err);
//     } else if (err.initErr) {
//       logger.info("----ERROR INTIALIZING APP-----", err.initErr);
//       logger.info("-----QUITING APP-----");
//       studioContext = err.context;
//       studioContext.browerWindow.exitAppWindow();
//     } else {
//       logger.info("----ERROR WITH POST INIT STEP-----", err);
//     }
//   })