!macro customInit
  ; !addplugindir "$INSTDIR\resources\externalResources\nsis_plugins"
  !include "WordFunc.nsh"

  ; IfFileExists "$PROGRAMFILES64\PostgreSQL\12\uninstall-postgresql.exe" message end

  ; message: 
    ; MessageBox MB_OK|MB_ICONSTOP "it exists" IDOK end

  ; ; list of users code from http://nsis.sourceforge.net/Windows_Users,_Detect_%26_List
  ; ; GetParent code from http://nsis.sourceforge.net/Get_parent_directory
  ; Push $R0
  ; Push $R1
  ; Push $R2
  ; Push $R3
  ; Push $R4

  ; Push "$PROFILE"
  ; ; GetParent
  ; ; Function to get parent directory of $PROFILE
  ;   Exch $R0
  ;   Push $R1
  ;   Push $R2
  ;   Push $R3

  ;   StrCpy $R1 0
  ;   StrLen $R2 $R0

  ;   parentStrLoop:
  ;     IntOp $R1 $R1 + 1
  ;     IntCmp $R1 $R2 getParentStr 0 getParentStr
  ;     StrCpy $R3 $R0 1 -$R1
  ;     StrCmp $R3 "\" getParentStr
  ;   Goto parentStrLoop

  ;   getParentStr:
  ;     StrCpy $R0 $R0 -$R1

  ;   Pop $R3
  ;   Pop $R2
  ;   Pop $R1
  ;   Exch $R0
  ; ; End GetParent
  ; Pop $R2
  ; StrCpy $R2 "$R2"
  ; FindFirst $R0 $R1 "$R2\*"
  ; StrCmp $R1 "" findend 0

  ; findDirLoop:
  ;   ; Var /GLOBAL current_user  
  ;   ; StrCpy $current_user "$R2\$R1\*.*"
  ;   IfFileExists "$R2\$R1\*.*" 0 notDir
  ;   ; MessageBox MB_OK "The users we are looping over $current_user" IDOK 0
  ;   StrCmp $R1 "" notDir
  ;   StrCmp $R1 "." notDir
  ;   StrCmp $R1 ".." notDir
  ;   StrCmp $R1 "All Users" notDir
  ;   StrCmp $R1 "Default User" notDir
  ;   StrCmp $R1 "Default" notDir
  ;   StrCmp $R1 "Public" notDir
  ;   ; Try uninstalling Squirrel installation
  ;   IfFileExists "$R2\$R1\AppData\Local\xraystudio" alreadyInstalledMsg 0

  ; notDir:
  ;   FindNext $R0 $R1
  ;   StrCmp $R1 "" findend 0
  ;   Goto findDirLoop

  ; findend:
  ;   FindClose $R0

  ; Pop $R4
  ; Pop $R3
  ; Pop $R2
  ; Pop $R1
  ; Pop $R0
  ${ifNot} ${isUpdated}
    DetailPrint "Checking Previous Install"
    IfFileExists "$INSTDIR\${APP_EXECUTABLE_FILENAME}" alreadyInstalledMsg continue

    alreadyInstalledMsg:
      MessageBox MB_OK|MB_ICONSTOP "A version of xraystudio is already installed on this computer, please un-install that one and try again." IDOK end


    ; checkVersion:
    ;   Var /GLOBAL previous_version 
    ;   Var /GLOBAL version_compare
    ;   Var /GLOBAL major_version_compare
      
    ;   ${If} ${RunningX64}
    ;     SetRegView 64
    ;     ReadRegStr $previous_version HKCR "xraystudio\version" "version"
    ;     SetRegView LastUsed
    ;   ${Else}
    ;     ReadRegStr $previous_version HKCR "xraystudio\version" "version"
    ;   ${EndIf}

    ;   ${VersionCompare} $previous_version ${version} $version_compare

    ;   ${If} $version_compare == '0'
    ;     MessageBox MB_YESNO|MB_ICONQUESTION "Version ${version} of xraystudio is already installed on this computer, Would you like to continue with the install?" IDYES continue IDNO end
    ;   ${ElseIf} $version_compare == '1'
    ;     StrCpy $0 $previous_version 1
    ;     StrCpy $1 ${version} 1

    ;     ${VersionCompare} $0 $1 $major_version_compare

    ;     ${If} $major_version_compare == '1'
    ;       MessageBox MB_OK|MB_ICONSTOP "Version $previous_version of xraystudio is already installed and cannot be downgraded to ${version}!!" IDOK end
    ;     ${EndIf}
    ;   ${EndIf} 

    ;   Goto continue
  
  ${endif}

  end:
    Quit

  continue:
!macroend

!macro customInstall
  ${ifNot} ${isUpdated}

    DetailPrint "Removing old un-used squirrel files"
    ; Try uninstalling Squirrel version per user
    ; Use list of folders one level above $PROFILE, ignoring some common non-users
    
    Push $R0
    Push $R1
    Push $R2
    Push $R3
    Push $R4

    Push "$PROFILE"
    ; GetParent
    ; Function to get parent directory of $PROFILE
      Exch $R0
      Push $R1
      Push $R2
      Push $R3

      StrCpy $R1 0
      StrLen $R2 $R0

      parentStrLoop:
        IntOp $R1 $R1 + 1
        IntCmp $R1 $R2 getParentStr 0 getParentStr
        StrCpy $R3 $R0 1 -$R1
        StrCmp $R3 "\" getParentStr
      Goto parentStrLoop

      getParentStr:
        StrCpy $R0 $R0 -$R1

      Pop $R3
      Pop $R2
      Pop $R1
      Exch $R0
    ; End GetParent
    Pop $R2
    StrCpy $R2 "$R2"
    FindFirst $R0 $R1 "$R2\*"
    StrCmp $R1 "" findend 0

    findDirLoop:
      IfFileExists "$R2\$R1\*.*" 0 notDir
      StrCmp $R1 "" notDir
      StrCmp $R1 "." notDir
      StrCmp $R1 ".." notDir
      StrCmp $R1 "All Users" notDir
      StrCmp $R1 "Default User" notDir
      StrCmp $R1 "Default" notDir
      StrCmp $R1 "Public" notDir
      ; Try uninstalling Squirrel installation
      nsExec::ExecToStack  "$\"$R2\$R1\AppData\Local\xraystudio\Update.exe$\" --uninstall -s"
      pop $0
      pop $1
      Delete /REBOOTOK "$R2\$R1\Desktop\xraystudio.lnk"
      RMDir /r /REBOOTOK "$R2\$R1\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\xraystudio"
      RMDir /r /REBOOTOK "$R2\$R1\AppData\Local\xraystudio"
      RMDir /r /REBOOTOK "$R2\$R1\AppData\Roaming\xraystudio"
      ; done with Squirrel uninstall

    notDir:
      FindNext $R0 $R1
      StrCmp $R1 "" findend 0
      Goto findDirLoop

    findend:
      FindClose $R0

    Pop $R4
    Pop $R3
    Pop $R2
    Pop $R1
    Pop $R0
    
    ; Attempt to uninstall any of our previous Squirrel MSI versions
    ; Can remove these once everyone is away from Squirrel installations
    ; nsExec::Exec "MsiExec.exe /x{MY_MSI_GUID} /quiet"

    DetailPrint "Registering xraystudio keys"

    DeleteRegKey HKCR "xraystudio"
    WriteRegStr HKCR "xraystudio" "" "URL:xraystudio"
    WriteRegStr HKCR "xraystudio" "URL Protocol" ""
    WriteRegStr HKCR "xraystudio\version" "version" "${version}"
    WriteRegStr HKCR "xraystudio\location" "installLoc" "$INSTDIR\${APP_EXECUTABLE_FILENAME}"
    WriteRegStr HKCR "xraystudio\shell" "" ""
    WriteRegStr HKCR "xraystudio\shell\Open" "" ""
    WriteRegStr HKCR "xraystudio\shell\Open\command" "" "$INSTDIR\${APP_EXECUTABLE_FILENAME} %1"

    ; creating programData xraystudio folder
    StrCpy $0 $SYSDIR 2 

    IfFileExists "$0\ProgramData\xraystudio" 0 createXrayStudioDir

    createXrayStudioDir:
      CreateDirectory "$0\ProgramData\xraystudio"
      Goto setPremissions

    setPremissions:
      ; setting proper permistions for xraystudio image folder
      DetailPrint "Setting premissions for xray studio folder"
      nsExec::ExecToStack '"$SYSDIR\icacls.exe" $0\ProgramData\xraystudio\ /grant:r Users:(OI)(CI)F /T '
      Pop $0
      Pop $1
      Goto downloadDriver
    
    ; checkForPreviousPostgresInstall:
    ;   DetailPrint "Checking for previous postgreSql install"
    ;   IfFileExists "$PROGRAMFILES64\PostgreSQL\12\uninstall-postgresql.exe" setPostgresEnv downloadPostgres

    ; start3rdPartyInstall:
    ;   ;check if postgress already downloaded. 
    ;   IfFileExists "$TEMP\postgresql.exe" installPostgres downloadPostgres
    
    ; downloadPostgres:
    ;   IfFileExists "$TEMP\postgresql.exe" installPostgres download

    ;   download:
    ;     DetailPrint "Starting postgresql download"
    ;     inetc::get /caption "setting up postgresql" /banner "Downloading Postgresql v12..." /nocancel "https://images.tab32.com/postgresqlx64/postgresqlx64.exe"  "$TEMP\postgresql.exe" /end
    ;     Pop $0 # return value = exit code, "OK" if OK
        
    ;     ${if} $0 == 'OK' 
    ;     ${OrIf} $0 == 0

    ;       Goto installPostgres

    ;     ${else}
        
    ;       MessageBox MB_OK "Download Failed! with code $0" IDOK exit

    ;     ${endIf}
    ;     ; DetailPrint "unzip postgresql zip"
    ;     ; nsisunz::UnzipToLog "$TEMP\postgresql.zip" "$TEMP\postgresql.exe"

    ; installPostgres:

    ;   DetailPrint "Starting postgresql install"
    ;   nsExec::ExecToStack '"$TEMP\postgresql.exe"'
    ;   Pop $0 # return value/error/timeout
    ;   Pop $1

    ;   ${if} $0 == "OK"
    ;   ${OrIf} $0 == 0

    ;     DetailPrint "Deleting postgres exe file"
    ;     Delete "$TEMP\postgresql.exe"
    ;     Goto setPostgresEnv

    ;   ${else}
    ;     DetailPrint "Failed to install postgres: $0"
    ;     DetailPrint "$1"

    ;     MessageBox MB_OK|MB_ICONEXCLAMATION "Installation failed or was cancelled with error code  $0! This could effect the funtionality of this program. It is recommended that you please un-install the program and re-install it." IDOK continue

    ;   ${endIf}
    
    ; setPostgresEnv:
    ;   DetailPrint "Adding postgreSQL to local Env path Variable"

    ;   ExpandEnvStrings $0 %COMSPEC%
    ;   nsExec::ExecToStack '$0 /C call "$INSTDIR\resources\externalResources\nsis\scripts\setDbEnvPath.bat"'
     
    ;   Pop $0
    ;   Pop $1

    ;   ${if} $0 == "OK"
    ;   ${OrIf} $0 == 0
    ;     ; MessageBox MB_OK "env set $0 --- $1" IDOK installDriver
    ;     DetailPrint "Enviornment variable for PostgreSQL set"
    ;     Goto downloadDriver
    ;   ${else}
    ;     DetailPrint "Env Var failed to se for PostgreSQL: $0"
    ;     ; MessageBox MB_OK|MB_ICONEXCLAMATION  "failed to set environment variable , $0 ---- $1" IDOK exit
    ;   ;   MessageBox MB_OK "env alredy set $0 ---- $1" IDOK installDriver
    ;     Goto downloadDriver
    ;   ${endIf}

    downloadDriver:
      IfFileExists "$INSTDIR\resources\externalResources\tab32_driver_setup\tab32XraySetup.exe" installDriver fetchDriverExe

      fetchDriverExe:
        DetailPrint "Starting xray driver download"
        inetc::get /caption "fetching imaging studio driver" /banner "Downloading imaging studio driver..." /nocancel "https://images.tab32.com/imagingStudio/driver/tab32XraySetup.exe"  "$INSTDIR\resources\externalResources\tab32_driver_setup\tab32XraySetup.exe" /end
        Pop $0 # return value = exit code, "OK" if OK
        
        ${if} $0 == 'OK' 
        ${OrIf} $0 == 0

          Goto installDriver

        ${else}
        
          MessageBox MB_OK "Download Failed! with code $0" IDOK exit

        ${endIf}
        ; DetailPrint "unzip postgresql zip"
        ; nsisunz::UnzipToLog "$TEMP\postgresql.zip" "$TEMP\postgresql.exe"

    installDriver:

      DetailPrint "Starting Xray Studio Driver Installation."
      nsExec::ExecToStack '"$INSTDIR\resources\externalResources\tab32_driver_setup\tab32XraySetup.exe"'
      Pop $0
      Pop $1

      ${if} $0 == "OK"
      ${OrIf} $0 == 0

        Goto continue

      ${else}

        MessageBox MB_OK|MB_ICONEXCLAMATION  "t32Xray Driver installation failed or was cancelled with error code $0! This could effect the funtionality of this program. It is recommended that you please un-install the program and re-install it." IDOK continue

      ${endIf}

    exit: 
      Quit
    continue:

     
  ${else}
    DetailPrint "Changing registry key for verison"

    DeleteRegKey HKCR "xraystudio\version"
    WriteRegStr HKCR "xraystudio\version" "version" "${version}"
  ${endIf}
!macroend

!macro customUnInit
  ; Moving xray studio driver installer out to temp folder
  CreateDirectory "$TEMP\tab32_driver_setup"
  CopyFiles "$INSTDIR\resources\externalResources\tab32_driver_setup\tab32XraySetup.exe" "$TEMP\tab32_driver_setup"
  ; !include WinMessages.nsh
  ; DetailPrint "Before uninstall main, uninstalling tab32Driver"
  ; nsExec::ExecToStack '"$INSTDIR\resources\externalResources\tab32_driver_setup\tab32XraySetup.exe"'
  ; MessageBox MB_OK "Xray Studio Driver Removed" IDOK continue
  ; DetailPrint "Xray Studio Driver removed"
  ; continue:
  ; GetDlgItem $0 $HWNDPARENT 1
  ; ShowWindow $0 ${SW_MINIMIZE}
  ; Sleep 1000
  ; ShowWindow $0 ${SW_RESTORE}
!macroend

!macro customUnInstall

  ${IfNot} ${isUpdated}

    DetailPrint "Removing app data files from user if any"
    ; Try uninstalling Squirrel version per user
    ; Use list of folders one level above $PROFILE, ignoring some common non-users
    
    ; list of users code from http://nsis.sourceforge.net/Windows_Users,_Detect_%26_List
    ; GetParent code from http://nsis.sourceforge.net/Get_parent_directory
    Push $R0
    Push $R1
    Push $R2
    Push $R3
    Push $R4

    Push "$PROFILE"
    ; GetParent
    ; Function to get parent directory of $PROFILE
      Exch $R0
      Push $R1
      Push $R2
      Push $R3

      StrCpy $R1 0
      StrLen $R2 $R0

      parentStrLoop:
        IntOp $R1 $R1 + 1
        IntCmp $R1 $R2 getParentStr 0 getParentStr
        StrCpy $R3 $R0 1 -$R1
        StrCmp $R3 "\" getParentStr
      Goto parentStrLoop

      getParentStr:
        StrCpy $R0 $R0 -$R1

      Pop $R3
      Pop $R2
      Pop $R1
      Exch $R0
    ; End GetParent
    Pop $R2
    StrCpy $R2 "$R2"
    FindFirst $R0 $R1 "$R2\*"
    StrCmp $R1 "" findend 0

    findDirLoop:
      IfFileExists "$R2\$R1\*.*" 0 notDir
      StrCmp $R1 "" notDir
      StrCmp $R1 "." notDir
      StrCmp $R1 ".." notDir
      StrCmp $R1 "All Users" notDir
      StrCmp $R1 "Default User" notDir
      StrCmp $R1 "Default" notDir
      StrCmp $R1 "Public" notDir
      RMDir /r /REBOOTOK "$R2\$R1\AppData\Local\xraystudio-updater"

    notDir:
      FindNext $R0 $R1
      StrCmp $R1 "" findend 0
      Goto findDirLoop

    findend:
      FindClose $R0

    Pop $R4
    Pop $R3
    Pop $R2
    Pop $R1
    Pop $R0

    DetailPrint "Removing xraystudio keys"
    DeleteRegKey HKCR "xraystudio"
    DetailPrint "Uninstalling Xray Studio Driver"
    nsExec::ExecToStack '"$TEMP\tab32_driver_setup\tab32XraySetup.exe"'
    DetailPrint "Xray Studio Driver removed"

    ;remove the temp installer out of the temp folder
    SetOutPath $TEMP\tab32_driver_setup
    SetOutPath $TEMP
    RMDir $TEMP\tab32_driver_setup

    DetailPrint "Uninstall Complete"
  ${endIf}

!macroend