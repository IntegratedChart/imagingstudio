module.exports = class UploadPatient {
    constructor(id, name, dob, isOffline, noteCt, treatCt) {
        this.patientId = id;
        this.patientName = name;
        this.patientDob = dob;
        this.isOffline = isOffline;
        this.uploadData = {
            noteCount: noteCt,
            treatmentCount: treatCt
        };
        this.isStarted = false;
        this.isComplete = false;
        this.isError = false;
    }
}