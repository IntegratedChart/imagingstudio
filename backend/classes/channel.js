module.exports = class Channel {
    constructor(name, handlerName) {
        this.handlerName = handlerName;
        this.name = name;
        this.recievedMsgs = [];
        this.sentMsgs = [];
    }

    getSentMsgs(){
        return this.sentMsgs;
    }

    addSentMsg(msg){
        this.sentMsgs.push(msg)
    }

    getRecievedMsgs(){
        return this.recievedMsgs;
    }

    addRevievedMsg(msg){
        this.recievedMsgs.push(msg);
    }

    getAllMsgs(){
        return this.sentMsgs.concat(this.recievedMsgs);
    }

    getSentMsgByType(type){
        return this.sentMsgs.filter((msg) => {
            return msg.msgType == type;
        });
    }

    getRecievedMsgsByType(type){
        return this.recievedMsgs.filter((msg) => {
            return msg.msgType == type;
        });
    }

    getHandlerName(){
        return this.handlerName;
    }
}