// const _ = require('lodash');
module.exports = class Queue {
    constructor(name) {
        this.name = name;
        this.queueList = [];
    }

    add(obj) {
        this.queueList.push(obj);
    }

    count() {
        return this.queueList.length;
    }

    getBy(key, value) {
        // let found = _.find(this.queueList, function (obj) {
        //     return obj[key] == value
        // })
        let found = this.queueList.find( (obj)  => {
            return obj[key] == value
        })

        return found;
    }

    removeBy(key, value) {
        // _.remove(this.queueList, function (obj) {
        //     return obj[key] == value;
        // })
        this.queueList = this.queueList.filter( (obj) => {
            return obj[key] !== value;
        });
    }

    clear() {
        this.queueList = [];
        return;
    }

    findAndReplaceBy(key, value, newObj) {

        // let index = _.findIndex(this.queueList, function (obj) {
        //     return obj[key] == value;
        // })

        let index = this.queueList.findIndex( (obj) => {
            return obj[key] == value;
        })

        if (index == -1) {
            this.queueList.push(newObj);
            return 
        }

        this.queueList[index] = newObj;
        return 
    }

    show(){
        return this.queueList;
    }

}