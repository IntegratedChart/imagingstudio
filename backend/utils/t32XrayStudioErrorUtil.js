var logger = require('electron-log');

module.exports.createError = createError;

function createError(from='system', msg='tab32XrayStudioError', sysErr='', code=0){
    return {
        from: from,
        error: msg,
        sysErr: sysErr
    }
}
