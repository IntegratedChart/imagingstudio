const logger = require('electron-log');
const moment = require('moment'); 

module.exports.getZoneInsensitiveDate = this.getZoneInsensitiveDate;
module.exports.convertToZoneInsensitiveUtcDate = this.convertToZoneInsensitiveUtcDate;
            
function getZoneInsensitiveDate () {
    let date = new Date();
    return convertToZoneInsensitiveUtcDate(date);
}

function convertToZoneInsensitiveUtcDate (date) {
    return moment(date).utc().hour(12).toDate();
}