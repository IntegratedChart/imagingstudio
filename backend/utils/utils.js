const logger = require("electron-log");

module.exports.createRandomString = createRandomString;
module.exports.createMapFromList = createMapFromList;
module.exports.pause = pause;

function createRandomString (strLen = 10) {
    if (strLen < 2) {
        strLen = 2
    }
    
    return Math.random().toString(36).substring(2, strLen/2) + Math.random().toString(36).substring(2, strLen/2);
}

function createMapFromList(list, keyName) {
    let map = {}
    for (let item of list) {
        map[item[keyName]] = item;
    }

    return map;
}

async function pause (time) {
    try {
        return await new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve();
            }, time)
        })
    } catch (e) {
        logger.info("failed to pause ", e);
        return 
    }
}