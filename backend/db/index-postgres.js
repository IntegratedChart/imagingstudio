const { Client } = require("pg");
const ScriptRunner = require("../scripts");
const logger = require("electron-log");
// let eventMap = {}
// let pgClient;
var actions = {
    connect: null,
    query: null,
    on: null,
}
var initialize = setupInit(actions);

module.exports.initialize = initialize;
module.exports.actions = actions;
module.exports.findDb = findDb;
module.exports.createDb = createDb;
module.exports.findUser = findUser;
module.exports.createUser = createUser;
module.exports.createTable = createTable;
module.exports.dropTable = dropTable;


function setupInit(actions) {
    let pgClient;
    let eventMap = {};
    return function (config) {
        try {      
            let dbConfig = {
                user: config.username,
                database: config.database,
                password: config.password,
            }

            pgClient = config ?  new Client(dbConfig) : pgClient ? new Client() : pgClient;
            // logger.info("setup Init ", pgClient)
            actions.connect = connectFuncInit(pgClient);
            actions.query = queryFuncInit(pgClient);
            actions.on = onFuncInit(eventMap);
            return registerListeners(pgClient, eventMap);
        
        } catch (e) {
            throw e
        }
    }
};

function connectFuncInit(client) {
    // let pgClient = client;
    return async function () {
        try {
            return await client.connect();
        } catch (e) {
            throw e;
        }
    }
}

function queryFuncInit(client) {
    // let pgClient = client;
    return async function (statement, params) {
        try {
            return await client.query(statement, params);
        } catch (e) {
            throw e;
        }
    }
}

function onFuncInit(eventMap) {
    
    return function (event, callback) {
        if (!eventMap[event]) {
            eventMap[event] = [];
        }

        eventMap[event].push(callback);
    }
}

function registerListeners(client, eventMap) {
    client.on('end' , (msg) => {
        if (eventMap['end'] && eventMap['end'].length) {
            for (callback in eventMap['end']) {
                callback(msg);
            }
        }
    });

    client.on('error', (msg) => {
        if (eventMap['error'] && eventMap['error'].length) {
            for (callback in eventMap['error']) {
                callback(msg);
            }
        }
    })
}

// function on(event, callback) {
//     if (!eventMap[event]) {
//         eventMap[event] = [];
//     }

//     eventMap[event].push(callback);
// }


// async function connect(config) {
//     try {
//         initialize(config);
//         return await pgClient.connect();
//     } catch (e) {
//         throw e;
//     }
// }

// async function query(statement, params) {
//     try {
//         return await pgClient.query(statement, params);
//     } catch (e) {
//         throw e;
//     }
// }

async function findDb(defaultCredentials, dbName) {
    try {
        let args = [defaultCredentials.username, defaultCredentials.password, defaultCredentials.database, dbName]
        return await ScriptRunner.runScript('findDb', args)
    } catch (e) {
        throw e
    }
}

async function createDb(defaultCredentials, dbName, dbOwner){
    try {
        let args = [defaultCredentials.username, defaultCredentials.password, defaultCredentials.database, dbName, dbOwner]
        return await ScriptRunner.runScript('createDb', args)
    } catch (e) {
        throw e;
    }
}

async function findUser(defaultCredentials, username) {
    try {
        let args = [defaultCredentials.username, defaultCredentials.password, defaultCredentials.database, username]
        return await ScriptRunner.runScript('findUser', args)
    } catch (e) {

    }
}

async function createUser(defaultCredentials, username, password) {
    try {
        let args = [defaultCredentials.username, defaultCredentials.password, defaultCredentials.database, username, password]
        return await ScriptRunner.runScript('createUser', args)
    } catch (e) {
        throw e;
    }
}

/**
 * @async
 * @description creates table in database if not exists
 * @param { string } tableName 
 * @param { string[] } schemaDefinition
 * 
 * @returns { Promise<any> }
 */
async function createTable(tableName, schemaDefinition) {
    try {
        if (!tableName || !schemaDefinition) {
            throw 'Table name and schema definition required';
        }

        if (!Array.isArray(schemaDefinition)) {
            throw 'Schema Definition must be an string array';
        }

        const schema = schemaDefinition.join(',');

        return await actions.query(`CREATE TABLE IF NOT EXISTS ${tableName} (${schema})`);
        
    } catch (error) {
        throw ' createTable ERROR::' + error;
    }
}

/**
 * @async
 * @param {string} tableName
 * 
 * @returns { Promise<any> }
 */
async function dropTable(tableName) {
    try {
        if (!tableName) {
            throw 'Table name required';
        }

        return await actions.query(`DROP TABLE IF EXISTS ${tableName}`);
    } catch (error) {
        throw 'DB/index.js dropTable ERROR::' + error;
    }
}

/**
 * @description Upserts data if exists
 * @async
 * @param { string } tableName 
 * @param { string } cols 
 * @param { string | string[] } values 
 * @param {*} upsertOn
 * 
 * @returns { Promise<any> } 
 */
async function upsert(tableName, cols, values, upsertOn) {
    try {
        if (!tableName || !cols || !values) {
            throw 'Missing required argument(s)'
        }
        
    } catch (error) {
        throw 'DB/index.js upsert ERROR:' + error;
    }
}