'use strict';

// these are pulled from local_cdt_code document on EHR and flattened out into own collection. Both custom and global codes are defined here.
const Provider = {
    name: 'Provider',
    primaryKey: '_id',
    properties: {
        _id: 'string?',
        provider_id: 'string',
        name: 'string',
    }
}

module.exports = Provider