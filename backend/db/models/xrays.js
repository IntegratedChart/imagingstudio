'use strict';

const Xrays = {
    name: 'Xrays',
    primaryKey: '_id',
    properties: {
        _id: 'string',
        patient_id: 'string',
        storagePath: {type: 'string', default: null},
        isComplete: {type: 'bool', default: false}
    }
}

module.exports = Xrays