'use strict';

const PatientUpload = {
    name: 'PatientUpload',
    primaryKey: '_id',
    properties: {
        _id: 'string?',
        patientId: 'string?',
        patientName: 'string?',
        patientDob: 'string?',
        isOffline: {type: 'bool', default: false},
        isStarted: {type: 'bool', default: false},
        isComplete: {type: 'bool', default: false},
        isError: {type: 'bool', default: false},
        uploadData: 'UploadData'
    }
}

module.exports = PatientUpload