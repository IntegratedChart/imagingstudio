'use strict';

const UploadLog = {
    name: 'UploadLog',
    primaryKey: '_id',
    properties: {
        _id: 'string',
        clinic_id: 'string?',
        logDate: 'string',
        uploads: 'PatientUpload[]',
    }
}

module.exports = UploadLog