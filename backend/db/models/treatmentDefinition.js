'use strict';

const TreatmentDefinition = {
    name: 'TreatmentDefinition',
    primaryKey: '_id',
    properties: {
        _id: 'string',
        quickKeyId: 'string',
        cdtCode: 'string',
        description: 'string?',
        short_description: 'string?',
        tooth_number: 'string?',
        surfaces: 'string?',
        isCervicalSurface: 'bool?',
        //oral_cvaity refers to quadarant
        oral_cavity: 'string?',
    }
}

module.exports = TreatmentDefinition;