'use strict';

const Note = {
    name: 'Note',
    primaryKey: '_id',
    properties: {
        _id: 'string',
        patient_id: 'string',
        date: 'date',
        alert: 'bool?',
        noteType: 'string',
        notes: 'string',
        reference_id: 'string?',
        reference_desc: 'string?',
        is_offline: { type: 'bool', default: false },
    }
}

module.exports = Note;