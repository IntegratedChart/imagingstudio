'use strict';

const Patient = {
    name: 'Patient',
    primaryKey: '_id',
    properties: {
        _id: 'string',
        clinic_id: 'string',
        lname: 'string?',
        fname: 'string?', 
        mname: 'string?',
        name: 'string?',
        nickname: 'string?',
        notes: 'string?',        
        //address fields
        add_line1: 'string?',
        add_line2: 'string?',
        city: 'string?',
        state: 'string?',
        zipcode: 'string?',
        zip_last_4 : 'string?',
        phone: 'string?',
        preferred_contact: 'string?',
        cell_phone : 'string?',
        gender: 'int?',
        dob: 'string?',
        marital_status: 'string?',
        email: 'string?',
        emailConsent: 'bool?',
        smsPhone: 'string?',
        smsConsent: 'bool?',
        eStatementConsent: 'bool?',
        preferEmail: { type: 'bool', default: false },
        preferText: { type: 'bool', default: false },
        preferCall: { type: 'bool', default: false},
        //clinic data fields
        chartId: 'int?',
        foreignId: 'string?',
        guardianName: 'string?',
        parentalGuardian: 'bool?',
        search_string: 'string?',
        locationId: 'string?',
        patient_notes: 'Note[]',
        treatments: 'Treatment[]',
        is_offline: { type: 'bool', default: false },
        refreshDate: 'date?',
        expiryDate: 'date?',
        isUploading: {type: 'bool', default: false},
        xrays: 'Xrays?'
        // isRefreshing: {type: 'bool', default: false}
    }
}

module.exports = Patient;