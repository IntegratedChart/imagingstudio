'use strict';

const UploadData = {
    name: 'UploadData',
    primaryKey: '_id',
    properties: {
        _id: 'string',
        noteCount: 'int',
        treatmentCount: 'int'
    }
}

module.exports = UploadData