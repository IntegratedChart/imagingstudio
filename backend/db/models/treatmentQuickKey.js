'use strict';

const TreatmentQuickKey = {
    name: 'TreatmentQuickKey',
    primaryKey: '_id',
    properties: {
        _id: 'string',
        clinic_id: 'string',
        name: 'string',
        treatments: 'TreatmentDefinition[]'
    }
}

module.exports = TreatmentQuickKey;