'use strict';

const NoteTemplate = {
    name: 'NoteTemplate',
    primaryKey: '_id',
    properties: {
        _id: 'string',
        clinic_id: 'string',
        label: 'string',
        textTemplates: 'TextTemplate[]',
    }
}

module.exports = NoteTemplate;