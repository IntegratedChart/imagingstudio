'use strict';

// these are pulled from local_cdt_code document on EHR and flattened out into own collection. Both custom and global codes are defined here.
const Location = {
    name: 'Location',
    primaryKey: '_id',
    properties: {
        _id: 'string?',
        name: 'string',
        clinicProviders: 'Provider[]',
    }
}

module.exports = Location