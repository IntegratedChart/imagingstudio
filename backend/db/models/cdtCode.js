'use strict';

// these are pulled from local_cdt_code document on EHR and flattened out into own collection. Both custom and global codes are defined here.
const CdtCode = {
    name: 'CdtCode',
    primaryKey: 'cdt_code',
    properties: {
        // _id: 'string?',
        clinic_id: 'string',
        // autoNotes: 'string?',
        cdt_code: 'string',
        // cdt_class: 'string',
        cdt_desc: 'string',
        short_description: 'string'
    }
}

module.exports = CdtCode