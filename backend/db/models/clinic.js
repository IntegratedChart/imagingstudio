'use strict';

// these are pulled from local_cdt_code document on EHR and flattened out into own collection. Both custom and global codes are defined here.
const Clinic = {
    name: 'Clinic',
    primaryKey: '_id',
    properties: {
        _id: 'string?',
        name: 'string',
        unique_name: 'string',
        locations: 'Location[]',
        offlineStudio: {type: 'bool', default: false}
    }
}

module.exports = Clinic