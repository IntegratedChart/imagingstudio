'use strict';

const Treatment = {
    name: 'Treatment',
    primaryKey: '_id',
    properties: {
        _id: 'string',
        patient_id: 'string',
        cdtCode: 'string',
        description: 'string?',
        short_description: 'string?',
        tooth_number: 'string?',
        surfaces: 'string?',
        isCervicalSurface: 'bool?',
        oral_cavity: 'string?',
        enteredDate: 'date',
        completed_date: 'date?',
        completed: 'bool?',
        is_offline: { type: 'bool', default: false },
        locationId: 'string?',
        providerId: 'string?',
    }
}

module.exports = Treatment;