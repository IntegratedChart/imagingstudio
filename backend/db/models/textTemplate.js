'use strict';

const TextTemplate = {
    name: 'TextTemplate',
    primaryKey: '_id',
    properties: {
        _id: 'string',
        pieceType: 'string',
        text: 'string'
    }
}

module.exports = TextTemplate