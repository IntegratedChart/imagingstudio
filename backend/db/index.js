'use strict';
const Realm = require('realm');
const logger = require('electron-log');
const t32XrayStuioSettingsService = require('../services/t32XrayStudioSettingsService');

/** add models for realm db here */
const models = [
    require('./models/note'),
    require('./models/treatment'),
    require('./models/patient'),
    require('./models/noteTemplate'),
    require('./models/textTemplate'),
    require('./models/cdtCode'),
    require('./models/clinic'),
    require('./models/location'),
    require('./models/provider'),
    require('./models/treatmentQuickKey'),
    require('./models/treatmentDefinition'),
    require('./models/uploadLog'),
    require('./models/patientUpload'),
    require('./models/uploadData'),
    require('./models/xrays')
];

/** configuration object for realm */
const realmConfig = { 
    //TODO:jag for Mac build and other builds need to have diff path
    path: "C:/ProgramData/xraystudio/offlineData.realm", 
    schema: models, 
    schemaVersion: 3, // increment this whenever there are new schema changes
}

// const realm = new Realm(realmConfig);

let realm;
initializeRealm();

/**
 * For getting license from settings and initializing realm db
 */
async function initializeRealm() {
    try {
        // get studio settings
        const settings = await t32XrayStuioSettingsService.getSettings()
        
        if (!settings.machineId) {
            throw 'machineId not found';
        }

        let key = '';
        const machineIdLength = settings.machineId.length;

        do {
            key += settings.machineId[ key.length % machineIdLength ];
        } while (key.length < 64);

        const base64Key = Buffer.from(key).toString('base64');
        const binaryKey = Buffer.from(base64Key, 'base64').toString('binary');
        realmConfig.encryptionKey = new Int8Array( binaryKey.split('') );

        /** open realm connection*/
        realm = await Realm.open(realmConfig);
        return;
    } catch (error) {
        logger.error('realm connection error', err); 
    }
}


/**
 * @description For inserting/updating records into real db
 * @async
 * @param { string } modelName 
 * @param { any } data 
 * @param { boolean } upsert 
 */
const save = async (modelName, data, upsert) => {
    try {
        const updateMode = upsert ? 'modified' : 'never';

        realm.write(() => {
            if (Array.isArray(data)) {
                for (let row of data) {
                    realm.create(modelName, row, updateMode);
                }
            } else {
                realm.create(modelName, data, updateMode);
            }
        });
        
        return 'done';
    } catch (error) {
        throw error;
    }
}

// /**
//  * 
//  * @param {*} target 
//  * @param {*} targetFilter 
//  * @param {*} data 
//  */
// const updateLists = async (target, targetFilter, data) => {
//     try {
//         realm.write(() => {
//             const parentDoc = realm.objects(target).filtered(targetFilter);

//             for (let key of data) {
//                 for (let row of data[key]) {
//                     parentDoc[key].push(row);
//                 }
//             }
//         });

//         return true;
//     } catch (error) {
//         throw error;
//     }
// }

/**
 * @description For finding and returning existing list items index
 * @param { RealmObject[] } list 
 * @param { string } key 
 * @param { any } value
 * 
 * @returns { number | string }
 */
const findExistingListItemsIndex = (list, key, value) => {
    let returnIndex;

    for (let index in list) {
        if (list[index][key] == value) {
            returnIndex = index;
            break;
        }
    }

    return returnIndex != undefined ? { index: returnIndex } : null;
} 

/**
 * @description For updating or inserting item into list.
 * uses parentObject from this.context set by caller method i.e. updateList method
 * @param { string } listName name of list
 * @param { any } data data to save
 * @param { string } listPrimaryKey primary key of list items
 */
const updateOrPushListItem = function (listName, data, listPrimaryKey) {
    const existingListItem = findExistingListItemsIndex(this.parentObject[listName], listPrimaryKey, data[listPrimaryKey]);

    if (existingListItem) {
        const index = existingListItem.index;
    
        Object.keys(data).forEach(key => {
            if (key != listPrimaryKey) {
                this.parentObject[listName][index][key] = data[key];
            }
        });
    } else {
        this.parentObject[listName].push(data);
    }
}

/**
 * @description For save list items in parent object
 * @async
 * @param { Realm.object } parentObject 
 * @param { string } listName 
 * @param { object[] | object } data 
 * @param { string } listPrimaryKey 
 */
const updateList = async (parentObject, listName, data, listPrimaryKey) => {
    try {
        realm.write(() => {
            if (Array.isArray(data)) {
                // push items array
                for (let doc of data) {
                    updateOrPushListItem.call({ parentObject }, listName, doc, listPrimaryKey);
                }
            } else {
                // push single item
                updateOrPushListItem.call({ parentObject }, listName, data, listPrimaryKey);
            }
        });

        return true;
    } catch (error) {
        throw error;
    }
}

/**
 * @description For find records from realm db
 * @async
 * @param { string } modelName 
 * @param { any } data 
 */
const get = async (modelName, filter = null) => {
    try {
        const records = filter ? realm.objects(modelName).filtered(filter) : realm.objects(modelName);
        return Array.from(records);
    } catch (error) {
        throw error;
    }
}


/**
 * @description For deleting records from realm db
 * @async
 * @param { string } modelName 
 * @param { string } filter 
 */
const deleteRecord = async (modelName, filter) => {
    try {
        realm.write(() => {
            const records = realm.objects(modelName).filtered(filter);
            
            realm.delete(records);
        });
    } catch (error) {
        throw error;
    }
}

const testDelete = async (modelName, id) => {
    try {
        let deleteQuery = `_id = "${id}"`;
        let deleted = deleteRecord(modelName, deleteQuery);
        console.log('delete complete')
        return deleted;
    } catch (e) {
        throw e;
    }
}

module.exports = {
    save,
    get,
    deleteRecord,
    updateList,
    testDelete
}