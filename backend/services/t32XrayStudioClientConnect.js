const logger = require('electron-log');

module.exports.waitForClient = waitForClient;
module.exports.setConnection = setConnection;
module.exports.initialize = initialize;

var studioContext = null;

function initialize (context) {
    studioContext = context;
    return
}

async function waitForClient() {

    logger.info('Waiting for client to load');
    return new Promise((resolve, reject) => {
        let count = 0;
        if (!studioContext.clientReady) {
            let wait = setInterval(() => {
                //the context object client ready flag is manipulated inside of t32XrayStudioIpcMsgHandler,
                //when frontend ipc messeaging is registered, it fires an client ready event
                if (studioContext.clientReady) {
                    logger.info('Client loaded');
                    clearInterval(wait);
                    resolve()
                }
    
                if (count == 60) {
                    reject("client timeout")
                }
            }, 1000)
        } else {
            resolve();
        }
    })
}

function setConnection() {
    studioContext.clientReady = true; 
}