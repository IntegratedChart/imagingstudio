// const _ = require('lodash'); 
const logger = require('electron-log');
const Queue = require('../classes/queue');

var uploadQueue = new Queue('uploadQueue');

module.exports.getQueueCount = getQueueCount;
module.exports.add = add;
module.exports.removeBy = removeBy;
module.exports.findAndReplaceBy = findAndReplaceBy;
module.exports.getBy = getBy;
module.exports.clearQueue = clearQueue;


function getQueueCount() {
    return uploadQueue.count();
}

function getBy(key, value) {
    logger.info("*** The queue we are searching: ", uploadQueue.show());
    logger.info("*** getting queue dir " + key + " - " + value + " ***");
    return uploadQueue.getBy(key, value)
}

function add(obj) {
    logger.info("*** adding dir to queue *** ", obj);
    uploadQueue.add(obj);
}

function removeBy(key, value) {
    logger.info("*** removing dir to queue by " + key + " - " + value + " ***");
    uploadQueue.removeBy(key, value)
}

function clearQueue() {
    uploadQueue.clear();
}

function findAndReplaceBy(key, value, newObj) {
    uploadQueue.findAndReplaceBy(key, value, newObj);
}

