// const chokidar = require('chokidar');
// const logger = require('electron-log');
// const path = require('path');
// var t32XrayStudioDirHandler = require('./t32XrayStudioDirHandler');

// // module.exports.watchXrayDirectory = watchXrayDirectory;
// // module.exports.stopDirWatcher = stopDirWatcher;
// // module.exports.initialize = initialize;

// var currentWatchDirectory = null;
// var dirWatcher;
// var studioContext = null;

// function initialize(theContext){
//     studioContext = theContext;
//     // watchXrayDirectory();
// }
// /**
//  * Watch the tab32RootFolder dir to check for any image changes and handle new files that come in.
//  */
// function watchXrayDirectory() {
//     // logger.info( "this....", this);
//     // watch directory, ignore .*
//     logger.info("Watch Xray Dir ", studioContext.setting.tab32Root ? studioContext.setting.tab32Root : null);

//     if (studioContext.setting.tab32Root !== currentWatchDirectory) {
//         currentWatchDirectory = studioContext.setting.tab32Root;

//         dirWatcher = chokidar.watch(studioContext.setting.tab32Root,
//             {
//                 ignored: function (f) {
//                     var ext = path.extname(f);
//                     return ext == '.json' || ext == '.log'
//                 },
//                 // awaitWriteFinish: {
//                 //     stabilityThreshold: 500,
//                 //     pollInterval: 100
//                 // }
//             })

//         dirWatcher.on('all', (event, path) => {
//             // logger.info('DETECT CHANGE EVENT ... ')
//             var msg = { msgType: 'DirectoryUpdated' };
//             /* 
//             Run the code to handle file names, if we are taking xray-images. We have to find a way to differnciate betweeen file Upload and xray-image.
//             */
//             setTimeout(function () {
//                 if (studioContext.setting.viewMode == 'close') {
//                     // logger.debug("...we are trying to close the folder and delete them....");
//                 }
//                 else {
//                     t32XrayStudioDirHandler.checkForNewFiles(studioContext, studioContext.ignoreFileList)
//                         .then(function (hasNewFile) {
//                             if (hasNewFile) {
//                                 studioContext.browserWindow.notifyClient(msg);
//                             } else {
//                                 logger.debug('... no new files found.. do not trigger client notification')
//                             }
//                         })
//                 }
//             }, 1000)
//         });
//     }
// }


// function stopDirWatcher() {
//     logger.info("ABOUT TO CLOSE CHOKIDAR");
//     currentWatchDirectory = null;
//     dirWatcher.close();
// }
