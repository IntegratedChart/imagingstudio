const fs = require('fs-extra');
const logger = require('electron-log');
const os = require('os');
const loadJsonFile = require('load-json-file');
const writeJsonFile = require('write-json-file');

module.exports.ensureDirSync = ensureDirSync;
module.exports.ensureDir = ensureDir;
module.exports.readJson = readJson;
module.exports.readJsonSync = readJsonSync;
module.exports.writeJson = writeJson;
module.exports.writeJsonSync = writeJsonSync;
module.exports.writeFile = writeFile;
module.exports.writeFileSync = writeFileSync;
module.exports.rename = rename;
module.exports.readFileSync = readFileSync;
module.exports.readdirSync = readdirSync;
module.exports.lstatSync = lstatSync;
module.exports.existsSync = existsSync;

function ensureDirSync(dirPath, permission = 0o777){
    try{
        fs.ensureDirSync(dirPath, permission);
        // if(os.platform() == 'win32'){
        //     //run permissions for windows folders to let all users gain access.
        // }
        return 
    }
    catch(err){
        logger.error('error creating dir', err);
        throw err;
    } 
}

async function ensureDir(dirPath, permission = 0o765){
    try{
        await fs.ensureDir(dirPath, permission)
        // if (os.platform() == 'win32') {
        //     //run permissions for windows folders to let all users gain access.
        // }
        return 
    }
    catch(err){
        logger.error('error creating dir', err);
        throw err;
    } 
}

async function readJson(filePath, options){
    try {
        if(options){
            return await loadJsonFile(filePath, options)
        }
        return await loadJsonFile(filePath)
    } catch(err) {
        logger.error('error reading json', err);
        throw err;
    }
}

function readJsonSync(filePath, options = null){
    try {
        if(options){
            return loadJsonFile.sync(filePath, options);
        }
        return loadJsonFile.sync(filePath);
    }catch(err){
        logger.error('error reading json sync', err);
        throw err
    }
}

async function writeJson(filePath, content, options = null){
    try{
        if(options){
            return await writeJsonFile(filePath, content, options)
        }
        return await writeJsonFile(filePath, content)
    }catch(err){
        logger.error('error writing json', err);
        throw err;
    }

}

function writeJsonSync(filePath, content, options = null){
    try{
        if(options){
            return writeJsonFile(filePath, content, options);
        }
    
        return writeJsonFile(filePath, content, options)
    }catch(err){
        logger.error('error writing json sync', err);
        throw err
    }
}

async function rename(source, target) {
    try {
        return await new Promise((resolve, reject) => {
            fs.rename(source, target, (e) => {
                if (e) {
                    logger.error(e);
                    reject(e);
                }
                else {
                    resolve();
                }
            });
        })
    } catch (e) {
        throw e;
    }
}

async function writeFile(file, data, options={}) {
    try {
        return await fs.writeFile(file, data, options)
    } catch (e) {
        throw e;
    }
}

function readFileSync(path, options={}) {
    try {
        return fs.readFileSync(path, options);
    } catch (e) {
        throw e;
    }
}

function existsSync(path) {
    try {
        return fs.existsSync(path);
    } catch (e) {
        throw e;
    }
}

function lstatSync(path) {
    try {
        return fs.lstatSync(path);
    } catch (e) {
        throw e;
    }
}

function readdirSync(path, options={}) {
    try {
        return fs.readdirSync(path, options);
    } catch (e) {
        throw e;
    }
}

function writeFileSync(path, data, options={}) {
    try {
        return fs.writeFileSync(path, data, options);
    } catch (e) {
        throw e;
    }
}