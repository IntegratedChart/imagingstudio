const execFile = require('child_process').execFile;
const exec = require('child_process').exec;
// const spawn = require('child_process').spawn;
// const process = require('process');
// const q = require("q");
const path = require("path");
const logger = require('electron-log');
const fs = require('fs-extra');
const t32XrayStudioErrorUtil = require('../utils/t32XrayStudioErrorUtil');
const t32XrayStudioDispatchMsg = require('./t32XrayStudioDispatchMsg');
const idGenerator = require('../utils/idGenerator');
const xrayStudioMain = require('../t32XrayStudioMain');
const t32XrayStudioDirHandler = require('../services/t32XrayStudioDirHandler');

module.exports.initialize = initialize;
module.exports.sendCommand = sendCommand;
module.exports.checkXrayStudioDriverStatus = checkXrayStudioDriverStatus;
module.exports.aquireImage = aquireImage;
module.exports.notifyOnFileChange = notifyOnFileChange;

var studioContext = null;
async function initialize(context){
    // var deferred = q.defer();
    try {
        studioContext = context;
    
        logger.info('-------XRAY STUDIO DRIVER INIT------');
    
        var twainPathWithoutDriveExt = studioContext.setting.twainDriverPath.replace(/[a-z]?[A-Z]?:\\?\\?\/?\/?\s?/gm, "");
    
        var executionPath = path.join(studioContext.theApp.getRootPath(), twainPathWithoutDriveExt) || '';
    
        // console.log("the execution path is ", executionPath);
        var args = [
            'reInitializeDriver=' + studioContext.setting.reInitializeDriver,
            'disableSourceAfterAcquire=' + studioContext.setting.disableSourceAfterAcquire,
            'showUi=' + studioContext.setting.showUi
        ];
    
        await checkXrayStudioDriverStatus(10)
        // .then(() => {
        //     logger.info('----DRIVER ALEADY RUNNING----');
        //     deferred.resolve();
        // })
        // .catch((err) => {
            
        // })
        return
    } catch (e) {
        logger.info('-------XRAY STUDIO DRIVER INIT COMPLETE------');
        try {
            return await sendCommand(executionPath, args)
                // .then(() => {
                //     logger.info('-------XRAY STUDIO DRIVER INIT COMPLETE------');
                //     deferred.resolve();
                // })
                // .catch((err) => {
                //     deferred.reject(err);
                // })
        } catch (e) {
            throw e
        }
        // throw e
    }


    // return deferred.promise;
}

/**
 * Sends command to xrayDriver through execFile process
 * @param {String} executionPath the exe location
 * @param {Array} args the configuration params
 */
async function sendCommand(executionPath, args){
    // var deferred = q.defer();
    try {
        // logger.info("sending send command---------->")
        execFile(executionPath, args, (err, stdout, stderr) => {
    
            if (err) {
                var errMsg = t32XrayStudioErrorUtil.createError('xrayStudioDriver', 'Xray Driver Quit Unexpectedly', err);
                logger.info(errMsg);
                t32XrayStudioDispatchMsg.sendFailedDriverCommsMsgToClient(errMsg.error)
                // deferred.reject(errMsg);
                return
            }
            else{
                var errMsg = t32XrayStudioErrorUtil.createError('xrayStudioDriver', 'Xray Driver Was Stopped', err);
                logger.info(errMsg);
                t32XrayStudioDispatchMsg.sendFailedDriverCommsMsgToClient(errMsg.error)
                // deferred.reject(errMsg);
                return
            }
        });

        // logger.info("checking driver status")
        return await checkXrayStudioDriverStatus(60)
    } catch (e) {
        throw e;
    }
    // checkXrayStudioDriverStatus(60)
    // .then(function(){
    //     deferred.resolve();
    // })
    // .catch((err) => {
    //     deferred.reject(err);
    // })

    // return deferred.promise;
}

async function checkXrayStudioDriverStatus(maxAttempts){
    // var deferred = q.defer();
    try {
        var status = {
            inProgress: false,
            error: false,
            attempts: 1,
            maxAttempts: maxAttempts,
            processRunning: false
        };
        // var error = false;
        var errMsg = t32XrayStudioErrorUtil.createError('xrayStudioDriver');
    
        return await checkDriverStatus(status, errMsg);
    } catch (e) {
        throw (e);
    }
    // return deferred.promise;
}


async function checkDriverStatus (status, errMsg) {
    return await new Promise((resolve, reject) => {
        var driverStatus = setInterval(function () {
            logger.info("CHECKIING PROCESS RUNNING")
            isProcessRunning('Tab32ImageCapture.exe', null, null, status)
            .then(() => {
                if (status.attempts > status.maxAttempts) {
                    clearInterval(driverStatus);
                    errMsg.error = 'connection attempt limit reached'
                    reject(errMsg);
                }
                else if (status.error) {
                    errMsg.error = 'Error Communicating With Driver'
                    reject(errMsg);
                }
                else if (status.processRunning) {
                    clearInterval(driverStatus);
                    resolve();
                }
                else {
                    status.attempts++;
                }
            })
            .catch((err) => {
                logger.info("error in isProcessRunning ", err);
                errMsg.error = 'Error Communicating With Driver'
                clearInterval(driverStatus);
                reject(errMsg);
            })
        }, 1000);
    }) 
}

async function isProcessRunning(win, mac, linux, status) {

    try {
        if (status.inProgress) {
            return
        }
    
        logger.info('attempt ', status.attempts);
        var platform = process.platform;
        var command = platform == 'win32' ? 'tasklist' : (platform == 'darwin' ? "ps -ax | grep " + mac : (platform == 'linux' ? "ps -A" : ""));
        command += ' /fi "imagename eq Tab32ImageCapture.exe" ';
        // var proc = platform == 'win32' ? win : (platform == 'darwin' ? mac : (platform == 'linux' ? linux : ''));
        
        return await new Promise((resolve, reject) => {
            exec(command, function (err, stdout, stderr) {
                if (err) {
                    logger.info("----error running command -----", err)
                    status.error = true;
                    resolve();
                }
        
                var output = stdout.replace(/(\r\n|\n|\r)/gm, "");
                var processNotRunningStr = "INFO: No tasks are running which match the specified criteria."
        
                logger.info("The xrayStudioDriver is running ", output !== processNotRunningStr);
        
                status.inProgress = false;
                if (output !== processNotRunningStr) {
                    status.processRunning = true;
                    resolve();
                }
                
                resolve();
            })
        
            status.inProgress = true;
        })
    } catch (e) {
        throw e;
    }
}

async function aquireImage(location, destination) {
    // var deferred = q.defer();
    try {
        var ext = path.extname(location);
    
        // studioContext.stopDirWatcher();
    
        var newName = idGenerator.generate() + ext;
        var newFile = path.join(destination, newName);
        
        // fs.copyFile(location, newFile, (err) => {
        //     if (err) {
        //         // studioContext.watchXrayDirectory();
        //         logger.error("We have error aquiring image from driver's specified folder ", err);
        //         // deferred.reject(err);s
        //         throw err
        //     }
        //     else{
        //         // studioContext.watchXrayDirectory();
        //         return await notifyOnFileChange(studioContext)
        //         // deferred.resolve();
        //     }
        // });
        fs.copyFileSync(location, newFile);
        return await notifyOnFileChange(studioContext)
    } catch (e) {
        throw e
    }

    // return deferred.promise;
}

async function notifyOnFileChange(studioContext) {
    try {
       let hasNewFile = await t32XrayStudioDirHandler.checkForNewFiles(studioContext, studioContext.ignoreFileList);

        let msg = {
            msgType: 'DirectoryUpdated'
        }

        if (hasNewFile) {
            studioContext.browserWindow.notifyClient(msg);
        } else {
            logger.info("...no new files found...do not trigger client notify")
        }

        return 
    } catch (err) {
        logger.error("error checking for file change after aquiring image from driver/simulation ", err);
        throw err;
    }
}