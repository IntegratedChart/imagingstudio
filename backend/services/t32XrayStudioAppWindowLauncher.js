const logger = require("electron-log"),
t32XrayStudioBrowserWindowHandler = require('./t32XrayStudioBrowserWindowHandler'),
url = require("url");
path = require('path'),
t32XrayStudioUploadQueue = require('./t32XrayStudioUploadQueue');

module.exports.initialize = initialize;
async function initialize(fileUrl, isDevelopment) {

    try {
        // Create main window
        // Other options available at:
        // http://electron.atom.io/docs/latest/api/browser-window/#new-browserwindow-options
        let windowOpts = {
            name: "tab32XrayStudio",
            // width: 3000,
            // height: 3000,
            // useContentSize: true,
            show: false,
            thickFrame: true,
            toolbar: false,
            webPreferences: {
                nodeIntegration: true
            }
        }

        var browserWindow = t32XrayStudioBrowserWindowHandler.initialize(windowOpts);

        var mainWindow = browserWindow.mainWindow;


        if (!isDevelopment) {
            mainWindow.setMenu(null);
        }

        // Target HTML file which will be opened in window
        // logger.info("loading index.html");
        // logger.info("here is the dirname ", __dirname)
        // logger.info(path.join(__dirname, "frontend/xrayStudioV1/index.html"))
        // 'file://' + __dirname + "/frontend/index.html"
        mainWindow.loadURL(
            // url.format({pathname: path.join(__dirname, "/frontend/imagingStudio/index.html"), protocol: 'file:', slashes: true})
            url.format({ pathname: fileUrl, protocol: 'file:', slashes: true })
        );

        // Uncomment to use Chrome developer tools
        // if (isDevelopment) {
            // mainWindow.webContents.openDevTools({
                // mode: 'right'
            // });
        // }
        mainWindow.maximize();
        mainWindow.show();
        registerListeners(mainWindow, browserWindow);

        return browserWindow;
    } catch (e) {
        throw e
    }

}

function registerListeners (mainWindow, browserWindow) {
    // Cleanup when main window is closed
    mainWindow.on('close', (e) => {
        logger.info("we are closing window ")
        if (t32XrayStudioUploadQueue.getQueueCount() <= 0) {
            /* the user tried to quit the app */
            logger.info("----EXITING-------");
            mainWindow = null;
        } else {
            /* the user only tried to close the window */
            e.preventDefault();
            browserWindow.notifyClient({
                msgType: "uploadsPending"
            })
        }
    })

    mainWindow.webContents.on('did-frame-finish-load', () => {
        logger.info("Frame loaded");
    });
}
