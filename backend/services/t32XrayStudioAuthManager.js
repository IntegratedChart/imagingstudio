const logger = require('electron-log');
const t32RestServices = require('../t32RestServices');
const t32RestEndPoints = require('../t32RestEndPoints');
const t32GoogleAuthApi = require('../t32GoogleAuthApi');

module.exports.initialize = initialize;
module.exports.isAuthenticated = isAuthenticated;
module.exports.getEhrToken = getEhrToken;
module.exports.getAuthCode = getAuthCode;
module.exports.getGoogleAuthTokens = getGoogleAuthTokens;
module.exports.removeEhrToken = removeEhrToken;
module.exports.removeAuthCode = removeAuthCode;
module.exports.removeGoogleAuthTokens = removeGoogleAuthTokens;
module.exports.setAuthCode = setAuthCode;
module.exports.setEhrToken = setEhrToken;
module.exports.setGoogleAuthTokens = setGoogleAuthTokens;
module.exports.getGoogleAccessToken = getGoogleAccessToken;
module.exports.getXrayStudioAuthToken = getXrayStudioAuthToken;

var studioContext = null;
function initialize(context){
    studioContext = context;
}

async function isAuthenticated(){
    try{
        return studioContext.googleTokens ? true : false;
    }
    catch(err){
        throw err;
    }
}

function setEhrToken(token){
    studioContext.ehrToken = token;
}

function getEhrToken(){
    return studioContext.ehrToken || null;
}

function removeEhrToken(){
    if(studioContext.ehrToken){
        delete studioContext.ehrToken;
    }
}

function setGoogleAuthTokens(tokens) {
    studioContext.googleTokens = tokens;
}

function getGoogleAuthTokens() {
    return studioContext.googleTokens || null;
}

function setAuthCode(token) {
    studioContext.authCode = token;
}

function getAuthCode() {
    return studioContext.authCode || null;
}

function removeGoogleAuthTokens(){
    if(studioContext.googleTokens){
        delete studioContext.googleTokens
    }
}

function removeAuthCode(){
    if(studioContext.authCode){
        delete studioContext.authCode
    }
}

async function getGoogleAccessToken(){
    try{
        if(!getAuthCode()){
            throw 'No code generated'
        }

        let tokens = await t32GoogleAuthApi.getGoogleAccessToken(getAuthCode());

        setGoogleAuthTokens(tokens);

        if (!studioContext.setting.authTokenRegistered){
            logger.info("registering public key")
            let registered = await registerXrayStudioPublicKey();

            
            studioContext.setting.authTokenRegistered = registered ? true : false; 
            
            logger.info("is public key registered ", studioContext.setting.authTokenRegistered);

            var msg = {};
            msg.keyValToChange = {
                authTokenRegistered: registered ? true : false
            };
            msg.msgType = 'updateXrayStudioSettingsProperty'

            studioContext.browserWindow.notifyClient(msg);

            setTimeout(function(){
                return
            }, 100)
        }
        else{
            return 
        }
    }
    catch(err){
        throw err;
    }
}


async function registerXrayStudioPublicKey() {
    try { 
        if (!studioContext.setting.license || !studioContext.setting.machineId) {
            throw 'No License found'
        }

        let urlParam = null;
        let headers = {};
        let postBody = {
            publicKey: studioContext.rsaKeys.publicKey
        };

        logger.info("Registering public key with Ehr ");
        logger.info(postBody);

        await t32RestServices.execForResult(
            t32RestEndPoints.registerXrayStudioPublicKey.routePath,
            t32RestEndPoints.registerXrayStudioPublicKey.method,
            urlParam,
            postBody,
            headers,
            null)

        return true;
    } catch (err) {
        logger.info(err);
        // return false;
        throw err;
    }
}

async function getXrayStudioAuthToken(tokenId){
    try {
        if (!studioContext.setting.authTokenRegistered) {
           throw 'Public Id Registration Required';
        }

        let postBody = {
            tokenId: tokenId,
            // publicKey: studioContext.rsaKeys.publicKey
        }

        let urlParam = null;
        let headers = {};

        logger.debug("Getting auth token from Ehr");
        logger.debug(postBody);

        return await t32RestServices.execForResult(
            t32RestEndPoints.getXrayStudioAuthToken.routePath,
            t32RestEndPoints.getXrayStudioAuthToken.method,
            urlParam,
            postBody,
            headers,
            null)
    } catch (err) {
        throw err;
    }
}