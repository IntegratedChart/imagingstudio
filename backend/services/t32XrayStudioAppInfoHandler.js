const electron = require('electron');
const { app } = electron;
const os = require('os');
const path = require('path');
const RootPath = process.cwd().split(path.sep)[0];

/**
 * 3-20-19 Jagvir Sarai
 * Gets the app home path based on env variable(if any), where tab32 root folder and images will exist. Allows for one install on computer, rather than mulitple.
 */
const getAppHome = function () {
    //adjust code here to get the 
    if (this.os.platform() === 'darwin') {
        return this.app.getPath('home')
    } else if (this.os.platform() === 'win32') {
        return path.join(this.rootPath, '/ProgramData/xraystudio');
        console.log('the home path', appHomePath);
    }

}

const getOsUserName = function () {
    return this.os.userInfo().username;
}

const getOsHostName = function () {
    return this.os.hostname();
}

const getRootPath = function () {
    return this.rootPath;
}

const appInfo = Object.freeze({
    app: app,
    os: os,
    rootPath: RootPath,
    getAppHome: getAppHome,
    getOsUserName: getOsUserName,
    getOsHostName: getOsHostName,
    getRootPath: getRootPath,
});

module.exports.appInfo = appInfo;
// console.log("the app info ", appInfo)

// module.exports.initialize = initialize;
