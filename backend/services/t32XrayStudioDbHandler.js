const logger = require("electron-log")
const child = require('child_process');
const path = require('path');
const t32XrayStudioClientConnect = require("./t32XrayStudioClientConnect");
const fs = require('fs-extra');
const t32EncryptionHandler = require("./t32XrayStudioEncryptionHandler");
const utils = require("../utils/utils");
const DbClient = require('../db');
const ActionFunctions = {
  onCredentialSubmission: null
}


module.exports.initialize = initialize;
module.exports.actions = ActionFunctions;

/**
 * Attempts to connect to db, first getting user credentials from file (if avialable), then ensuring that the default tab32 user and db exist (if not tries to create them).
 * Functions sets up 
 * 
 * @param {*} context the studio context 
 */
async function initialize(context){
  try {
    const defaultUser = 'tab32Admin'
    const defaultDb = 'tab32ImagingStudio';
    let defualtDbConfig = {
      username: null,
      database: defaultDb,
      isTab32Admin: false,
      isConnected: false,
    }
    let studioContext = context;
    let actions = ActionFunctions;
    let databaseRef = DbClient;

    studioContext.db = !studioContext.db ? defualtDbConfig : studioContext.db;

    logger.info("setting up function contexts");
    let udpateContextDbInfo = updateDbInfoFuncInit(studioContext);
    let sendClientNotificaiton = clientNotifyFuncInit(studioContext.browserWindow.notifyClient);
    let sendConnectionChangeUpdate = connectionChangeFuncInit(sendClientNotificaiton, studioContext, defaultDb);
    let updateConnection = updateConnectionFuncInit(udpateContextDbInfo, sendConnectionChangeUpdate);
    let getDbConfigByType = setupDbConfigFunc(defaultDb, defaultUser)
    let registerDbListners = registerDbListnersInit(databaseRef, udpateContextDbInfo, sendConnectionChangeUpdate);
    actions.onCredentialSubmission = credentialSubmissionFuncInit(studioContext, updateConnection, registerDbListners, getDbConfigByType, databaseRef);
    logger.info("finished setup of function contexts");
    
    let credentialFilePath = path.join(studioContext.theApp.getAppHome(), "\\tab32\\dbCredentials.txt");

    if (!studioContext.rsaKeys) {
      logger.info("rsa keys not found");
      return null
    }

    let dbCredentials = await getDbCredentials(credentialFilePath, studioContext.rsaKeys.privateKey, studioContext.machineId);
    let tab32DbConfig = getDbConfigByType(dbCredentials, 'tab32');
    let masterDbConfig = getDbConfigByType(dbCredentials, 'master');

    if(dbCredentials && dbCredentials.master) {

      await runDbConnectionSteps (masterDbConfig, tab32DbConfig);

      registerDbListners();

      if (!studioContext.clientReady) {
        await t32XrayStudioClientConnect.waitForClient();
      } 

      runClientUpdateSteps(tab32DbConfig, true, updateConnection);

    } else {
      // send password prompt when client is ready
      if (!studioContext.clientReady) {
        await t32XrayStudioClientConnect.waitForClient();
      } 

      logger.info("sending client update message")

      runClientUpdateSteps(masterDbConfig, false, updateConnection)

      return await sendPromptForCredentials(null, defaultDb, sendClientNotificaiton)
    }

  } catch (e) {
    logger.error("error initilaizing db ", e);
    runClientUpdateSteps(null, false, updateConnection)
    throw e
  }

}

/**
 * Updates sutdio context object with db info client side on any connection change. 
 * @param {*} dbConfig 
 * @param {*} isConnected 
 */
function runClientUpdateSteps(dbConfig, isConnected, notifyCallback) {

  let dbInfo = null;
  if (dbConfig) {
    dbInfo = {
      username: dbConfig.username,
      database: dbConfig.database,
      isTab32Admin: dbConfig.isTab32Admin,
      isConnected: isConnected
    }
  }

  notifyCallback(dbInfo)
}

/**
 * Ensures user, db and then tries to connect with tab32 credentials
 * @param {*} masterDbConfig 
 * @param {*} tab32DbConfig 
 */
async function runDbConnectionSteps(masterDbConfig, tab32DbConfig, databaseRef=null) {
  try {

    await ensureUser(masterDbConfig, tab32DbConfig, databaseRef);

    await ensureDb(masterDbConfig, tab32DbConfig, databaseRef);

    await connectDb(tab32DbConfig, databaseRef);
    
    return 
  } catch (e) {
    throw e;
  }

}


/**
 * Sends client a prompt notification to collect db master information
 * @param {*} username 
 * @param {*} database 
 */
async function sendPromptForCredentials(username=null, database=null, notifyCallback) {
  try {
    let msg = {
      msgType: 'incomingCommsChannelMsg',
      channel: 't32XrayStudioDb',
      msg: {
        msgType: 'credentialPrompt',
        credentials: {
          username: username,
          database: database,
          isTab32Admin: false,
        }
      }
    }

    return notifyCallback(msg);
  } catch (e) {
    throw e;
  }
}

/**
 * Ensures that windows has the pasql bin folder inside of the path env variable, this will let us use psql command and other postgreSql commands without always going into files. 
 */
// async function ensureCmdShortcutPath() {
//   try {
//     let filePath = path.resolve("./scripts/setDbEnvPath.bat");
//     let args = [];
//     let results = await runExecFile(filePath, args);

//     logger.info('-------results are -----', results);
//     if(results == 1) {
//       return false
//     } else {
//       return true;
//     }
//   } catch (e) {
//     throw e;
//   }
// }

/**
 * tries to get encrypted credentials and then decrypts them.
 * @param {*} credentialFilePath 
 * @param {*} privateKey 
 * @param {*} secret 
 */
async function getDbCredentials(credentialFilePath, privateKey, secret) {
  try {

    logger.info("------getting db creds------")
    if(!secret) {
      logger.info("credential pre-req fail")
      return null;
    }

    if (fs.existsSync(credentialFilePath)) {

      let encryptedCredentials = await fs.readFileSync(credentialFilePath, {encoding: 'utf8'});
      let dbCreds = await t32EncryptionHandler.decryptMsg(privateKey, secret, encryptedCredentials);

      return JSON.parse(dbCreds)
    } 

    logger.info("db creds file path  " + credentialFilePath + " not found");
    return null
  } catch (e) {
    logger.error('error getting db creds', e);
    return null
  }
}

/**
 * Connects to postgres db
 * @param {*} dbConfig 
 */
async function connectDb(dbConfig, databaseRef=null) {
  try {
    logger.info("trying to connect to db");
    let db = databaseRef || DbClient;
    db.initialize(dbConfig)
    return await db.actions.connect()
  } catch (e) {
    logger.info("failed to connect to db ", e);
    throw e
  }
}


/**
 * Ensures that a db exists, if not tries to create it.
 * @param  {Object} masterDbConfig - the users credentials for db
 * @param  {Object} tab32DbConfig - tab32 default credentials
 */
async function ensureDb(masterDbConfig, tab32DbConfig, databaseRef=null) {
  try {
    logger.info("ensuring imaging studio db");
    let db = databaseRef || DbClient;
    let hasDb = await db.findDb(masterDbConfig, tab32DbConfig.database);

    logger.info("has hasDb ", hasDb);
    if (!hasDb || hasDb == 0) {
      logger.info("creating db");
      await db.createDb(masterDbConfig, tab32DbConfig.database, tab32DbConfig.username)
    }

    return 
  } catch (e) {
    logger.error("failed to ensure Db", e)
    throw e
  }
}

/**
 * Ensures that a user exists, if not tries to create it.
 * @param  {Object} masterDbConfig - the users credentials for db
 * @param  {Object} tab32DbConfig - tab32 default credentials
 */
async function ensureUser(masterDbConfig, tab32DbConfig, databaseRef=null) {
  try {
    logger.info("ensuring imaging studio user");
    // logger.info('the db config ');
    // logger.info(masterDbConfig);
    let db = databaseRef || DbClient;
    let user = await db.findUser(masterDbConfig, tab32DbConfig.username);


    logger.info("has user ", user);
    if (!user || user == 0) {
      logger.info("creating new user");
      await db.createUser(masterDbConfig, tab32DbConfig.username, tab32DbConfig.password);
    }

    return 
  } catch (e) {
    logger.error("failed to ensure user", e)
    throw e
  }
}

function createDbPassword() {
  return utils.createRandomString();
}


function updateDbInfoFuncInit(context) {
  let studioContext = context;

  return function (config) {
    if (config) {
      for (let key in config) {
        studioContext.db[key] = config[key];
      }
    }
  }
}

function clientNotifyFuncInit(func) { 
  let handler = func;
  return function (msg) {
    return handler(msg);
  } 
}

function connectionChangeFuncInit(func, context, defaultDb) {
  let handler = func;
  let studioContext = context;

  return function () {
    let msg = {
      msgType: 'incomingCommsChannelMsg',
      channel: 't32XrayStudioDb',
      msg: {
        msgType: 'dbConnectionChange',
        update: {
          username: studioContext.db.username || '',
          database: studioContext.db.database || defaultDb,
          isConnected: studioContext.db.isConnected || false,
          isTab32Admin: studioContext.db.isTab32Admin || false
        }
      }
    }

    return handler(msg);
  }
}

function registerDbListnersInit(databaseRef, updateContext, sendClientUpdate){
  let db = databaseRef;
  let connectionUpdate = (info) => {
    updateContext(info);
    sendClientUpdate();
  }

  return function () {
    db.actions.on('end', (msg) => {
      logger.info("db connection ended");
      connectionUpdate({ connected: false });
    })

    db.actions.on('error', (err) => {
      logger.info("error comming from db ", err);
      connectionUpdate({ connected: false });
    })

    db.actions.on('notification', (msg) => {
      logger.info("information comming from db ", err);
      logger.info("notification is ", msg);


      connectionUpdate({ connected: false });
    })

    return
  }
}

function setupDbConfigFunc (defaultDb, defaultUser) {

  return function (dbCredentials, credentialType, overwriteCreds = null) {
    let config = {
      database: defaultDb,
      username: null,
      password: null,
      isTab32Admin: false
    };
  
    if (credentialType == 'tab32') {
      config = {
        database: defaultDb,
        username: defaultUser,
        password: createDbPassword(),
        isTab32Admin: true
      }
    } else if (overwriteCreds) {
      config = overwriteCreds;
      config.isTab32Admin = false;
    }
  
    return dbCredentials && dbCredentials[credentialType] ? dbCredentials[credentialType] : config
  }
}

function credentialSubmissionFuncInit(studioContext, updateConnection, registerDbListners, getDbConfigByType, databaseRef) {

  return async function (credentials, credentialFilePath) {
    try {
      logger.info("submitting credentials");

      let dbCredentials = await getDbCredentials(credentialFilePath, studioContext.rsaKeys.privateKey, studioContext.machineId);

      let tab32DbConfig = getDbConfigByType(dbCredentials, 'tab32');
      let masterDbConfig = getDbConfigByType(dbCredentials, 'master', credentials);

      await runDbConnectionSteps(masterDbConfig, tab32DbConfig, databaseRef)

      registerDbListners();
      //updates our client side connection status widget.
      runClientUpdateSteps(tab32DbConfig, true, updateConnection);

      return {
        master: masterDbConfig,
        tab32: tab32DbConfig
      }
    } catch (e) {
      logger.info("error submitting credentials at db handler level")
      throw e
    }
  }
}

function updateConnectionFuncInit(updateContext, sendChange) {
  return function (info) {
    updateContext(info);
    sendChange()
  }
}

// let sendConnectionChangeUpdate = (sendClientNotificaiton) => {

//   let notifyClient = sendClientNotificaiton;

//   return function (config) {
//     let msg = {
//       msgType: 'incomingCommsChannelMsg',
//       channel: 't32XrayStudioDb',
//       msg: {
//         msgType: 'dbConnectionChange',
//         update: {
//           username: config.username || '',
//           database: config.database || defaultDb,
//           isConnected: config.isConnected || false,
//           isTab32Admin: config.isTab32Admin || false
//         }
//       }
//     }

//     return notifyClient(msg);
//   }
// }
