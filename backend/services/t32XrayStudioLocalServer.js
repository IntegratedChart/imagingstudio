const express = require('express');
const logger = require('electron-log');
const path = require('path');
const t32XrayStudioAuthManager = require('./t32XrayStudioAuthManager');
// const t32XrayStudioBrowserWindowHandler = require('../services/t32XrayStudioBrowserWindowHandler');
const t32AppInstanceHandler = require('../services/t32XrayStudioAppInstanceHandler');

module.exports.initialize = initialize;


function initialize(studioContext = this) {
    logger.info('... creating local server ...');
    return startServer(studioContext);
}


function startServer(studioContext){
    try {
        if (studioContext.localExpressServer) {
            logger.info('...... take down existing local server....');
            studioContext.localExpressServer.close();
            studioContext.localExpressServer = null;
        }

        var app = express();

        logger.debug('...... set doc-root to ' + studioContext.setting.tab32Root);

        app.use(express.static(studioContext.setting.tab32Root));

        /**
         * Logic to handle return from google auth page.
         */
        var callbackEntry = '/auth/google/return';
        studioContext.googleCallbackUrl = 'http://localhost:' + studioContext.setting.localTcpPort + callbackEntry;

        logger.debug('...... set google callback url to : ' + studioContext.googleCallbackUrl);

        app.get(callbackEntry, function (req, res) {
            logger.debug('Receive google callback, authCode :' + req.query.code);

            t32XrayStudioAuthManager.setAuthCode(req.query.code);
            // now request google access token
            t32XrayStudioAuthManager.getGoogleAccessToken()
            .then(function(){
                var msg = { msgType: 'Authentication', authCode: studioContext.authCode };

                studioContext.browserWindow.notifyClient(msg);

                
                var resFile = __dirname + '/frontend/xrayStudioV1/pageAfterGoogleLogin.html';
                // var resFile = path.resolve(__dirname, '../../frontend/xrayStudioV1/pageAfterGoogleLogin.html');

                console.log('sending back to ', resFile);
                res.sendFile(resFile);
                

                studioContext.browserWindow.show();
            })
            .catch(function(err){
                logger.error(err);
            })

            // t32GoogleAuthApi.getGoogleAccessToken(req.query.code)
            //     .then(function (tokens) {
            //         //    // get the refresh token as well.
            //         //    return t32GoogleAuthApi.refreshGoogleAccessToken(tokens);
            //         // })
            //         // .then(function(tokens){
            //         // at this point, we have got the refresh token.
            //         studioContext.googleTokens = tokens;
            //         logger.debug('... getting google token ');
            //         logger.debug(tokens);

            //         var resFile = path.resolve('./app/frontend/pageAfterGoogleLogin.html');

            //         console.log('sending back to ', resFile);

            //         res.sendFile(resFile);
            //         studioContext.showMainWindow();

            //         var msg = { msgType: 'Authentication', authCode: studioContext.authCode };
            //         studioContext.notifyClient(msg)
            //     })
            //     .catch(function (err) {
            //         logger.error(err);
            //     })
        });

        studioContext.localExpressServer = app.listen(studioContext.setting.localTcpPort, function () {
            logger.info('Local express server started listening on port : ' + studioContext.setting.localTcpPort);
        }).on('error', function (err) {
            logger.info('----App Listen Error-----', err);
            if (err.toString().match(/EADDRINUSE/gi) && process.platform == 'win32') {
                t32AppInstanceHandler.instanceEmitter.emit('appPortAlreadyInUse');
            }
            else {
                setTimeout(function () {
                    // studioContext.stopDirWatcher();
                    studioContext.theApp.app.quit();
                }, 5000)

                throw new Error("FATAL ERROR: ERROR SETTING UP PORT, EXITING IN 5 SECONDS...");
            }
        });

        return
    }
    catch (err) {
        throw err;
    }
}