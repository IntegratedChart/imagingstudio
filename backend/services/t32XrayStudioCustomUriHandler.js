const logger = require('electron-log');
const t32XrayStudioActionHandler = require('./t32XrayStudioActionHandler');
const path = require("path");

module.exports.handleCustomUri = handleCustomUri;


function handleCustomUri(msg){
    try{
        var customUri = msg.args;
        var actionObj = decodeUri(customUri);
        logger.info('handing off to action handler ', actionObj);
        t32XrayStudioActionHandler.handleAction(actionObj);
    }
    catch(err){
        err;
    }

}


function decodeUri(uri) {
    logger.info('trying to decode uri ', uri);
    uri = decodeURI(uri);
    var actionSplit = uri.split("?");
    var actionProtocolSplit = actionSplit[0].split("://");

    var protocol = actionProtocolSplit[0];
    var action = actionProtocolSplit[1].replace('/', '');

    var params = {};

    decodeURIComponent(actionSplit[actionSplit.length - 1])
        .split("&")
        .map((function (splitUp) {
            return splitUp.split('=');
        }))
        .map(function (splitUpSplit) {
            return params[splitUpSplit[0]] = splitUpSplit[1];
        })


    return {
        protocol: protocol,
        action: action,
        params: params
    }
}