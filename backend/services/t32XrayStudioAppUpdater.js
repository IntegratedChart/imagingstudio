const logger = require('electron-log');
const { autoUpdater } = require("electron-updater");
const os = require('os');

module.exports.initialize = initialize;
module.exports.checkForUpdates = checkForUpdates;

async function initialize(appVersion, updateFeed, callback) {
    try {
        // let appVersion = studioContext.theApp.app.getVersion();
        // let browserWindow = studioContext.browserWindow;

        let trimmedAppVersion = trimVersion(appVersion);

        if (os.platform() === 'darwin') {
            updateFeed = updateFeed + 'osx';
        } else if (os.platform() === 'win32') {
            updateFeed = updateFeed + 'win/' + os.arch();
        }

        autoUpdater.on("update-available", (evt, info) => {
            logger.info("A new update is available");
            let message = 'A new version of the app is avaliable and will be downloaded automatically.';
            // setTimeout(function () {
            //     browserWindow.notifyClient({
            //         msgType: "softwareUpdate",
            //         msg: message
            //     });
            // }, 5000)
            callback({action: 'notifyClient', msg: message})
        });

        autoUpdater.on("update-downloaded", (updateInfo) => {
            var msg = "A new update is ready to install, version " + updateInfo.version + " is downloaded and will be automatically installed on restart ";
            logger.info(msg);

            logger.info("The update info", updateInfo);

            /**
             * Updated 2.27.19 - Jagvir Sarai
             * New logic for controlled releasese. 
             * When minor update(not patch fix), we remove the app update urls
             */
            var trimmedUpdateVersion = trimVersion(updateInfo.version);

            if (isDiffMinorVersions(trimmedAppVersion, trimmedUpdateVersion)) {
                //remove app update url, so any machine that has manual update url will be moved over and point to latest world code.
                // t32XrayStudioSettingService.getSettings()
                //     .then(function (setting) {
                //         setting.appUpdateUrl = "";
                //         var msg = {};
                //         msg.keyValToChange = {
                //             appUpdateUrl: ""
                //         };
                //         msg.msgType = 'updateXrayStudioSettingsProperty'

                //         studioContext.browserWindow.notifyClient(msg);
                //     })
                callback({action: 'removeAppUrl'})
            };
            var message = `Software update v${updateInfo.version} downloaded and will be installed upon restart.`;

            // browserWindow.notifyClient({
            //     msgType: "softwareUpdate",
            //     msg: message
            // });

            callback({action: "notifyClient", msg: message})
        });

        autoUpdater.on("error", (error) => {
            // logger.info('error with auto update ----->', error);
            // var message = 'There was an error during the software update.';
            // browserWindow.notifyClient({
            //     msgType: "softwareUpdate",
            //     msg: message
            // });
            callback({action: error, msg: error});
        });

        autoUpdater.on("checking-for-update", (event) => {
            logger.info("--------Checking for update---------");
        });

        autoUpdater.on("update-not-available", () => {
            logger.info("----------Update not available--------");
        });

        // const feedURL = updateFeed + '?v=' + appVersion;
        // logger.info('Setting feed update url : ' + feedURL);
        var feedOptions = {
            provider: "generic",
            url: updateFeed,
            publishAutoUpdate: true,
            updaterCacheDirName: 'tab32-imaging-studio-updater',
        };

        autoUpdater.setFeedURL(feedOptions);
        // return true;
        // feedUrlSet = true;
        
        // logger.info("going into checking update");
        return checkForUpdates();
    } catch (e) {
        logger.error("error with app updater ", e);
        return
    }
}

function checkForUpdates() {
    return autoUpdater.checkForUpdates();
}

function trimVersion(appVersion) {
    var reg = /\d\.\d/;
    var appVerMatch = appVersion.match(reg);
    logger.info(appVerMatch);
    var trimVer = appVerMatch && appVerMatch.length ? appVerMatch[0] : null;

    logger.info("The trimmed app version")
    logger.info(trimVer);
    return trimVer;
}

/**
 * Checks to see if we have a minor or major update
 * @param {*} trimmedApVer 
 * @param {*} trimmedUpdateVer 
 */
function isDiffMinorVersions(trimmedApVer, trimmedUpdateVer) {
    return trimmedApVer !== trimmedUpdateVer ? true : false;
}
