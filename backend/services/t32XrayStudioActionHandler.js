const logger = require('electron-log');
const t32XrayStudioAuthManager = require('./t32XrayStudioAuthManager');
const t32XrayStudioDispatchMsg = require('./t32XrayStudioDispatchMsg');
const t32RestServices = require('../t32RestServices');
const t32RestEndPoints = require('../t32RestEndPoints');
const t32XrayStudioEncryptionHandler = require('./t32XrayStudioEncryptionHandler');
const studioContext = require('../t32XrayStudioMain').getStudioContext();

module.exports.handleAction = handleAction;
module.exports.runGetPatientAction = runGetPatientAction;

const frontEndChannel = 't32XrayStudioAction';

function handleAction(actionObj){
    logger.info('handling action')
    try {
        if (actionObj.action == 'getPatient') {
            if (!actionObj.params) {
                logger.info("missing required paramaters to run get patient action");
                return
            }

            try{
                logger.info('sending msg for getPatient request');
                t32XrayStudioDispatchMsg.sendMsgToClient(frontEndChannel, actionObj);
            }catch(err){
                throw 'Failed to run getPatient command for xraystudio'
            }
        }
    }
    catch (err) {
        throw err
    }
}


async function runGetPatientAction(actionObj){
    //have to figure out how we are going to handle authentication if there is no token, do we then ask user to auth or ignore request and run the instance the way it is.
    try{
        await runAuthStep(actionObj);
        var patientId = actionObj.params.patientId;
        var clinicId = actionObj.params.clinicId;
        return await getPatientByPublicId(patientId, clinicId)
    }
    catch(err){
        logger.info('register error at get patient action');
        logger.error(err);
        throw err;
    }
}

/**
 * Runs steps to ensure that user is authenticated, if user isnt authenticated, we try to get ehr token, if no token given, we will let front-end side handle the appropriate errors.
 * @param { Object } actionObj
 */
async function runAuthStep(actionObj){
    try{
        let isAuthenticated = await t32XrayStudioAuthManager.isAuthenticated();

        if(!isAuthenticated){            
            if (!actionObj.params.token) {
                logger.info('There was no auth token found with the action request,');
                throw 'No Auth Token';
            } else {
                //the token param being passed to us the oid of the real token that will need to be fetched
                
                logger.info("running token query")
                //the token will come as object with msg that is ecrypted with AES cipher and secret keys to unlock that message which are ecrypted with the rsa pub key
                let encryptedMsg = await t32XrayStudioAuthManager.getXrayStudioAuthToken(actionObj.params.token);
                
                logger.info('we have the encrypted token');

                let decryptedKeys = t32XrayStudioEncryptionHandler.decryptMsg(studioContext.rsaKeys.privateKey, studioContext.machineId, encryptedMsg.keys).split('/IV:')

                let decryptedToken = t32XrayStudioEncryptionHandler.decryptCipher(decryptedKeys[0], decryptedKeys[1], encryptedMsg.token).data.split("/clinicId:");

                logger.info("the decrypted token ", decryptedToken)
            
                t32XrayStudioAuthManager.setEhrToken({
                    token: decryptedToken[0],
                    clinicId: decryptedToken[1]
                });
                return 
            }
        }

        return 
    }
    catch(err){
        throw err;
    }
}

async function getPatientByPublicId(patientPublicId, clinicId, isClinicIdPub = true) {
    try{
        let urlParam = null;
        let headers = {};

        // if(clinicId){
        //     headers['clinic-id'] = clinicId
        // }

        let postBody = {
            patientPubId: patientPublicId,
            clinicId: clinicId,
            isClinicIdPub: isClinicIdPub
        };

        logger.debug("Get Patient by Public Id ");
        logger.debug(postBody);
        return await t32RestServices.execForResult(
                t32RestEndPoints.getXrayStudioPatientByPublicId.routePath,
                t32RestEndPoints.getXrayStudioPatientByPublicId.method,
                urlParam,
                postBody,
                headers,
                null)
    }
    catch(err){
        throw err;
    }

}