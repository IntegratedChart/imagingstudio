// const q = require('q');
const logger = require('electron-log');
const exec = require('child_process').exec;
// const process = require('process');
const t32XrayStudioDispatchMsg = require('./t32XrayStudioDispatchMsg');
var t32XrayStudioMsgHandler = require('./t32XrayStudioMsgHandler');
var t32XrayStudioErrorUtil = require('../utils/t32XrayStudioErrorUtil');
var redis = require("redis"),
    msgMap = {},
    subscriber,
    publisher;

var platform = process.platform;
var serviceRunning = false;
var error = false;
var attempts = 1;
var alreadyChecking = false;

module.exports.initialize = initialize;
module.exports.publishMsg = publishMsg;
module.exports.subscribeChannel = subscribeChannel;
module.exports.isReady = isReady;

var isReady = false;
var redisConfig = {
    retry_strategy: function (options) {
        if (options.total_retry_time > 1000 * 60) {
            // End reconnecting after a specific timeout and flush all commands
            // with a individual error
            return new Error('Retry time exhausted');
        }
        // reconnect after
        return 3000
    }
}

async function initialize(studioContext) {
    // var deferred = q.defer();
    try {
        logger.info("-----STARTING REDIS CONNECT INIT-------");
        publisher = null;
        subscriber = null;
        isReady = false;
    
        redisConfig.port = studioContext.setting.redisPort || 6379;
    
        await checkRedisServiceStatus();

        try {
            subscriber = redis.createClient(redisConfig);
            publisher = redis.createClient(redisConfig);
        }
        catch(err){
            logger.info("Redis connection error ", err);
            var errMsg = t32XrayStudioErrorUtil.createError('redisConnect', 'Failed To Connect To Redis Server', err)
            // deferred.reject(errMsg);
            t32XrayStudioDispatchMsg.sendFailedDriverCommsMsgToClient(errMsg.error)
            return
        }

        registerListeners(subscriber);

        // checkRedisServiceStatus()
        // .then(() => {
        //     try{
        //         subscriber = redis.createClient(redisConfig);    
        //         publisher = redis.createClient(redisConfig);
        //     }
        //     catch(err){
        //         logger.info("Redis connection error ", err);
        //         var errMsg = t32XrayStudioErrorUtil.createError('redisConnect', 'Failed To Connect To Redis Server', err)
        //         deferred.reject(errMsg);
        //         t32XrayStudioDispatchMsg.sendFailedDriverCommsMsgToClient(errMsg.error)
        //         return
        //     }
        
        
        //     subscriber.on("error", function (err) {
        //         logger.error("Error with Redis: ", err);
        //         var errMsg = t32XrayStudioErrorUtil.createError('redisConnect', 'Error with Redis Subscriber', err)
        //         deferred.reject(errMsg);
        //         t32XrayStudioDispatchMsg.sendFailedDriverCommsMsgToClient(errMsg.error)
        //         return
        //     });
        
        //     subscriber.on("ready", function () {
        //         isReady = true;
        //         logger.info("Redis connected!!");
        //         deferred.resolve();
        //     });
        
        //     subscriber.on("message", function (channel, message) {
        //         logger.info("We have a message on ", channel);
        //         var msgToPassOn = JSON.parse(message);
        //         t32XrayStudioMsgHandler.handleIncomingMessage(channel, msgToPassOn);
        //     });
    
        // })
        // .catch((err) => {
        //     deferred.reject(err);
        // })
    
        // return deferred.promise;

        // return
    } catch (e) {
        throw e
    }
}


function registerListeners(subscriber) {

    subscriber.on("error", function (err) {
        logger.error("Error with Redis: ", err);
        var errMsg = t32XrayStudioErrorUtil.createError('redisConnect', 'Error with Redis Subscriber', err)
        // deferred.reject(errMsg);
        t32XrayStudioDispatchMsg.sendFailedDriverCommsMsgToClient(errMsg.error)
        return
    });

    subscriber.on("ready", function () {
        isReady = true;
        logger.info("Redis connected!!");
        // deferred.resolve();
    });

    subscriber.on("message", function (channel, message) {
        logger.info("We have a message on ", channel);
        var msgToPassOn = JSON.parse(message);
        t32XrayStudioMsgHandler.handleIncomingMessage(channel, msgToPassOn);
    });

    return 
}

async function checkRedisServiceStatus() {
    // var deferred = q.defer();
    try {
        return await checkServiceStatus('Redis');
    } catch (e) {
        throw e;
    }
    // return deferred.promise;
}

async function checkServiceStatus (service) {
    return new Promise((resolve, reject) => {
        var checkStatus = setInterval(function () {
            isServiceRunning(service, null)
            .then(() => {
                if (attempts > 60) {
                    clearInterval(checkStatus);
                    var errMsg = t32XrayStudioErrorUtil.createError('redisConnect', 'Failed to connect to Redis Server, please ensure that it is installed.')
                    reject(errMsg);
                }
                else if (error) {
                    clearInterval(checkStatus);
                    var errMsg = t32XrayStudioErrorUtil.createError('redisConnect', 'Failed to start Redis Server');
                    reject(errMsg);
                }
                else if (serviceRunning) {
                    clearInterval(checkStatus);
                    resolve();
                }
                else {
                    attempts++;
                }
            })

        }, 1000);
    })
}

async function isServiceRunning(win, mac) {

    return await new Promise((resolve, reject) => {
        if (alreadyChecking) {
            resolve()
        }
    
        logger.info('attempt ', attempts);
        var command = platform == 'win32' ? 'net start' : "ps -ax | grep " + mac;
    
        exec(command, function (err, stdout, stderr) {
            var output = stdout.replace(/(\r\n|\n|\r)/gm, "");
    
            logger.info('is Redis running?... ', output.match(win) && output.match(win).length > 0);
            if (output.match(win)) {
                alreadyChecking = false;
                serviceRunning = true;
                resolve()
            }
            else {
                runService('Redis', null)
                .then(function () {
                    alreadyChecking = false;
                    resolve()
                })
                .catch((err) => {
                    alreadyChecking = false;
                    error = true;
                    resolve()
                })
            }
        })
        alreadyChecking = true;
    })
}

async function runService(win, mac) {
    // var deferred = q.defer();
    return await new Promise((resolve, reject) => {
        var command = platform == 'win32' ? 'net start ' + win : "ps -ax | grep " + mac;
    
        exec(command, function (err, stdout, stderr) {
            var output = stdout.replace(/(\r\n|\n|\r)/gm, "");
    
            if (output.match('The service name is invalid.')) {
                reject()
            }
            else {
                resolve();
            }
        })
    })

    // return deferred.promise;
}

function subscribeChannel(channel) {
    try {
        subscriber.subscribe(channel);
        logger.info("----Subscribed to " + channel + " ----");
        return
    } catch (e) {
        throw e
    }
}

function publishMsg(channel, msg, callback) {
    var message = JSON.stringify(msg);
    logger.info("Redis about to send message ", message);
    publisher.publish(channel, message, function (err, reply) {
        if (err) {
            logger.info("Error publishing on " + channel);
            logger.error(err);
            callback('error');
        }
        else {
            logger.info("Successfully publishing on " + channel);
            callback('success')
        }
    })
}


function isReady(){
    return isReady
}