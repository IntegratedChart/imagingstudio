var logger = require('electron-log');
const t32XrayStudioDispatchMsg = require('../t32XrayStudioDispatchMsg');
const t32XrayStudioDriver = require('../t32XrayStudioDriverHandler');
const Channel = require('../../classes/channel');


module.exports.handleIncomingMsg = handleIncomingMsg;
module.exports.handleOutgoingMsg = handleOutgoingMsg;
module.exports.handleSentMsg = handleSentMsg;

var channelName = 'tab32 x ray driver channel';
const driverChannel = new Channel(channelName, 't32XrayStudioDriverChannel');
var selectedFolder = null;

function handleIncomingMsg(msg){

    if (msg.sender && msg.sender == 'tab32XrayStudio'){
        return 
    }
    var msg = msg;
    driverChannel.addRevievedMsg(msg);

    var msgType = msg.msgType;
    //We are leaving this structure incase we need to do other action on backend before dispatching msg to front end. 
    if (msgType == 'AvailableDrivers') {
        logger.info("The List of Drivers ", msg.drivers);
        t32XrayStudioDispatchMsg.sendMsgToClient(channelName, msg)
    }
    else if (msgType == 'HeartBeatFromDriver') {
        logger.info("the driver is running ", msg.timestamp)
        t32XrayStudioDispatchMsg.sendMsgToClient(channelName, msg)
    }
    else if (msgType == 'ImageReady') {
        logger.info("Driver msg: The image is ready");
        //ask studio to handle file changes.
        logger.info("image path ", msg.imagePath); 
        logger.info("selected folder ", selectedFolder); 
        t32XrayStudioDriver.aquireImage(msg.imagePath, selectedFolder)
        .then(() => {
            t32XrayStudioDispatchMsg.sendMsgToClient(channelName, msg)
        })
        .catch((err) => {
            logger.info("There was an error aquiring image from driver defined path ", err);
            t32XrayStudioDispatchMsg.sendMsgToClient(channelName, msg)
        })
    }
    else if (msgType == 'StopAck') {
        logger.info('Image aquiring stopped');
        t32XrayStudioDispatchMsg.sendMsgToClient(channelName, msg)
    }
    else if (msgType == 'QuitAck') {
        logger.info("Quiting driver...");
        t32XrayStudioDispatchMsg.sendMsgToClient(channelName, msg)
    }
}

async function handleOutgoingMsg(msg){
    try{
        logger.info('---T32 XRAY STUDIO DRIVER CHANNEL MSG ', msg);
    
        if(msg.msgType == 'StartScan'){
            selectedFolder = msg.selectedFolder;
        }
        
        return msg;
        // throw 'error test'
    }
    catch(err){
        t32XrayStudioDispatchMsg.sendFailedDriverCommsMsgToClient('Failed to communicate with xray studio driver');
        throw {
            error: err,
            origin: 'channel'
        }
    }
}

function handleSentMsg(msg){
    driverChannel.addSentMsg(msg)
}
