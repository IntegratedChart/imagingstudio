// const q = require('q');
const logger = require('electron-log');
var t32XrayStudioMsgHandler = require('../t32XrayStudioMsgHandler');
var t32XrayStudioErrorUtil = require('../../utils/t32XrayStudioErrorUtil');
var t32XrayStudioSessionManager = require('../t32XrayStudioSessionManager');
// module.exports.initialize = initialize;
module.exports.handleIncomingChannelMsg = handleIncomingChannelMsg;
module.exports.handleOutgoingChannelMsg = handleOutgoingChannelMsg;
module.exports.handleChannelErrorMsg = handleChannelErrorMsg;
module.exports.run = run;

var channelMap = {
    'tab32 x ray driver channel': 't32XrayStudioDriverChannel.js',
    't32XrayStudioAppInstance': 't32XrayStudioAppInstanceChannel.js'
};

// function initialize(channel, msgType) {
//     var deferred = q.defer();

//     var msg = {
//         "msgType": msgType
//     }

//     logger.info("---INIT CHANNEL MANAGER----");
//     handleOutgoingChannelMsg(channel, msg)
//     .then(() => {
//         deferred.resolve();
//     })
//     .catch((err) => {
//         var errMsg = t32XrayStudioErrorUtil.createError('studioChannelManager', 'Failed To Communicate With Redis')
//         deferred.reject(errMsg);
//     })

//     return deferred.promise;
// }

function handleIncomingChannelMsg(channel, msg){
    if(channelMap[channel]){
        var channelHandler = require('./' + channelMap[channel]);
        channelHandler.handleIncomingMsg(msg);
    }
}

async function handleOutgoingChannelMsg(channel, msg){
    try{
        var modifiedMsg = null;
        msg.sender = 'tab32XrayStudio';
        msg.timestamp = new Date();
        msg.sessionId = t32XrayStudioSessionManager.getSessionId();

        logger.info('channel manager handle outgoing msg');
        logger.info(channel);
        logger.info(msg);
        logger.info(channelMap[channel]);

        if(channelMap[channel]){
            var channelHandler = require('./' + channelMap[channel]);
    
            modifiedMsg = await channelHandler.handleOutgoingMsg(msg);
    
            modifiedMsg.status = await t32XrayStudioMsgHandler.sendOutgoingMessage(channel, modifiedMsg);

            channelHandler.handleSentMsg(modifiedMsg);

            if(modifiedMsg.status == 'fail'){
                throw "Error sending message through redis"
            }
            
            return
        }
        else{
            throw "noChannel"
        }
    }
    catch(err){
        if(err == 'noChannel'){
            throw 'channel not recognized'
        }
        else if(err.error){
            if(err.origin == 'channel'){
                throw "Error in channel handling " + err.error;
            }
        }
        else{
            throw err;
        }
    }
}

function handleChannelErrorMsg(channel, msg) {
    if (channelMap[channel]) {
        var channelHandler = require('./' + channelMap[channel]);
        channelHandler.handleErrorMsg(msg);
    }
}

function run(channel, action, msg){
    logger.info('inisde run func channel ', channel);
    logger.info('inisde run func channelMap[channel] ', channelMap[channel]);
    try{
        if(channelMap[channel]){
            var channelHandler = require('./' + channelMap[channel]);
            logger.info('we are running ' + action + ' with params ' + msg);
            channelHandler[action](msg);
        }
    }catch(err){
        throw err;
    }
}