// const q = require("q");
const logger = require('electron-log');
const Channel = require('../../classes/channel');
const t32XrayStudioAppInstanceHandler = require('../t32XrayStudioAppInstanceHandler');
const t32XrayStudioDispatchMsg = require('../t32XrayStudioDispatchMsg');
const t32XrayStudioSessionManager = require('../t32XrayStudioSessionManager');
const t32XrayStudioBrowserWindowHandler =  require('../t32XrayStudioBrowserWindowHandler');
const t32XrayStudioCustomUriHandler = require('../t32XrayStudioCustomUriHandler');

module.exports.handleIncomingMsg = handleIncomingMsg;
module.exports.handleOutgoingMsg = handleOutgoingMsg;
module.exports.handleSentMsg = handleSentMsg;
module.exports.handleErrorMsg = handleErrorMsg;
module.exports.handleWakeUpMsg = handleWakeUpMsg;

var channelName = 't32XrayStudioAppInstance';
const instanceChannel = new Channel(channelName, 't32XrayStudioAppInstanceChannel');

function handleIncomingMsg(msg) {

    if (msg.sender && msg.sender == 'tab32XrayStudio'){
        if(msg.sessionId !== t32XrayStudioSessionManager.getSessionId()){
            handleMsg(msg);
        }
        else{
            logger.info('the session matches the session we are running');
        }
    }
    else{
        handleMsg(msg);
    }

}

function handleMsg(msg){
    var msg = msg;
    instanceChannel.addRevievedMsg(msg);

    var msgType = msg.msgType;

    if (msgType == 'wakeUp') {
        logger.info("Wake up message recieved from another app instance", msg);
        handleWakeUpMsg(msg);
    }
}

async function handleOutgoingMsg(msg) {
    //noting special to do here.
    return msg;
}

function handleSentMsg(msg) {
    instanceChannel.addSentMsg(msg)
}

function handleWakeUpMsg(msg){
    // t32XrayStudioAppInstanceHandler.handleWakeUp(msg);
    try{
        logger.info(JSON.stringify(msg));
        logger.info('handling wake up msg from', msg.args);
        if(msg.requestType){
            handleActionReq(msg);
        }
        else{
            //COMMENTING OUT FOR NOW, DON'T NEED. GOOD FOR TESTING, DO NOT REMOVE.
            // handleGeneralReq(msg);
        }
    }
    catch(err){
        throw err;
    }
}

function handleErrorMsg(msg){
    t32XrayStudioDispatchMsg.sendMsgToClient(channelName, msg);
}

function handleActionReq(msg){
    try{
        if(msg.requestType == 'customUri'){
            logger.info('handling custom uri msg')
            t32XrayStudioBrowserWindowHandler.show();
            t32XrayStudioCustomUriHandler.handleCustomUri(msg);
        }
    }catch(err){
        throw err
    }
}

function handleGeneralReq(msg){
    try{
        t32XrayStudioBrowserWindowHandler.show();
        // setTimeout(() => {
            // msg.action = 'getPatient';
            // t32XrayStudioDispatchMsg.sendMsgToClient('t32XrayStudioAction', msg)
            t32XrayStudioDispatchMsg.sendMsgToClient(channelName, msg);
        // }, 2000)
    }
    catch(err){
        throw err;
    }
}