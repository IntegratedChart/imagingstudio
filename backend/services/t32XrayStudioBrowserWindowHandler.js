const electron = require("electron");
const { BrowserWindow } = electron;
const { app } = require("../services/t32XrayStudioAppInfoHandler").appInfo;
const logger = require("electron-log");
const url = require("url");

var browserWindow = {};

const show = function (winName = 'main'){
    if (this.mainWindow) {
        this.allWins[winName] ? this.allWins[winName].show() : null;
    } else {
        browserWindow.allWins[winName] ?browserWindow.allWins[winName].show() : null;
    }

    // this.mainWindow ? this.mainWindow.show() : browserWindow.mainWndow.show();
}

const close = function(winName = "main") {
    if (this.mainWindow) {
        this.allWins[winName] ? this.allWins[winName].close() : null;
        delete this.allWins[winName]
    } else {
        browserWindow.allWins[winName] ? browserWindow.allWins[winName].close() : null;
        delete browserWindow.allWins[winName];
    }

    // this.mainWindow ? this.mainWindow.close() : browserWindow.mainWindow.close();
}

const notifyClient = function(msg, msgType = 'BackendPushNotification', winName = 'main') {


    let win = this.mainWindow ? this.allWins[winName] : browserWindow.allWins[winName]

    if (win) {
        win.webContents.send(msgType, msg);
        
        const children = win.getChildWindows();
    
        if (children.length) {
            for (let child of children) {
                child.webContents.send(msgType, msg);
            }
        }
    }
}

const exitAppWindow = function(winName = 'main') {
    // studioContext.stopDirWatcher();
    this.close ? this.close(winName) : browserWindow.close();
}


const exitApp = function() {
    // studioContext.stopDirWatcher();
    app.quit();
}

const getWindowByName = (name) => {
    let allWins = this.mainWindow ? this.allWins : browserWindow.allWins;

    return allWins[name] || null; 
}

const openChildWindow = function (filepath, winName, config=null, isDev=true) {


    let mainWindow = this.mainWindow || browserWindow.mainWindow;
    let allWins = this.mainWindow ? this.allWins : browserWindow.allWins;
    let newWin = null;
    let showWin = this.show || browserWindow.show;

    if ( allWins[winName] ) {
        showWin();
    } else {
        let windowConfig = config || {
            modal: true,
            name: "Imaging Studio",
            // show: false,
            width: 1150,
            height: 850,
            frame: false,
            toolbar: false,
            webPreferences: {
                nodeIntegration: true
            }
        }
        
        if (!windowConfig.parent) {
            windowConfig.parent = mainWindow;
        }
    
        newWin = new BrowserWindow(windowConfig);
    
        if ( this.mainWindow ){
            this.allWins[winName] = newWin;
        } else {
            browserWindow.allWins[winName] = newWin;
        }

        logger.info("the all win after ", browserWindow.allWins);
    
        newWin.setMenu(null);
    
        // if (isDev) {
            // newWin.webContents.openDevTools({
            //   mode: 'right'
            // });
        // }
        
        logger.info('attempting to open ', path.join(__dirname, filepath))
        newWin.loadURL(
            url.format({
              pathname: path.join(__dirname, filepath),
              protocol: 'file:',
              slashes: true
            })
        )
        
        newWin.maximize();

        //register listeners to handle closing off window
        if (winName !== 'main') {
            registerListeners(winName);
        }
    }



    return newWin
}


function registerListeners(winName) {
    let allWins = this.allWins || browserWindow.allWins;
    let notifyClient = this.notifyClient || browserWindow.notifyClient;

    allWins[winName].on("close", (evt) => {
        logger.info("closed window ", winName)
        delete allWins[winName];
        logger.info("the all wins after ", browserWindow.allWins);
        let msg = {
            msgType: 'windowClose',
            winName: winName
        }
        notifyClient(msg);
    });
}

function initialize(options) {
    let mainWindow = new BrowserWindow(options);

    browserWindow = Object.freeze({
        mainWindow: mainWindow,
        app: app,
        show: show,
        close: close,
        notifyClient: notifyClient,
        exitAppWindow: exitAppWindow,
        exitApp: exitApp,
        openChildWindow: openChildWindow,
        allWins: {
            main: mainWindow
        },
        registerListeners: registerListeners
    })
    
    return browserWindow;
}

module.exports.initialize = initialize;
// module.exports.browserWindow = browserWindow;
module.exports.close = close;
module.exports.show = show;
module.exports.notifyClient = notifyClient;
module.exports.exitAppWindow = exitAppWindow;
module.exports.exitApp = exitApp;
module.exports.openChildWindow = openChildWindow;
module.exports.getWindowByName = getWindowByName;
