// const { publicEncrypt } = require('crypto');
// const { privateDecrypt } = require('crypto');
// const { generateKeyPairSync } = require('crypto');
// const {promisify} = require('util');
const forge = require('node-forge');
forge.options.usePureJavaScript = true;
const pki = forge.pki;
const cipher = forge.cipher;
// let generateKeyPairAsync =  promisify(forge.pki.rsa.generateKeyPair);
const logger = require('electron-log');

module.exports.generateKeys = generateKeys;
module.exports.encryptMsg = encryptMsg;
module.exports.decryptMsg = decryptMsg;
module.exports.decryptCipher = decryptCipher;

/**
 * Generates key pair of rsa puclic and encrypted private key
 * @param {String} secret - used to encrypt the private key itself
 */
async function generateKeys(secret){
    try{
        let keypair = pki.rsa.generateKeyPair({bits: 2048});

        return {
            privateKey: pki.encryptRsaPrivateKey(keypair.privateKey, secret),
            publicKey: pki.publicKeyToPem(keypair.publicKey)
        }

    }catch(err){
        throw err;
    }
}

/**
 * Ecrypts a msg with rsa public key and encrypts to default pkcs#1 v1.5
 * @param {*} publicKey 
 * @param {*} msg 
 */
function encryptMsg(publicKey, msg){
    try {
        return pki.publicKeyFromPem(publicKey).encrypt(msg);
    } catch (err) {
        throw err;
    }
}

/**
 * Decrypts a pem formatted encrypted private key and then decrpts the message
 * @param {*} privateKey 
 * @param {*} secret - used to decrypt the private key itself
 * @param {*} msg 
 */
function decryptMsg(privateKey, secret, msg){
    try {
        return pki.decryptRsaPrivateKey(privateKey, secret).decrypt(msg);
    } catch (err) {
        throw err;
    }
}


function decryptCipher(key, iv, encryptedMsg){
    var decipher = cipher.createDecipher('AES-CBC', key);
    decipher.start({
        iv: iv
    });
    decipher.update(forge.util.createBuffer(encryptedMsg));
    decipher.finish();

    return decipher.output;
}

