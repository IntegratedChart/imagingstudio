//Hadles all ipc communication between frontend(angluar) and backend(electron/node) side of code. 
const { ipcMain } = require('electron');
const logger = require('electron-log');
const t32XrayStudioClientConnect = require('./t32XrayStudioClientConnect');
const t32XrayStudioWindowActions = require("./t32XrayStudioWindowActions");

module.exports.initialize = initialize;

var studioContext = null;

function initialize(theContext = this) {
    studioContext = theContext;
    setUpElectronMessaging();
}

function setUpElectronMessaging(){
    logger.debug('......initialize messaging handlers.......')

    ipcMain.on('asynchronous-message', (event, request) => {
        var handler = getHandlerByKey(request.requestType).handleRequest;
        studioContext.ignoreFileList = getHandlerByKey(request.requestType).ignoreFileList;

        if (handler && typeof handler === "function") {
            logger.info('Receive request id/type : ' + request.requestId + '/' + request.requestType);

            var args = [studioContext, request.requestId, request.requestMsg];
            handler.apply(this, args)
                .then(function (wrapper) {
                    logger.debug("...Insdie of the request handler")
                    var wrapper = {
                        requestId: wrapper.requestId,
                        result: wrapper.result
                    }
                    event.sender.send('asynchronous-reply', wrapper);
                })
                .catch(function (result) {
                    logger.info("WE have an error coming back from handler ", result.requestId);
                    logger.error(result.error);

                    var wrapper = {
                        requestId: result.requestId,
                        error: result.error
                    }

                    event.sender.send('asynchronous-reply', wrapper);
                })
                .finally(function () {
                    logger.debug("WE ARE IN THE FINALLY PORTION OF ", request.requestId);
                    studioContext.ignoreFileList = false;
                })
        }
        else {
            logger.warn('Messaging Handler not found for : ' + request.request.Type);
        }
    })

    ipcMain.on('clientInitComplete', () => {
        logger.info("-----------client init complete------")
        t32XrayStudioClientConnect.setConnection(studioContext);

        //currently, im adding this logic here, but need to refactor this out. Right now our second window that comes form offlineEhr side needs to have gray background on client side before we show it.
        if (t32XrayStudioWindowActions.open.xrayTemplate) {
            t32XrayStudioWindowActions.open.xrayTemplate('xrayTemplate');
        }
    })
}

function getHandlerByKey(key) {
    var handler = key ? studioContext.handlerMap[key.toString()] : null;
    if (!handler) {
        handler = require('../messageHandlers/' + key);
        studioContext.handlerMap[key.toString()] = handler;
    }
    return handler;
}