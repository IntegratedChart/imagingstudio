const { readJson } = require('./t32XrayStudioFs');
const { writeJson } = require('./t32XrayStudioFs');
const { machineIdSync } = require("node-machine-id");
const { appInfo } = require('../services/t32XrayStudioAppInfoHandler');

const appVersion = require('../../package.json').version,
path = require('path'),
logger = require('electron-log'),
fs = require('fs-extra'),
xrayStudioFs = require('../services/t32XrayStudioFs'),
homeDir = appInfo.getAppHome(), 
tab32Root = path.join(homeDir, 'tab32'),
settingFilePath = path.join(tab32Root, 'tab32XrayStudioSetting.json'),
tab32UploadRoot = path.join(homeDir, "tab32Upload");

// module.exports.initialize = initialize;
module.exports.getSettings = getSettings;
module.exports.saveSettings = saveSettings;
module.exports.saveSettingsImmediate = saveSettingsImmediate;
module.exports.storeSettingsInJsonDelayed = storeSettingsInJsonDelayed;
module.exports.storeSettingsInJsonImmediate = storeSettingsInJsonImmediate;

/**
 * this is a super global, so it will be avaliable through out the app.
 */
XRAYSTUDIOSETTINGS_GLOBAL = {
    setting: null
};

var storeSettingsTimer = null;

function getSettings(){

    return new Promise((resolve, reject) => {
        if(XRAYSTUDIOSETTINGS_GLOBAL.setting){
            resolve(XRAYSTUDIOSETTINGS_GLOBAL.setting);
        }
        else{
            try{
                // ensure directory exists otherwise create it
                // fs.ensureDirSync(tab32Root);
                xrayStudioFs.ensureDirSync(tab32Root);
                
                //ensure upload directory exits otherwise create it
                // fs.ensureDirSync(tab32UploadRoot);
                xrayStudioFs.ensureDirSync(tab32UploadRoot);
            }
            catch(err){
                logger.info("Failed to ensure dir for tab32 root and tab32Upload");
                logger.error(err);
                logger.info("Contuining get studio settings")
            }
    
            logger.info(settingFilePath);
    
            readJson(settingFilePath)
            .then((setting) => {
                XRAYSTUDIOSETTINGS_GLOBAL.setting = setting;
    
                //ensure that the app version shows up when settings are pulled.
                XRAYSTUDIOSETTINGS_GLOBAL.setting.appVersion = appVersion;
    
                resolve(XRAYSTUDIOSETTINGS_GLOBAL.setting);
            })
            .catch((err) => {
                logger.info("Loading json xray studio setting failed");
                logger.error(err);
    
                logger.info("Setting up a default settings file to send over")
    
                logger.info("The app version ", appVersion);
    
                XRAYSTUDIOSETTINGS_GLOBAL.setting = {
                    ehrServer: 'https://treatment.tab32.com',
                    appHomePath: homeDir,
                    tab32Root: tab32Root,
                    tab32UploadRoot: tab32UploadRoot,
                    appVersion: appVersion,
                    simulation: true,
                    logLevel: 'warn',
                    machineId: getMachineId(),
                    twainDriverPath: 'C:/Program Files (x86)/Tab32 Driver/Tab32ImageCapture.exe',
                    reInitializeDriver: true,
                    disableSourceAfterAcquire: false,
                    showUi: true,
                    viewMode: 'xray',
                    localTcpPort: 23228  // reverse of 'tab32' on keypad
                };
    
                storeSettingsInJsonImmediate()
                resolve(XRAYSTUDIOSETTINGS_GLOBAL.setting)
            })
        } 
        
    }) 
}

function saveSettings(setting){

    for(let key in setting){
        XRAYSTUDIOSETTINGS_GLOBAL.setting[key] = setting[key];
    }
    // XRAYSTUDIOSETTINGS_GLOBAL.setting = setting;

    storeSettingsInJsonDelayed();
    return XRAYSTUDIOSETTINGS_GLOBAL.setting;
}

function saveSettingsImmediate(setting){
    
    for (let key in setting) {
        XRAYSTUDIOSETTINGS_GLOBAL.setting[key] = setting[key];
    }

    storeSettingsInJsonImmediate();
    return XRAYSTUDIOSETTINGS_GLOBAL.setting;
}

function storeSettingsInJsonDelayed(){
    if(storeSettingsTimer !== null){
        clearTimeout(storeSettingsTimer);
        storeSettingsTimer = null;
    }

    storeSettingsTimer = setTimeout(function(){
        writeJson(settingFilePath, XRAYSTUDIOSETTINGS_GLOBAL.setting)
        .then(() => {
            logger.info('xray studio setting stored as json');
        })
        .catch((err) => {
            logger.info("error trying to storing studio setting: delayed");
            logger.error(err);
        })
    }, 300000)
}

function storeSettingsInJsonImmediate(){
 
    return new Promise((resolve, reject) => { 
        if (storeSettingsTimer !== null) {
            clearTimeout(storeSettingsTimer);
            storeSettingsTimer = null;
        }
    
        writeJson(settingFilePath, XRAYSTUDIOSETTINGS_GLOBAL.setting)
        .then(() => {
            logger.info('xray studio setting stored as json');
            resolve()
        })
        .catch(err => {
            logger.info("error trying to storing studio setting: immediate");
            logger.error(err);
            reject(err)
        })
    })

}

function getMachineId() {
    logger.info('... figuring out machine unique id ...');

    let id = machineIdSync({ original: true });
    logger.info('...... machine unique id : ' + id);
    return id;
}