// const q = require("q");
const logger = require('electron-log');
const t32XrayStudioMain = require('../t32XrayStudioMain');
const t32XrayStudioRedisHandler = require('./t32XrayStudioRedisHandler');
const t32XrayStudioErrorUtil = require('../utils/t32XrayStudioErrorUtil');
const t32XrayStudioChannelManager = require('./channelServices/t32XrayStudioChannelManager');

module.exports.handleIncomingMessage = handleIncomingMessage;
module.exports.sendOutgoingMessage = sendOutgoingMessage; 

function handleIncomingMessage(channel, msg){
    // redis will wake up even when we send a msg on the channel we subscribe to, 
    // therefore we need to only handle messages that aren't sent by xrayStudio 
    logger.info("we have a message to handle ", msg);
    if(channel){
        t32XrayStudioChannelManager.handleIncomingChannelMsg(channel, msg);
    }
}

async function sendOutgoingMessage(channel, msg){
    try{
        if (!t32XrayStudioRedisHandler.isReady()) {
            // logger.info("Redis is not connected yet...");
            throw "connectionError"
        }

        t32XrayStudioRedisHandler.publishMsg(channel, msg, function(result){
            if(result == 'error'){
                deferred.reject("publishingError");
            }
            else{
                return "success"
            }
        });

    }
    catch(err){
        logger.error("error sending message via redis ", err)
        return 'fail'
    }
}

