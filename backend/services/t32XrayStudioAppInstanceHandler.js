const EventEmitter = require('events');
const instanceEmitter = new EventEmitter();
const logger = require('electron-log');
const exec = require('child_process').exec;
const t32XrayStudioBrowserWindowHandler = require('./t32XrayStudioBrowserWindowHandler');
const t32XrayStudioDispatchMsg = require('./t32XrayStudioDispatchMsg');
const os = require('os');
const username = os.userInfo().username;
const t32XrayStudioChannelManager = require('./channelServices/t32XrayStudioChannelManager');
const t32XrauStudioClientConnect = require("./t32XrayStudioClientConnect");

module.exports.initialize = initialize;
// module.exports.handleWakeUp = handleWakeUp;

const appInstanceHandler = {};
const channelName = 't32XrayStudioAppInstance';
// var testUrl = 'xraystudio://getPatient?patientId=n5VG3S9C6&clinicId=rJCdP0gL&token=5d4a4729086606173cf04899'

function initialize(args){
    logger.info('init for app instance handler ', args);
    logger.info('args length ', args.length)
    if(args.length > 1){
        args.splice(0, 1);
    }

    // appInstanceHandler.getInstanceInfo = getRunningInstances;
    appInstanceHandler.appArgs = args[0];
    appInstanceHandler.secondInstance = false;
    appInstanceHandler.isFirstInstanceRunByCurrentUser = false;
    
    return registerAppInstanceHandlerEvents(args);
}

function registerAppInstanceHandlerEvents(){

    instanceEmitter.on('appPortAlreadyInUse', () => {
        if(process.platform == 'win32'){
            appInstanceHandler.secondInstance = true;
        }
        else{
            sendClientErrorMsgAndClose();
        }
        //send message to first instance, with or without the url args.
    })

    instanceEmitter.on('appReadyForMessaging', (context) => {
        logger.info("app is ready for instance message if any, sending instance message....")
        sendInstanceMessage(context);
    })

    appInstanceHandler.instanceEmitter = instanceEmitter;

    return appInstanceHandler
}

async function getRunningInstances(){
    logger.info('checking for another instance');
    try{        
        let command = 'tasklist /v /fi "imagename eq tab32 Imaging Studio.exe" /fo csv';
        
        return await runExecCommandAsync(command);
    }
    catch(err){
        logger.error(err);
        return [];
    }

}

async function sendInstanceMessage(context) {

    try {
        var msg = {
            msgType: 'wakeUp',
            // args: testUrl
            args: appInstanceHandler.appArgs
        }

        if(isCustomUriReq(msg.args)){
            msg.requestType = 'customUri';
        }

        logger.info('inside of send instance message', msg)

        if (appInstanceHandler.secondInstance) {

            var results = await getRunningInstances();

            logger.info('after getting running instance ', results);

            var currentUser = context.theApp.getOsHostName() + "\\" + context.theApp.getOsUserName();
            if (!allInstancesFromCurrentUser(results, currentUser)) {
                logger.info("The runing instance of the app is being used by another user");
                sendClientErrorMsgAndClose();
            }
            else {
                logger.info('sending message to first instance');

                t32XrayStudioChannelManager.handleOutgoingChannelMsg('t32XrayStudioAppInstance', msg).then(() => {
                    t32XrayStudioBrowserWindowHandler.exitApp();
                })
            }
        } else if (appInstanceHandler.appArgs) {
            //this is a new instance of app(no prior running), we cannot send redis message so we call action request through channelManager
            logger.info('handling wakeup inside current instance');
            try{
                await waitForClientAndSendWakeUpMsg(context, msg);
                // t32XrayStudioChannelManager.run(channelName, 'handleWakeUpMsg', msg);
            }catch (err){
                let msg = {
                    msgType: "commandFailed",
                    error: err,
                    channel: channelName
                }
                t32XrayStudioChannelManager.handleChannelErrorMsg(channelName, msg)
            }
        }
    }
    catch (err) {
        logger.error("There was error handling instance info ", err)
        sendClientErrorMsgAndClose();
    }
}


async function waitForClientAndSendWakeUpMsg(studioContext, msg){
    try {
        await t32XrauStudioClientConnect.waitForClient();
        t32XrayStudioChannelManager.run(channelName, 'handleWakeUpMsg', msg);
    } catch (e) {
        throw e;
    }
}

function allInstancesFromCurrentUser(instanceInfo, currentUser){
    logger.info('running all instance check for current user: ' , currentUser);
    instanceInfo.forEach((instance) => {
        logger.info('the user to compare to ', instance['User Name'])
        if(instance['User Name'] !== currentUser){
            return false;
        }
    })

    return true;
}


function runExecCommandAsync(command){
    return new Promise((resolve, reject) => {
        exec(command, (error, stdout, stderr) => {
            if (error) {
                logger.error('error from windows cmd for another instance running check', error)
                reject(error);
            }
            else {
                logger.info('parsing app instance info ', stdout);
                var results = [];
                let notRunning = stdout.match(/No tasks are running/g);

                if (notRunning) {
                    resolve(results);
                }
                
                let lineSplit = stdout.trim().split("\r\n");
                // logger.info("The line split ", lineSplit);

                var titleRows = lineSplit.splice(0, 1);

                // logger.info('the title row', titleRows);
                titleRows = titleRows[0].split('\"');
                // logger.info('the title row', titleRows);

                lineSplit.forEach((line, idx) => {
                    let parsedObj = {};
                    let lineVals = line.split('\"');
                    // logger.info("The line vals ", lineVals);

                    titleRows.forEach((title, index) => {
                        // logger.info("title", title);
                        // logger.info(title[0])
                        if (title !== '' && title !== ",") {
                            parsedObj[title] = lineVals[index];
                        }
                    });

                    results.push(parsedObj);
                })
                resolve(results);
            }
        })
    })
}

var defaultErrMsg = 'It seems that the app is already being used by different user, please make sure that all other running instances of the app are closed and try again.';

function sendClientErrorMsgAndClose(waitTime = 2000, error = defaultErrMsg){
    setTimeout(function () {
        let msg = {
            msgType: "appAlreadyRunning",
            error: error,
            channel: channelName
        }

        t32XrayStudioChannelManager.handleChannelErrorMsg(channelName, msg)
    }, waitTime)
}

function isCustomUriReq(req){
    logger.info('custom uri check ', req);
    logger.info('the args are type of ', typeof req);

    if(req && typeof req == 'string' && req.match('xraystudio://')){
        logger.info("there is a match");
        return true;
    }
    
    return false;
}