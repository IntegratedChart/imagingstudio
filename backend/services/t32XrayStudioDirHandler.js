// const q = require("q");
// const _ = require('lodash');
const path = require("path");
// const getDirectoryFileList = require('../messageHandlers/getDirectoryFileList');
const logger = require('electron-log');
// const async = require("async");
const t32XrayStudioFs = require('../services/t32XrayStudioFs');   
const t32XrayStudioManipulation = require("../services/t32XrayStudioImageManipulation");
const moment = require('moment');


module.exports.checkForNewFiles = checkForNewFiles
module.exports.getDirectoryFileList = getDirectoryFileList
module.exports.sortFileByDateModified = sortFileByDateModified

var inDirHandling;

async function checkForNewFiles(studioContext, ignoreFileList) {
    try {
        // logger.debug("WE are trying to see if files have changed");
        // logger.debug("THE IGNORE FILE LIST IS");
        // logger.debug(ignoreFileList);
        logger.info("inside dir watcher file check ", studioContext.setting.viewMode)
    
        if (ignoreFileList) {
            return false;
        } else if(inDirHandling){
            logger.info('Dir checker still in process');
            return false;
        } else if (studioContext && studioContext.setting.viewMode) {
            if (studioContext.setting.viewMode === 'xray') {
                let currentSelectedFolder = studioContext.setting.currentSelectedFolder;
                let dirPath = path.join(studioContext.setting.tab32Root, currentSelectedFolder);
                
                if (currentSelectedFolder !== undefined && currentSelectedFolder !== null) {
                    inDirHandling = true;
                    let fileList = await getDirectoryFileList(dirPath);

                    let hasNewFile = await handleNewFiles(studioContext, fileList, 'xray');
                    inDirHandling = false;
                    return hasNewFile;
                }
                else {
                    inDirHandling = false;
                    return false;
                }
            }
            // else if (studioContext.setting.viewMode === 'file') {
            //     // todo:
            //     inDirHandling = false;
            //     return false;
            // }
            // else{
            //     inDirHandling = false;
            //     return false;
            // }
        } else {
            inDirHandling = false;
            return false;
        }

    } catch (e) {
        logger.info("error xrayStudioDirHandler ", e)
        return false;
    }
}

/**
 * Checks if we have any new files in the patient directory and then rotates and renames them accordingly. 
 * @param {*} studioContext - Object containing the settings json files
 * @param {*} fileList - the list of files in the patient directory
 * @param {*} viewMode - the view mode that we are currently in i.e xray or image upload.
 */
async function handleNewFiles(studioContext, fileList, viewMode) {
    try {
        let hasNewFile = false;
    
        if(fileList.length <= 0){
            return false
        }
    
        let dirPath = path.join(studioContext.setting.tab32Root, studioContext.setting.currentSelectedFolder);
        let currentRotationSettings = studioContext.setting.currentRotationSettingObj;
        // logger.debug("THE CURRENT ROTATION SETTINGS");
        // logger.debug(currentRotationSettings);
        let activeImageCell = studioContext.setting.activeImageCell;
    
        //currently there is bug in jimp where the image will rotate opposite way so we are multiplying by -1 to get it to perform correctly.
        let rotationDeg = currentRotationSettings && activeImageCell ? parseInt(currentRotationSettings[activeImageCell]) * -1 : 0;
    
    
        //TODO: refactor to incluse file viewMode
        if (viewMode === 'xray') {

            for (let i = 0; i < fileList.length; i++) {
                let file = fileList[i];
                let reg = /tab32Xray/g
                let found = file.match(reg);
                
                if (found == null) {
                    let regEx = /\.\w+/g;
                    let extention = file.match(regEx)[0];
                    let randomNum = Math.random().toString().replace("0.", "");
                    let replaceStr = "tab32Xray_" + randomNum + "_" + activeImageCell + extention;
                    logger.debug("THE REPLACE STRING");
                    logger.debug(replaceStr);
    
                    let newFileName = file.replace(file, replaceStr);
                    let source = dirPath + '/' + file;
                    let target = dirPath + '/' + newFileName;
                    let originals = dirPath + '/originals/' + newFileName;
    
                    if (rotationDeg !== 0) {
                        await t32XrayStudioManipulation.rotateImage(source, rotationDeg);
                    }
                    await copyAndRename(source, target, originals);
                    hasNewFile = true;
                }
            }

        }
        return hasNewFile;
    } catch (e) {
        throw e
    }
}


async function copyAndRename(source, target, original = undefined) {
    // var deferred = q.defer();
    try {
        logger.info('COPYING IMAGE AND RENAMING....');
        logger.info(source);
        logger.info(target);
        logger.info(original);
    
        if (original) {
            await t32XrayStudioFs.writeFile(original, t32XrayStudioFs.readFileSync(source));
        }

        return await t32XrayStudioFs.rename(source, target);

    } catch (e) {
        throw e;
    }

}


async function getDirectoryFileList(dirPath, fileExtentions = ['.jpg', '.jpeg', '.png', '.tiff', '.gif']) {
    try {
        let fileList = [];
        if (t32XrayStudioFs.existsSync(dirPath)) {
            fileList = t32XrayStudioFs.readdirSync(dirPath).filter(function (file) {
                logger.debug("Here are the files");
                logger.debug(file)
                var extName = path.extname(file) || '';
    
                return t32XrayStudioFs.lstatSync(path.join(dirPath, file)).isFile() && fileExtentions.indexOf(extName.toLowerCase()) !== -1;
            });
    
            return sortFileByDateModified(fileList, dirPath);
        } else {
            throw 'directory not found'
        }
    } catch (e) {
        throw e;
    }
}


function sortFileByDateModified(fileList, dirPath) {
    try {
        // logger.debug("The Machine info: ");
        // logger.debug(os.platform());
        let sortedFileList = fileList.sort((fileA, fileB) => {
            let directoryPathA = path.join(dirPath, fileA);
            let statsA = t32XrayStudioFs.lstatSync(directoryPathA);
            let directoryPathB = path.join(dirPath, fileB);
            let statsB = t32XrayStudioFs.lstatSync(directoryPathB);

            //   // logger.debug("the ctime of the file");
            //   // logger.debug(file);
            //   // logger.debug(stats.ctime);
            //   // logger.debug("the stats of the file");
            //   // logger.debug(stats);

            //   //in the stats output the ctime is the mtimeMs. The birthtime is the time created. 
            //   return new moment(stats.ctime);
            let dateA = new Date(moment(statsA.ctime))
            let dateB = new Date(moment(statsB.ctime))

            if (dateA > dateB) {
                return -1;
            }
            if (dateA < dateB) {
                return 1;
            }
            return 0;
        })

        logger.debug("This is the sorted filelist ");
        logger.debug(sortedFileList);

        return sortedFileList;
    }
    catch (err) {
        logger.debug("error sorting directory file list")
        throw err;
    }

}

// function checkForNewFiles(studioContext, ignoreFileList) {
//     var deferred = q.defer();

//     var fileList;
//     // logger.debug("WE are trying to see if files have changed");
//     // logger.debug("THE IGNORE FILE LIST IS");
//     // logger.debug(ignoreFileList);
//     logger.info("inside dir watcher file check ", studioContext.setting.viewMode)

//     if (ignoreFileList) {
//         deferred.resolve(false);
//     }
//     else if (inDirHandling) {
//         logger.info('Dir checker still in process');
//         deferred.resolve(false);
//     }
//     else if (studioContext && studioContext.setting.viewMode) {
//         if (studioContext.setting.viewMode === 'xray') {
//             var currentSelectedFolder = studioContext.setting.currentSelectedFolder;

//             if (currentSelectedFolder !== undefined && currentSelectedFolder !== null) {
//                 inDirHandling = true;
//                 getDirectoryFileList.handleRequest(studioContext, 'none', currentSelectedFolder)
//                     .then(function (obj) {
//                         logger.debug("files detected");
//                         // logger.debug(obj);
//                         fileList = obj.result;
//                         return handleNewFiles(studioContext, fileList, 'xray');
//                     })
//                     .then(function (hasNewFile) {
//                         inDirHandling = false;
//                         deferred.resolve(hasNewFile);
//                     })
//                     .catch(function (err) {
//                         console.log("The err is catched inside of hangle dir change")
//                         inDirHandling = false;
//                         logger.error(err);
//                         deferred.resolve(false);
//                     })
//             }
//             else {
//                 inDirHandling = false;
//                 deferred.resolve(false);
//             }
//         }
//         // else if (studioContext.setting.viewMode === 'file') {
//         //     // todo:
//         //     inDirHandling = false;
//         //     deferred.resolve(false);
//         // }
//         // else{
//         //     inDirHandling = false;
//         //     deferred.resolve(false);
//         // }
//     }
//     else {
//         inDirHandling = false;
//         deferred.resolve(false);
//     }

//     return deferred.promise;
// }

// function handleNewFiles(studioContext, fileList, viewMode) {
//     var deferred = q.defer();
//     var hasNewFile = false;

//     if (fileList.length <= 0) {
//         deferred.resolve(hasNewFile);
//         return
//     }

//     var imageFileExtList = ['.jpg', '.jpeg', '.png', '.tiff', '.gif'];
//     var dirPath = path.join(studioContext.setting.tab32Root, studioContext.setting.currentSelectedFolder);
//     var currentRotationSettings = studioContext.setting.currentRotationSettingObj;
//     // logger.debug("THE CURRENT ROTATION SETTINGS");
//     // logger.debug(currentRotationSettings);
//     var activeImageCell = studioContext.setting.activeImageCell;

//     //currently there is bug in jimp where the image will rotate opposite way so we are multiplying by -1 to get it to perform correctly.
//     var rotationDeg = currentRotationSettings && activeImageCell ? parseInt(currentRotationSettings[activeImageCell]) * -1 : 0;


//     //TODO: refactor to incluse file viewMode
//     if (viewMode === 'xray') {
//         async.eachSeries(fileList, function (file, callback) {
//             var reg = /tab32Xray/g
//             var found = file.match(reg);
//             // logger.debug("WE HAVE FOUND A MATCH ");
//             // logger.debug(found);
//             if (found === null) {
//                 // logger.debug("Here is the str that we need to replace");
//                 // logger.debug(str);
//                 // logger.debug(file);
//                 var regEx = /\.\w+/g
//                 var extention = file.match(regEx)[0];
//                 var randomNum = Math.random().toString().replace("0.", "");
//                 var replaceStr = "tab32Xray_" + randomNum + "_" + activeImageCell + extention;

//                 logger.debug("THE REPLACE STRING");
//                 logger.debug(replaceStr);

//                 var newFileName = file.replace(file, replaceStr);
//                 var source = dirPath + '/' + file;
//                 var target = dirPath + '/' + newFileName;
//                 var originals = dirPath + '/originals/' + newFileName;
//                 // logger.debug("THE FILE CHANGED");
//                 // logger.debug(newFileName);
//                 // logger.debug(source);
//                 // logger.debug(target);
//                 // logger.debug("Here is the originals folder and name we want");
//                 // logger.debug(originals);


//                 // logger.info("Here is the rotation deg " + rotationDeg);
//                 //rotate image first and then save to originals path and source path and then rename the the source path.
//                 if (rotationDeg !== 0) {
//                     rotateImage(source, rotationDeg)
//                         .then(() => {
//                             copyAndRename(source, target, originals)
//                                 .then(() => {
//                                     hasNewFile = true;
//                                     callback();
//                                 })
//                         })
//                         .catch((err) => {
//                             copyAndRename(source, target, originals)
//                                 .then(() => {
//                                     hasNewFile = true;
//                                     callback();
//                                 })
//                                 .catch(() => {
//                                     callback();
//                                 })
//                         })
//                 }
//                 else {
//                     copyAndRename(source, target, originals)
//                         .then(() => {
//                             hasNewFile = true;
//                             callback();
//                         })
//                         .catch(() => {
//                             callback();
//                         })
//                 }
//             } else {
//                 callback();
//             }
//         }, function (err) {
//             if (err) {
//                 deferred.reject(err);
//             } else {
//                 deferred.resolve(hasNewFile);
//             }
//         });
//     }
//     else {
//         deferred.resolve(hasNewFile);
//     }
//     return deferred.promise;
// }


// function rotateImage(source, rotationDeg) {
//     var deferred = q.defer();
//     logger.info("ROTATING IMAGE....")
//     Jimp.read(source)
//     .then((img) => {
//         img.rotate(rotationDeg).getBuffer(Jimp.AUTO, (err, buf) => {
//             if (err) {
//                 logger.error(err);
//                 deferred.reject();
//             }
//             else {
//                 t32XrayStudioFs.writeFileSync(source, buf);
//                 deferred.resolve();
//             }
//         })
//     })
//     .catch((err) => {
//         logger.error(err);
//         logger.error(err.stack);
//         deferred.reject();
//     })

//     return deferred.promise;
// }
