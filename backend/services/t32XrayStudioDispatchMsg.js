const t32XrayStudioBrowserWindowHandler = require('../services/t32XrayStudioBrowserWindowHandler')

module.exports.sendMsgToClient = sendMsgToClient;
module.exports.sendFailedDriverCommsMsgToClient = sendFailedDriverCommsMsgToClient;

function sendMsgToClient(channelName, msg){
    var msgToSendClient = {
        msgType: 'incomingCommsChannelMsg',
        channel: channelName,
        msg: msg,
    }

    t32XrayStudioBrowserWindowHandler.notifyClient(msgToSendClient)
}

function sendFailedDriverCommsMsgToClient(msg){
    var msgToSendClient = {
        msgType: 'failedDriverCommunication',
        msg: msg,
    }

    t32XrayStudioBrowserWindowHandler.notifyClient(msgToSendClient)
}
