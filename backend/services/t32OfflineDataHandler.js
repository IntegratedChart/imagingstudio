'use strict';
const db = require('../db');
const logger = require('electron-log');
const idGenerator = require('../utils/idGenerator');
const moment = require("moment");
const offlineRecordPrefix = 'offline-';
const uploadPatient = require("../classes/uploadPatient");
const t32XrayStudioBrowserWindowHandler = require("../services/t32XrayStudioBrowserWindowHandler");
const utils = require("../utils/utils")
const t32RestServices = require("../t32RestServices");
const t32RestEndPoints = require("../t32RestEndPoints");
const t32XrayStudioUplaodQueue =  require("../services/t32XrayStudioUploadQueue");
const fs = require("fs-extra")
const path = require("path");
//TODO:ABSTRACT OUT EACH MODEL OPERTAIONS INTO THIER OWN FILES.

// const tableName = 'offline_data';

// /**
//  * @async
//  * @description Creates 'offline_data' table if not exists 
//  *
// const createTable = async () => {
//     try {
//         return db.createTable(
//             tableName,
//             [
//                 "id VARCHAR(50) PRIMARY KEY",
//                 "data JSONB",
//                 "created_date TIMESTAMP DEFAULT NOW()"
//             ]
//         );
//     } catch (error) {
//         throw error;
//     }
// }

// /**
//  * @async
//  * @description upserts downloaded data into offline_data table
//  * @param { object[] } data 
//  */
// const saveData = async (data = []) => {
//     try {
//         const table = await createTable();
//         logger.info('..table', table);

//         if (table) {
//             const insertCols = '(id, data)';
//             const insertValues = [];

//             for (let patient of data) {
//                 insertValues.push(`('${patient._id.toString()}', '${JSON.stringify(patient)}')`);
//             }

//             if (insertValues.length) {
//                 let query = `INSERT INTO ${tableName} ${insertCols} VALUES ${insertValues.join(',')}`;
//                 query += ` ON CONFLICT (id) DO UPDATE
//                     SET data = excluded.data,
//                         created_date = CURRENT_TIMESTAMP
//                 `;

//                 const insertResult = await db.actions.query(query);
//             } else {
//                 throw 'Empty insert values';
//             }
//         }
//     } catch (error) {
//         logger.error('t32OfflineDataHandler', error);
//     }
// }

// /**
//  * @async
//  * @param { string[] } columns column names
//  * @param { string []} whereConditions where conditions
//  */
// const queryData = async (columns = [], whereConditions = []) => {
//     try {
//         const queryCols = Array.isArray(columns) && columns.length ? 
//             columns.join(',') 
//             : '*';

//         const where = Array.isArray(whereConditions) && whereConditions.length ?
//             `WHERE ${whereConditions.join(' ')}`
//             : '';

//         //build query string
//         const query = `SELECT ${queryCols} FROM ${tableName} ${where}`;
//         const data = await db.actions.query(query);
//         return data.rows;
//     } catch (error) {
        
//     }
// }


// /**
//  * Saves data to db 
//  * @param { Array } data 
//  * @param { String } tableName 
//  * @param { String } idIdentifier 
//  */
// const save = async (data = [], tableName, idIdentifier) => {
//     try {
//         let table = await createTable(tableName);

//         if (table) {
//             const insertCols = '(id, data)';
//             const insertValues = [];

//             for (let item of data) {
//                 insertValues.push(`('${item[idIdentifier].toString()}', '${JSON.stringify(item)}')`);
//             }

//             if (insertValues.length) {
//                 let query = `INSERT INTO ${tableName} ${insertCols} VALUES ${insertValues.join(',')}`;
//                 query += ` ON CONFLICT (id) DO UPDATE
//                     SET data = excluded.data,
//                         created_date = CURRENT_TIMESTAMP
//                 `;

//                 const insertResult = await db.actions.query(query);
//             } else {
//                 throw 'Empty insert values';
//             }
//         }
//     } catch (e) {
//         logger.error('t32OfflineDataHandler:save', e);
//     }
// }
/**
 * Saves data to db 
 * @param { Array } data 
 * @param { String } id 
 */
const saveCdtCodes = async (data) => {
    try {
        return await db.save('CdtCode', data, true);
    } catch (e) {
        logger.error('t32OfflineDataHandler:saveCdtCodes', e);
    }
}

const getCdtCodes = async (clinicId) => {
    try {
        let query = `clinic_id = "${clinicId}"`;
        return await db.get('CdtCode', query);
    } catch (e) {
        logger.error('t32OfflineDataHandler:getCdtCodes', e);
    }
}

const saveClinic = async (data, update=false) => {
    try{
        if (data && data.subscription && data.subscription.hasOwnProperty('offlineStudio')) {
            logger.info("clinic has offline access ", data.subscription.offlineStudio);
            data.offlineStudio = data.subscription.offlineStudio;
        }
        
        logger.info('saving clinic ', data._id);
        await db.save('Clinic', data, true);
        let clinic = await getClinic(data._id);

        logger.info("about to update clinic ", clinic);
        if (update) {
            let updateMsg = {
                msgType: 'clinicUpdate',
                msg: {
                    clinic: clinic ? clinic : null 
                }
            }

            t32XrayStudioBrowserWindowHandler.notifyClient(updateMsg, 'BackendPushNotification');
        }

    } catch (e) {
        logger.error('t32OfflineDataHandler:saveCinic', e);
    }
}

const getClinic = async (id) => {
    try {
        let query = `_id = "${id}"`;
        let clinics = await db.get('Clinic', query);

        // logger.info("here is the clinic information returned from query ", clinics);

        let clinic = null;

        if (clinics && clinics.length) {
            clinic = JSON.parse(JSON.stringify(clinics[0]));
            // logger.info("the clinic locations ", clinic.locations);
            clinic.locations = Object.values(clinic.locations);

            for (let location of clinic.locations) {
                // logger.info("the location ", location);
                location.clinicProviders = Object.values(location.clinicProviders);
            }
        } else {
            clinic = null;
        }

        // logger.info("the clinic ", clinic)
        return clinic
    } catch (e) {
        logger.error('t32OfflineDataHandler:getClinic', e);
    }
}
// const saveCdtCodes = async (data = [], tableName, id) => {
//     try {
//         let table = await createTable(tableName);

//         if (table) {
//             const insertCols = '(id, data)';
//             const insertValues = [];

//             insertValues.push(`('${id.toString()}', '${JSON.stringify(data)}')`);

//             if (insertValues.length) {
//                 let query = `INSERT INTO ${tableName} ${insertCols} VALUES ${insertValues.join(',')}`;
//                 query += ` ON CONFLICT (id) DO UPDATE
//                     SET data = excluded.data,
//                         created_date = CURRENT_TIMESTAMP
//                 `;

//                 const insertResult = await db.actions.query(query);
//             } else {
//                 throw 'Empty insert values';
//             }
//         }
//     } catch (e) {
//         logger.error('t32OfflineDataHandler:saveCdtCodes', e);
//     }
// }
/**
 * Saves data to db 
 * @param { Array } data 
 * @param { String } tableName 
 * @param { String } id 
 */
const saveNoteTemplates = async (data) => {
    try {
        return await db.save('NoteTemplate', data, true);
    } catch (e) {
        logger.error('t32OfflineDataHandler:saveNoteTemplates', e);
    }
}

const getNoteTemplates = async (clinicId) => {
    try {
        let query = `clinic_id = "${clinicId}"`;
        return await db.get('NoteTemplate', query);
    } catch (e) {
        logger.error('t32OfflineDataHandler:getNoteTemplates', e);
    }
}

const getClinicData = async (clinicId) => {
    try {
        let clinic = await getClinic(clinicId);
        let cdtCodes = await getCdtCodes(clinicId);
        let noteTemplates = await getNoteTemplates(clinicId);
        let treatmentQuickKeys = await getTreatmentQuickKeys(clinicId);

        let clinicData = {
            clinic: clinic || null,
            noteTemplates: noteTemplates || null,
            cdtCodes: cdtCodes || null,
            treatmentQuickKeys: treatmentQuickKeys || null
        }

        // logger.info("clinic DATA ", clinicData);
        return clinicData
    } catch (e) {
        logger.error("t32OfflineDataHandler:getClinicData", e);
    }
} 

// const saveNoteTemplates = async (data = [], tableName, id) => {
//     try {
//         let table = await createTable(tableName);

//         if (table) {
//             const insertCols = '(id, data)';
//             const insertValues = [];

//             insertValues.push(`('${id.toString()}', '${JSON.stringify(data)}')`);

//             if (insertValues.length) {
//                 let query = `INSERT INTO ${tableName} ${insertCols} VALUES ${insertValues.join(',')}`;
//                 query += ` ON CONFLICT (id) DO UPDATE
//                     SET data = excluded.data,
//                         created_date = CURRENT_TIMESTAMP
//                 `;

//                 const insertResult = await db.actions.query(query);
//             } else {
//                 throw 'Empty insert values';
//             }
//         }
//     } catch (e) {
//         logger.error('t32OfflineDataHandler:saveNoteTemplates', e);
//     }
// }

/**
 * @description For finding patients from database
 * @async
 * @param { string } filter 
 */
const queryOfflinePatients = async (filter) => {
    try {
        return await db.get('Patient', filter);
    } catch (error) {
        throw error;
    }
}

/**
 * @description For saving offline patients (doesn't modify downloaded data).
 * Only for saving patient details, for notes and treatments we have separate methods
 * @async
 * @param { object } newPatient
 * 
 * @returns { object } saved patient
 */
const saveOfflinePatient = async (newPatient) => {
    try {
        let update = false;

        if (newPatient._id && !newPatient._id.startsWith(offlineRecordPrefix)) { 
            throw 'Invalid patient type';
        }

        if (!newPatient._id) {
            newPatient._id = idGenerator.generate(offlineRecordPrefix);
        } else {
            const existingRecord = await queryOfflinePatients(`_id = "${newPatient._id}" AND is_offline = true`);
            logger.debug('existing record', existingRecord);
            
            if (!existingRecord.length) { 
                throw 'Invalid Patient';
            } else { // populate patient notes and treatments from existing record
                newPatient.patient_notes = existingRecord[0].patient_notes;
                newPatient.treatments = existingRecord[0].treatments;
            }
            
            update = true;
        }

        newPatient.gender = (newPatient.gender) ?
                parseInt(newPatient.gender):
                null;

        newPatient.is_offline = true;
        const result = await db.save('Patient', newPatient, update);
        
        //  get saved patient
        const patient = await queryOfflinePatients(`_id = '${newPatient._id}'`);
        return patient && patient.length ? patient[0] : null;
    } catch (error) {
        logger.log('save offline patient error:', error);
        throw error;
    }
}

/**
 * @async
 * @description For saving offline note for patient
 * @param { object } note 
 */
const saveOfflineNote = async (note) => {
    try {
        if (note._id && !note._id.startsWith(offlineRecordPrefix)) {
            throw 'Invalid note';
        }

        if (!note._id) {
            note._id = idGenerator.generate(offlineRecordPrefix);
        }

        note.is_offline = true;
        const patient = await queryOfflinePatients( `_id = "${note.patient_id}"`);

        if (!patient.length) {
            throw 'Save offline Note: Invalid patient';
        }

        return await db.updateList(patient[0], 'patient_notes', note, '_id');
    } catch (error) {
        throw error;
    }
}

/**
 * @async
 * @description For saving offline treatments for patient
 * @param { object[] } data 
 */
const saveOfflineTreatments = async (data) => {
    try {
        const { provider, location, patientId, treatments } = data;
        const now = Date().toString();

        if (!treatments.length) {
            throw '0 treatments';
        }

        for (let treatment of treatments) {
            if (treatment._id && !treatment._id.startsWith(offlineRecordPrefix)) {
                throw 'Invalid treatment _id detected';
            }

            if (!treatment.cdtCode) { 
                throw 'Invalid treatment cdt code detected';
            }
            
            // generate treatmenet id
            if (!treatment._id) {
                treatment._id = idGenerator.generate(offlineRecordPrefix);
            }
            
            if (!treatment.enteredDate) {
                treatment.enteredDate = now;
            }

            //set completed date
            if (treatment.completed) {
                treatment.completed_date = now;
            } else {
                treatment.completed_date = null;
            }
    
            treatment.providerId = provider;
            treatment.locationId = location;
            treatment.patient_id = patientId;
            treatment.is_offline = true;
        }

        logger.debug('treatments', treatments);

        const patient = await queryOfflinePatients( `_id = "${patientId}"`);

        if (!patient.length) {
            throw 'Save offline treatments: Invalid patient';
        }

        return await db.updateList(patient[0], 'treatments', treatments, '_id');
    } catch (error) {
        logger.debug('error', error);
        throw error;
    }
}

/**
 * @description For generating filter string. Useful when quering multiple records on same field but different value.
 * @example field = _id, valueArr = ['1', '2'] and operator = 'OR' will return 
 * "_id = '1' OR _id = '2'"
 * 
 * @param { string } field field name
 * @param { any[] } valueArr values to match agaisnt field
 * @param { string } operator valid realm operator
 * 
 * @returns { string } 
 */
const filterBuilder = (field, valueArr, operator) => {
    if (!Array.isArray(valueArr)) { return; }

    let filter = '';

    for (let [index, val] of valueArr.entries()) {
        if (!val) { continue; }

        if (index) {
            filter += ` ${operator} `
        }
        filter += `${field}=${val}`;
    }
    
    return filter;
}

/**
 * @async
 * @description deletes patient records
 * @param { string } type 
 * @param { any[] } idArr
 * 
 * @returns { Promise<any> }
 */
const deletePatientData = async (type, idArr) => {
    try {
        idArr = idArr.map(id => `"${id}"`);

        if (type === 'patient') {
            const filter = filterBuilder('_id', idArr, 'OR');
            await db.deleteRecord('Patient', filter);
    
            // generate filter to delete patient notes and treatments
            const delListFilter = filterBuilder('patient_id', idArr, 'OR');
            await db.deleteRecord('Note', delListFilter);
            await db.deleteRecord('Treatment', delListFilter);
        } else if (type == 'treatment') {
            const filter = filterBuilder('_id', idArr, 'OR');
            await db.deleteRecord('Treatment', filter);
        } else if (type == 'patient_note') {
            const filter = filterBuilder('_id', idArr, 'OR');
            await db.deleteRecord('Note', filter);
        }

        return;
    } catch (error) {
        throw error;
    }
}

const handleOfflinePatientRequest = async (requestMsg) => {
    try {
        const { requestType, data } = requestMsg;
        let result;

        if (requestType === 'savePatient') {
            result = await saveOfflinePatient(data);
        } else if (requestType === 'getPatient') {
            result = await queryOfflinePatients(`_id = "${ data }"`);
        } else if (requestType === 'saveNote') {
            result = await saveOfflineNote(data);
        } else if (requestType === 'saveTreatment') {
            result = await saveOfflineTreatments(data);
        } else if (requestType === 'deletePatientData') {
            result = await deletePatientData(data.type, data.values);
        } else {
            throw 'Wrong request type';
        }
        
        return result;
    } catch (error) {
        logger.error("handleOfflinePatientRequest error: ", error);
        throw error;
    }
}

const getTreatmentQuickKeys = async (clinicId) => {
    try {
        try {
            let query = `clinic_id = "${clinicId}"`;
            let keys = await db.get('TreatmentQuickKey', query);

            logger.info("the treatment quick keys ", keys);
            if (keys && keys.length) {
                let parsedKeys = [];
                for (let key of keys ) {
                    let keyCopy = JSON.parse(JSON.stringify(key));
                    keyCopy.treatments = Object.values(keyCopy.treatments);
                    parsedKeys.push(keyCopy);
                }

                return parsedKeys;
            } else {
                return [];
            }
        } catch (e) {
            logger.error('t32OfflineDataHandler:getTreatmentQuickKeys', e);
        }
    } catch (e) {
        logger.info("error getTreatmentQuickKeys")
        throw e;
    }
}

const deleteTreatmentDefiniton = async (id) => {
    try {
        let query = `quickKeyId = "${id}"`;
        await db.deleteRecord('TreatmentDefinition', query);
    } catch (e) {
        logger.error("errorDeleteingTreatmentDef ", e);
        throw e;
    }
}

const saveTreatmentQuickKeys = async (clinicId, keys) => {
    try {
        if (!keys || !keys.length) {
            return 
        }

        for (let key of keys) {
            if (!key._id) {
                key._id = idGenerator.generate();
            }

            key.clinic_id = clinicId;

            if (!key.treatments || !key.treatments.length) {
                throw "missing treatments on quick key";
            }

            for (let treatmentKey of key.treatments) {
                logger.info("going over treatments ", key);
                if (!treatmentKey._id) {
                    treatmentKey._id = idGenerator.generate();
                }
                
                treatmentKey.quickKeyId = key._id;
                await deleteTreatmentDefiniton(key._id);
            }
        }

        logger.info("the keys ", keys);

        return await db.save('TreatmentQuickKey', keys, true);
    } catch (e) {
        logger.info('t32OfflineDataHandler:getTreatmentQuickKeys', e);
        throw e;
    }
}

const deleteTreatmentQuickKeys = async (keys) => {
    try {
        if (!keys || !keys.length) {
            return 
        }

        for (let key of keys) {
            await deleteTreatmentDefiniton(key._id);

            logger.info("after delete treatment definiton ");

            let delQuery = `_id = "${key._id}"`;
            await db.deleteRecord('TreatmentQuickKey', delQuery);
        }

    } catch (e) {
        throw e;
    }
}


const handleOfflineTreatmentsRequest = async (requestMsg) => {
    try {
        const { requestType, data } = requestMsg;

        let result;

        if (requestType === 'getTreatmentQuickKeys') {
            return await getTreatmentQuickKeys(data.clinicId);
        } else if (requestType === 'saveTreatmentQuickKeys') {
            logger.info("got request to save treatment quick keys ", data);
            await saveTreatmentQuickKeys(data.clinicId, data.keys);
            return await getTreatmentQuickKeys(data.clinicId);
        } else if (requestType === 'deleteTreatmentQuickKeys') {
            logger.info("got request to delete treatment quick keys ", data);
            await deleteTreatmentQuickKeys(data.keys);
            return await getTreatmentQuickKeys(data.clinicId);
        } 
        else {
            throw 'Wrong request type';
        }
    } catch (e) {
        logger.error("error handleOfflineTreatmentsRequest ", e);
        throw e;
    }
}

//TODO: Once a in memory queue is started, we need to rework the isUploading logic and cross reference the queried patients with the in memory queue.
const getOfflineUploadData = async (data) => {
    try {
        let dataQuery = ``;
        // let dataQuery = ``;

        let today = moment().utc();
        today.hour(0);
        today.minute(0);
        today.second(0);
        today.millisecond(0);
        today = moment(today).format("YYYY-MM-DD@HH:mm:ss.mmm");

        // TODO: Test with real data once peices are in.
        if (data.type == 'includeAll') {
            dataQuery += `(isUploading = false) AND (is_offline = true OR expiryDate <= ${today} OR patient_notes.is_offline = true OR treatments.is_offline = true)`;
            // dataQuery += `expiryDate >= ${today}`;
        } else if (data.type == 'expiredOnly') {
            dataQuery += `isUploading = false AND expiryDate <= ${today}`;
        }
        logger.info ('the query ', dataQuery);
        let patients = JSON.parse(JSON.stringify(await queryOfflinePatients(dataQuery)));
        let dataToUpload = [];

        logger.info("the patients found ", patients);
        if (patients && patients.length) {
            for (let patient of patients) {
                let notes = Object.values(patient.patient_notes);
                let treatments = Object.values(patient.treatments);
                logger.info("notes ", notes);
                logger.info("treatments ", treatments);
                let hasOfflineData = false;
                let newNoteCount = 0;
                let newTreatmentCount = 0;

                if (patient.is_offline) {
                    hasOfflineData = true;
                }
                // else {
                    for (let note of notes) {
                        if (note.is_offline) {
                            newNoteCount += 1;
                        }
                    }

                    for (let treatment of treatments) {
                        if (treatment.is_offline) {
                            newTreatmentCount += 1
                        }
                    }
                // }

                if (hasOfflineData || newNoteCount > 0 || newTreatmentCount > 0) {
                    dataToUpload.push(new uploadPatient(patient._id, patient.name, patient.dob, hasOfflineData, newNoteCount, newTreatmentCount))
                }
            }
        }

        logger.info("data to upload ", dataToUpload)
        return dataToUpload
    } catch (e) {
        throw e;
    }
}

const saveUploadLog = async (log) => {
    try {
        return await db.save('UploadLog', log, true);
    } catch (e) {
        throw e;
    }
}

const getUploadLog = async (query) => {
    try {
 
        let logs = await db.get('UploadLog', query);

        let parsedLog = [];
        
        logger.info("the log in getUploadLog ", logs);
        for (let log of logs) {
            let logCopy = JSON.parse(JSON.stringify(log));
            logCopy.uploads = Object.values(logCopy.uploads);

            // for (let upload of logCopy.uploads) {
            //     upload.uploadData = JSON.parse(JSON.stringify(upload.uploadData[0]));
            //     logger.info('getuploadLog uploadData ', upload.uploadData);
            // }
            parsedLog.push(logCopy);
        }

        return parsedLog
    } catch (e) {
        logger.info('error getUploadLog')
        throw e;
    }
}

const updateUploadLog = async (patientUploads=null, currentLog=null) => {
    try {

        // if (!clinicId) {
        //     throw 'clinic Id is required to create upload log'
        // }
        let log = currentLog ? currentLog : {
            _id: idGenerator.generate(),
            // clinic_id: clinicId,
            logDate: moment(new Date()).format("LLL"),
            uploads: patientUploads
        }

        logger.info("updating uploading log ", log);

        if (!log || !log.uploads.length) {
            throw 'no patient uploads to create log for';
        }

        // let uploads = [];
        logger.info("looping over patientUploads");

        for (let patientUpload of patientUploads) {
            patientUpload._id = patientUpload._id || idGenerator.generate();
            patientUpload.uploadData._id = patientUpload.uploadData._id || idGenerator.generate();
            // patientUpload.uploadData = [patientUpload.uploadData];
        }

        // log.uploads = uploads;
        logger.info('saving upload log ', log);
        await saveUploadLog(log);
        logger.info('saved log!');

        let query = `_id = '${log._id}'`;
        
        logger.info("the query ", query);
        let savedLog =  await getUploadLog(query);

        logger.info('saved log ', savedLog)
        if (savedLog && savedLog.length) {
            return savedLog[0]
        } else {
            return null;
        }

    } catch (e) {
        logger.info("error updating upload log ", e);
        throw e;
    }
}

const startPatientUpload = async (patientUpdatedData, patientUploadsMap, uploadLog) => {
    try {
        t32XrayStudioUplaodQueue.add(uploadLog);

        for (let patientData of patientUpdatedData) {

            let patientId = patientData._id;
            let patientUpload = patientUploadsMap[patientId];
            let error = null;

            let patientDataCopy = JSON.parse(JSON.stringify(patientData));

            let updateMsg = {
                msgType: 'uploadQueue',
                msg: {
                    action: 'updateQueue',
                    id: uploadLog._id,
                    patientId: patientId,
                    data: {
                        isStarted: true
                    }
                }
            }

            t32XrayStudioBrowserWindowHandler.notifyClient(updateMsg, 'BackendPushNotification');
            logger.info("send update queue notify ", updateMsg);

            if (patientDataCopy.patient_notes && patientDataCopy.patient_notes.length) {
                patientDataCopy.patient_notes = patientDataCopy.patient_notes.filter((note) => {
                    return note.is_offline;
                })
            }

            if (patientDataCopy.treatments && patientDataCopy.treatments.length) {
                patientDataCopy.treatments = patientDataCopy.treatments.filter((treatment) => {
                    return treatment.is_offline;
                })
            }

            patientDataCopy.sessionId = uploadLog._id;


            try {
                await uploadPatientData(patientDataCopy);
            } catch (e) {
                logger.info('error with uploading patient ', patientDataCopy._id);
                error = true;
            }

            updateMsg.msg.data = {
                isComplete: error ? false : true,
                isError: error ? true : false
            }

            t32XrayStudioBrowserWindowHandler.notifyClient(updateMsg, 'BackendPushNotification');
            logger.info("send update queue notify ", updateMsg);

            console.log("the patient upload uploadData ", patientUpload.uploadData);
            
            patientUpload.isStarted = true;
            patientUpload.isComplete = error ? false : true;
            patientUpload.isError = error ? true : false
            // patientUpload.uploadData = [patientUpload.uploadData];

            console.log("the patient upload ", patientUpload);
            // patientUpdatedUploads.push(patientUpload);
            await db.save("PatientUpload", patientUpload, true);

            if (error) {
                patientData.isUploading = false;
                await db.save('Patient', patientData, true);
            } else {
                let query = `_id = "${patientId}"`
                await db.deleteRecord('Patient', query, true);
                patientData.remove = true;
            }
        }

        t32XrayStudioUplaodQueue.removeBy("_id", uploadLog._id);
        return 'done' 
    } catch (e) {
        //TODO: loop over the patientData and set isUploading flag to false, error out any remaining patient uploads that are not completed and send client side notification.
        t32XrayStudioUplaodQueue.removeBy("_id", uploadLog._id);
        for (let patientData of patientUpdatedData) {
           
            let patientUpload = patientUploadsMap[patientData._id];
            if (!patientUpload.isComplete) {
                patientUpload.isError = true;
            }
            // patientUpload.uploadData = [patientUpload.uploadData]
            await db.save('PatientUpload', patientUpload, true);

            patientData.isUploading = false;
            if (!patientData.remove) {
                db.save('Patient', patientData, true);
            }
        }

        logger.info("error startPatientUpload")
        throw e
    }
}

//TODO: Maintain a queue that will be stored in session that keeps track of patients being uploaded. For this we need to remove/get rid of idea of isUploading flag on patient  
const uploadOfflineData = async (data) => {
    try {
        var patients = data.uploadData;
        // let clinicId = data.clinicId;

        if (!patients.length) {
            return null;
        }

        //used to gather patient objects (copied) for uploading and also updating the isUploading flags.
        let patientUpdatedData = [];

        for (let patient of patients) {
            logger.info("patient ", patient);

            let query = `_id = '${patient.patientId}'`;

            logger.info("the query ", query);
            let patientData = await queryOfflinePatients(query);

            if (patientData && patientData.length) {
                for (let data of patientData) {
                    let dataCopy = JSON.parse(JSON.stringify(data));
                    dataCopy.isUploading = true;
                    dataCopy.patient_notes = Object.values(dataCopy.patient_notes);
                    dataCopy.treatments = Object.values(dataCopy.treatments);
                    patientUpdatedData.push(dataCopy);
                }

            }
        }

        logger.info("saving patient isUplaoding flag ", patientUpdatedData);
        await db.save('Patient', patientUpdatedData, true);

        logger.info("updating/creating upload log");
        let uploadLog = await updateUploadLog(patients);
        logger.info("log ", uploadLog);

        logger.info("configuring uploadLogId");
        let dateAddition = " " + uploadLog.logDate;
        let originalId = uploadLog._id;
        uploadLog._id = originalId + dateAddition;

        let updateMsg = {
            msgType: 'uploadQueue',
            msg: {
                action: 'addToQueue',
                data: uploadLog
            }
        }
    
        t32XrayStudioBrowserWindowHandler.notifyClient(updateMsg, 'BackendPushNotification');
        logger.info("sent log notification");
        // let patientUpdatedUploads= [];

        // create a map of the uploads that are in a particular upload log. Later we update each individual upload after successfull/failed upload
        let patientUploadsMap = utils.createMapFromList(uploadLog.uploads, "patientId");

        startPatientUpload(patientUpdatedData, patientUploadsMap, uploadLog);

        // logger.info("about to return")
        return 'done';
    } catch (e) {
        throw e;
    }
}

async function uploadPatientData(patientData) {
    try {
        // logger.info("mocking successfull upload");
        // return await t32RestServices.mockRestCallSuccess(5000);

        return await t32RestServices.execForResult(
            t32RestEndPoints.offlineUpload.routePath,
            t32RestEndPoints.offlineUpload.method,
            null,
            patientData,
            null,
            null,
        );
    } catch (e) {
        logger.debug("failed to upload patient data", e)
        throw e;
    }
}

const handleOfflineUploadRequest = async (requestMsg) => {
    try {
        const { requestType, data } = requestMsg;

        if (requestType === 'getOfflineUploadData') {
            return await getOfflineUploadData(data);
        } else if (requestType === 'uploadOfflineData') {
            return await uploadOfflineData(data);
        }
    } catch (e) {
        logger.error('error handleOfflineUploadRequest', e);
        throw e;
    }
}
/**
 * @description For saving downloaded patients in database
 * @async
 * @param { object[] } data 
 */
const saveDownloadedPatients = async (data) => {
    try {
        const today = moment().utc();
        today.hour(0);
        today.minute(0);
        today.second(0);
        today.millisecond(0);

        const expiryDate = today.clone().add(30, 'days').toDate();
        const refreshDate = today.clone().add(14, 'days').toDate();

        for (let patient of data) {
            const existing = await queryOfflinePatients(`_id = "${patient._id}"`);

            if (existing.length) {
                const offlineNotes = existing[0].patient_notes.filter(item => item.is_offline == true);
                const offlineTreatments = existing[0].treatments.filter(item => item.is_offline == true);
                const offlineXrays = existing[0].xrays;

                if (!offlineNotes.length && !offlineTreatments.length && !offlineXrays) {
                    await deletePatientData('patient', [ existing[0]._id ]);
                } else {
                    // use existing expiration date if not deleting record
                    patient.expiryDate = existing[0].expiryDate;

                    // append offline notes and treatments
                    patient.patient_notes = [...offlineNotes, ...patient.patient_notes];
                    patient.treatments = [...offlineTreatments, ...patient.treatments];
                }

            }

            // set refreshDate and expiryDate if not set
            patient.expiryDate = patient.expiryDate || expiryDate;
            patient.refreshDate = refreshDate;

            // set address fields
            if (patient.address) {
                patient.add_line1 = patient.address.add_line1 || '';
                patient.add_line2 = patient.address.add_line2 || '';
                patient.city = patient.address.city || '';
                patient.state = patient.address.state || '';
                patient.zipcode = patient.address.zipcode || '';
                patient.zip_last_4 = patient.address.zip_last_4 || '';
                patient.phone = patient.address.phone || '';
                patient.preferred_contact = patient.address.preferred_contact || '';
                patient.cell_phone = patient.address.cell_phone || '';
            }

            // set clinic data
            if (patient.clinicData) {
                patient.chartId = patient.clinicData.chartId || undefined;
                patient.foreignId = patient.clinicData.foreignId || '';
            }
        }
        
        return await db.save('Patient', data, true);
    } catch (error) {
        throw error;
    }
}

const fetchRefreshData = async (getCountOnly=false) => {
    try {
        let today = moment().utc();
        today.hour(0);
        today.minute(0);
        today.second(0);
        today.millisecond(0);
        today = moment(today).format("YYYY-MM-DD@HH:mm:ss.mmm");
    
        // let dataQuery = forCount ? `is_offline = false AND refreshDate <= ${today}` : `isRefreshing = false AND refreshDate <= ${today}`
        let dataQuery = `is_offline = false AND refreshDate <= ${today}`
    
        let patients = JSON.parse(JSON.stringify(await queryOfflinePatients(dataQuery)));

        return getCountOnly ? patients.length : patients
    } catch (e) {
        throw e;
    }
}

const downloadOfflineXrays = async (data) => {
    try {
        const studioContext = require("../t32XrayStudioMain").getStudioContext();
        const appHomePath = studioContext.theApp.getAppHome();
        const { clinic_id, patientIds } = data;
        let today = moment().utc();
        today.hour(0);
        today.minute(0);
        today.second(0);
        today.millisecond(0);
        let twoYearsBack = moment(today).subtract('2', "years");
        // var currentPatientDetails = {};
        let errorFilePath = path.join(__dirname, "/frontend/imagingStudio/assets/img/error.png");
        if (!fs.existsSync(errorFilePath)) {
            logger.info("the file path ", errorFilePath);
            throw 'file path not found';
        }
        let failedImgData = fs.readFileSync(errorFilePath);
        
        logger.info("sending download notification");
        sendDownloadNotification({isDownloading: true})

        for (var patientId of patientIds) {
            // currentPatientDetails.patientId = patientId;
            let patients = await queryOfflinePatients(`_id = "${patientId}"`);
            let currentPatient = null;

            if (patients && patients.length) {
                try {
                    currentPatient = JSON.parse(JSON.stringify(patients[0]));
                    logger.info("current patient ", currentPatient);
                    // logger.info("trying to convert patient notes");
                    currentPatient.patient_notes = Object.values(currentPatient.patient_notes);
                    // logger.info("trying to convert patient treatments");
                    currentPatient.treatments = Object.values(currentPatient.treatments);

                    logger.info("about to query ")
                    let postData = {
                        clinic_id : clinic_id,
                        patientId: patientId,
                        startDate: moment(twoYearsBack).toDate(),
                        endDate: moment(today).toDate()
                    } 
            
                    let xrayInfo = await t32RestServices.execForResult(
                        t32RestEndPoints.offlineDownloadXrayInfo.routePath,
                        t32RestEndPoints.offlineDownloadXrayInfo.method,
                        null,
                        postData,
                        null,
                        null,
                    );
                    
                    let xrayDates = Object.keys(xrayInfo);
                    let promises = [];
                    //TODO:Jag Send Front end notifications for xray progress. 
                    if (xrayDates && xrayDates.length) {
                        try {
                            let recordId = currentPatient.xrays ? currentPatient.xrays._id : idGenerator.generate();
                            logger.info("record id ", recordId);
                            logger.info("current patient id ", currentPatient && currentPatient.xrays ? currentPatient.xrays._id : null);

                            var tempPatXrayFolder = path.join(appHomePath, `/temp/xrays/${recordId}`);
                            var mainPatXrayFolder = path.join(appHomePath, `/offlineDownloads/xrays/${recordId}`);

                            if (currentPatient.xrays && currentPatient.xrays.storagePath && fs.existsSync(currentPatient.xrays.storagePath)) {
                                logger.info("removing current xray storage path");
                                fs.removeSync(currentPatient.xrays.storagePath);
                            }

                            // currentPatient.xrays.storagePath = mainPatXrayFolder;
                            // currentPatientDetails.tempDownloadFolder = tempPatXrayFolder;
                            // currentPatientDetails.mainDownloadFolder = mainPatXrayFolder;
            
                            logger.info("the temp patient foelder path ", tempPatXrayFolder);
            
                            fs.ensureDirSync(tempPatXrayFolder);
            
                            for (let date of xrayDates) {
                                let dateSubFolder = path.join(tempPatXrayFolder, `/${date}`);
                                logger.info('ensuring date subfolder');
                                fs.ensureDirSync(dateSubFolder);
                                
                                // currentPatientDetails.dateSubFolder = dateSubFolder;
    
                                for (let xray of xrayInfo[date]) {
                                    if (xray.metadata.image_pos) {
                                        try {
                                            // logger.info("the xray is ", xray);
                                            let query = {
                                                clinic_id: clinic_id,
                                                contentType: xray.contentType,
                                                storagePath: xray.storagePath || null,
                                                fileId: xray._id
                                            };
                                            
                                            let xrayFilePath = path.join(tempPatXrayFolder, `/${date}/${xray.metadata.image_pos}.jpeg`);
                                            
                                            promises.push(downloadXrayFile(query, xrayFilePath, failedImgData))
                                        } catch (e) {
                                            logger.info(`Error getting download for ${patientId}, ${xray.metadata.image_pos}`)
                                        }
                                    }
                                }
            
                            }
            
                            await Promise.all(promises);
                            // fs.ensureDirSync(tempPatXrayFolder);
                            await utils.pause(1000);
    
                            fs.moveSync(tempPatXrayFolder, mainPatXrayFolder);
                            fs.removeSync(tempPatXrayFolder);
                            
                            currentPatient.xrays = {
                                _id: recordId,
                                patient_id: patientId,
                                storagePath: mainPatXrayFolder,
                                isComplete: true
                            }
                            
                            await db.save('Patient', currentPatient, true);
                            logger.info("downloaded all xrays for ", patientId);
                        } catch (e) {
                            logger.info(`error getting xrays for patient ${patientId} `, e);
                            try {

                                if (fs.existsSync(tempPatXrayFolder)) {
                                    fs.removeSync(tempPatXrayFolder)
                                }
                                
                                if (fs.existsSync(mainPatXrayFolder)) {
                                    fs.removeSync(mainPatXrayFolder)
                                }
    
                                delete currentPatient.xrays;
    
                                await db.save('Patient', currentPatient, true);
    
                            } catch (e) {
                                logger.info(`error trying to remove xray field from patient ${patientId} `,  e);
                            }
                        }
                    }
                } catch (e) {
                    logger.info(`error getting xray info for ${patientId} `, e);
                }
            }
        }

        sendDownloadNotification({isDownloading: false});

        return 'done';
    } catch (e) {
        logger.info("failed to run offline xray download", e);
        sendDownloadNotification({isDownloading: false})
        throw e;
    }
}

function sendDownloadNotification(msg) {
    let updateMsg = {
        msgType: 'offlineDownloadUpdate',
        msg: msg
    }

    t32XrayStudioBrowserWindowHandler.notifyClient(updateMsg, 'BackendPushNotification');
}

async function downloadXrayFile(query, filePath, failedImgData) {
    try {

        let results = await t32RestServices.execForResult(
            t32RestEndPoints.offlineDownloadXray.routePath,
            t32RestEndPoints.offlineDownloadXray.method,
            null,
            query,
            null,
            null,
            'fileStream'
        );
        
        return await new Promise((resolve, reject) => {
            results
            .on('error', function (err) {
                // throw err
                logger.error('dowload failed:', err);
                createErrorImg(failedImgData, filePath);
                resolve();
            }).on('response', function (resp) {
                // logger.info('the response ', resp);
                if (resp.statusCode == 500 || resp.statusCode == 400 || resp.statusCode == 401 || resp.statusCode == 503 || resp.statusCode == 404) {
                    logger.info("error from server ", resp.statusCode)
                    // throw 'failed to download'
                    logger.error('dowload failed');
                    createErrorImg(failedImgData, filePath);
                    resolve()
                } else {
                    try {
                        logger.info("creating write stream ", filePath)
                        let xrayImg = fs.createWriteStream(filePath);
                        resp.pipe(xrayImg);
                        logger.info("piped info for " + filePath);
                    } catch (e) {
                        logger.info("error with stream ", e);
                        if (fs.existsSync(filePath)) {
                            fs.removeSync(filePath);
                        }
                        try {
                            createErrorImg(failedImgData, filePath);
                            resolve();
                        } catch (e) {
                            logger.error("failed on attempt to create error xray file ", e);
                        }
                    }
                }
            }).on('end', function() {
                logger.info('stream has finished for ' + filePath);
                resolve();
            })
        })
    } catch (e) {
        throw e;
    }
}

function createErrorImg(failedImgData, filePath) {
    // let xrayFailedStream = fs.createWriteStream(filePath);
    // failedImgStream.pipe(xrayFailedStream);
    fs.writeFileSync(path.resolve(filePath), failedImgData)
}

const getOfflineXrays = async (storagePath) => {
    try {
        if (!storagePath) {
            throw 'No storage path defined';
        }
        let imageFileExtList = ['.jpg', '.jpeg', '.png', '.tiff', '.gif'];

        let dirList = fs.readdirSync(storagePath)
            .filter(file => fs.lstatSync(path.join(storagePath, file)).isDirectory());

        logger.info("the offline dir list ", dirList);
        let offlineXrayDirMap = {};

        for (let dir of dirList) {
            if (!offlineXrayDirMap[dir]) {
                let dirPath = path.join(storagePath, dir);

                offlineXrayDirMap[dir] = fs.readdirSync(dirPath)
                    .filter((file) => {
                        // logger.info('the file', file);
                        let extName = path.extname(file) || '';
                        let fullFilePath = path.join(dirPath, file);
                        return fs.lstatSync(fullFilePath).isFile() &&
                            imageFileExtList.indexOf(extName.toLowerCase()) !== -1
                    }).map((file) => {
                        let filePath = path.join(dirPath, file);
                        let ext = path.extname(filePath);
                        return {
                            filePath: filePath,
                            filename: path.basename(filePath).replace(/_/g, " ").replace(ext, "")
                        }
                    })

                
            }
        }

        logger.info("the offline XrayDir Map")
        logger.info(offlineXrayDirMap);

        return offlineXrayDirMap;
    } catch (e) {
        throw e;
    }
}

const handleOfflineXraysRequest = async (requestMsg) => {
    try {
        const { requestType, data } = requestMsg;

        if (requestType === 'downloadXrays') {
            return await downloadOfflineXrays(data);
        } else if (requestType === 'getOfflineXrays') {
            return await getOfflineXrays(data.storagePath);
        }
    } catch (e) {
        logger.error('error hanldeOfflineXrayRequest', e);
        throw e;
    }
}

const getOfflineImagingStudioAccess = async (clinicId) => {
    try {
        if (!clinicId) {
            throw 'no clinic id with request';
        }

        let clinic = await getClinic(clinicId)

        logger.info("clinic offline access ", clinic);
        return clinic && clinic.offlineStudio ? true : false;
    } catch (e) {
        throw e;
    }
}

const handleOfflineClinicRequest = async (requestMsg) => {
    try {
        const { requestType, data } = requestMsg;

        if (requestType === 'getOfflineImagingStudioAccess') {
            return await getOfflineImagingStudioAccess(data.clinicId);
        } 
    } catch (e) {
        logger.error('error hanldeOfflineClinicRequest:getOfflineClinicSubscriptioin --> ', e);
        throw e;
    }
}

const getRefreshDataCount = async () => {
    try {
        return await fetchRefreshData(true);
    } catch (e) {
        logger.info("error getting refresh data count", e);
        throw e
    }
}

const refreshOfflineData = async (data = null) => {
    try {
        let patients = data.patients && data.patients.length ? data.patients : await fetchRefreshData();

        let clinicId = data.clinic_id;

        let patientIds = patients.map((patient) => { return patient._id });

        let postData = { clinic_id: clinicId, patientIds: patientIds }

        logger.info("the post data ", postData);
        const result = await t32RestServices.execForResult(
            t32RestEndPoints.offlineDownload.routePath,
            t32RestEndPoints.offlineDownload.method,
            null,
            postData,
            null,
            null,
        );

        logger.info('...offline downloaded patients for refresh');

        await saveDownloadedPatients(result);

        logger.info("....offline starting xray download for refresh");

        handleOfflineXraysRequest({
            requestType: 'downloadXrays',
            data: {
                clinic_id: clinicId,
                patientIds: result.map((patient) => patient._id.toString())
            }
        })

    } catch (e) {
        logger.info("error refreshing offline data", e);
        throw e;
    }
}

const handleOfflineRefreshRequest = async (requestMsg) => {
    try {
        const { requestType, data } = requestMsg;

        if (requestType === 'getRefreshDataCount') {
            return await getRefreshDataCount();
        } else if (requestType === 'refreshOfflineData') {
            return await refreshOfflineData(data);
        }
    } catch (e) {
        logger.error('error handleOfflineUploadRequest', e);
        throw e;
    }
}

module.exports = {
    saveDownloadedPatients,
    queryOfflinePatients,
    handleOfflinePatientRequest,
    saveCdtCodes,
    saveNoteTemplates,
    saveClinic,
    getClinicData,
    handleOfflineTreatmentsRequest,
    handleOfflineUploadRequest,
    handleOfflineRefreshRequest,
    handleOfflineXraysRequest,
    handleOfflineClinicRequest
}
