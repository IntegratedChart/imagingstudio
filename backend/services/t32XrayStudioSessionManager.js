const idGenerator = require('../utils/idGenerator');

module.exports.getSessionId = getSessionId;

var sessionId = idGenerator.generate();

function getSessionId(){
    return sessionId;
}