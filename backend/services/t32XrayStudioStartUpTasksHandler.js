// const q = require("q");
// const _ = require('lodash');
const path = require("path");
const logger = require('electron-log');
const fs = require('fs-extra');
const xrayStudioFs = require('./t32XrayStudioFs');
const t3XrayStudioEncryptionHandler = require('./t32XrayStudioEncryptionHandler');

module.exports.ensureUploadFolder = ensureUploadFolder
module.exports.initialize = initialize;
// module.exports.ensureSSHFile = ensureSSHFile;

var studioContext = null;

async function initialize(theContext){
    try {
        studioContext = theContext;
        ensureUploadFolder()
        let rsaKeys = await getSSHKeys(studioContext);
        // logger.info(rsaKeys);
        studioContext.rsaKeys = rsaKeys;
        clearTempFiles(studioContext);
        return
    } catch (err) {
        throw err;
    }
}

function clearTempFiles(studioContext) {
    let homeDir = studioContext.theApp.getAppHome();
    let tempFilesPath = path.resolve(homeDir, 'temp');

    if (fs.existsSync(tempFilesPath)) {
        fs.removeSync(tempFilesPath);
    }
    
    return
}
/**
 * Moves any left over directories form uploads folder back to tab32 folder
 * @param {*} studioContext The main app context with app/machine enviorment info
 *
 */
function ensureUploadFolder(){

    try {
        // var uploadFolder = path.join(studioContext.theApp.getPath('home'), 'tab32Upload'); 
        // var tab32RootFolder = path.join(studioContext.theApp.getPath('home'), 'tab32');
        var homeDir = studioContext.theApp.getAppHome();
        var uploadFolder = path.join(homeDir, 'tab32Upload');
        var tab32RootFolder = path.join(homeDir, 'tab32');
        // fs.ensureDirSync(uploadFolder);
        xrayStudioFs.ensureDirSync(uploadFolder);
    
        var dirList = fs.readdirSync(uploadFolder).filter((dir)=>{
            return fs.lstatSync(path.join(uploadFolder, dir)).isDirectory()
        });
        
        logger.debug("----------Startup Task: getting dir list-----------");
        // logger.debug(dirList);

        if(dirList.length > 0){
            for (let dir of dirList) {
                fs.moveSync(path.join(uploadFolder, dir) , path.join(tab32RootFolder, dir));
            }
        }

        return 
    } catch(err) {
        logger.error("There was an error with upload folder");
        logger.error(err);
        return
    }
}

async function getSSHKeys(studioContext){
    try {
        logger.info("getting ssh keys");
        
        let homeDir = studioContext.theApp.getAppHome();
        let tab32RootFolder = path.join(homeDir, 'tab32');
        let rsaFilePath = path.join(tab32RootFolder, 'xraystudio_ehr_rsa_keys.json');
        let secret = studioContext.machineId || null;
        
        if(!secret){
            throw 'ssh key generation failed'
        }
        let rsaKeys = null;
    
        if (!fs.existsSync(rsaFilePath)) {
            rsaKeys = await t3XrayStudioEncryptionHandler.generateKeys(secret);
            await xrayStudioFs.writeJson(rsaFilePath, rsaKeys);
            return rsaKeys
        } else {
            rsaKeys = await xrayStudioFs.readJson(rsaFilePath);
    
            if(!rsaKeys.publicKey || !rsaKeys.privateKey){
                rsaKeys = await t3XrayStudioEncryptionHandler.generateKeys(secret);
                await xrayStudioFs.writeJson(rsaFilePath, rsaKeys);
            }
            
            return rsaKeys
        }
    } catch (err) {
        throw err;
    }
}