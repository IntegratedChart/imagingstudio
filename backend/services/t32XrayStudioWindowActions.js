const logger = require('electron-log');
const t32XrayStudioBrowserWindowHandler = require("./t32XrayStudioBrowserWindowHandler");
const t32XrayStudioDispatchMsg = require("./t32XrayStudioDispatchMsg");
const open = {
}

module.exports.triggerXrayTemplate = triggerXrayTemplate;
module.exports.open = open;

function triggerXrayTemplate(studioContext, info) {

    let clearPatientXrayContextAction = {
        action: "clearPatientContext"
    }

    t32XrayStudioDispatchMsg.sendMsgToClient('t32XrayStudioAction', clearPatientXrayContextAction);

    let windowConfig = {
        modal: true,
        name: "Xray Template",
        frame: true,
        toolbar: false,
        show: false,
        // parent: t32XrayStudioBrowserWindowHandler.getWindowByName(info.parentWinName),
        // maximizable: false,
        // minimizable: false,
        webPreferences: {
            nodeIntegration: true
        }
    }

    let winName = 'xrayTemplate'

    
    info.winInstance = winName;
    
    let triggerAction = {
        action: 'triggerXrayTemplate',
        info: info
    }
    
    open['xrayTemplate'] = openXrayTemplate(triggerAction);
    
    let newWin = t32XrayStudioBrowserWindowHandler.openChildWindow("/frontend/xrayStudioV1/index.html", winName, windowConfig);
    // setTimeout(() => {
    //     let hideWinViewMsg = {
    //         msgType: "hideView",
    //         msg: true
    //     }
        
    //     t32XrayStudioBrowserWindowHandler.notifyClient(hideWinViewMsg, 'BackendPushNotification', winName);
    // }, 1000);

    // setTimeout(() => {
        // t32XrayStudioBrowserWindowHandler.show(winName);
    // }, 1600);

    
    // setTimeout(() => {
    //     t32XrayStudioDispatchMsg.sendMsgToClient('t32XrayStudioAction', triggerAction);
    // }, 2000)

    return 
}

function openXrayTemplate(triggerAction) {
    
    let trigger = triggerAction;

    return function (winName){
        let hideWinViewMsg = {
            msgType: "hideView",
            msg: true
        }
        t32XrayStudioBrowserWindowHandler.notifyClient(hideWinViewMsg, 'BackendPushNotification', winName);

        setTimeout(() => {
            var msgToSendClient = {
                msgType: 'incomingCommsChannelMsg',
                channel: 't32XrayStudioAction',
                msg: trigger,
            }

            t32XrayStudioBrowserWindowHandler.notifyClient(msgToSendClient, 'BackendPushNotification', winName);
        }, 1500);
    
        setTimeout(() => {
            t32XrayStudioBrowserWindowHandler.show(winName);
        }, 1000)
    }
}