const path = require("path");
const fs = require('fs');
const logger = require('electron-log');
const Jimp = require("jimp");


module.exports.rotateImage = rotateImage;
module.exports.manipulateImage = manipulateImage;
module.exports.flipImage = flipImage;


/**
 * 
 * @param { string } fullFilePath 
 * @param { string } horizontalFlip 
 * @param { string } verticalFlip 
 */
async function flipImage(fullFilePath, horizontalFlip, verticalFlip) {
    try {
        let img = await Jimp.read(fullFilePath)
        logger.debug(img);

        return await new Promise((resolve, reject) => {
            img.flip(horizontalFlip, verticalFlip).getBuffer(Jimp.AUTO, function (err, buf) {
                if (err) {
                    reject(err)
                }
                else {
                    fs.writeFile(fullFilePath, buf, function (err) {
                        if (err) {
                            reject(err)
                        }
                        else {
                            resolve();
                        }
                    });
                }
            });
        })
    } catch (e) {
        logger.error("error flipping image ", e);
        throw e
    }

}

/**
 * 
 * @param {string} fullFilePath 
 * @param {string} rotationDeg 
 */
async function rotateImage(fullFilePath, rotationDeg) {
    try {
        let img = await Jimp.read(fullFilePath);

        return await new Promise((resolve, reject) => {
            img.rotate(rotationDeg).getBuffer(Jimp.AUTO, function (err, buf) {
                if (err) {
                    reject(err);
                }
                else {
                    fs.writeFile(fullFilePath, buf, function (err) {
                        if (err) {
                            reject(err);
                        }
                        else {
                            resolve();
                        }
                    });
                }
            })
        });
    } catch (e) {
        logger.error("error rotating image ", e);
        throw e
    }
}

/**
 * 
 * @param {string} originalFilePath 
 * @param {string} filePath 
 * @param {string} brightValue 
 * @param {string} contrastValue 
 */
async function manipulateImage(originalFilePath, filePath, brightValue, contrastValue) {
    try {
        let img = await Jimp.read(originalFilePath);

        return await new Promise((resolve, reject) => {
            img.brightness(brightValue).contrast(contrastValue).getBuffer(Jimp.AUTO, (err, buf) => {
                logger.debug("We have image buffer to manipulate")
                // logger.debug(image);
                if (err) {
                    reject(err)
                }
                else {
                    fs.writeFile(filePath, buf, (err) => {
                        if (err) {
                            logger.error("error writing to file");
                            reject(err);
                        }
                        else {
                            resolve();
                        }
                    });
                }
            });
        })
    } catch (e) {
        throw e
    }
}