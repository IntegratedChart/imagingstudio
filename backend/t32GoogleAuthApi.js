'use strict';
/**
* api file holds re-usable methods that are not tied to req / res object.
*
* If method involves async activity, suggest to use $q promise interface to 
* communicate with the caller.
*/
// const q = require("q");
const google = require('googleapis');
const OAuth2 = google.auth.OAuth2;
const plus = google.plus('v1');
const logger = require('electron-log');


/* Public Interface */
module.exports.getGoogleOAuthRequestUrl = getGoogleOAuthRequestUrl;
module.exports.getGoogleAccessToken = getGoogleAccessToken;
module.exports.refreshGoogleAccessToken = refreshGoogleAccessToken;

async function getGoogleOAuthRequestUrl( googleCallBackUrl ) {
   try {
      let oauth2Client = new OAuth2(
         '936255628801.apps.googleusercontent.com',
         'LVzYTUPgQIsh8tDE-eP-b3-O',
         googleCallBackUrl
      );
   
      // generate a url that asks permissions for Google+ and Google Calendar scopes
      let scopes = [
         'https://www.googleapis.com/auth/plus.me',
         'https://www.googleapis.com/auth/userinfo.email',
         'https://www.googleapis.com/auth/userinfo.profile'
      ];
   
      return oauth2Client.generateAuthUrl({
         // 'online' (default) or 'offline' (gets refresh_token)
         access_type: 'offline',
         approval_prompt: 'force',
         scope: scopes,
      });
   
   } catch (e) {
      throw e
   }
}


/**
 * Given a google auth code, get the user email.
 */
async function getGoogleAccessToken(authCode) {
   try {
      logger.debug('getGoogleAccessToken, authCode :' + authCode);
   
      let oauth2Client = new OAuth2(
         '936255628801.apps.googleusercontent.com',
         'LVzYTUPgQIsh8tDE-eP-b3-O',
         'http://localhost:23228/auth/google/return' );
         
      return await new Promise((resolve, reject) => {
         oauth2Client.getToken(authCode, function (err, tokens) {
            if (err) {
               logger.info("failed to get google access token ", err);
               reject(err);
            } else {
               logger.info('Got token: ')
               // logger.info(tokens);
      
               oauth2Client.setCredentials(tokens);

               plus.people.get( { userId: 'me', auth: oauth2Client }, function (err, profile) {
                  if (err) {
                     logger.info("failed to get google access token ", err);
                     reject(err);
                  } else {
                     resolve(tokens);
                  }
               });
            }
         });
      })  
   } catch (e) {
      throw e;
   }
}


async function refreshGoogleAccessToken( tokens ) {
   try {
      let oauth2Client = new OAuth2(
         '936255628801.apps.googleusercontent.com',
         'LVzYTUPgQIsh8tDE-eP-b3-O',
         'http://localhost:23228/auth/google/return' );
   
      oauth2Client.setCredentials(tokens);

      return await new Promise((resolve, reject) => {
         oauth2Client.refreshAccessToken(function(err, tokens) {
            if (err) {
               logger.info("error with refresh google access token ", err);
               reject(err);
            } else {
               logger.info('Got refresh token : ')
               resolve(tokens);
            }
         });   
      })
  
   } catch (e) {
      throw e;
   }

}

// function getGoogleOAuthRequestUrl( googleCallBackUrl )
// {
//    var deferred = q.defer();

//    var oauth2Client = new OAuth2(
//       '936255628801.apps.googleusercontent.com',
//       'LVzYTUPgQIsh8tDE-eP-b3-O',
//       googleCallBackUrl
//    );

//    // generate a url that asks permissions for Google+ and Google Calendar scopes
//    var scopes = [
//       'https://www.googleapis.com/auth/plus.me',
//       'https://www.googleapis.com/auth/userinfo.email',
//       'https://www.googleapis.com/auth/userinfo.profile'
//    ];

//    var url = oauth2Client.generateAuthUrl({
//       // 'online' (default) or 'offline' (gets refresh_token)
//       access_type: 'offline',
//       approval_prompt: 'force',
//       scope: scopes,
//    });

//    deferred.resolve(url);

//    return deferred.promise;
// }


// /**
//  * Given a google auth code, get the user email.
//  */
// function getGoogleAccessToken(authCode) {
//    var deferred = q.defer();

//    logger.debug('getGoogleAccessToken, authCode :' + authCode);

//    var oauth2Client = new OAuth2(
//       '936255628801.apps.googleusercontent.com',
//       'LVzYTUPgQIsh8tDE-eP-b3-O',
//       'http://localhost:23228/auth/google/return' );

//    oauth2Client.getToken(authCode, function (err, tokens) {
//       if (err) 
//       {
//          logger.error(err);
//          deferred.reject(err);
//       }
//       else
//       {
//          logger.debug('Got token: ')
//          logger.debug(tokens);

//          oauth2Client.setCredentials(tokens);
//          plus.people.get( { userId: 'me', auth: oauth2Client }, function (err, profile) {
//             if (err) {
//                logger.error(err);
//                deferred.reject(err);
//             }
//             else
//             {
//                deferred.resolve(tokens);
//             }
//          });
//       }
//    });

//    return deferred.promise;    
// }


// function refreshGoogleAccessToken( tokens ) {
//    var deferred = q.defer();

//    var oauth2Client = new OAuth2(
//       '936255628801.apps.googleusercontent.com',
//       'LVzYTUPgQIsh8tDE-eP-b3-O',
//       'http://localhost:23228/auth/google/return' );

//    oauth2Client.setCredentials(tokens);
//    oauth2Client.refreshAccessToken(function(err, tokens) {
//       if (err) 
//       {
//          logger.error(err);
//          logger.error(err.stack);
//          deferred.reject(err);
//       }
//       else
//       {
//          logger.debug('Got refresh token : ')
//          logger.debug(tokens);
//          deferred.resolve(tokens);
//       }
//    });   
//    return deferred.promise;    
// }