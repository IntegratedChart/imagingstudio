var path = require("path");
var t32RestServices = require('./t32RestServices');
var logger = require('electron-log');
var os = require('os');
const {machineId, machineIdSync} = require("node-machine-id");  
const t32XrayStudioStartUpTasksHandler = require('./services/t32XrayStudioStartUpTasksHandler');  
var http = require('http');
var t32XrayStudioDriverHandler = require('./services/t32XrayStudioDriverHandler');
var t32XrayStudioRedisHandler = require('./services/t32XrayStudioRedisHandler');
var t32XrayStudioChannelManager = require('./services/channelServices/t32XrayStudioChannelManager');
var t32XrayStudioBrowserWindowHandler = require('./services/t32XrayStudioBrowserWindowHandler');
var t32XrayStudioLocalServer = require('./services/t32XrayStudioLocalServer');
var t32XrayStudioDirWatcher = require('./services/t32XrayStudioDirWatcher');
var t32XrayStudioIpcMsgHandler = require('./services/t32XrayStudioIpcMsgHandler');
var t32XrayStudioAppInstanceHandler = require('./services/t32XrayStudioAppInstanceHandler');
var t32XrayStudioAuthManager = require('./services/t32XrayStudioAuthManager');
var t32XrayStudioClientConnect = require('./services/t32XrayStudioClientConnect');
var t32XrayStudioDbHandler = require('./services/t32XrayStudioDbHandler');
var t32XrayStudioSettingService = require('./services/t32XrayStudioSettingsService');

module.exports.buildStudioContext = buildStudioContext;
module.exports.startDriverComms = startDriverComms;
module.exports.getStudioContext = getStudioContext;
// module.exports.startDbConnection = startDbConnection;
var studioContext;

/**
 * Updated 2/15/19 Jagvir Singh
 * No loger fetching setting at this point and having them passed in from main.js
 * @param {*} appInfo the main app information
 * @param {*} browserWindow The main browser window
 * @param {*} setting the xray studio settings
 */
async function buildStudioContext(appInfo, browserWindow, setting, appArgs ) {
    // var deferred = q.defer();

    try{
        logger.info('Initializing tab32 Xray Studio with settings... ');
    
        studioContext = {
            theApp                : appInfo,
            browserWindow         : browserWindow,
            handlerMap            : {},
            machineId             : machineIdSync({original: true}), //uses machind uid reg key on windows, and IOPlatformUUID for osx
            setting               : setting,
            appInstanceHandler: t32XrayStudioAppInstanceHandler.initialize(appArgs)
        };
        
        t32XrayStudioAuthManager.initialize(studioContext);
        t32XrayStudioLocalServer.initialize(studioContext);
        t32XrayStudioClientConnect.initialize(studioContext);
        t32XrayStudioIpcMsgHandler.initialize(studioContext);
        t32RestServices.initialize(studioContext);
        // t32XrayStudioDirWatcher.initialize(studioContext);
        await t32XrayStudioStartUpTasksHandler.initialize(studioContext);
        await initializeRedisConnect(studioContext);
        studioContext.appInstanceHandler.instanceEmitter.emit('appReadyForMessaging', studioContext);

        return studioContext
    }
    catch(err){
        logger.info("-------App failed to initialize--------");
        logger.error(err);
        throw { initErr: err, context: studioContext };
    }

}

// async function startDbConnection(context) {
//     try {
//         return await t32XrayStudioDbHandler.initialize(context);
//     } catch (e) {
//         throw e
//     }
// }

async function startDriverComms(context){
    try{
        await initializeXrayStudioDriver(context);
        await sendInitialDriverMsg(context);
    }catch(err){
        throw err;
    }
}

async function initializeXrayStudioDriver(context = this){
    try{
        if (os.platform() !== 'win32' || context.setting.simulation) {
            return true
        } else {
            return await t32XrayStudioDriverHandler.initialize(context)
        }
    }catch(err){
        throw err;
    }
}

async function sendInitialDriverMsg(context = this){
    try{
        if (os.platform() !== 'win32' || context.setting.simulation) {
            return 
        } else {
            logger.info("---SEDING INITIAL DRIVER MSG----");

            let msg = {
                msgType: 'ListDrivers'
            }

            return await t32XrayStudioChannelManager.handleOutgoingChannelMsg('tab32 x ray driver channel', msg)
        }
    }catch(err){
        throw err;
    }
}

async function initializeRedisConnect(context = this){
    // var deferred = q.defer();
    try {
        if(os.platform() !== 'win32' || context.setting.simulation){
            // deferred.resolve();
            return
        }
        else{
            // t32XrayStudioRedisHandler.initialize(context)
            // .then(() => {
            //     t32XrayStudioRedisHandler.subscribeChannel("tab32 x ray driver channel");
            //     t32XrayStudioRedisHandler.subscribeChannel("t32XrayStudioAppInstance");
            //     deferred.resolve();
            // })
            // .catch((err) => {
            //     deferred.reject(err);
            // })
            await t32XrayStudioRedisHandler.initialize(context)
            t32XrayStudioRedisHandler.subscribeChannel("tab32 x ray driver channel");
            t32XrayStudioRedisHandler.subscribeChannel("t32XrayStudioAppInstance");
            return
        }
    } catch (e) {
        throw e
    }

    // return deferred.promise;
}

function getStudioContext(){
    return studioContext;
}