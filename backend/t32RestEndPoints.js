'use strict';
/**
*/

module.exports.getClinicListForXrayStudio = { routePath : '/api/tab32ac/xrayStudio/getClinicListForXrayStudio',  method : 'GET' };
module.exports.patientSearchForXrayStudio = { routePath : '/api/tab32ac/patients/patientSearchForXrayStudio/:clinic_id',  method : 'POST' };
module.exports.findPatientByIdForXrayStudio = { routePath : '/api/tab32ac/patients/findPatientByIdForXrayStudio/:clinic_id/:patient_id',  method : 'GET' };
module.exports.uploadPatientXrayForXrayStudio = { routePath : '/api/tab32ac/xrayStudio/uploadPatientXrayForXrayStudio/:clinic_id/:patient_id',  method : 'POST' };
module.exports.activateXrayStudioLicense = { routePath : '/api/tab32ac/xrayStudio/activateXrayStudioLicense',  method : 'POST' };
module.exports.getMasterSensorList = { routePath : '/api/tab32ac/xraySensor/getMasterSensorList',  method : 'GET' };

module.exports.generateNewChartId = { routePath : '/api/tab32ac/xrayStudio/generateNewChartId/:clinic_id',  method : 'PUT' };
module.exports.upsertBusinessObject = { routePath : '/api/tab32ac/xrayStudio/upsertBusinessObject/:clinic_id/:schemaName',  method : 'POST' };
// defined on the ac side already.. need to implement in backend message handler.
// module.exports.queryBusinessObject = { routePath : '/api/tab32ac/xrayStudio/queryBusinessObject/:clinic_id/:schemaName',  method : 'POST' };
// module.exports.querySingleBusinessObject = { routePath : '/api/tab32ac/xrayStudio/querySingleBusinessObject/:clinic_id/:schemaName',  method : 'POST' };
// module.exports.removeBusinessObjectById = { routePath : '/api/tab32ac/xrayStudio/removeBusinessObjectById/:clinic_id/:schemaName/:id',  method : 'PUT' };

module.exports.getPatientXrayTakenDates = { routePath: '/api/tab32ac/xrayStudio/getPatientXrayTakenDates', method: 'POST'};
module.exports.getPatientXraysByDate = { routePath: '/api/tab32ac/xrayStudio/getPatientXraysByDate', method: 'POST'};

module.exports.getPatientXrayFiles = { routePath: '/api/xrayStudio/fileUpload/getFile/:file_id/:file_type', method: 'GET'};

module.exports.getXrayTemplates = { routePath: '/api/tab32ac/xrayStudio/getXrayTemplates', method: 'POST'}
module.exports.saveXrayTemplates = { routePath: '/api/tab32ac/xrayStudio/saveXrayTemplates', method: 'POST'}

module.exports.getXrayTemplatesV2 = { routePath: '/api/tab32ac/xrayStudio/getXrayTemplatesV2', method: 'POST'}
module.exports.saveXrayTemplatesV2 = { routePath: '/api/tab32ac/xrayStudio/saveXrayTemplatesV2', method: 'POST'}

module.exports.uploadEditedXray = {
    routePath: '/api/tab32-ac/xrayStudio/uploadEditedXray/:clinic_id/:patient_id', method: 'POST'}

module.exports.uploadOriginalXray = {
    routePath: '/api/tab32-ac/xrayStudio/uploadOriginalXray/:clinic_id/:patient_id', method: 'POST'
}

module.exports.getRotationSettings = { routePath: '/api/tab32ac/xrayStudio/getRotationSettings', method: 'POST' }
module.exports.saveRotationSettings = { routePath: '/api/tab32ac/xrayStudio/saveRotationSettings', method: 'POST' }


module.exports.getXraySensorSettings = { routePath: '/api/tab32ac/xrayStudio/getXraySensorSettings', method: 'POST' }
module.exports.saveXraySensorSettings = { routePath: '/api/tab32ac/xrayStudio/saveXraySensorSettings', method: 'POST' }

module.exports.getXrayStudioLicencedClinic = { routePath: '/api/tab32ac/xrayStudio/getXrayStudioLicencedClinic', method: 'POST' }

module.exports.getXrayStudioPatientByPublicId = { routePath: '/api/tab32ac/xrayStudio/getXrayStudioPatientByPublicId', method: 'POST' }

module.exports.getXrayStudioAuthToken = {
    routePath: '/api/tab32ac/xrayStudio/getXrayStudioAuthToken',
    method: 'POST'
}

module.exports.registerXrayStudioPublicKey = {
    routePath: '/api/tab32ac/xrayStudio/registerXrayStudioPublicKey',
    method: 'POST'
}

module.exports.getPreviousXraySessionDate = {
    routePath: '/api/tab32ac/xrayStudio/getPreviousXraySessionDate/:clinic_id/:patient_id',
    method: 'POST'
}

module.exports.offlineDownload = {
    routePath: '/api/tab32ac/xrayStudio/offlineDownload',
    method: 'POST'
}

module.exports.offlineDownloadCdtCodes = {
    routePath: '/api/tab32ac/xrayStudio/offlineDownloadCdtCodes',
    method: 'POST'
}

module.exports.offlineDownloadNoteTemplates = {
    routePath: '/api/tab32ac/xrayStudio/offlineDownloadNoteTemplates',
    method: 'POST'
}

module.exports.offlineUpload = {
    routePath: '/api/tab32ac/xrayStudio/offlineUpload',
    method: 'POST'
}

module.exports.offlineDownloadXray = {
    routePath: '/api/tab32ac/xrayStudio/offlineDownloadXray',
    method: 'POST'
}

module.exports.offlineDownloadXrayInfo = {
    routePath: '/api/tab32ac/xrayStudio/offlineDownloadXrayInfo',
    method: 'POST'
}

module.exports.getXrayStudioXrayFile = {
    routePath: '/api/xrayStudio/fileUpload/getXrayStudioXrayFile/:file_id/:file_type',
    method: 'GET'
}