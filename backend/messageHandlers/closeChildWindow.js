var logger = require('electron-log');
var t32XrayStudioBrowserWindowHandler = require("../services/t32XrayStudioBrowserWindowHandler");
/**
 * Request handler for openImagingStudio
 *
 * @param studioContext our studio context (see t32RequestDispatcher)  
 *
 */

module.exports.handleRequest = handleRequest;

async function handleRequest(studioContext, requestId, requestObject) {
    try {
        logger.info('-----Closing Imaging Studio Window-----', requestObject);
      
        studioContext.browserWindow.close(requestObject.winName);
      
        return {
          requestId: requestId,
          result: 'success'
        }
        
    } catch (err) {
        logger.error("error opening new window ", err);
        throw {
            requestId: requestId,
            error: err
        };
    }
}
