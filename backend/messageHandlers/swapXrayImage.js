// const q = require("q");
const path = require("path");
const fs = require('fs-extra');
const logger = require('electron-log');

module.exports.handleRequest = handleRequest;

async function handleRequest(studioContext, requestId, requestObject){
    try {
        logger.info('...swapXrayImages...');    
        // logger.info('HERE IS THE REQUEST OBJ');
        // logger.info(requestObject);

        let fileName = requestObject.fileName;
        let originalFileName = requestObject.originalFileName;
        let originalId = requestObject.originalId;
        let replacementId = requestObject.replacementId;
    
        let currentFilePath = path.join(studioContext.setting.tab32Root, fileName);
        let currentOriginalFilePath = path.join(studioContext.setting.tab32Root, originalFileName);
        
        // logger.info("currentFilePath");
        // logger.info(currentFilePath);
        // logger.info("currentOriginalFilePath");
        // logger.info(currentOriginalFilePath);

        let newFileNames = replaceFileNames(studioContext, fileName, originalFileName, originalId, replacementId);
        let newFileName = newFileNames.fileName

        let newOriginalFileName = newFileNames.originalFileName
        let newFilePath = path.join(studioContext.setting.tab32Root, newFileName);
        let newOriginalFilePath = path.join(studioContext.setting.tab32Root, newOriginalFileName);

        // logger.info("HERE IS NEW ORIG FILE PATH");
        // logger.info(currentOriginalFilePath);
        // logger.info(newOriginalFilePath);

        //make sure that there are no identical file names that match the new filename. if there are, we rename the new file with random number in between. 
        if(fs.existsSync(newFilePath)){
            logger.info('***WE ALREADY HAVE FILENAME***')
            let paths = addRandomNumberToFileNames(newFilePath, newOriginalFilePath, replacementId);

            newFilePath = paths.newFilePath;
            newOriginalFilePath = paths.newOriginalFilePath;
        }

        if (requestObject.originalId !== requestObject.replacementId) {
            //upon rename the file system will modify the ctime of the replacement.
            //For uploading, we need to upload by the ctime and not the mtime.
            logger.info("About to swap file names");
            logger.info(currentFilePath);
            logger.info(newFilePath);
            logger.info(currentOriginalFilePath);
            logger.info(newOriginalFilePath);
            
            fs.renameSync(currentFilePath, newFilePath);

            if(fs.existsSync(currentOriginalFilePath)){
                fs.renameSync(currentOriginalFilePath, newOriginalFilePath);
            }
            else{
                fs.copySync(newFilePath, newOriginalFilePath);
            }
            // var newDate = new Date();
            // logger.info("THE NEW DATE AFTER RENAME IS ");
            // logger.info(new Date());
            // fs.utimesSync(newFilePath, newDate, newDate);
            let parsedFileName = parseFileName(studioContext.setting, newFilePath);

            return { requestId : requestId, result: parsedFileName};

        } else {
            logger.info("THE NEW DATE BEFORE MODIFYING TIME");
            logger.info(new Date());

            let newDate = new Date();
            fs.utimesSync(currentFilePath, newDate, newDate);
            fs.utimesSync(currentOriginalFilePath, newDate, newDate)
            
            return  { requestId : requestId, result: "matchingIds"};
        }
    } catch (e) {
        logger.info("error swapXrayImage", e);
        throw { requestId: requestId, error: e };
    }
}


function replaceFileNames(studioContext, fileName, originalFileName, originalId, replacementId){
    logger.debug("***ORIGINAL ID " + originalId);
    logger.debug("***REPLACEMENT ID " + replacementId);
    //if our original id is not there, meaning our file doesn't have an id, we need to matach with replacementId
    let fileNames = "";

    if(originalId == ""){
        fileNames = addIdToFileName(studioContext, fileName, replacementId);
    }
    else{
        fileNames = replaceNames(originalId, replacementId, fileName, originalFileName); 
        // fileName.replace(originalId, replacementId);
    }

    logger.debug("Here is the replacement fileName ");
    logger.debug(fileNames);
    return fileNames;
}   


function addIdToFileName(studioContext, fileName, replacementId){
    let ext = path.extname(fileName);
    logger.debug("Here is the ext : " + ext);
    let currentFolder = studioContext.setting.currentSelectedFolder;
    let randomNum = Math.random().toString().replace("0.", "");
    let newFileName = currentFolder + "/tab32Xray_" + randomNum + "_" + replacementId + ext;
    let originalFileName = currentFolder + "/originals/" + "tab32Xray_" + randomNum + "_" + replacementId + ext;

    return {fileName: newFileName, originalFileName: originalFileName};
}

function replaceNames(originalId, replacementId, fileName, originalFileName){
    let fileNames = {
        fileName: "",
        originalFileName: ""
    }

    fileNames.fileName = fileName.replace(originalId, replacementId);
    fileNames.originalFileName = originalFileName.replace(originalId, replacementId);

    return fileNames
}

function addRandomNumberToFileNames(filePath, originalFilePath, replacementId){
    // let prefix = /tab32Xray/g
    let randomNum = Math.random().toString().replace("0.", "");
    logger.debug(randomNum);
    let newSuffix = randomNum + "_" + replacementId;
    logger.debug(newSuffix);
    let newFilePath = filePath.replace(replacementId, newSuffix);
    let newOriginalFilePath = originalFilePath.replace(replacementId, newSuffix);

    return {newFilePath: newFilePath, newOriginalFilePath: newOriginalFilePath}
}

function parseFileName(setting, newFilePath){
    let tab32Root = setting.tab32Root;
    return newFilePath.replace(tab32Root, "");
}