const logger = require('electron-log');
const t32XrayStudioWidowActions = require("../services/t32XrayStudioWindowActions");
/**
 * Request handler for openImagingStudio
 *
 * @param studioContext our studio context (see t32RequestDispatcher)  
 *
 */

module.exports.handleRequest = handleRequest;

async function handleRequest(studioContext, requestId, requestObject) {
    try {
        logger.info('-----Incomming window comms request-----', requestObject);

        if (requestObject.msg == 'triggerXrayTemplate') {
            t32XrayStudioWidowActions.triggerXrayTemplate(studioContext, requestObject.additionalInfo);
        }

        return {requestId: requestId, result: "ok"}
    } catch (err) {
        logger.error("error opening new window ", err);
        throw {
            requestId: requestId,
            error: err
        };
    }
}
