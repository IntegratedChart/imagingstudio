const t32XrayStudioActionHandler = require('../services/t32XrayStudioActionHandler');


module.exports.handleRequest = handleRequest;

/**
 *
 * @param studioContext our studio context (see t32RequestDispatcher)  
 * @param requestObject : 
 *              {
 *                  actionObj : actionObj
 *              }
 */
async function handleRequest(studioContext, requestId, requestObject) {
    try{        
        let patient = await t32XrayStudioActionHandler.runGetPatientAction(requestObject.actionObj);

        let wrapper = {
            requestId: requestId,
            result: patient
        }

        return wrapper
    }
    catch(err){
        throw {
            requestId: requestId,
            error: err
        };
    }
}
