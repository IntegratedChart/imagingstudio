// const q = require("q");
const path = require("path");
const fsExtra = require('fs-extra');
const logger = require('electron-log');
const t32XrayStudioSettingsService = require('../services/t32XrayStudioSettingsService');

module.exports.handleRequest = handleRequest;

/**
* Request handler for getXrayStudioSetting
*
* @param studioContext our studio context (see t32RequestDispatcher)
* @return @type {Object}
*/
async function handleRequest(studioContext, requestId){

   try {
      logger.info('... getXrayStudioSetting...');
      let setting = await t32XrayStudioSettingsService.getSettings()
      
      return { requestId: requestId, result: setting };

   } catch (e) {
      logger.info("error getXrayStudioSettings ", e);
      throw {requestId: requestId, error: e};
   }
}
