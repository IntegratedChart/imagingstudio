
// const q = require("q");
const t32GoogleAuthApi = require('../t32GoogleAuthApi');
const logger = require('electron-log');
const {shell} = require('electron')


module.exports.handleRequest = handleRequest;

// var theContext;

/**
* Handle 'getGoogleAuthUrl' request from the client side.
*
* @param studioContext our studio context (see t32XrayStudioMain)
*
* @return the google authCode will be set in the studioContext when google callback returns
*/
async function handleRequest( studioContext, requestId ){
  try {
    logger.info('... getGoogleAuthUrl ... ')
    let googleAuthUrl = await t32GoogleAuthApi.getGoogleOAuthRequestUrl(studioContext.googleCallbackUrl);
    
    shell.openExternal(googleAuthUrl);
    
    return { requestId : requestId, result : googleAuthUrl };
  } catch (e) {
    logger.error('...... getGoogleAuthUrl err = ' + err);
    throw { requestId: requestId, error: err };
  }
}