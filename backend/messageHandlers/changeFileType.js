
// const q = require("q");
const path = require("path");
const fs = require('fs-extra');
const moment = require('moment');
const logger = require('electron-log');


module.exports.handleRequest = handleRequest;


/**
* Handle 'changeFileType' request from the client side.
*
* @param studioContext our studio context (see t32XrayStudioMain) 
* @param requestObject : 
            var requestObject = { 
               fileName   : // path to the file
               fileType   : // desired file type.
            }
*/
async function handleRequest( studioContext, requestId, requestObject ){

   try { 
      logger.debug('changeFileType: fileName/fileType : ' + requestObject.fileName + '/' + requestObject.fileType);
      
      let orgName = path.join(studioContext.setting.tab32Root, requestObject.fileName);
      let newName = path.join(studioContext.setting.tab32Root, buildModifiedName(requestObject.fileName, requestObject.fileType));
      
      return await new Promise((resolve, reject) => {
         fs.move(orgName, newName, function (err) {
            if (err) {
               logger.error(err);
               // logger.error(err.stack);
               reject({ requestId: requestId, error: err });
            }
            else {
               console.log('rename successfull')
               resolve({ requestId: requestId });
            }
         });
      })

   } catch (e) {
      logger.info("error changing file type ", e);
      throw { requestId: requestId, error: e }
   }
   // var deferred = q.defer();

   // logger.debug('changeFileType: fileName/fileType : ' + requestObject.fileName + '/' + requestObject.fileType );
   
   // var orgName = path.join(studioContext.setting.tab32Root, requestObject.fileName );
   // var newName = path.join(studioContext.setting.tab32Root, buildModifiedName(requestObject.fileName, requestObject.fileType ) );

   // fs.move(orgName, newName, function(err){
   //    if ( err )
   //    {
   //       logger.error(err);
   //       // logger.error(err.stack);
   //        deferred.reject({ requestId: requestId, error: err });
   //    }
   //    else
   //    {
   //       console.log('rename successfull')
   //       deferred.resolve({requestId : requestId});
   //    }
   // });
   // return deferred.promise;
}

/*
* we will code the type into the file name
* 
* by default, files are for xray images.  For non-xray images
* file name will be xxx_type_fileType_type.jpg.
*
*/
function buildModifiedName( fileName, newFileType )
{
   var newFileName;

   var fileExt = path.extname(fileName);

   var nameWithNoExt = fileName.substring(0, fileName.lastIndexOf('.'));
   var tokens = nameWithNoExt.split('_type_');

   if ( newFileType == 'xray' )
   {
      newFileName = tokens[0] + fileExt;
   }
   else
   {
      newFileName = tokens[0] + '_type_' + newFileType + '_type_' + fileExt;
   }
   return newFileName;
}