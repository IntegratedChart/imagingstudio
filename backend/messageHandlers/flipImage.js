// const q = require("q");
const path = require("path");
const fs = require('fs-extra');
const logger = require('electron-log');
const t32XrayStudioImageManipulation = require("../services/t32XrayStudioImageManipulation");



module.exports.handleRequest = handleRequest;

/**
 *
 * @param studioContext our studio context (see t32RequestDispatcher)  
 * @param requestObject
 */
async function handleRequest(studioContext, requestId, requestObject) {
  try {    
    // logger.debug(requestObject.imageUrl)
    let tab32Root = studioContext.setting.tab32Root;
    let fullFilePath = path.join(tab32Root, requestObject.imageUrl);
    let originalFilePath = path.join(tab32Root, requestObject.originalImageUrl);
    let horizontalFlip = requestObject.horizontalFlip;
    let verticalFlip = requestObject.verticalFlip;
  
    logger.debug('flipImage, filePath : ' + fullFilePath);
    logger.debug('flipImage, originalFilePath : ' + originalFilePath);
  
  
    //if we have an original file
    if (!fs.existsSync(originalFilePath)) {
      fs.copySync(fullFilePath, originalFilePath)
    }
  
    await t32XrayStudioImageManipulation.flipImage(fullFilePath, horizontalFlip, verticalFlip)
    t32XrayStudioImageManipulation.flipImage(originalFilePath, horizontalFlip, verticalFlip)
    
    return { requestId: requestId, result: "Ok" }
  } catch (e) {
    logger.info("error flipImage ", e);
    throw {requestId: requestId, error: e}
  }

}

// /**
//  *
//  * @param studioContext our studio context (see t32RequestDispatcher)  
//  * @param requestObject
//  */
// function handleRequest(studioContext, requestId, requestObject) {
//   var deferred = q.defer();

//   // logger.debug(requestObject.imageUrl)
//   var tab32Root = studioContext.setting.tab32Root;
//   var fullFilePath = path.join(tab32Root, requestObject.imageUrl);
//   var originalFilePath = path.join(tab32Root, requestObject.originalImageUrl);
//   var horizontalFlip = requestObject.horizontalFlip;
//   var verticalFlip = requestObject.verticalFlip;

//   logger.debug('flipImage, filePath : ' + fullFilePath);
//   logger.debug('flipImage, originalFilePath : ' + originalFilePath);


//   //if we have an original file
//   if (!fs.existsSync(originalFilePath)) {
//     try {
//       fs.copySync(fullFilePath, originalFilePath)
//     }
//     catch (err) {
//       deferred.reject({ requestId: requestId, error: 'failed to copy original file' });
//       return
//     }
//   }

//   flipImage(requestId, fullFilePath, horizontalFlip, verticalFlip)
//   .then(function () {

//     var wrapper = { requestId: requestId, result: "Ok" }
//     deferred.resolve(wrapper);
//     return flipImage(requestId++, originalFilePath, horizontalFlip, verticalFlip)
//   })
//   .catch(function (err) {
//     logger.error(err);
//     deferred.reject(err);
//   })

//   return deferred.promise;
// }

// function flipImage(requestId, fullFilePath, horizontalFlip, verticalFlip) {
//   var deferred = q.defer();
//   Jimp.read(fullFilePath).then(function (img) {
//     logger.debug(img);
//     img.flip(horizontalFlip, verticalFlip).getBuffer(Jimp.AUTO, function (err, buf) {
//       if (err) {
//         deferred.reject({requestId: requestId, error: err});
//       }
//       else {
//         fs.writeFile(fullFilePath, buf, function (err) {
//           if (err) {
//             logger.error(err);
//             logger.error(err.stack);
//             deferred.reject({ requestId: requestId, error: err });
//           }
//           else {
//             deferred.resolve();
//           }
//         });
//       }
//     });
//   }).catch(function (err) {
//     console.log(err)
//     deferred.reject({ requestId: requestId, error: err });
//   });

//   return deferred.promise;
// }

