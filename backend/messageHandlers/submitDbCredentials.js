const t32XrayStudioEncryptionHandler = require('../services/t32XrayStudioEncryptionHandler');
const fs = require("fs-extra");
const t32xrayStudioFs = require('../services/t32XrayStudioFs');
const t32XrayStudioDbHanlder = require("../services/t32XrayStudioDbHandler");
const logger = require("electron-log");
module.exports.handleRequest = handleRequest;
const path = require("path");

async function handleRequest(studioContext, requestId, requestObject){

    try {
        //attempt to establish connection with db. returns dbName
        logger.info('request obj ', requestObject);

        // if (studioContext.db.isConnected) {
        //     logger.info("db already connected ")
        //     return {
        //         requestId: requestId,
        //         result: "ok"
        //     }
        // }

        let credentialsFilePath = path.join(studioContext.theApp.getAppHome(), "\\tab32\\dbCredentials.txt");

        let returnedCreds = await t32XrayStudioDbHanlder.actions.onCredentialSubmission(requestObject, credentialsFilePath);
        
        
        //ecrypt db credentials (username, password, database)
        let encryptedCreds = t32XrayStudioEncryptionHandler.encryptMsg(studioContext.rsaKeys.publicKey, JSON.stringify(returnedCreds)); 



        fs.writeFileSync(credentialsFilePath, encryptedCreds);
        // await t32xrayStudioFs.writeJson(credentialsFilePath, encryptedCreds);

        return {
            requestId: requestId,
            result: "ok"
        }
    } catch (e) {
        logger.error('error with credential submission ', e)
        throw { requestId: requestId, error: e };
    }    
}
