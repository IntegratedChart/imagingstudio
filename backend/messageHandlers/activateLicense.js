
// const q = require("q");
const t32RestServices = require('../t32RestServices');
const t32RestEndPoints = require('../t32RestEndPoints');
const logger = require('electron-log');


module.exports.handleRequest = handleRequest;


/**
* Handle 'activateLicense' request from the client side.
*
* @param studioContext our studio context (see t32XrayStudioMain) 
* @param searchObject : {
            clinic_id : 
            patient_id : 
         }
*/
async function handleRequest( studioContext, requestId, license ){
   // var deferred = q.defer();
   try {
      let urlParam = null;
      let headers = null;
      let postBody = {license : license, machineId : studioContext.setting.machineId};
   
      logger.info('... activateLicense license/machineId : ' + license + '/' + studioContext.setting.machineId );

      let result = await t32RestServices.execForResult( 
         t32RestEndPoints.activateXrayStudioLicense.routePath, 
         t32RestEndPoints.activateXrayStudioLicense.method,
         urlParam,
         postBody,
         headers)

      return { requestId: requestId, result: result };
      // deferred.resolve(wrapper);
      // .then(function(result){
      //    logger.info('...... EHR response for activateLicence : ' + result );
         
      //    var wrapper = { requestId : requestId, result : result};
      //    deferred.resolve(wrapper);
      // })
      // .catch(function(err){
      //    logger.error('...... error for activateLicence : ' + err );
      //    deferred.reject(err);
      // })
   } catch (e) {
      logger.info("error activating licence ", e)
      throw {requestId: requestId, error: e };
   }

   // return deferred.promise;
}
