
const os = require('os');
const logger = require('electron-log');
// const q = require('q');


module.exports.handleRequest = handleRequest;

/**
 * Get the os information from machine
 * @param {*} studioContext 
 * @param {*} requestId 
 */
async function handleRequest(studioContext, requestId) {
    try {
        logger.info('... Getting Os info ...')
        let platform = os.platform();
        return { requestId: requestId, result: platform };
    } catch (e) {
        logger.info('error getOSInfo', e)
        throw { requestId: requestId, error: e }
    }
}