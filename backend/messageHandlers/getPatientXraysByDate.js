// const q = require("q");
// const _ = require("lodash");
const moment = require('moment');
const t32RestServices = require('../t32RestServices');
const t32RestEndPoints = require('../t32RestEndPoints');
const logger = require('electron-log');

module.exports.handleRequest = handleRequest;

/**
 * 
 * @param {*} studioContext 
 * @param {*} requestId 
 * @param {*} requestObject 
 */
async function handleRequest(studioContext, requestId, requestObject){

    try {
        logger.debug('...getPatientXraysByDate');
    
        let postBody = {
          clinic_id:  requestObject.clinic_id,
          patient_id: requestObject.patient_id,
          date:       requestObject.date
        }
    
        let headers = null;
        let urlParam = null;
        let formData = null;
    
        let xrays = await t32RestServices.execForResult(
          t32RestEndPoints.getPatientXraysByDate.routePath, 
          t32RestEndPoints.getPatientXraysByDate.method, 
          urlParam, 
          postBody, 
          headers,
          formData)


        xrays = sortDates(xrays);
        logger.debug("WE ARE DONE SORTING");
        logger.debug(xrays);
        
        return { requestId: requestId, result: xrays }
    } catch (e) {
        logger.info("error getPatientXrayByDate ", e);
        throw {requestId: requestId, error: e}
    }
}

function sortDates(xrays){

    logger.debug("WE ARE SORTING THE DATES");

    return xrays.sort(function(xrayA, xrayB){
        var xrayADate = new Date(moment(xrayA.metadata.image_taken_date));
        var xrayBDate = new Date(moment(xrayB.metadata.image_taken_date));

        // logger.debug("The date of xrayA");
        // logger.debug(xrayA);
        // logger.debug("the date of xrayB");
        // logger.debug(xrayB);

        if (xrayADate > xrayBDate) {
            return -1;
        }
        if (xrayADate < xrayBDate) {
            return 1;
        }
        return 0;
    });

    // var sortedXrays = _.sortBy(xrays, function(xray){
    //     logger.debug("The xray being sorted");
    //     logger.debug(xray);
    //     logger.debug("The image taken date")
    //     logger.debug(xray.metadata.image_taken_date);

    //     return new moment (xray.metadata.image_taken_date)
    // }).reverse();

    // logger.debug("WE ARE DONE SORTING");
    // logger.debug(sortedXrays)

    // return sortedXrays;
}
