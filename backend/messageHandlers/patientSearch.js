
// const q = require("q");
const t32RestServices = require('../t32RestServices');
const t32RestEndPoints = require('../t32RestEndPoints');
const logger = require('electron-log');


module.exports.handleRequest = handleRequest;


/**
* Handle 'patientSearch' request from the client side.
*
* @param studioContext our studio context (see t32XrayStudioMain) 
* @param searchObject : {
            clinic_id : 
            searchInput :
         }
*/
async function handleRequest( studioContext, requestId, searchObject ){

   try {
      logger.debug('patientSearch : ' + searchObject.searchInput );
   
      let urlParam = {clinic_id : searchObject.clinic_id};
      let postBody = {searchInput : searchObject.searchInput };
      let headers = {'clinic-id' : searchObject.clinic_id };
   
      let patientList = await t32RestServices.execForResult( 
         t32RestEndPoints.patientSearchForXrayStudio.routePath, 
         t32RestEndPoints.patientSearchForXrayStudio.method,
         urlParam,
         postBody,
         headers)
      return { requestId, requestId, result : patientList};

   } catch (e) {
      logger.info("error patientSearch ", e);
      throw { requestId: requestId, error: e }
   }

}
