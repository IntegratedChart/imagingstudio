
// const q = require("q");
const t32RestServices = require('../t32RestServices');
const t32RestEndPoints = require('../t32RestEndPoints');
const logger = require('electron-log');


module.exports.handleRequest = handleRequest;


/**
* Handle 'getMasterSensorList' request from the client side.
*
* @param studioContext our studio context (see t32XrayStudioMain) 
* @param searchObject : {
            clinic_id : 
            patient_id : 
         }
*/
async function handleRequest( studioContext, requestId ){
   try {
      logger.debug('... getMasterSensorList ... ')
      let urlParam = null;
      let headers = null;
      let postBody = {};
   
      let result = await t32RestServices.execForResult( 
         t32RestEndPoints.getMasterSensorList.routePath, 
         t32RestEndPoints.getMasterSensorList.method,
         urlParam, 
         postBody, 
         headers, 
         null);

      return { requestId: requestId, result: result };

   } catch (e) {
      logger.info("error getMasterSensorList ", e)
      throw { requestId: requestId, error: e }
   }
}
