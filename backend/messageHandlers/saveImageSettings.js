const {writeJson} = require('../services/t32XrayStudioFs');
// const q = require("q");
const path = require("path");
const logger = require('electron-log');

module.exports.handleRequest = handleRequest;

async function handleRequest(studioContext, requestId, settings) {
    try {
        let settingsPath = studioContext.setting.currentSelectedFolder + "/settings.json"
        let settingsFullPath = path.join(studioContext.setting.tab32Root, settingsPath);
    
        logger.info('SAVING IMAGE SETTINGS');
    
        await writeJson(settingsFullPath, settings);

        return { requestId: requestId, result: settings }
    } catch (e) {
        logger.info("error saveImageSettings ", e)
        throw {requestId: requestId, error: e};
    }
}
