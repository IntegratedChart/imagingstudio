'use strict';
const t32RestServices = require('../t32RestServices');
const t32RestEndPoints = require('../t32RestEndPoints');
const logger = require('electron-log');
const t32OfflineDataHandler = require('../services/t32OfflineDataHandler');

module.exports.handleRequest = handleRequest;

/**
 * @async
 * @param studioContext our studio context (see t32RequestDispatcher)
 * @msgContent download query
 */
async function handleRequest(studioContext, requestId, msgContent) {
    try {
        logger.info('...offline download cdt codes reqeuestId', requestId);

        const result = await t32RestServices.execForResult(
            t32RestEndPoints.offlineDownloadNoteTemplates.routePath,
            t32RestEndPoints.offlineDownloadNoteTemplates.method,
            null,
            msgContent,
            null,
            null,
        );

        // logger.info('...offline downloaded note templates', result);

        // const saveResult = await t32OfflineDataHandler.save(result, 'note_templates', '_id');
        // const saveResult = await t32OfflineDataHandler.saveNoteTemplates(result, 'note_templates', msgContent.clinic_id);
        const saveResult = await t32OfflineDataHandler.saveNoteTemplates(result);

        return { requestId: requestId, result: true };
    } catch (error) {
        logger.error('offline download note templates error', error);
        // throw error;
        throw { requestId: requestId, error: error };
    }
}