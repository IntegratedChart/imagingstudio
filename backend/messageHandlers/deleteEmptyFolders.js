// const q = require("q");
// const _ = require("lodash");
const path = require("path");
const fs = require('fs-extra');
const logger = require('electron-log');
// const async = require('async');
// var studioContext = require('../t32XrayStudioMain');

module.exports.handleRequest = handleRequest;

/**
* @param studioContext our studio context (see t32XrayStudioMain)  
*/
async function handleRequest(studioContext, requestId, requestObject){
   try {
    logger.info("----DELETE EMPTY FOLDERS-----");

    var pathsToRemove = requestObject.pathsToRemove;
    logger.info(pathsToRemove);
  
    if(pathsToRemove && pathsToRemove.length){

      for (let i = 0; i < pathsToRemove.length; i++) {
        var pathToRemove = path.normalize(pathsToRemove[i]);

        await deleteFolderFilesRecursive(pathToRemove);

        if (fs.existsSync(pathToRemove)) {
          await removeAllDirectories(pathToRemove)
          logger.info("WE HAVE DELETED ALL THE FOLDERS"); 
        }
      }
    } 

    return { requestId: requestId, result: 'ok' };

  } catch (e) {
    logger.info("error with delete empty folder ", e)
    throw { requestId: requestId, error: e };
  }
}

/**
 * Recursively remove files from all folders
 * @param {*} path File path to start recurssive delete from 
 */
var deleteFolderFilesRecursive = async function (path) {
	try {
		if (fs.existsSync(path)) {
			let files = fs.readdirSync(path);
			for (let i = 0; i < files.length; i++) {
				let curPath = path + "/" + files[i];

				if (fs.lstatSync(curPath).isDirectory()) {
					await deleteFolderFilesRecursive(curPath)
				} else {
					await fs.unlink(curPath);
				}

				pause(i, 500)
			}
			return 
		}
		
		return

	} catch (e) {
		throw e
	}
}

function pause(i, ms) {
	setTimeout(function () {
		// Add tasks to do 
		logger.info("resuming")
	}, ms * i);
} 

async function removeAllDirectories(folder) {
	// var deferred = q.defer();
	try {
		var deleteArray = [];
		cleanEmptyFoldersRecursively(folder, deleteArray);
		deleteArray = deleteArray.reverse();
		deleteArray.push(folder);
		
		for (let i = 0; i < deleteArray.length; i++) {
			let filePath = deleteArray[i];

			await new Promise((resolve, reject) => {
				fs.rmdir(filePath, function (err) {
					if (err) {
						reject(err);
					}
					else {
						setTimeout(function () {
							resolve();
						}, 500)
					}
				})
			})
		}
		
		return
	} catch (e) {
		throw e
	}

	// return deferred.promise;
}

function cleanEmptyFoldersRecursively(folder, deleteArray) {
	logger.info('INSIDE THE CLEAN EMPTY FOLDERS ' + folder);
	var files = fs.readdirSync(folder);
	if (files.length > 0) {
		files.forEach(function (file) {
			var fullPath = path.join(folder, file);
			deleteArray.push(fullPath);
			cleanEmptyFoldersRecursively(fullPath, deleteArray);
		});
	} else {
		return
	}
}


// /**
// * @param studioContext our studio context (see t32XrayStudioMain)  
// */
// function handleRequest(studioContext, requestId, requestObject){
//    var deferred = q.defer();

// //    var tab32Root = studioContext.setting.tab32Root;
// //    var imageFileExtList = ['.jpg', '.jpeg', '.png', '.tiff', '.gif'];

// //    var dirList = fs.readdirSync(tab32Root)
// // 				 .filter(file => fs.lstatSync(path.join(tab32Root, file)).isDirectory());
// 	logger.info("----DELETE EMPTY FOLDERS-----")
// 	var pathsToRemove = requestObject.pathsToRemove;
// 	logger.info(pathsToRemove);
// 	// logger.info("----STOPING CHOKIDAR-----");
// 	// studioContext.stopDirWatcher();
// 	if(pathsToRemove && pathsToRemove.length){
// 		async.eachSeries(pathsToRemove, function (fullPath, callback) {
// 			logger.info('****FULL PATH TO REMOVE***');
// 			logger.info(fullPath);
			
// 			// var originalsPath = path.normalize(fullPath + "/originals");
// 			var pathsToRemove = path.normalize(fullPath);

// 			deleteFolderFilesRecursive(pathsToRemove)
// 			.then(function () {
// 				logger.info("WE HAVE DELETED ALL THE FILES")
// 				if (fs.existsSync(pathsToRemove)) {
// 					removeAllDirectories(pathsToRemove)
// 					.then(function () {
// 						logger.info("WE HAVE DELETED ALL THE FOLDERS");
// 						callback();
// 					})
// 					.catch(function (err) {
// 						logger.error(err);
// 						callback(err);
// 					})
// 				}
// 			})
// 			.catch(function (err) {
// 				logger.error(err);
// 				callback(err);
// 			})
// 		}, function (err) {
// 			if (err) {
// 				// logger.info("----Start CHOKIDAR-----");
// 				// studioContext.watchXrayDirectory();
// 				deferred.reject({requestId: requestId, error: err});
// 			}
// 			else {
// 				// logger.info("----Start CHOKIDAR-----");
// 				// studioContext.watchXrayDirectory();
// 				deferred.resolve({requestId: requestId, result: 'ok'});
// 			}
// 		})
// 	}
// 	else{
// 		// logger.info("----Start CHOKIDAR-----");
// 		// studioContext.watchXrayDirectory();
// 		deferred.resolve({ requestId: requestId, result: 'ok' });
// 	}

// 	// async.eachSeries(dirList, function (dirPath, callback) {
// 	// 	var directoryPath = dirPath;
// 	// 	var fullPath = path.join(tab32Root, dirPath);
// 	// 	logger.info('****FULL PATH***');
// 	// 	logger.info(fullPath);
// 	// 	logger.info('***FILE LIST***');
// 	// 	logger.info(fs.readdirSync(fullPath));
// 	// 	var fileList = fs.readdirSync(fullPath)
// 	// 		.filter(file => _.contains(imageFileExtList, path.extname(file)));

// 	// 	var originalsPath = fullPath + "/originals";
// 	// 	var existsOriginalPath = fs.existsSync(originalsPath);

// 	// 	operateOnFolder(fullPath, fileList, originalsPath, existsOriginalPath)
// 	// 		.then(function () {
// 	// 			callback();
// 	// 		})
// 	// 		.catch(function (err) {
// 	// 			callback(err);
// 	// 		})
// 	// }, function (err) {
// 	// 	if (err) {
// 	// 		deferred.reject(err);
// 	// 	}
// 	// 	else {
// 	// 		deferred.resolve();
// 	// 	}
// 	// })

// 	// _.each(dirList, function(dirPath){
// 	// 	var directoryPath = dirPath;
// 	// 	var fullPath = path.join(tab32Root, dirPath);
// 	// 	logger.info('****FULL PATH***');
// 	// 	logger.info(fullPath);
// 	// 	logger.info('***FILE LIST***');
// 	// 	logger.info(fs.readdirSync(fullPath));
// 	// 	var fileList = fs.readdirSync(fullPath)
// 	// 						.filter( file => _.contains(imageFileExtList, path.extname(file)));

// 	// 	var originalsPath = fullPath +  "/originals";
// 	// 	var existsOriginalPath = fs.existsSync(originalsPath);
		
// 	// 	operateOnFolder(fullPath, fileList, originalsPath, existsOriginalPath)
// 	// 	.then(function(){
// 	// 		deferred.resolve();
// 	// 	})
// 	// 	.catch(function(err){
// 	// 		deferred.reject();
// 	// 	})
// 	// });	 	

//    return deferred.promise;
// }

/**
 * Recursively remove files from all folders
 * @param {*} path File path to start recurssive delete from 
 */
// var deleteFolderFilesRecursive = function (path) {
// 	var deferred = q.defer();
// 	if (fs.existsSync(path)) {

// 		async.each(fs.readdirSync(path), function (file, callback) {
// 			var curPath = path + "/" + file
// 			if (fs.lstatSync(curPath).isDirectory()) {
// 				deleteFolderFilesRecursive(curPath)
// 				.then(function(){
// 					console.log("Deleted files 1");
// 					callback()
// 				});
// 			}
// 			else {
// 				fs.unlink(curPath).then(function(){
// 					setTimeout(function(){
// 						callback();
// 					}, 500)
// 				});
// 			}
// 		}, function (err) {
// 			if (err) {
// 				deferred.reject(err);
// 			}
// 			else {
// 				console.log("Deleted files 2");
// 				deferred.resolve()
// 			}
// 		})
// 	}
// 	else {
// 		deferred.resolve();
// 	}

// 	return deferred.promise;
// };

/**
 * Recursively remove all dir folders
 * @param {*} folder dir name to start with
 */
// function removeAllDirectories(folder) {
// 	var deferred = q.defer();

// 	var deleteArray = [];
// 	cleanEmptyFoldersRecursively(folder, deleteArray);
// 	deleteArray = deleteArray.reverse();
// 	deleteArray.push(folder);
	
// 	async.eachSeries(deleteArray, function (filePath, callback) {
// 		logger.info('THIS IS THE FILE PATH THAT WE REMOVE')
// 		logger.info(path.normalize(filePath));
// 		logger.info('THE CONTENTS OF THE FILE');
// 		logger.info(fs.readdirSync(filePath));

// 		fs.rmdir(filePath, function(err){
// 			if(err){
// 				callback(err);
// 			}
// 			else{
// 				setTimeout(function(){
// 					callback();
// 				}, 500)
// 			}
// 		})
// 		// var repeatCheck = setInterval(function(){
// 		// 	try{
// 		// 		if(fs.readdirSync(filePath).length <= 0){
// 		// 			clearInterval(repeatCheck);
// 		// 			fs.rmdirSync(filePath);
// 		// 			callback()
// 		// 		}
// 		// 		else{
// 		// 			logger.info("We need to wait to clear content");
// 		// 		}
// 		// 	}
// 		// 	catch(err){
// 		// 		logger.error(err);
// 		// 	}
// 		// }.bind(filePath), 500)
		
// 	}, function(err){
// 		if(err){
// 			deferred.reject(err);
// 		}
// 		else{
// 			deferred.resolve();
// 		}
// 	})

	
// 	return deferred.promise;
// }

// function cleanEmptyFoldersRecursively(folder, deleteArray) {
// 	logger.info('INSIDE THE CLEAN EMPTY FOLDERS ' + folder);
// 	var files = fs.readdirSync(folder);
// 	if (files.length > 0) {
// 		files.forEach(function (file) {
// 			var fullPath = path.join(folder, file);
// 			deleteArray.push(fullPath);
// 			cleanEmptyFoldersRecursively(fullPath, deleteArray);
// 		});
// 	}
// }


// function operateOnFolder(fullPath, fileList, originalsPath, existsOriginalPath){
// 	var deferred = q.defer();
// 	var imageFileExtList = ['.jpg', '.jpeg', '.png', '.tiff', '.gif'];

// 	if (fileList.length == 0 && !existsOriginalPath) {
// 		deleteFolderFilesRecursive(fullPath)
// 		.then(function () {
// 			logger.info("WE HAVE DELETED ALL THE FILES")
// 			if (fs.existsSync(fullPath)) {
// 				removeAllDirectories(fullPath)
// 				.then(function () {
// 					logger.info("WE HAVE DELETED ALL THE FOLDERS");
// 					deferred.resolve();
// 				})
// 				.catch(function (err) {
// 					logger.error(err);
// 					deferred.reject();
// 				})
// 			}
// 		})
// 		.catch(function (err) {
// 			logger.error(err);
// 			deferred.reject();
// 		})
// 	}
// 	else if (fileList.length > 0 && !existsOriginalPath) {
// 		_.each(fileList, function (file) {
// 			var fullFilePath = path.join(fullPath, file);
// 			var fullOriginalsFilePath = originalsPath + "/" + file;

// 			logger.info(fullFilePath);
// 			logger.info(fullOriginalsFilePath);
// 			try {
// 				fs.copySync(fullFilePath, fullOriginalsFilePath);
// 			}
// 			catch (err) {
// 				logger.info('remove sync error while deleting originals folder')
// 				logger.error(err);
// 				// deferred.reject({ requestId: requestId, error: err });
// 				// return
// 			}
// 		});
// 		deferred.resolve();
// 	}
// 	else {
// 		try {
// 			var originalsFileList = fs.readdirSync(originalsPath)
// 				.filter(file => _.contains(imageFileExtList, path.extname(file)));
// 		}
// 		catch (err) {
// 			logger.info('remove sync error while reading originalsPath folder')
// 			logger.info(err);
// 			deferred.reject()
// 		}

// 		try {
// 			fileList = fs.readdirSync(fullPath)
// 				.filter(file => _.contains(imageFileExtList, path.extname(file)));
// 		}
// 		catch (err) {
// 			logger.info('remove sync error while reading fullPath folder')
// 			logger.info(err);
// 			deferred.reject()
// 		}

// 		if (originalsFileList.length > 0 && fileList.length > 0) {
// 			//do nothing
// 			logger.info("We do nothing ");
// 			deferred.resolve();
// 		}
// 		else if (originalsFileList.length == 0 && fileList.length == 0) {
// 			deleteFolderFilesRecursive(fullPath)
// 				.then(function () {
// 					logger.info("WE HAVE DELETED ALL THE FILES")
// 					if (fs.existsSync(fullPath)) {
// 						removeAllDirectories(fullPath)
// 							.then(function () {
// 								logger.info("WE HAVE DELETED ALL THE FOLDERS");
// 								deferred.resolve();
// 							})
// 							.catch(function (err) {
// 								logger.error(err);
// 								deferred.reject();
// 							})
// 					}
// 					else {
// 						deferred.resolve();
// 					}
// 				})
// 				.catch(function (err) {
// 					logger.error(err);
// 					deferred.reject();
// 				})
// 		}
// 		else if (originalsFileList.length > 0 && fileList.length == 0) {
// 			_.each(originalsFileList, function (file) {
// 				var fullOriginalsFilePath = path.join(originalsPath, file);
// 				var fullFilePath = path.join(fullPath, file);

// 				logger.info(fullFilePath);
// 				logger.info(fullOriginalsFilePath);
// 				try {
// 					fs.copySync(fullOriginalsFilePath, fullFilePath);
// 				}
// 				catch (err) {
// 					logger.info('remove sync error while deleting originals folder')
// 					logger.info(err);
// 					deferred.reject()
// 				}
// 			});
// 			deferred.resolve()
// 		}
// 		else if (fileList.length > 0 && originalsFileList.length == 0) {
// 			_.each(fileList, function (file) {
// 				var fullFilePath = path.join(fullPath, file);
// 				var fullOriginalsFilePath = originalsPath + "/" + file;

// 				logger.info(fullFilePath);
// 				logger.info(fullOriginalsFilePath);
// 				try {
// 					fs.copySync(fullFilePath, fullOriginalsFilePath);
// 				}
// 				catch (err) {
// 					logger.info('remove sync error while deleting originals folder')
// 					logger.info(err);
// 					deferred.reject()
// 				}
// 			});
// 			deferred.reject();
// 		}
// 	}

// 	return deferred.promise;
// }
