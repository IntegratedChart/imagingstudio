// const q = require("q");
const t32RestServices = require('../t32RestServices');
const t32RestEndPoints = require('../t32RestEndPoints');
const logger = require('electron-log');

module.exports.handleRequest = handleRequest;

async function handleRequest(studioContext, requestId, requestObject){
    try {

        logger.debug('...getXrayTemplates V2');
    
        let postBody = {
            clinic_id: requestObject.clinic_id
        }
    
        let headers = null;
        let urlParam = null;
        let formData = null;
    
        let templates = await t32RestServices.execForResult(
          t32RestEndPoints.getXrayTemplatesV2.routePath, 
          t32RestEndPoints.getXrayTemplatesV2.method, 
          urlParam, 
          postBody, 
          headers,
          formData)
          
        return {
            requestId: requestId,
            result: templates
        }
    } catch (e) {
        logger.info("error getXrayTemplates ", e);
        throw {requestId: requestId, error: e};
    }

}