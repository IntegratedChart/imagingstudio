const logger = require('electron-log');
// const q = require('q');

/**
 * Request handler for reloadXrayStudioSetting
 * @param studioContext our studio context (see t32RequestDispatcher)  
 */
module.exports.handleRequest = handleRequest;
async function handleRequest(studioContext, requestId) {
	try {
		logger.debug('-----EXITING APP WINDOW-----');
		return studioContext.browserWindow.exitApp();
	} catch (e) {
		logger.error("failed to exit app window ", e);
	}
}
