// const q = require("q");
const path = require("path");
// const mkdirp = require('mkdirp');
const t32XrayStudioFs = require("../services/t32XrayStudioFs");
const logger = require('electron-log');
const t32XrayStudioSettingsService = require('../services/t32XrayStudioSettingsService');


module.exports.handleRequest = handleRequest;

/**
 * Request handler for saveXrayStudioSetting
 *
 * @param studioContext our studio context (see t32RequestDispatcher)  
 *
 * @param setting
 *      {
 *          ehrServer       : String
 *          tab32Root       : String,
 *          localTcpPort    : String    
 *      }
 */
async function handleRequest(studioContext, requestId, setting){
   // var deferred = q.defer();

   // make sure all directory existing
   try{
      t32XrayStudioFs.ensureDirSync(setting.tab32Root);
      t32XrayStudioSettingsService.saveSettings(setting);
      return { requestId: requestId, result: 'ok' }
   }
   catch(err){
      logger.info("There was an error trying to ensure dir sync");
      logger.error(err);
      // deferred.reject(err);
      return { requestId: requestId, error: err }
   }
   
   // var settingFilePath = path.join(setting.tab32Root, 'tab32XrayStudioSetting.json');

   // update log level
   //  logger.transports.file.level = setting.logLevel; 
   //  logger.transports.console.level = setting.logLevel;   

    //    logger.info("the settings ", setting);
   // t32XrayStudioSettingsService.saveSettings(setting);
   
    // Testing purposes, do not remove 
    // t32XrayStudioMain.updateStudioSettings(setting, 'frontend');

   //  var wrapper = { requestId: requestId, result: 'ok' }
   //  deferred.resolve(wrapper);


   // return deferred.promise;
}
