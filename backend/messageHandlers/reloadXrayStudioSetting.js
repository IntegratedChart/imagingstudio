/**
 * Updated 2/15/19 
 * This is no longer in use. Todo: remove file from code base once we are sure we dont need this function anywhere
 */

// const t32XrayStudioMain = require('../t32XrayStudioMain');
// const logger = require('electron-log');
// const q = require('q');

// module.exports.handleRequest = handleRequest;

// /**
//  * Request handler for reloadXrayStudioSetting
//  *
//  * @param studioContext our studio context (see t32RequestDispatcher)  
//  *
//  */
// function handleRequest(studioContext, requestId) {
//     var deferred = q.defer();

//     logger.debug('reloading Xray Studio Setting...');

//     t32XrayStudioMain.reloadSetting(studioContext)
//     .then(function () {
//         var wrapper = { requestId: requestId }
//         deferred.resolve(wrapper);
//     })
//     .catch(function (err) {
//         logger.error(err);
//         logger.error(err.stack);
//         deferred.reject(err);
//     });
    
//     return deferred.promise;
// }
