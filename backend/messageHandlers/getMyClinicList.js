
// const q = require("q");
const t32RestServices = require('../t32RestServices');
const t32RestEndPoints = require('../t32RestEndPoints');
const logger = require('electron-log');


module.exports.handleRequest = handleRequest;


/**
* Handle 'getMyClinicList' request from the client side.
*
* @param studioContext our studio context (see t32XrayStudioMain) 
*/
async function handleRequest( studioContext, requestId ){

   try {       
      logger.info('... getMyClinicList ...')
      let urlParam = null;
   
      let clinicList = await t32RestServices.execForResult( 
         t32RestEndPoints.getClinicListForXrayStudio.routePath, 
         t32RestEndPoints.getClinicListForXrayStudio.method, 
         urlParam, 
         null, 
         null, 
         null)

      return { requestId : requestId, result : clinicList};
   } catch (e) {
      logger.info("error getMyClinicList ", e)
      throw { requestId: requestId, error: e }
   }
}
