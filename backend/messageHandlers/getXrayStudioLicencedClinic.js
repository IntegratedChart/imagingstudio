
// const q = require("q");
const t32RestServices = require('../t32RestServices');
const t32RestEndPoints = require('../t32RestEndPoints');
const logger = require('electron-log');
const t32OfflineDataHandler = require("../services/t32OfflineDataHandler");

module.exports.handleRequest = handleRequest;


/**
* Handle 'getXrayStudioLicencedClinic' request from the client side.
*
* @param studioContext our studio context (see t32XrayStudioMain) 
*/
async function handleRequest(studioContext, requestId) {
    try {
    
        let urlParam = null;
        let headers = null;
        let postBody = { 
            license: studioContext.setting.license, 
            machineId: studioContext.setting.machineId };
    
        logger.debug("The license to get clinic");
        logger.debug(postBody);
        let result = await t32RestServices.execForResult(
            t32RestEndPoints.getXrayStudioLicencedClinic.routePath,
            t32RestEndPoints.getXrayStudioLicencedClinic.method,
            urlParam,
            postBody,
            headers)
        logger.info('...... EHR response : ' + result);

        if (result && result._id) {
            logger.info("saving clinic to local db")
            t32OfflineDataHandler.saveClinic(result, true);
        }

        return { requestId: requestId, result: result };
 
    } catch (e) {
        logger.info("---error getXrayStudioLicencedClinic---", e);
        throw {requestId: requestId, error: e};
    }
}
