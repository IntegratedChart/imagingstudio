const t32RestServices = require('../t32RestServices');
const t32RestEndPoints = require('../t32RestEndPoints');
const logger = require('electron-log');

module.exports.handleRequest = handleRequest;

/**
 * Gets specific xray for given patient
 * @param {*} studioContext 
 * @param {*} requestId 
 * @param {*} requestObject 
 *  {
 *      file_id,
 *      file_type
 *  }
 */
async function handleRequest(studioContext, requestId, requestObject){

  try {
    logger.debug('...getPatientXrayFile');

    let urlParam = {
      file_id:       requestObject.file_id,
      file_type:     requestObject.file_type
    }

    logger.debug(requestObject);

    let headers = null;
    let postBody = null;
    let formData = null;

    let img = await t32RestServices.execForResult(
      t32RestEndPoints.getPatientXrayFiles.routePath, 
      t32RestEndPoints.getPatientXrayFiles.method, 
      urlParam, 
      postBody, 
      headers,
      formData)


    return { requestId: requestId,result: img }
  } catch (e) {
    logger.info("error getPatientXrayFile ", e)
    throw {requestId: requestId, error: e};
  }
}