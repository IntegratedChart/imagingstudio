'use strict';
const t32RestServices = require('../t32RestServices');
const t32RestEndPoints = require('../t32RestEndPoints');
const logger = require('electron-log');
const t32OfflineDataHandler = require('../services/t32OfflineDataHandler');

module.exports.handleRequest = handleRequest;

/**
 * @async
 * @param studioContext our studio context (see t32RequestDispatcher)
 * @msgContent download query
 */
async function handleRequest(studioContext, requestId, msgContent) {
    try {
        logger.info('...offline download reqeuestId', requestId);

        const result = await t32RestServices.execForResult(
            t32RestEndPoints.offlineDownload.routePath,
            t32RestEndPoints.offlineDownload.method,
            null,
            msgContent,
            null,
            null,
        );

        logger.info('...offline downloaded', result.length);
        
        const saveResult = await t32OfflineDataHandler.saveDownloadedPatients(result);
        
        logger.info("....offline download starting xray download ")
        t32OfflineDataHandler.handleOfflineXraysRequest({ 
            requestType: 'downloadXrays', 
            data: {
                clinic_id: msgContent.clinic_id,
                patientIds: result.map((patient) => patient._id.toString())
            } 
        })

        return { requestId: requestId, result: true};

    } catch (error) {
        logger.error('offline download error', error);
        logger.info(error.stack)
        throw {requestId, error};
    }
}