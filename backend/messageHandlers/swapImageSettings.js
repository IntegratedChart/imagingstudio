// const q = require("q");
const path = require("path");
const logger = require('electron-log');
const {writeJson} = require('../services/t32XrayStudioFs');


module.exports.handleRequest = handleRequest;

async function handleRequest(studioContext, requestId, requestObject) {
    try {
        logger.debug('RUNNING UPDATE IMAGE SETTINGS');
        logger.debug(requestObject);
    
        let settings = requestObject.settings;
        let fileDesc = requestObject.fileDesc;
        let newFileDesc = requestObject.newFileDesc;
        let keepBothNames = requestObject.keepBothNames;
    
        settings = replaceNames(settings, fileDesc, newFileDesc, keepBothNames);
    
        let settingsPath = studioContext.setting.currentSelectedFolder + "/settings.json";
    
        let settingsFullPath = path.join(studioContext.setting.tab32Root, settingsPath);
    
        logger.debug("SETTINGS IMAGE PATH");
        logger.debug(settingsFullPath);
    
        await writeJson(settingsFullPath, settings)
        
        return { requestId: requestId, result: settings }
    } catch (e) {
        logger.info("error swapImageSettings ", e);

    }
}

function replaceNames(settings, fileDesc, newFileDesc, keepBothNames){

    if (keepBothNames) {
        if (settings.hasOwnProperty(fileDesc)) {
            //do nothing, keep it
        } else {
            settings[fileDesc] = {brightness: 0, contrast: 0, sharpen: false, angle: 0, flipX: false, flipY: false}
        }

        if (settings.hasOwnProperty(newFileDesc)) {
            //do nothing, keep it 
        } else if (newFileDesc) {
            settings[newFileDesc] = {brightness: 0, contrast: 0, sharpen: false, angle: 0, flipX: false, flipY: false}
        }
    } else {
        if (settings.hasOwnProperty(fileDesc) && newFileDesc) {
            logger.debug("THE SETTINGS HAS fileDesc PROP")
            logger.debug(settings[fileDesc]);
            tempObj = Object.assign({}, settings[fileDesc]);
            logger.debug(tempObj);
            settings[newFileDesc] = tempObj;
            delete settings[fileDesc];
        } else {
            logger.debug("THE SETTINGS HAS NO fileDesc PROP");
            logger.debug(settings[fileDesc]);
            settings[newFileDesc] = { brightness: 0, contrast: 0, sharpen: false, angle: 0, flipX: false, flipY: false }
        }
    }

    return settings;
}

