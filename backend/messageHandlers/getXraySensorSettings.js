// const q = require("q");
const t32RestServices = require('../t32RestServices');
const t32RestEndPoints = require('../t32RestEndPoints');
const logger = require('electron-log');

module.exports.handleRequest = handleRequest;

async function handleRequest(studioContext, requestId, requestObject) {
    try {
        logger.debug('...getXraySensorSettings');
    
        let postBody = {
            clinic_id: requestObject.clinic_id
        }
    
        let headers = null;
        let urlParam = null;
        let formData = null;
    
        let settings = await t32RestServices.execForResult(
            t32RestEndPoints.getXraySensorSettings.routePath,
            t32RestEndPoints.getXraySensorSettings.method,
            urlParam,
            postBody,
            headers,
            formData)

        return {
            requestId: requestId,
            result: settings
        }

    } catch (e) {
        logger.info("error getXraySensorSettings ", e);
        throw {requestId: requestId, error: e};
    }
}