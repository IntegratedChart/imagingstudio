// const q = require("q");
const path = require("path");
const fs = require('fs-extra');
const logger = require('electron-log');
const t32XrayStudioImageManipulation = require("../services/t32XrayStudioImageManipulation");



module.exports.handleRequest = handleRequest;

/**
 *
 * @param studioContext our studio context (see t32RequestDispatcher)  
 * @param requestObject : 
 *              {
 *                  imageUrl : imageUrl
 *              }
 */
async function handleRequest(studioContext, requestId, requestObject){
    try {
        logger.info("flip image urls ", requestObject.imageUrl);

        let tab32Root = studioContext.setting.tab32Root;
        let fullFilePath = path.join(tab32Root, requestObject.imageUrl);
        let originalFilePath = path.join(tab32Root, requestObject.originalImageUrl);
        let rotationDeg = parseInt(requestObject.rotationDeg);
        logger.info('rotateImage, filePath : ' + fullFilePath);
        logger.info('rotateImage, originalFilePath : ' + originalFilePath);
    
        //if we have an original file
        if(!fs.existsSync(originalFilePath)){
            fs.copySync(fullFilePath, originalFilePath)
        }
    
        await t32XrayStudioImageManipulation.rotateImage(fullFilePath, rotationDeg)
        t32XrayStudioImageManipulation.rotateImage(originalFilePath, rotationDeg)
    
        return { requestId: requestId, result: "Ok" }
    } catch (e) {
        logger.info("error rotateImage ", e);
        throw { requestId: requestId, error: e }
    }
    
}



// function handleRequest(studioContext, requestId, requestObject){
//     var deferred = q.defer();

//     logger.debug(requestObject.imageUrl)
//     var tab32Root = studioContext.setting.tab32Root;
//     var fullFilePath = path.join(tab32Root, requestObject.imageUrl);
//     var originalFilePath = path.join(tab32Root, requestObject.originalImageUrl);
//     var rotationDeg = parseInt(requestObject.rotationDeg);
//     // var filePath = requestObject.imageUrl
//     // logger.debug('rotateImage, filePath : ' + filePath);
//     logger.debug('rotateImage, filePath : ' + fullFilePath);
//     logger.debug('rotateImage, originalFilePath : ' + originalFilePath);

//     //if we have an original file
//     if(!fs.existsSync(originalFilePath)){
//         try {
//             fs.copySync(fullFilePath, originalFilePath)
//         }
//         catch (err) {
//             logger.error(err);
//             deferred.reject({ requestId: requestId, error: 'failed to copy original file' });
//             return
//         }
//     }

//     rotateImage(requestId, fullFilePath, rotationDeg)
//     .then(function(){

//         var wrapper = {requestId: requestId, result: "Ok"}
//         deferred.resolve(wrapper);
//         return rotateImage(requestId++, originalFilePath, rotationDeg)
//     })
//     .catch(function(err){
//         console.log(err);
//         deferred.reject(err);
//     })

//     return deferred.promise;
// }

// function rotateImage(requestId, fullFilePath, rotationDeg){
//     var deferred =  q.defer();
//     Jimp.read(fullFilePath).then(function (img) {
//         logger.debug(img);
//         img.rotate(rotationDeg)                
//         .getBuffer(Jimp.AUTO, function (err, buf) {
//             if(err){
//                 deferred.reject({ requestId: requestId, error: err });
//             }
//             else{
//                 fs.writeFile(fullFilePath, buf, function (err) {
//                     if (err) {
//                         logger.error(err);
//                         logger.error(err.stack);
//                         deferred.reject({requestId: requestId, error: err});
//                     }
//                     else {
//                         deferred.resolve();
//                     }
//                 });
//             }
//         });
//     })
//     .catch(function (err) {
//         console.log(err)
//         deferred.reject({ requestId: requestId, error: err });
//     });

//     return deferred.promise;
// }
