// const q = require("q");
const path = require("path");
// const mkdirp = require('mkdirp');
const t32XrayStudioFs = require("../services/t32XrayStudioFs");
const logger = require('electron-log');
const {writeJson} = require('../services/t32XrayStudioFs');
// const fs = require('fs');


module.exports.handleRequest = handleRequest;

/**
*
* @param studioContext our studio context (see t32XrayStudioMain)  
* @param requestObject 
*/
async function handleRequest(studioContext, requestId, requestObject ){
	try {
		let targetPath = null;
		if ( requestObject && requestObject.selectedFolder ) {
			targetPath = path.join(studioContext.setting.tab32Root, requestObject.selectedFolder );
		}

		logger.debug('Ensure Path : ' + targetPath );
		let originalsFolderPath = path.join(targetPath, "originals");
		let settingsFilePath = path.join(targetPath, "settings.json");
		
		let settings = {filePath: targetPath, originalsFilePath: originalsFolderPath};
		
		t32XrayStudioFs.ensureDirSync(targetPath);
		t32XrayStudioFs.ensureDirSync(originalsFolderPath);
		
		if(!t32XrayStudioFs.existsSync(settingsFilePath)){
			await writeJson(settingsFilePath, settings);
		}
		
		return { requestId: requestId, result: 'success' };
	} catch (e) {
		logger.info('error ensuring folder ', e)
		throw {requestId: requestId, error: e}
	}
}

// /**
// *
// * @param studioContext our studio context (see t32XrayStudioMain)  
// * @param requestObject 
// */
// function handleRequest(studioContext, requestId, requestObject ){
//     var deferred = q.defer();

//     var targetPath = null;
//     if ( requestObject && requestObject.selectedFolder ) {
//         targetPath = path.join(studioContext.setting.tab32Root, requestObject.selectedFolder );
//     }
//     logger.debug('Ensure Path : ' + targetPath );
//     var originalsFolderPath = path.join(targetPath, "originals");
//     var settingsFilePath = path.join(targetPath, "settings.json");

//     var settings = {filePath: targetPath, originalsFilePath: originalsFolderPath};

//     mkdirp.sync(targetPath);
//     mkdirp.sync(originalsFolderPath);

//     var wrapper = { requestId : requestId };

//     if(!fs.existsSync(settingsFilePath)){
//         writeJson(settingsFilePath, settings)
//         .then(function(){
//             deferred.resolve(wrapper);
//         })
//         .catch(function(err){
//             logger.debug(err);
//             deferred.reject({ requestId: requestId, error: 'error setting up settings file' });
//         })
//     }
//     else{
//         deferred.resolve(wrapper)
//     }

//     return deferred.promise;
// }