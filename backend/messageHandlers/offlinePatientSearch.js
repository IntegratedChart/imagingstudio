'use strict';
const logger = require('electron-log');
const t32OfflineDataHandler = require('../services/t32OfflineDataHandler');

/**
 * @async
 * @description search patients from offline data.
 * @param { object } studioContext 
 * @param { string } requestId 
 * @param { object } searchObject 
 */
const handleRequest = async (studioContext, requestId, searchObject = {}) => {
    try {
        const { searchInput, clinic_id } = searchObject;

        if (!searchInput || !clinic_id) {
            throw 'Missing required data';
        }
   
        // const whereConditions = [
        //     `(
        //         data->'patient'->>'search_string' ILIKE '%${searchInput}%'
        //         OR data->'patient'->'clinicData'->>'chartId' = '${searchInput}'
        //     )`,
        //     `AND data->>'clinic_id' = '${clinic_id}'`
        // ];

        // const result = await t32OfflineDataHandler.queryData([], whereConditions)
        // const patients = result ? result.map(item => item.data.patient) : [];
        // const data = result ? result.map(item => item.data) : [];
        
        let query = `search_string LIKE "*${ searchInput.toLowerCase() }*?"`;

        if (Number(searchInput)) {
            query += ` OR chartId = "${ parseInt(searchInput) }"`;
        }

        const data = await t32OfflineDataHandler.queryOfflinePatients(query);
        return { requestId, result: data };
    } catch (error) {
        logger.error('offlinePatientSearch handleRequest', { requestId, error });
        throw {requestId, error};
    }
}

module.exports.handleRequest = handleRequest;