// const q = require("q");
const t32RestServices = require('../t32RestServices');
const t32RestEndPoints = require('../t32RestEndPoints');
const logger = require('electron-log');

module.exports.handleRequest = handleRequest;

async function handleRequest(studioContext, requestId, requestObject){
    try {
        // const XrayStudioMain = require('../t32XrayStudioMain');
        logger.debug('...saveXrayTemplates');    
        let postBody = {
            clinic_id: requestObject.clinic_id,
            templatesToBeSaved: requestObject.templatesToBeSaved,
            templatesToBeDeleted: requestObject.templatesToBeDeleted
        }
    
        //we can go in and get the templates that don't have _id and then send that as array to server
    
        let headers = null;
        let urlParam = null;
        let formData = null;
    
        let results = await t32RestServices.execForResult(
          t32RestEndPoints.saveXrayTemplatesV2.routePath, 
          t32RestEndPoints.saveXrayTemplatesV2.method, 
          urlParam, 
          postBody, 
          headers,
          formData)

        // logger.debug("HERE IS THE RESULT");
        // logger.debug(templates);
        // if (typeof results === 'object' && results.hasOwnProperty('conflict')) {
        //     logger.debug("there were conflicts in save xray templates syncing, that have been resolved");
        //     logger.debug(results);
        //     // throw new Error("Conflicts with request that need to be resolved")
        //     var msg = {
        //         msgType: 'recordConflict',
        //         recordType: 'Xray Templates',
        //         conflictArr: results.conflict || [{oldVal: "test", newVal: "test"}]
        //     }

        //     studioContext.browserWindow.notifyClient(msg);
        // }
        return {
            requestId: requestId,
            result: results.records ? results.records : results
        }
    } catch (e) {
        logger.info('error save xrayTemplates ', e);
        throw {requestId: requestId, error: e};
    }
}