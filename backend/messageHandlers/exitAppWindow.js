const logger = require('electron-log');
// const q = require('q');
const t32XrayStudioBrowserWindowHandler = require('../services/t32XrayStudioBrowserWindowHandler');
/**
 * Request handler for reloadXrayStudioSetting
 * @param studioContext our studio context (see t32RequestDispatcher)  
 */

module.exports.handleRequest = handleRequest;
function handleRequest(studioContext, requestId) {
  try {
    logger.debug('-----EXITING APP WINDOW-----'); 
    return studioContext.browserWindow.exitAppWindow();
    
  } catch (e) {
    logger.info('error exiting app window ', e)
  }

}
