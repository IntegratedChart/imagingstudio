
// const q = require("q");
const path = require("path");
const fs = require('fs');
const logger = require('electron-log');


module.exports.handleRequest = handleRequest;

/**
 * Request handler for provide directory listing.
 *
 * @param studioContext our studio context (see t32RequestDispatcher)  
 */
async function handleRequest(studioContext, requestId, msgContent){
  try {
    let tab32Root = studioContext.setting.tab32Root;
    logger.info('... getImageDirectoryList under root : ' + tab32Root );
  
    let dirList = fs.readdirSync(tab32Root)
                  .filter(file => fs.lstatSync(path.join(tab32Root, file)).isDirectory());
    
    logger.info(dirList);
    return { requestId: requestId, result: dirList};
  } catch (e) {
    logger.info("error: getImageDirectoryList ", e)
    throw { requestId: requestId, error: e};
  }
}