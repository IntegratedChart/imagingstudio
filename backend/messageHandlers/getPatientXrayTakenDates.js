// const q = require("q");
const t32RestServices = require('../t32RestServices');
const t32RestEndPoints = require('../t32RestEndPoints');
const logger = require('electron-log');

module.exports.handleRequest = handleRequest;

async function handleRequest(studioContext, requestId, requestObject){
    try {   
        logger.debug('...getPatientXrayTakenDates');
    
        // logger.debug('HERE IS THE REQUEST OBJ');
        // logger.debug(requestObject);
    
        let postBody = {
            clinic_id: requestObject.clinic_id,
            patient_id: requestObject.patient_id
        }
    
        let headers = null;
        let urlParam = null;
        let formData = null;
    
        let dates = await t32RestServices.execForResult(
          t32RestEndPoints.getPatientXrayTakenDates.routePath, 
          t32RestEndPoints.getPatientXrayTakenDates.method, 
          urlParam, 
          postBody, 
          headers,
          formData)

        return { requestId: requestId, result: dates }

    } catch (e) {
        logger.info("error getPatientXrayTakenDates", e);
        throw { requestId: requestId, error: e }
    }
}