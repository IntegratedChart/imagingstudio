'use strict';
const logger = require('electron-log');
const t32OfflineDataHandler = require('../services/t32OfflineDataHandler');
// const t32OfflineDownload = require('./offlineDownload');

module.exports.handleRequest = handleRequest;

/**
 * @async
 * @param studioContext our studio context (see t32RequestDispatcher)
 * @msgContent download query
 */
async function handleRequest(studioContext, requestId, msgContent) {
    try {
        logger.info('...offline upload reqeuestId', requestId);

        const result = await t32OfflineDataHandler.handleOfflineRefreshRequest(msgContent);

        return { requestId, result };
    } catch (error) {
        logger.error('offline upload error', error);
        logger.info(error.stack)
        throw { requestId, error };
    }
}