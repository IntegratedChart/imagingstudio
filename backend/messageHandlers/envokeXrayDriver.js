
// const q = require("q");
const child = require('child_process').execFile;
const path = require("path");
const fs = require('fs-extra');
const logger = require('electron-log');
// const _ = require('lodash');

module.exports.handleRequest = handleRequest;

/**
 * Update 4.2.19 Jagvir Sarai
 * This method will be deprecated since we are using new logic to communicate with driver through ipc.
 */

/**
* Handle 'envokeXrayDriver' request from the client side.
*
* @param studioContext our studio context (see t32XrayStudioMain) 
* @param requestObject - the message content.
	var requestObject = {
		command : command,
		args    : args
	}
*/
var index = 1;

async function handleRequest( studioContext, requestId, requestObject ){

	try { 

		let imageFileExtList = ['.jpg', '.jpeg', '.png', '.tiff', '.gif'];
	
		logger.info('... Invoke Xray Driver : command' + requestObject.command );
		logger.info('....... args list : ' );
		logger.info(JSON.stringify(requestObject.args));
	
		if ( studioContext.setting.simulation && requestObject.cmd !== 'stop') {
			logger.debug('...... simulation mode ' );
			// let dirList = fs.readdirSync(path.join(__dirname, '../../frontend/sampleXrays'));
			let dirPath = path.join(__dirname, '../../frontend/xrayStudioV1/sampleXrays');
	
			let dirList = fs.readdirSync(dirPath).filter((file) => {
				// logger.debug("Here are the files");
				// logger.debug(file)
				let extName = path.extname(file) || '';
	
				return fs.lstatSync(path.join(dirPath, file)).isFile() &&
					imageFileExtList.indexOf(extName.toLowerCase()) !== -1
			});
	
			let targetDir = requestObject.args[0];
	
			let theFile = (index < dirList.length) ? dirList[index] :  dirList[index % dirList.length];
			let sourcePath = path.join(__dirname, '../../frontend/xrayStudioV1/sampleXrays', theFile );
			let targetPath = path.join(targetDir, index + '.jpg' );
			index ++;
	
			logger.info("About to copy files from simulation")
			fs.copySync(sourcePath, targetPath)
		} else {

			await new Promise((resolve, reject) => {
				child( requestObject.command, requestObject.args, function(err, data){
					if(err){
						logger.info('...... Xray Driver stopped : ' + err );
						var msg = { msgType : 'XrayDriverExit' };
						studioContext.browserWindow.notifyClient(msg);
						resolve();
					}
				})
			})
		}

		return { requestId : requestId, result : 'ok'}

	} catch (e) {
		logger.info("error envoking xray driver ", e);
		throw { requestId: requestId, error: e };
	}
}


// function handleRequest( studioContext, requestId, requestObject ){
// 	var deferred = q.defer();

// 	var imageFileExtList = ['.jpg', '.jpeg', '.png', '.tiff', '.gif'];

// 	logger.info('... Invoke Xray Driver : command' + requestObject.command );
// 	logger.info('....... args list : ' );
// 	logger.info(JSON.stringify(requestObject.args));

// 	if ( studioContext.setting.simulation && requestObject.cmd !== 'stop')
// 	{
// 		logger.debug('...... simulation mode ' );
// 		// var dirList = fs.readdirSync(path.join(__dirname, '../../frontend/sampleXrays'));
// 		var dirPath = path.join(__dirname, '../../frontend/xrayStudioV1/sampleXrays');

// 		var dirList = fs.readdirSync(dirPath)
// 				.filter(function (file) {
// 				// logger.debug("Here are the files");
// 				// logger.debug(file)
// 				var extName = path.extname(file) || '';

// 				return fs.lstatSync(path.join(dirPath, file)).isFile() &&
// 					_.contains(imageFileExtList, extName.toLowerCase());
// 				});

// 		var targetDir = requestObject.args[0];

// 		var theFile = (index < dirList.length) ? dirList[index] :  dirList[index % dirList.length];
// 		var sourcePath = path.join(__dirname, '../../frontend/xrayStudioV1/sampleXrays', theFile );
// 		var targetPath = path.join(targetDir, index + '.jpg' );
// 		index ++;

// 		try {
// 			logger.info("About to copy files from simulation")
// 			fs.copySync(sourcePath, targetPath)
// 		} catch (err) {
// 			logger.error(err)
// 		}
// 	}
// 	else
// 	{
// 		child( requestObject.command, requestObject.args, function(err, data){
// 				if(err){
// 					logger.info('...... Xray Driver stopped : ' + err );
// 					var msg = { msgType : 'XrayDriverExit' };
// 					studioContext.browserWindow.notifyClient(msg);

// 					return;
// 				}
// 		})
// 	}

// 	var wrapper = { requestId : requestId, result : 'ok'}
// 	deferred.resolve(wrapper);
// 	return deferred.promise;
// }
