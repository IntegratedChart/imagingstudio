'use strict';
const logger = require('electron-log');
const t32XrayStudioAuthManager = require("../services/t32XrayStudioAuthManager");
module.exports.handleRequest = handleRequest;

/**
 * @async
 * @param studioContext our studio context (see t32RequestDispatcher)
 * @msgContent download query
 */
async function handleRequest(studioContext, requestId, msgContent) {
    try {
        let status = await t32XrayStudioAuthManager.isAuthenticated();
        return { requestId: requestId, result: status };
    } catch (e) {
        logger.error('failed to get auth code', e);
        logger.info(e.stack)
        throw { requestId: requestId, error: e };
    }
}