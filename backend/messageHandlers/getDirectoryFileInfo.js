
// const q = require("q");
// const _ = require("lodash");
const path = require("path");
const fs = require('fs');
const logger = require('electron-log');


module.exports.handleRequest = handleRequest;
module.exports.createFileMapByCreation = createFileMapByCreation;

/**
 * Given a directory path, return all files in the directory
 *
 * @param studioContext our studio context (see t32XrayStudioMain)  
 */
async function handleRequest(studioContext, requestId, requestObj) {
	try {
		let imageFileExtList = ['.jpg', '.jpeg', '.png', '.tiff', '.gif'];
		logger.debug('..... getDirectoryFileInfo for ' + requestObj.directoryPath);

		let dirPath = path.join(studioContext.setting.tab32Root, requestObj.directoryPath);

		let infoMap = (fileList, dirPath, infoNeeded) => {
			logger.info("Inside of the info map");
			// logger.info("File List ", fileList);
			// logger.info("Dir Path ", dirPath);
			// logger.info('info needed ', infoNeeded);

			let map =  {
				'createTime': createFileMapByCreation(fileList, dirPath),
				'modifiedTime': createFileMapByModified(fileList, dirPath)
			}

			return map[infoNeeded];
		}
 
		let fileList = [];
 
		if (fs.existsSync(dirPath)) {
			fileList = fs.readdirSync(dirPath).filter(function (file) {
				logger.debug("Here are the files");
				logger.debug(file)
				let extName = path.extname(file) || '';

				return fs.lstatSync(path.join(dirPath, file)).isFile() && imageFileExtList.indexOf(extName.toLowerCase()) !== -1;
			})

			logger.info("The file list so far ", fileList);

			fileList = infoMap(fileList, dirPath, requestObj.infoNeeded);

			logger.info("The file list after info fileMap ", fileList);

			return { requestId: requestId, result: fileList };
		}
		else {
			throw "directory not found: " + dirPath;
		}

	} catch (e) {
		logger.info("error getting directory file info ", e)
		throw { requestId: requestId, error: e }
	}   


    return deferred.promise;
}

function createFileMapByCreation(fileList, dirPath) {
	let fileListMap = {};

	try {
		fileList.forEach((file) => {
			let directoryPath = path.join(dirPath, file);
			let stats = fs.lstatSync(directoryPath);
			fileListMap[file] = new Date(stats.birthtime);
		});

		return fileListMap;
	}
	catch (err) {
		logger.info("error getting file list by creation date")
		throw err;
	}
}

function createFileMapByModified(fileList, dirPath){
	let fileListMap = {};

	try {
		fileList.forEach((file) => {
			var directoryPath = path.join(dirPath, file);
			var stats = fs.lstatSync(directoryPath);
			fileListMap[file] = new Date(stats.ctime);
		});

		return fileListMap;
	}
	catch (err) {
		logger.info("error getting file list by modified date")
		throw err;
	}
}

// function handleRequest(studioContext, requestId, requestObj) {
//     var deferred = q.defer();

//     var imageFileExtList = ['.jpg', '.jpeg', '.png', '.tiff', '.gif'];
//     logger.debug('..... getDirectoryFileList for ' + requestObj.directoryPath);

//     var dirPath = path.join(studioContext.setting.tab32Root, requestObj.directoryPath);

//     var infoMap = function(fileList, dirPath, infoNeeded){
//         logger.info("Inside of the info map");
//         // logger.info("File List ", fileList);
//         // logger.info("Dir Path ", dirPath);
//         // logger.info('info needed ', infoNeeded);

//         var map =  {
//             'createTime': createFileMapByCreation(fileList, dirPath),
//             'modifiedTime': createFileMapByModified(fileList, dirPath)
//         }

//         return map[infoNeeded];
//     }

//     var fileList = [];

//     try {
//         if (fs.existsSync(dirPath)) {
//             fileList = fs.readdirSync(dirPath)
//                 .filter(function (file) {
//                     logger.debug("Here are the files");
//                     logger.debug(file)
//                     var extName = path.extname(file) || '';

//                     return fs.lstatSync(path.join(dirPath, file)).isFile() &&
//                         _.contains(imageFileExtList, extName.toLowerCase());
//                 })

//             logger.info("The file list so far ", fileList);

//             fileList = infoMap(fileList, dirPath, requestObj.infoNeeded);

//             logger.info("The file list after info fileMap ", fileList);

//             var wrapper = { requestId: requestId, result: fileList };

//             // logger.debug('........ found = ');
//             // logger.debug(fileList);
//             deferred.resolve(wrapper);
//         }
//         else {
//             throw new Error("directory not found: " + dirPath);
//         }

//     }
//     catch (err) {
//         logger.debug('lstat error')
//         logger.info(err);
//         deferred.reject({ requestId: requestId, error: "error reading files form directory" });
//     }


//     return deferred.promise;
// }

// function createFileMapByCreation(fileList, dirPath) {
//     var fileListMap = {};

//     try {
//         fileList.forEach((file) => {
//             var directoryPath = path.join(dirPath, file);
//             var stats = fs.lstatSync(directoryPath);
//             fileListMap[file] = new Date(stats.birthtime);
//         });

//         return fileListMap;
//     }
//     catch (err) {
//         logger.info("error getting file list by creation date")
//         throw new Error(err);
//     }
// }

// function createFileMapByModified(fileList, dirPath){
//     var fileListMap = {};

//     try {
//         fileList.forEach((file) => {
//             var directoryPath = path.join(dirPath, file);
//             var stats = fs.lstatSync(directoryPath);
//             fileListMap[file] = new Date(stats.ctime);
//         });

//         return fileListMap;
//     }
//     catch (err) {
//         logger.inof("error getting file list by modified date")
//         throw new Error(err);
//     }
// }
