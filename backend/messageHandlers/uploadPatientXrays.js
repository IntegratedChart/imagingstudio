// const q = require("q");
const path = require("path");
const fs = require('fs-extra');
// const _ = require("lodash");
// const os = require("os");
// const async = require('async');
const moment = require('moment');
const t32RestServices = require('../t32RestServices');
const t32RestEndPoints = require('../t32RestEndPoints');
const logger = require('electron-log');
const t32XrayStudioUploadQueue = require('../services/t32XrayStudioUploadQueue');
const utils = require("../utils/utils");
const IDGenerator = require('../utils/idGenerator');
const t32XrayStudioDirHandler = require('../services/t32XrayStudioDirHandler');

module.exports.handleRequest = handleRequest;
module.exports.ignoreFileList = true;

/**
* Handle 'getPatientById' request from the client side.
*
* @param studioContext our studio context (see studioContext) 
* @param requestObject : 
            var requestObject = { 
               clinic_id   : $scope.patient.clinic_id,
               patient_id  : $scope.patient._id,
               folder      : $scope.selectedFolder,
               imageTakenDate: Date
            }
*/
var studioContext = null;

async function handleRequest( context, requestId, requestObject ){

    try {
        studioContext = context;
        logger.info('UploadPatientXrays: folder : ' + requestObject.folder );
    
        var folderToUpload = requestObject.folder;
        let originalsFolder = requestObject.originalsFolder
        let studioSettings = studioContext.setting;
        // logger.info("the studio settings at moment of upload ", studioSettings);
        let userHomePath = studioContext.theApp.getAppHome();
        let tab32Root = studioSettings.tab32Root;
        let tab32UploadRoot = studioSettings.tab32UploadRoot || path.join(userHomePath, "tab32Upload");
        let sourceFolderPath = path.join(tab32Root, folderToUpload);
        let destFolderPath = path.join(tab32UploadRoot, folderToUpload);
    
        logger.info(sourceFolderPath);
        logger.info(destFolderPath);
        
        //if we are retrying our uploading lofic
        if(!requestObject.retry){
            if ( fs.existsSync(sourceFolderPath) && fs.existsSync(destFolderPath) ){
                logger.info("Another folder by this is name is already in the tab32Uploads folder ", folderToUpload);
                logger.info("renaming current folder inside of tab32Uploads");
                // let dateStamp = moment().format('MM-DD-YY-hh-mm-ss');
                // folderToUpload += ('-' + dateStamp);
                // originalsFolder = folderToUpload + "/originals";
                // destFolderPath = path.join(tab32UploadRoot, folderToUpload);
                // logger.info("The new destination path ", destFolderPath);
                let folderNameAlt = folderToUpload + '(failed-upload)'
                let destFolderPathAlt = path.join(tab32UploadRoot, folderNameAlt);
                fs.renameSync(destFolderPath, destFolderPathAlt);

                logger.info('moving current folder to tab32Uploads')
                fs.moveSync(sourceFolderPath, destFolderPath);
            } else if ( fs.existsSync(sourceFolderPath) ) {
                fs.moveSync(sourceFolderPath, destFolderPath);
            } else {
                throw "Folder does not exist " + sourceFolderPath;
            }
        }

        //this will skip trying to load and save settings json file and start the watcher 
        // studioContext.watchXrayDirectory();
        
        
        let dirPath = path.join(tab32UploadRoot, folderToUpload);
        let originalsDirPath = fs.existsSync(path.join(tab32UploadRoot, originalsFolder)) ? path.join(tab32UploadRoot, originalsFolder) : null
        let imageSettingsObj = requestObject.imageSettings ? requestObject.imageSettings : null;

        let filePathList = fs.readdirSync(dirPath)
            .filter(file => !file.endsWith(".DS_Store") && !file.endsWith(".json") && fs.lstatSync(path.join(dirPath, file)).isFile() );

        logger.info("The files that we need to upload");
        logger.info(filePathList);

        var uniqueId = IDGenerator.generate();
        logger.info("****Unique Id generated ", uniqueId + " ****");
        sendStartMsg(folderToUpload, filePathList, uniqueId, requestObject);

        let sortedFileList = t32XrayStudioDirHandler.sortFileByDateModified(filePathList, dirPath);

        // var now = new Date();
        // var hour = now.getHours();
        // var minutes = now.getMinutes();
        // var seconds = now.getSeconds();

        let currentImageDate = moment(requestObject.imageTakenDate).utc().set({h: 12, m: 0, s: 0, ms: 0}).toISOString();

        // let lastSessionDate = null;
        // need to have EHR_663 branch merged on server side to fix the date problem we have (this needs to be tested still).
        let lastSessionDate = await getPreviousXraySessionDate(requestObject.clinic_id, requestObject.patient_id, currentImageDate);


        logger.info("****LAST SESSION DATE****", lastSessionDate)
        var miliseconds = 1;

        if (lastSessionDate) {
            currentImageDate = lastSessionDate;
            miliseconds = moment(currentImageDate).get('ms');
            miliseconds++;
        }
        
        for (let i = 0; i < sortedFileList.length; i++) {
            // async.eachSeries(sortedFileList, function (filePath, callback) {
            let filePath = sortedFileList[i];
            let fullFilePath = path.join(dirPath, filePath);
            let fullOriginalsFilePath = originalsDirPath ? path.join(originalsDirPath, filePath) : null;

            let imageSettings = {}
            let filePathNoExt = removeExt(filePath);
            if (imageSettingsObj) {
                imageSettings = imageSettingsObj.hasOwnProperty(filePathNoExt) ? JSON.stringify(imageSettingsObj[filePathNoExt]) : JSON.stringify(createBlankSettingsObj())
            }
            // uploading by mtime only gives us the creation time stamp. 
            // we need ctime to get the date modified time stamp

            // let imageTakenDate = moment(requestObject.imageTakenDate).set({ h: hour, m: minutes, s: seconds, ms: miliseconds });
            // imageTakenDate = imageTakenDate.toISOString();

            let imageTakenDate = moment(currentImageDate).utc().set({h: 12, m: 0, s: 0, ms: miliseconds }).toISOString();

            miliseconds++;

            // logger.info("the image was created ", imageCreationDate);
            /**
             * We need to ensure the request is coming from xray template so we can parse out the image positions.
             * Otherwise the call to uploadSingleXray will handle file uploads with its type
             */
            if (requestObject.type === 'xray') {
                imagePos = parseXrayPosition(filePath);
            }
            else {
                imagePos = null;
            }

            logger.info("THE IMAGE IS READY FOR UPLAOD")
            logger.info("THE IMAGE WAS TAKEN ");
            logger.info(imageTakenDate);
            logger.info("HERE IS THE FILE PATH")
            logger.info(filePath);
            logger.info("FILE Full PATH");
            logger.info(fullFilePath);
            logger.info(imagePos);

            //new logic to handle originals xrays uploading with the edits. 
            if (fullOriginalsFilePath && fs.existsSync(fullOriginalsFilePath) && imagePos !== "") {
                let newImage = await uploadXrays(requestObject.clinic_id, requestObject.patient_id, imagePos, imageTakenDate, fullOriginalsFilePath, fullFilePath, imageSettings)
                logger.info('**** UPLOADED FILE ' + fullFilePath + " - " + imagePos + ' ****');
                logger.info('**** image id  ' + newImage._id + " *****");
                
                logger.info("*** Sending update message ***")
                setTimeout(function () {
                    sendUpdateMsg(folderToUpload, filePath, uniqueId);
                }, 200);

                logger.info("removing original file path ", fullOriginalsFilePath);
                await removeSync(fullOriginalsFilePath);
                logger.info("removing file path ", fullFilePath);
                await removeSync(fullFilePath);

            } else if (imagePos && imagePos !== "") {
                await uploadSingleXray(requestObject.clinic_id, requestObject.patient_id, imagePos, imageTakenDate, fullFilePath);

                await removeSync(fullFilePath);

                await utils.pause(200);
                sendUpdateMsg(folderToUpload, filePath, uniqueId);
            }
        }
        
        logger.info("removing uploads directory ", dirPath)
        await removeSync(dirPath);

        setTimeout(() => {
            sendStopMsg(folderToUpload, uniqueId);
        }, 500)

        logger.info("------------UPLOADING COMPLETE----------");
        logger.info('request id ' + requestId);
        return { requestId: requestId, result: "okay" };

    } catch (e) {
        logger.info('error uploadPatientXrays ', e);
        logger.info('sending error msg for upload patient xrays');
        sendErrorMsg(folderToUpload, e, uniqueId);
        throw { requestId: requestId, error: e }
    }
    
}

async function removeSync(path) {
    try {
        fs.removeSync(path);
        return
    } catch (e) {
        logger.info("error removing uploads path ", path);
        await utils.pause(3000);
        logger.info("retrying removing path ", path);
        try {
            fs.removeSync(path);
            return
        } catch (e) {
            logger.info("failed to remove path ", path);
            return
        }
    }
}

async function uploadSingleXray(clinic_id, patient_id, imagePos, imageTakenDate, filePath ) {
    try{       
        logger.info('...... uploading : ' + filePath );
        let urlParam = {clinic_id : clinic_id, patient_id : patient_id};
        let headers = {
            'clinic-id' : clinic_id,
            'image-taken-date' : imageTakenDate,
            'file-type' : getFileTypeByName(filePath),
            'image-pos' : imagePos
        };
        // let postBody = {imageTakenDate : imageTakenDate};
        // instead of passing in stream, passing in the file name 
        //  so we can re-initialize the file stream on retry.
        // let formData = {
        //    file   : fs.createReadStream(filePath),
        // };
        let formData = filePath;

        // logger.info("THE URL PARAMS");
        // logger.info(urlParam);

        // logger.info("THE IMAGE TAKEN DATE INSIDE OF UPLOAD FUNCTION");
        // logger.info(imageTakenDate);

        // logger.info("THE IMAGE POS INSIDE OF UPLOAD FUNCTION");
        // logger.info(imagePos);

        return await t32RestServices.execForResult( 
            t32RestEndPoints.uploadPatientXrayForXrayStudio.routePath, 
            t32RestEndPoints.uploadPatientXrayForXrayStudio.method,
            urlParam,
            null,
            headers,
            formData );
    } catch(err){
        throw err;
    }
}

/**
 * we will code the type into the file name
 * 
 * by default, files are for xray images.  For non-xray images
 * file name will be xxx_type_fileType_type.jpg.
 *
 */
function getFileTypeByName(fileName){
    let tokens = fileName.substring(0, fileName.lastIndexOf('.')).split('_type_');
    // let tokens = nameWithNoExt.split('_type_');
    return tokens.length > 1 ? tokens[1] : 'xray';
}

function parseXrayPosition(filePath){
    try{
        let idString = (/(id_\w+)/g);
        let match = filePath.match(idString);
        let newString = "";

        if(match && match.length > 0){
            let matchedString = filePath.match(idString)[0].replace("id_", "");
            newString = filePath.replace(filePath, matchedString);

            logger.info("THIS IS THE NEW STRING");
            logger.info(newString);
        }

        return newString;
    }
    catch(err){
        throw err
    }
}

function createBlankSettingsObj(){
    return {
        brightness: 0,
        contrast: 0,
        sharpen: false,
        angle: 0,
        flipX: false,
        flipY: false
    }
}

async function uploadXrays(clinic_id, patient_id, imagePos, imageTakenDate, fullOriginalsPath, fullEditedPath, imageSettings){
    try {
        let editedImageId = undefined;
        let originalImageId = undefined;
    
        logger.info("CHECKING IMAGE SETTINGS");

        if(imageSettings.hasOwnProperty('_id')){
            editedImageId = imageSettings._id;
        }
        
        if(imageSettings.hasOwnProperty('originalImageId')){
            originalImageId = imageSettings.originalImageId;
        }

        logger.info("DONE CEHCKING")
    
        let originalImage = await uploadOriginalXray(clinic_id, patient_id, originalImageId, imagePos, imageTakenDate, fullOriginalsPath, imageSettings);
        return await uploadEditedXray(clinic_id, patient_id, originalImage._id, editedImageId, fullEditedPath, imageTakenDate, imageSettings);
    } catch (e) {
        throw e;
    }
}

async function uploadEditedXray(clinic_id, patient_id, originalXrayId, editedXrayId, editedXrayFilePath, imageTakenDate, imageSettings){
    try{
        logger.info('...... uploading Edited: ' + editedXrayFilePath);
        // logger.info(imageSettings);
    
        let urlParam = { clinic_id: clinic_id, patient_id: patient_id};
        let headers = {
            'clinic-id': clinic_id,
            'image-taken-date': imageTakenDate,
            'original-file-id': originalXrayId,
            'image-settings': imageSettings,
            'edited-file-id': editedXrayId,
        };
    
        // let postBody = {imageTakenDate : imageTakenDate};
        // instead of passing in stream, passing in the file name 
        //  so we can re-initialize the file stream on retry.
        // let formData = {
        //     file: fs.createReadStream(editedXrayFilePath),
        // };
        let formData = editedXrayFilePath;

        // logger.info("THE URL PARAMS");
        // logger.info(urlParam);
    
        // logger.info("THE IMAGE TAKEN DATE INSIDE OF UPLOAD FUNCTION");
        // logger.info(imageTakenDate);
    
        // logger.info("THE IMAGE POS INSIDE OF UPLOAD FUNCTION");
        // logger.info(imagePos);

        return await t32RestServices.execForResult(
            t32RestEndPoints.uploadEditedXray.routePath,
            t32RestEndPoints.uploadEditedXray.method,
            urlParam,
            null,
            headers,
            formData)
    }
    catch(err){
        throw err;
    }

}

async function uploadOriginalXray(clinic_id, patient_id, originalImageId, imagePos, imageTakenDate, fullOriginalsPath, imageSettings){

    try{
        logger.info('...... uploading Original: ' + fullOriginalsPath);
    
        var urlParam = { clinic_id: clinic_id, patient_id: patient_id };
        var headers = {
            'clinic-id': clinic_id,
            'original-image-id': originalImageId,
            'image-taken-date': imageTakenDate,
            'image-pos': imagePos,
            'edited-image-settings': imageSettings ? imageSettings : null
        };
        // var postBody = {imageTakenDate : imageTakenDate};
        // var postBody = {imageTakenDate : imageTakenDate};
        // instead of passing in stream, passing in the file name 
        //  so we can re-initialize the file stream on retry.
        // var formData = {
        //     file: fs.createReadStream(fullOriginalsPath),
        // };
        var formData = fullOriginalsPath;
    
        // logger.info("THE URL PARAMS");
        // logger.info(urlParam);
    
        // logger.info("THE IMAGE TAKEN DATE INSIDE OF UPLOAD FUNCTION");
        // logger.info(imageTakenDate);
    
        // logger.info("THE IMAGE POS INSIDE OF UPLOAD FUNCTION");
        // logger.info(imagePos);
        return await t32RestServices.execForResult(
            t32RestEndPoints.uploadOriginalXray.routePath,
            t32RestEndPoints.uploadOriginalXray.method,
            urlParam,
            null,
            headers,
            formData)
    }
    catch(err){
        throw err;
    }
}

async function getPreviousXraySessionDate(clinicId, patientId, currentSessionDate) {
    try {
        let urlParam = {
            clinic_id: clinicId,
            patient_id: patientId
        };

        let headers = {
            'clinic-id': clinicId,
        };

        let postBody = {
            sessionDate: currentSessionDate
        };

        logger.info("Getting previous xray session date")

        return await t32RestServices.execForResult(
            t32RestEndPoints.getPreviousXraySessionDate.routePath,
            t32RestEndPoints.getPreviousXraySessionDate.method,
            urlParam,
            postBody,
            headers,
            null)

    } catch (err) {
        logger.error("Error with get previous xray session date ", err);
        throw err
    }
}

function removeExt(file) {
    try{
        var reg = /\.\w+/g
        // console.log(file.match(reg));
        var index = file.indexOf(file.match(reg)[0]);
        var id = file.slice(0, index);
        // console.log(id);
        return id
    }
    catch(err){
        throw err;
    }
}

function sendStartMsg(folder, fileList, id, originalRequest){
    try{
        var folderMap = createFileMap(folder, id, fileList, originalRequest);
    
        var msg = {
            msgType: 'uploadStart',
            folderName: folder,
            folderId: id,
            fileMap: folderMap
        }
    
        logger.info("*** adding to upload queue " + folder + "***");
        t32XrayStudioUploadQueue.add(folderMap);
    
        logger.info("*** sending start msg for " + folder + " ***");
        studioContext.browserWindow.notifyClient(msg);
    }
    catch(err){
        logger.info('*** Error sending start message **', err);
        if(t32XrayStudioUploadQueue.getBy('id', id)){
            try{
                t32XrayStudioUploadQueue.removeBy('id', id)
            }
            catch(err){
                logger.info('*** Error removing ' + folder + " from queue ***");
                logger.info('*** Clearing queue ' + folder + " from queue ***");
                t32XrayStudioUploadQueue.clearQueue();
            }
        }
    }
}

function sendStopMsg(folder, id){
    try{
        var fileMap = t32XrayStudioUploadQueue.getBy('id', id);
        fileMap.status = 'finished'
    
        var msg = {
            msgType: 'uploadFinish',
            folderName: folder,
            folderId: id,
            fileMap: fileMap
        }
    
        logger.info("*** removing from upload queue " + folder + " ***");
        t32XrayStudioUploadQueue.removeBy('id', id);
    
        logger.info("*** sending stop msg for " + folder + " ***");
        studioContext.browserWindow.notifyClient(msg);
    }
    catch (err) {
        logger.info('*** Error sending stop message **', err);
        if (t32XrayStudioUploadQueue.getBy('id', id)) {
            try {
                t32XrayStudioUploadQueue.removeBy('id', id)
            }
            catch (err) {
                logger.info('*** Error removing ' + folder + " from queue ***");
                logger.info('*** Clearing queue ' + folder + " from queue ***");
                t32XrayStudioUploadQueue.clearQueue();
            }
        }
    }
}

function sendUpdateMsg(folder, fileName, id){
    try{
        var fileMap = t32XrayStudioUploadQueue.getBy('id', id);
        logger.info("The fileName we are updating", fileName);
        logger.info("the fileMap ", fileMap);
        fileMap.files[fileName] = true;
        fileMap.uploaded += 1;
        fileMap.remaining -= 1;
    
        var msg = {
            msgType: 'uploadProgressUpdate',
            folderName: folder,
            folderId: id,
            fileMap: fileMap
        }

        logger.info("*** updating upload queue " + folder + "/" + fileName + "/" + id + " ***");
        t32XrayStudioUploadQueue.findAndReplaceBy('id', id, fileMap);
    
        logger.info("*** updating upload queue " + folder + "/" + fileName + "/" + id + " ***");
        studioContext.browserWindow.notifyClient(msg)
    }
    catch(err){
        logger.info('*** Error updating ' + fileName + " in " + folder + " queue ***", err);
    }
}

function sendErrorMsg(folder, err, id){
    try{
        var fileMap = t32XrayStudioUploadQueue.getBy('id', id);
        
        if(fileMap){
            fileMap.status = "failed";
        }
        
        var msg = {
            msgType: 'uploadFailed',
            folderName: folder,
            fileMap: fileMap || null,
            folderId: fileMap ? id : null,
            error: err
        }
    
        logger.info("*** removing from upload queue " + folder + " ***");
        t32XrayStudioUploadQueue.removeBy('id', id);
    
        logger.info("*** sending error msg for " + folder  + " ***");
        studioContext.browserWindow.notifyClient(msg);
    }
    catch(err){
        logger.info('*** Error sending error msg ***', err);
        
        logger.info("** Removing folder " + folder + " ***");
        try{
            t32XrayStudioUploadQueue.removeBy('id', id);
        }
        catch(err){
            logger.info("** error remvoing folder " + folder + " ***");
            logger.info("** clearing queue ***");
            t32XrayStudioUploadQueue.clearQueue();
            logger.info("** queue cleared **")
        }
    }
}

/**
 * Added 2/11/19 Jagvir Singh
 * Creates map of folder files with upload status of each file set to false
 * @param {*} folder The folder name
 * @param {String} id The unique id for this upload
 * @param {*} fileList the list of file names
 * @param {Object} originalRequest the original request coming in from client side. 
 * 
 * 
 * @returns @type {Object}
 */
function createFileMap(folder, id, fileList, originalRequest) {
    try{
        logger.info("trying to create file map");
        var map = {
            id: id,
            name: folder,
            files: {},
            status: 'inProgress',
            uploaded: 0,
            remaining: fileList.length,
            total: fileList.length,
            originalRequest: originalRequest
        }

        for (let fileName of fileList) {
            map.files[fileName] = false;
        }
        // _.each(fileList, function (fileName) {
        //     map.files[fileName] = false;
        // })

        logger.info("Here is the created id ", map['id']);
    }
    catch(err){
        logger.info("Error creating file map ", err);
        throw err
    }

    return map;
}


