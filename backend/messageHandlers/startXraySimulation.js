// const q = require("q");
const path = require("path");
const fs = require('fs');
const fsExtra = require('fs-extra');
const logger = require('electron-log');
// const _ = require('lodash');
const t32XrayStudioDriverHandler = require("../services/t32XrayStudioDriverHandler");
module.exports.handleRequest = handleRequest;

/**
* Handle 'simulation xray taking' request from the client side.
*
* @param studioContext our studio context (see t32XrayStudioMain) 
* @param requestObject - the message content.
            var requestObject = {
               command : command,
               args    : args
            }
*/
var index = 1;

async function handleRequest(studioContext, requestId, requestObject) {
    // var deferred = q.defer();

    try {
        let imageFileExtList = ['.jpg', '.jpeg', '.png', '.tiff', '.gif'];
    
        logger.info('... Xray Simulation request ...');
    
        // let dirPath = path.join(__dirname, '../../frontend/xrayStudioV1/sampleXrays');
        let dirPath = path.join(__dirname, '/frontend/xrayStudioV1/sampleXrays');
    
        let dirList = fs.readdirSync(dirPath)
            .filter(function (file) {
                logger.debug("Here are the files");
                logger.debug(file)
                let extName = path.extname(file) || '';
    
                return fs.lstatSync(path.join(dirPath, file)).isFile() &&
                    imageFileExtList.indexOf(extName.toLowerCase()) !== -1
            });
    
        let targetDir = requestObject.targetDir;
    
        let theFile = (index < dirList.length) ? dirList[index] : dirList[index % dirList.length];
        let sourcePath = path.join(__dirname, '/frontend/xrayStudioV1/sampleXrays', theFile);
        // let sourcePath = path.join(__dirname, '../../frontend/xrayStudioV1/sampleXrays', theFile);
        let targetPath = path.join(targetDir, index + '.jpg');
        index++;
    
        try {
            logger.info("About to copy files from simulation")
            fsExtra.copySync(sourcePath, targetPath);
            // let msg = { msgType: 'DirectoryUpdated' }
            // studioContext.browserWindow.notifyClient(msg);
            await t32XrayStudioDriverHandler.notifyOnFileChange(studioContext)
        } catch (err) {
            logger.error(err);
    
        }
    
        return { requestId: requestId, result: 'ok' }
        // deferred.resolve(wrapper);
        // return deferred.promise;
    } catch (e) {
        throw { requestId: requestId, error: e };
    }
}
