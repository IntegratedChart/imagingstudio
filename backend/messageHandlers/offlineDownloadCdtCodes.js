'use strict';
const t32RestServices = require('../t32RestServices');
const t32RestEndPoints = require('../t32RestEndPoints');
const logger = require('electron-log');
const t32OfflineDataHandler = require('../services/t32OfflineDataHandler');

module.exports.handleRequest = handleRequest;

/**
 * @async
 * @param studioContext our studio context (see t32RequestDispatcher)
 * @msgContent download query
 */
async function handleRequest(studioContext, requestId, msgContent) {
    try {
        logger.info('...offline download cdt codes reqeuestId', requestId);

        const result = await t32RestServices.execForResult(
            t32RestEndPoints.offlineDownloadCdtCodes.routePath,
            t32RestEndPoints.offlineDownloadCdtCodes.method,
            null,
            msgContent,
            null,
            null,
        );

        // logger.info('...offline downloaded cdt codes', result);

        // const saveResult = await t32OfflineDataHandler.save(result, 'cdt_codes', "_id");
        // const saveResult = await t32OfflineDataHandler.saveCdtCodes(result, 'cdt_codes', msgContent.clinic_id);
        const saveResult = await t32OfflineDataHandler.saveCdtCodes(result);
        
        return { requestId: requestId, result: true };
    } catch (error) {
        logger.error('offline download cdt codes error', error);
        throw {requestId, error};
    }
}