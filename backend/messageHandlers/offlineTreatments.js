'use strict';
const t32OfflineDataHandler = require('../services/t32OfflineDataHandler');
const logger = require('electron-log');

/**
 * @description Handle offline patient add/edit
 * @async
 * @param {*} studioContext 
 * @param {*} requestId 
 * @param {*} requestMsg 
 */
const handleRequest = async (studioContext, requestId, requestMsg) => {
    try {
        const result = await t32OfflineDataHandler.handleOfflineTreatmentsRequest(requestMsg);
        return { requestId, result }
    } catch (error) {
        throw { requestId, error };
    }
}

module.exports.handleRequest = handleRequest;