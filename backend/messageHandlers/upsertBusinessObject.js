
// const q = require("q");
const t32RestServices = require('../t32RestServices');
const t32RestEndPoints = require('../t32RestEndPoints');
const logger = require('electron-log');


module.exports.handleRequest = handleRequest;


/**
* Handle 'upsertBusinessObject' request from the client side.
*
* @param studioContext our studio context (see t32XrayStudioMain) 
* @param upsertObject : {
            clinic_id : 
            schemaName : 
            doc :  // the doc
         }
*/
async function handleRequest( studioContext, requestId, upsertObject ){

   try {
      logger.debug('upsertBusinessObject: ' + upsertObject );
   
      let urlParam = {clinic_id : upsertObject.clinic_id, schemaName : upsertObject.schemaName};
      let postBody = {doc : upsertObject.doc };
      let headers = {'clinic-id' : upsertObject.clinic_id };
   
      t32RestServices.execForResult( 
         t32RestEndPoints.upsertBusinessObject.routePath, 
         t32RestEndPoints.upsertBusinessObject.method,
         urlParam,
         postBody,
         headers)


      return { requestId, requestId, result : results};
   } catch (e) {
      logger.info('error upsertBusinessObject ', e);
      throw {requestId: requestId, error: e};
   }
}
