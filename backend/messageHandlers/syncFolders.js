// const q = require("q");
// const _ = require("lodash");
const path = require("path");
const fs = require('fs-extra');
const logger = require('electron-log');
// const async = require('async');
module.exports.handleRequest = handleRequest;

/**
* Ensure that originals and edited images are in sync. 
* @param studioContext our studio context (see t32XrayStudioMain)  
*/
async function handleRequest(studioContext, requestId) {

    try {
    
        logger.debug('-----SYNC FOLDERS------');
        let tab32Root = studioContext.setting.tab32Root;
        let imageFileExtList = ['.jpg', '.jpeg', '.png', '.tiff', '.gif'];
        let pathsToRemove = [];
        let dirList = fs.readdirSync(tab32Root)
            .filter(file => fs.lstatSync(path.join(tab32Root, file)).isDirectory());
        
        for (let i = 0; i < dirList.length; i++) {
            let fullPath = path.join(tab32Root, dirList[i]);
            logger.debug('****FULL PATH***');
            logger.debug(fullPath);
            logger.debug('***FILE LIST***');
            logger.debug(fs.readdirSync(fullPath));
            let fileList = fs.readdirSync(fullPath)
                .filter(file => imageFileExtList.indexOf(path.extname(file)) !== -1);

            let originalsPath = fullPath + "/originals";
            let existsOriginalPath = fs.existsSync(originalsPath);

            await syncFolder(fullPath, fileList, originalsPath, existsOriginalPath, pathsToRemove);
        }

        logger.debug("--paths to be removed---");
        logger.debug(pathsToRemove);
        return { requestId: requestId, result: pathsToRemove };

    } catch (e) {
        logger.info("error with syncFolder ", e);
        throw {requestId: requestId, error: e};
    }
}


async function syncFolder(fullPath, fileList, originalsPath, existsOriginalPath, foldersToDelete) {
    try {
        let imageFileExtList = ['.jpg', '.jpeg', '.png', '.tiff', '.gif'];
        
        if (fileList.length == 0 && !existsOriginalPath) {
            logger.debug("PUSHING TO BE DELETED");
            foldersToDelete.push(fullPath);
            logger.debug(foldersToDelete);
            return 
        } else if (fileList.length > 0 && !existsOriginalPath) {

            for (let file of fileList) {
                let fullFilePath = path.join(fullPath, file);
                let fullOriginalsFilePath = originalsPath + "/" + file;
    
                logger.debug(fullFilePath);
                logger.debug(fullOriginalsFilePath);
                try {
                    fs.copySync(fullFilePath, fullOriginalsFilePath);
                }
                catch (err) {
                    logger.debug('remove sync error while deleting originals folder')
                    logger.error(err);
                }
            }

            return 
        } else {
            let originalsFileList = fs.readdirSync(originalsPath)
                .filter(file => imageFileExtList.indexOf(path.extname(file)) !== -1);

    
            fileList = fs.readdirSync(fullPath)
                .filter(file => imageFileExtList.indexOf(path.extname(file)) !== -1);
    
            if (originalsFileList.length > 0 && fileList.length > 0) {
                //do nothing
                logger.debug("We do nothing ");
                return 
            } else if (originalsFileList.length == 0 && fileList.length == 0) {
                logger.debug("PUSHING TO BE DELETED");
                foldersToDelete.push(fullPath);
                logger.debug(foldersToDelete);

                return foldersToDelete;
            } else if (originalsFileList.length > 0 && fileList.length == 0) {

                for (let file of fileList) {
                    let fullOriginalsFilePath = path.join(originalsPath, file);
                    let fullFilePath = path.join(fullPath, file);
    
                    logger.debug(fullFilePath);
                    logger.debug(fullOriginalsFilePath);

                    fs.copySync(fullOriginalsFilePath, fullFilePath);
                }

                return 
            } else if (fileList.length > 0 && originalsFileList.length == 0) {

                for (let file of fileList) {
                    let fullFilePath = path.join(fullPath, file);
                    let fullOriginalsFilePath = originalsPath + "/" + file;
    
                    logger.debug(fullFilePath);
                    logger.debug(fullOriginalsFilePath);

                    fs.copySync(fullFilePath, fullOriginalsFilePath);
                }
                return 
            }
        }
        return
    } catch (e) {
        throw e
    }

}

// /**
// * Ensure that originals and edited images are in sync. 
// * @param studioContext our studio context (see t32XrayStudioMain)  
// */
// function handleRequest(studioContext, requestId) {
//     var deferred = q.defer();

//     logger.debug('-----SYNC FOLDERS------');
//     var tab32Root = studioContext.setting.tab32Root;
//     var imageFileExtList = ['.jpg', '.jpeg', '.png', '.tiff', '.gif'];
//     var pathsToRemove = [];
//     var dirList = fs.readdirSync(tab32Root)
//         .filter(file => fs.lstatSync(path.join(tab32Root, file)).isDirectory());

//     async.eachSeries(dirList, function (dirPath, callback) {
//         var directoryPath = dirPath;
//         var fullPath = path.join(tab32Root, dirPath);
//         logger.debug('****FULL PATH***');
//         logger.debug(fullPath);
//         logger.debug('***FILE LIST***');
//         logger.debug(fs.readdirSync(fullPath));
//         var fileList = fs.readdirSync(fullPath)
//             .filter(file => _.contains(imageFileExtList, path.extname(file)));

//         var originalsPath = fullPath + "/originals";
//         var existsOriginalPath = fs.existsSync(originalsPath);

//         syncFolder(fullPath, fileList, originalsPath, existsOriginalPath, pathsToRemove)
//         .then(function(){
//             callback();
//         })
//         .catch(function(err){
//             callback(err);
//         })
//     }, function (err) {
//         if (err) {
//             deferred.reject({ requestId: requestId, error: err });
//         }
//         else {
//             logger.debug("--paths to be removed---");
//             logger.debug(pathsToRemove);
//             deferred.resolve({requestId: requestId, result: pathsToRemove});
//         }
//     })	 	

//     return deferred.promise;
// }


// function syncFolder(fullPath, fileList, originalsPath, existsOriginalPath, foldersToDelete) {
//     var deferred = q.defer();
//     var imageFileExtList = ['.jpg', '.jpeg', '.png', '.tiff', '.gif'];
    
//     if (fileList.length == 0 && !existsOriginalPath) {
//         logger.debug("PUSHING TO BE DELETED");
//         foldersToDelete.push(fullPath);
//         logger.debug(foldersToDelete);
//         deferred.resolve(foldersToDelete);
//     }
//     else if (fileList.length > 0 && !existsOriginalPath) {
//         _.each(fileList, function (file) {
//             var fullFilePath = path.join(fullPath, file);
//             var fullOriginalsFilePath = originalsPath + "/" + file;

//             logger.debug(fullFilePath);
//             logger.debug(fullOriginalsFilePath);
//             try {
//                 fs.copySync(fullFilePath, fullOriginalsFilePath);
//             }
//             catch (err) {
//                 logger.debug('remove sync error while deleting originals folder')
//                 logger.error(err);
//             }
//         });
//         deferred.resolve();
//     }
//     else {
//         try {
//             var originalsFileList = fs.readdirSync(originalsPath)
//                 .filter(file => _.contains(imageFileExtList, path.extname(file)));
//         }
//         catch (err) {
//             logger.debug('remove sync error while reading originalsPath folder')
//             logger.info(err);
//             deferred.reject()
//         }

//         try {
//             fileList = fs.readdirSync(fullPath)
//                 .filter(file => _.contains(imageFileExtList, path.extname(file)));
//         }
//         catch (err) {
//             logger.debug('remove sync error while reading fullPath folder')
//             logger.info(err);
//             deferred.reject()
//         }

//         if (originalsFileList.length > 0 && fileList.length > 0) {
//             //do nothing
//             logger.debug("We do nothing ");
//             deferred.resolve();
//         }
//         else if (originalsFileList.length == 0 && fileList.length == 0) {
//             logger.debug("PUSHING TO BE DELETED");
//             foldersToDelete.push(fullPath);
//             logger.debug(foldersToDelete);
//             deferred.resolve(foldersToDelete);
//         }
//         else if (originalsFileList.length > 0 && fileList.length == 0) {
//             _.each(originalsFileList, function (file) {
//                 var fullOriginalsFilePath = path.join(originalsPath, file);
//                 var fullFilePath = path.join(fullPath, file);

//                 logger.debug(fullFilePath);
//                 logger.debug(fullOriginalsFilePath);
//                 try {
//                     fs.copySync(fullOriginalsFilePath, fullFilePath);
//                 }
//                 catch (err) {
//                     logger.debug('remove sync error while deleting originals folder')
//                     logger.info(err);
//                     deferred.reject()
//                 }
//             });
//             deferred.resolve()
//         }
//         else if (fileList.length > 0 && originalsFileList.length == 0) {
//             _.each(fileList, function (file) {
//                 var fullFilePath = path.join(fullPath, file);
//                 var fullOriginalsFilePath = originalsPath + "/" + file;

//                 logger.debug(fullFilePath);
//                 logger.debug(fullOriginalsFilePath);
//                 try {
//                     fs.copySync(fullFilePath, fullOriginalsFilePath);
//                 }
//                 catch (err) {
//                     logger.debug('remove sync error while deleting originals folder')
//                     logger.info(err);
//                     deferred.reject()
//                 }
//             });
//             deferred.resolve();
//         }
//     }

//     return deferred.promise;
// }

