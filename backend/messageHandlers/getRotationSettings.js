// const q = require("q");
const t32RestServices = require('../t32RestServices');
const t32RestEndPoints = require('../t32RestEndPoints');
const logger = require('electron-log');

module.exports.handleRequest = handleRequest;

async function handleRequest(studioContext, requestId, requestObject) {
    try {
        logger.debug('...getRotationSettings');
    
        // logger.debug('HERE IS THE REQUEST OBJ');
        // logger.debug(requestObject);
    
        let postBody = {
            clinic_id: requestObject.clinic_id
        }
    
        let headers = null;
        let urlParam = null;
        let formData = null;
    
        let settings = await t32RestServices.execForResult(
            t32RestEndPoints.getRotationSettings.routePath,
            t32RestEndPoints.getRotationSettings.method,
            urlParam,
            postBody,
            headers,
            formData)
            
        return {
            requestId: requestId,
            result: settings
        }

    } catch (e) {
        logger.info('error getRotationSettings ', e);
        throw {requestId: requestId, error: e}
    }
}