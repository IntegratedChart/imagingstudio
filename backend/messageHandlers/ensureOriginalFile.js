
// const q = require("q");
const path = require("path");
const logger = require('electron-log');
const fs = require('fs-extra');


module.exports.handleRequest = handleRequest;

/**
*
* @param studioContext our studio context (see t32XrayStudioMain)  
* @param requestObject 
*      {
*          selectedFolder 
*      }
*/
async function handleRequest(studioContext, requestId, requestObject) {
  try {
		logger.info("ENSURING ORIGINAL FILE EXISTS");
		logger.info(requestObject);
	
		let filePath = path.join(studioContext.setting.tab32Root, requestObject.fileName);
		let originalFilePath = path.join(studioContext.setting.tab32Root, requestObject.originalFileName);
	
		if(!fs.existsSync(originalFilePath)){
			fs.copySync(filePath, originalFilePath);
		}
		
		return {requestId: requestId, result: 'success'}
	} catch (e) {
		logger.info("error ensuring original file ", e);
		throw { requestId: requestId, error: err }
	} 

}

// /**
// *
// * @param studioContext our studio context (see t32XrayStudioMain)  
// * @param requestObject 
// *      {
// *          selectedFolder 
// *      }
// */
// function handleRequest(studioContext, requestId, requestObject) {
//     var deferred = q.defer();

//     logger.info("ENSURING ORIGINAL FILE EXISTS");
//     logger.info(requestObject);


//     var filePath = path.join(studioContext.setting.tab32Root, requestObject.fileName);

//     var originalFilePath = path.join(studioContext.setting.tab32Root, requestObject.originalFileName);


//     var wrapper = { requestId: requestId };

//     try{
//         if(fs.existsSync(originalFilePath)){
//             deferred.resolve(wrapper);   
//         }
//         else{
//             fs.copySync(filePath, originalFilePath)
//             deferred.resolve(wrapper);
//         }
//     }
//     catch(err){
//         deferred.reject({ requestId: requestId, error: err });
//     }

//     return deferred.promise;
// }