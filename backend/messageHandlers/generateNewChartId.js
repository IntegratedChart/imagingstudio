
// const q = require("q");
const t32RestServices = require('../t32RestServices');
const t32RestEndPoints = require('../t32RestEndPoints');
const logger = require('electron-log');


module.exports.handleRequest = handleRequest;


/**
* Handle 'generateNewChartId' request from the client side.
*
* @param studioContext our studio context (see t32XrayStudioMain) 
* @param requestObject : {
            clinic_id : 
         }
*/
async function handleRequest( studioContext, requestId, requestObject ){
  try {
    logger.debug('generateNewChartId: ' + requestObject );
  
    let urlParam = {clinic_id : requestObject.clinic_id};
    let postBody = null;
    let headers = {'clinic-id' : requestObject.clinic_id };
  
    let results = await t32RestServices.execForResult( 
      t32RestEndPoints.generateNewChartId.routePath, 
      t32RestEndPoints.generateNewChartId.method,
      urlParam,
      postBody,
      headers);

   return { requestId, requestId, result : results};
   
  } catch (e) {
    logger.infor('error generating new chart id ', e)
    throw {requestId : requestId, error: e};
  }
}
