
// const q = require("q");
const path = require("path");
const fs = require('fs');
const logger = require('electron-log');


module.exports.handleRequest = handleRequest;

/**
 *
 * @param studioContext our studio context (see t32RequestDispatcher)  
 * @param msgContent : 
 *              {
 *                  imageData : 
 *                  fileName  : 
 *              }
 */
async function handleRequest(studioContext, requestId, msgContent){

    try { 
        let tab32Root = studioContext.setting.tab32Root;
        let fullFilePath = path.join(tab32Root, msgContent.fileName);
        logger.debug('saveImage, fileName : ' + fullFilePath);
    
        let data = msgContent.imageData.replace(/^data:image\/\w+;base64,/, "");
        let buf = new Buffer(data, 'base64');

        await new Promise((resolve, reject) => {
            fs.writeFile(fullFilePath, buf, function(err){
                if ( err ) {
                    reject(err);
                }
                else {
                    resolve();          
                }
            });
        })

        return { requestId: requestId, result: {} };
    } catch (e) {
        logger.info('error saveImage ', e);
        return { requestId: requestId, error: e };
    }
}