
// const q = require("q");
// const _ = require("lodash");
const path = require("path");
const fs = require('fs');
const logger = require('electron-log');
const moment = require('moment');
const os = require('os');
const t32rayStudioDirHandler = require("../services/t32XrayStudioDirHandler");

module.exports.handleRequest = handleRequest;

/**
 * Given a directory path, return all files in the directory
 *
 * @param studioContext our studio context (see t32XrayStudioMain)  
 */
async function handleRequest(studioContext, requestId, directoryPath ){
  try {
    
    
    let imageFileExtList = ['.jpg', '.jpeg', '.png', '.tiff', '.gif'];
    logger.debug('..... getDirectoryFileList for ' + directoryPath);
      
    let dirPath = path.join(studioContext.setting.tab32Root, directoryPath );

    let fileList = await t32rayStudioDirHandler.getDirectoryFileList(dirPath, imageFileExtList);

    return { requestId: requestId, result: fileList };
    // if(fs.existsSync(dirPath)){
    //   fileList = fs.readdirSync(dirPath).filter(function(file){
    //     logger.debug("Here are the files");
    //     logger.debug(file)
    //     var extName = path.extname(file) || '';

    //     return fs.lstatSync(path.join(dirPath, file)).isFile() && imageFileExtList.indexOf( extName.toLowerCase() ) !== -1;
    //   });

    //   fileList = sortFileByDateModified(fileList, dirPath);

    //   return { requestId : requestId, result: fileList};
  
    // } else {
    //   throw 'directory not found'
    // }
  } catch (e) {
    logger.info("failed to get directory file ", e)
    throw { requestId: requestId, error: e };
  }
}

// function sortFileByDateModified(fileList, dirPath){
//   try{
//     logger.debug("The Machine info: ");
//     logger.debug(os.platform());
//     let sortedFileList = fileList.sort((fileA, fileB) => {
//       let directoryPathA = path.join(dirPath, fileA);
//       let statsA = fs.lstatSync(directoryPathA);
//       let directoryPathB = path.join(dirPath, fileB);
//       let statsB = fs.lstatSync(directoryPathB);

//       //   // logger.debug("the ctime of the file");
//       //   // logger.debug(file);
//       //   // logger.debug(stats.ctime);
//       //   // logger.debug("the stats of the file");
//       //   // logger.debug(stats);

//       //   //in the stats output the ctime is the mtimeMs. The birthtime is the time created. 
//       //   return new moment(stats.ctime);
//       let dateA = new Date(moment(statsA.ctime))
//       let dateB = new Date(moment(statsB.ctime))

//       if (dateA > dateB) {
//         return -1;
//       }
//       if (dateA < dateB) {
//         return 1;
//       }
//       return 0;
//     })
            
//     logger.debug("This is the sorted filelist ");
//     logger.debug(sortedFileList);

//     return sortedFileList;
//   }
//   catch(err){
//     logger.debug("error sorting directory file list")
//     throw err;
//   }
   
// }

// _.sortBy(fileList, function (file) {
//   let directoryPath = path.join(dirPath, file);
//   let stats = fs.lstatSync(directoryPath);

//   // logger.debug("the ctime of the file");
//   // logger.debug(file);
//   // logger.debug(stats.ctime);
//   // logger.debug("the stats of the file");
//   // logger.debug(stats);

//   //in the stats output the ctime is the mtimeMs. The birthtime is the time created. 
//   return new moment(stats.ctime);
// }).reverse();
