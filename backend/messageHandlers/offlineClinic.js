'use strict';
const t32RestServices = require('../t32RestServices');
const t32RestEndPoints = require('../t32RestEndPoints');
const logger = require('electron-log');
const t32OfflineDataHandler = require('../services/t32OfflineDataHandler');

module.exports.handleRequest = handleRequest;

/**
 * @async
 * @param studioContext our studio context (see t32RequestDispatcher)
 * @msgContent download query
 */
async function handleRequest(studioContext, requestId, msgContent) {
    try {
        logger.info('...offline clinic reqeuestId', requestId);

        let result = await t32OfflineDataHandler.handleOfflineClinicRequest(msgContent)

        return { requestId: requestId, result: result };

    } catch (error) {
        logger.error('offline download error', error);
        logger.info(error.stack)
        throw { requestId, error };
    }
}