var logger = require('electron-log');

module.exports.handleRequest = handleRequest;

async function handleRequest(studioContext, requestId, requestObject) {
    try{
        logger.info("we have request to get studio root path");
        let wrapper =  { requestId: requestId, result: studioContext.setting.tab32Root };
        return wrapper;
    }
    catch(err){
        logger.info('error', err);
        throw err;
    }

}