
// const q = require("q");
var path = require("path");
var fs = require('fs-extra');
var logger = require('electron-log');


module.exports.handleRequest = handleRequest;

/**
*
* @param studioContext our studio context (see t32XrayStudioMain)  
* @param requestObject 
*      {
*          isDirectory   :
*          filePath : the path (either a file or directory)
*      }
*/
async function handleRequest(studioContext, requestId, requestObject ){

	try { 
		logger.info('deleteXrays: requestObject= ' );
		logger.info(requestObject);
	
		var fullPath = requestObject.filePath ? path.join(studioContext.setting.tab32Root, requestObject.filePath) : null;
		
		var originalFullPath = requestObject.originalFilePath ? path.join(studioContext.setting.tab32Root, requestObject.originalFilePath) : null;
	
		if (!fullPath) {
			throw "no filepath defined";
		}
	
		if (requestObject.isDirectory) {
			await fs.emptyDir(fullPath)
			
			if(originalFullPath){
				await fs.emptyDir(originalFullPath)
			}
		} else {
			fs.removeSync(fullPath);
			fs.removeSync(originalFullPath);
		}

		return { requestId: requestId, result: 'ok' };

	} catch (e) {
		logger.info("error with deleting xrays ", e);
		return { requestId: requestId, error: e }
	}
}
// /**
// *
// * @param studioContext our studio context (see t32XrayStudioMain)  
// * @param requestObject 
// *      {
// *          isDirectory   :
// *          filePath : the path (either a file or directory)
// *      }
// */
// function handleRequest(studioContext, requestId, requestObject ){
// 	var deferred = q.defer();

// 	logger.info('deleteXrays: requestObject= ' );
// 	logger.info(requestObject);

// 	var fullPath = requestObject.filePath ? path.join(studioContext.setting.tab32Root, requestObject.filePath) : null;
	
// 	var originalFullPath = requestObject.originalFilePath ? path.join(studioContext.setting.tab32Root, requestObject.originalFilePath) : null;

// 	if(!fullPath){
// 		deferred.reject({requestId: requestId, error: "no filepath defined"});
// 	}

// 	if(requestObject.isDirectory){
// 		fs.emptyDir(fullPath)
// 		.then(() => {
// 			if(originalFullPath){
// 				fs.emptyDir(originalFullPath)
// 				.then(() => {
// 					var wrapper = { requestId: requestId, result: 'ok' };
// 					deferred.resolve(wrapper);
// 				})
// 			}
// 			else{
// 				var wrapper = { requestId: requestId, result: 'ok' };
// 				deferred.resolve(wrapper);				
// 			}
// 		})
// 		.catch(err => {
// 			deferred.reject({requestId: requestId, error: err});
// 		})
// 	}
// 	else{
// 		try{
// 			fs.removeSync(fullPath);
// 			fs.removeSync(originalFullPath);
// 			var wrapper = { requestId: requestId, result: 'ok' };
// 			deferred.resolve(wrapper);
// 		}
// 		catch(err){
// 			deferred.reject({ requestId: requestId, error: err });
// 		}
// 		// fs.remove(fullPath)	
// 		// .then(() => {
// 		// 	fs.remove(originalFullPath)
// 		// 	.then(() => {
// 		// 		var wrapper = { requestId: requestId, result: 'ok' };
// 		// 		deferred.resolve(wrapper);
// 		// 	})
// 		// 	.catch(function (err) {
// 		// 		logger.error(err);
// 		// 		logger.error(err.stack);
// 		// 		deferred.reject({ requestId: requestId, error: err });
// 		// 	})
// 		// })
// 		// .catch(function(err){
// 		// 	logger.error(err);
// 		// 	logger.error(err.stack);
// 		// 	deferred.reject({ requestId: requestId, error: err });
// 		// })
// 	}
// 	return deferred.promise;
// }
