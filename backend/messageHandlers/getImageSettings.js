const {readJson} = require('../services/t32XrayStudioFs');
// const q = require("q");
const path = require("path");
const logger = require('electron-log');
const t32XrayStudioSettingsService = require('../services/t32XrayStudioSettingsService');

module.exports.handleRequest = handleRequest;

/**
 * Get the image settings for each image, rotaiton, brightness, etc..
 * @param {*} studioContext 
 * @param {*} requestId 
 */
async function handleRequest(studioContext, requestId){
    try {
        let setting = await t32XrayStudioSettingsService.getSettings()

        let settingsPath = setting.currentSelectedFolder + "/settings.json"
        let settingsFullPath = path.join(setting.tab32Root, settingsPath);

        logger.debug("SETTINGS IMAGE PATH");
        logger.debug(settingsFullPath);

        let imageSettings = await readJson(settingsFullPath);

        return { requestId: requestId, result: imageSettings };
    } catch (e) {
        logger.info("error getImageSettings ", e);
        throw { requestId: requestId, error: err }
    }
}
