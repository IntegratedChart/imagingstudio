// const q = require("q");
const path = require("path");
const fs = require('fs-extra');
const logger = require('electron-log');
const t32XrayStudioImageManipulation = require("../services/t32XrayStudioImageManipulation");



module.exports.handleRequest = handleRequest;

/**
 *
 * @param studioContext our studio context (see t32RequestDispatcher)  
 * @param requestObject : 
 *              {
 *                  imageUrls : [{fileName, originalFileName}],
 *                  brightValue:
 *                  contrastValue:
 *              }
 */
async function handleRequest(studioContext, requestId, requestObject) {

    try {    
        logger.debug(requestObject.imageUrls)
        let tab32Root = studioContext.setting.tab32Root;
        let brightValue = parseInt(requestObject.brightValue) / 100;
        // let adjustValue = parseInt(requestObject.value) / 100;
        let contrastValue = parseInt(requestObject.contrastValue) / 100;
    
        // logger.debug("HERE IS THE ADJUST VALUE")
        // logger.debug(adjustValue);
        if (!requestObject.imageUrls || requestObject.imageUrls.length == 0 || requestObject.imageUrls == "") {
            logger.debug("WE ARE REJECTING")
            throw "Image url not found"
        } else {
            for (let i = 0; i < requestObject.imageUrls.length; i++) {
                let imageUrl = requestObject.imageUrls[i];

                // let fileDesc = imageUrl.desc;
                let fullFilePath = path.join(tab32Root, imageUrl.fileName);
                let fullOriginalFilePath = path.join(tab32Root, imageUrl.originalFileName);

                if (!fs.existsSync(fullOriginalFilePath)) {
                    fs.copySync(fullFilePath, fullOriginalFilePath)
                }

                await t32XrayStudioImageManipulation.manipulateImage(fullOriginalFilePath, fullFilePath, brightValue, contrastValue)
            }
        }

        return { requestId: requestId, result: true };

    } catch (e) {
        logger.info("error manipulateImage ", e);
        throw {requestId: requestId, error: e};
    }
}

// /**
//  *
//  * @param studioContext our studio context (see t32RequestDispatcher)  
//  * @param requestObject : 
//  *              {
//  *                  imageUrls : [{fileName, originalFileName}],
//  *                  brightValue:
//  *                  contrastValue:
//  *              }
//  */
// function handleRequest(studioContext, requestId, requestObject) {
//     var deferred = q.defer();

//     logger.debug(requestObject.imageUrls)
//     var tab32Root = studioContext.setting.tab32Root;
//     var brightValue = parseInt(requestObject.brightValue) / 100;
//     // var adjustValue = parseInt(requestObject.value) / 100;
//     var contrastValue = parseInt(requestObject.contrastValue) / 100;

//     // logger.debug("HERE IS THE ADJUST VALUE")
//     // logger.debug(adjustValue);
//     if(!requestObject.imageUrls || requestObject.imageUrls.length == 0 || requestObject.imageUrls == ""){
//         logger.debug("WE ARE REJECTING")
//         deferred.reject({ requestId: requestId, error: "Image url not found" });
//     }
//     else{
//         async.each(requestObject.imageUrls, function (imageUrl, callback) {
//             logger.debug("IMAGE URL...");
//             logger.debug(imageUrl);

//             var fileDesc = imageUrl.desc;
//             var fullFilePath = path.join(tab32Root, imageUrl.fileName);
//             var fullOriginalFilePath = path.join(tab32Root, imageUrl.originalFileName);
//             if (!fs.existsSync(fullOriginalFilePath)) {
//                 try {
//                     fs.copySync(fullFilePath, fullOriginalFilePath)
//                 }
//                 catch (err) {
//                     logger.error(err);
//                     deferred.reject({ requestId: requestId, error: 'failed to copy original file' });
//                     return
//                 }
//             }

//             manipulateImage(studioContext, requestId, fileDesc, fullOriginalFilePath, fullFilePath, brightValue, contrastValue).then(function (newImage) {
//                 logger.debug("THE NEW IMAGE COMING IN...")
//                 // logger.debug(newImage);
//                 callback();
//             })
//             .catch(function (err) {
//                 console.log(err);
//                 callback(err);
//             })
//         }, function (err) {
//             if (err) {
//                 deferred.reject({ requestId: requestId, error: err });
//             }
//             else {
//                 var wrapper = { requestId: requestId, result: true }
//                 deferred.resolve(wrapper);
//             }
//         })
//     }
//     return deferred.promise;
// }

// function manipulateImage(studioContext, requestId, fileDesc, originalFilePath, filePath, brightValue, contrastValue) {
//     var deferred = q.defer();
//         Jimp.read(originalFilePath)
//         .then(img => {
//             logger.debug(img);
//             img.brightness(brightValue).contrast(contrastValue)
//                 .getBuffer(Jimp.AUTO, (err, buf) => {
//                     logger.debug("HERE IS THE IMAGE ")
//                     // logger.debug(image);
//                     if (err) {
//                         deferred.reject(err);
//                     }
//                     else {
//                         fs.writeFile(filePath, buf, (err) => {
//                             if (err) {
//                                 logger.error(err);
//                                 logger.error(err.stack);
//                                 deferred.reject({ requestId: requestId, error: err });
//                             }
//                             else {
//                                 deferred.resolve();
//                                 // getImageSettings.handleRequest(studioContext, requestId++)
//                                 // .then(wrapper => {
//                                 //     var imageSettings = wrapper.result;
//                                 //     imageSettings[fileDesc] = {
//                                 //         brightness: brightValue,
//                                 //         contrast: contrastValue
//                                 //     }
    
//                                 //     // var requestObject = {};
//                                 //     // requestObject.settings = imageSettings;
    
//                                 //     var requestId = requestId += 2;
    
//                                 //     saveImageSettings.handleRequest(studioContext, requestId, imageSettings)
//                                 //     .then(wrapper => {
//                                 //         deferred.resolve();
//                                 //     })
//                                 //     .catch(err => {
//                                 //         deferred.reject({ requestId: requestId, error: err });
//                                 //     })
//                                 // })
//                                 // .catch(err => {
//                                 //     deferred.reject({ requestId: requestId, error: err });
//                                 // })
//                             }
//                         });
//                     }
//                 });
//         })
//         .catch(err => {
//             console.log(err)
//             deferred.reject({ requestId: requestId, error: err });
//         });
//     return deferred.promise;
// }
