// const q = require("q");
const logger = require('electron-log');
const t32XrayStudioChannelManager = require('../services/channelServices/t32XrayStudioChannelManager');

module.exports.handleRequest = handleRequest;

async function handleRequest(studioContext, requestId, requestObject) {
    try {        
        logger.debug('GETTING REQUEST TO SEND OUTGOING MSG');
        logger.debug(requestObject);
    
        let channel = requestObject.channel;
        let msg = requestObject.msg;
    
        await t32XrayStudioChannelManager.handleOutgoingChannelMsg(channel, msg);
    
        return {
            requestId: requestId,
            result: 'done'
        }
    } catch (e) {
        logger.info("error sendOutgoingMsg ", e);
    }

}