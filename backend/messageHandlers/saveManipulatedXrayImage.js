// const q = require("q");
const path = require("path");
const fs = require('fs');
const logger = require('electron-log');
// var Canvas = require('canvas');
// var fabric = require('fabric').fabric;


module.exports.handleRequest = handleRequest;

/**
 *
 * @param studioContext our studio context (see t32RequestDispatcher)  
 * @param requestObject : 
 *              {
 *                  filePath : [{fileName, originalFileName}],
 *                  imageObj : {},
 *                  canvasDimensions: {}
 *              }
 */
async function handleRequest(studioContext, requestId, requestObject) {
    try {
        if (!requestObject.filePath || requestObject.filePath == "") {
           throw "Image url not found";
        }
        // logger.debug('here is the request object')
        // logger.debug(requestObject);
        let tab32Root = studioContext.setting.tab32Root;
        let fullFilePath = path.join(tab32Root, requestObject.filePath);
        // let imageObj = requestObject.imageObj;
        // let imageSrc = requestObject.imageSrc;
        // let canvasDimensions = requestObject.canvasDimensions;
        // let imageFilters = requestObject.imageFilters;
        // logger.debug("HERE IS THE ADJUST VALUE")
        // logger.debug(adjustValue);
        let dataUri = requestObject.dataUri;
        
        await new Promise((resolve, reject) => {
            fs.writeFile(fullFilePath, dataUri.replace('data:image/png;base64,', ""), 'base64', (err) => {
                if (err) {
                   reject(err)
                }
                else {
                    resolve()
                }
            })
        })

        return { requestId: requestId, result: true }
    } catch (e) {
        logger.info("error saveManipulatedXrayImage ", e);
        throw { requestId: requestId, error: e };
    }
}





// The below funcitons can be used once we figure out 


// /**
//  * 
//  * @param {*} filePath File path of image
//  * @param {*} imageObj the fabric image object that we need to manipulate
//  * @param {*} canvasDimensions the dimensions of the parent canvas
//  * @param {*} imageFilters the image filter (brightness, contrast, sharpness etc..)
//  */
// function manipulateImage(filePath, imageObj, canvasDimensions, imageFilters) {
//     var deferred = q.defer();

//     logger.debug("HERE IS THE IMAGE OBJECT")
//     logger.debug(imageObj);
//     // var nodeCanvas = new Canvas(200, 200);
//     // var ctx = canvas.getContext('2d');
//     // logger.debug(nodeCanvas);
//     // logger.debug(ctx);
//     // var parsedImgObj = JSON.parse(imageObj);
//     // console.log("Here is the parsed image object");
//     // console.log(parsedImgObj);
//     // var height = parsedImgObj.height;
//     // var width = parsedImgObj.width;

//     var canvas = fabric.util.createCanvasElement();
//     var nodeCanvas = new fabric.StaticCanvas(canvas, {height: canvasDimensions.height, width: canvasDimensions.width});
//     nodeCanvas.clear();
//     logger.debug("HERE IS THE Canvas");
//     logger.debug(nodeCanvas);

//     // logger.debug("image source");
//     // logger.debug(imgSrc);

//     logger.debug("---IMAGE OBJ-----")
//     logger.debug(imageObj);

//     fabric.Image.fromObject(imageObj, function (img) {
//         if (img) {
//             nodeCanvas.add(img);
//             if (imageObj.hasOwnProperty('angle')) {
//                 if (imageObj.angle == 90 || imageObj.angle == 270 || imageObj.angle == -90 || imageObj.angle == -270) {
//                     nodeCanvas.setHeight(imageObj.width);
//                     nodeCanvas.setWidth(imageObj.height);
//                 }
//             }

//             img.set({
//                 scaleX: 1,
//                 scaleY: 1
//             });

//             nodeCanvas.centerObject(img);
//             nodeCanvas.renderAll();

//             applyImageFilters(imageFilters, img, nodeCanvas);

//             logger.debug(img);
//             logger.debug("HERE IS THE Canvas after image");
//             logger.debug(nodeCanvas);

//             try {
//                 fs.writeFile(filePath, nodeCanvas.toDataURL().replace('data:image/png;base64,', ""), 'base64')
//                 .then(function (err) {
//                     if (err) {
//                         throw new Error(err);
//                     }
//                     else {
//                         deferred.resolve();
//                     }
//                 })
//             }
//             catch (err) {
//                 deferred.reject(err);
//             }
//         }
//         else {
//             logger.error("The image did not load")
//             deferred.reject();
//         }
//     })

//     // fabric.Image.fromURL(imageObj.src, function(img){
//     //     if(img){
//     //         nodeCanvas.add(img);
//     //         if(imageObj.hasOwnProperty('angle')){
//     //             if(imageObj.angle == 90 || imageObj.angle == 270 || imageObj.angle == -90 || imageObj.angle == -270){
//     //                 nodeCanvas.setHeight(imageObj.width);
//     //                 nodeCanvas.setWidth(imageObj.height);
//     //             }

//     //             img.set({
//     //                 angle: imageObj.angle
//     //             });

//     //             nodeCanvas.centerObject(img);
//     //             nodeCanvas.renderAll();
//     //         }

//     //         img.set({
//     //             flipX: imageObj.hasOwnProperty('flipX') ? imageObj.flipX : false,
//     //             flipY: imageObj.hasOwnProperty('flipY') ? imageObj.flipY : false
//     //         });

//     //         nodeCanvas.centerObject(img);
//     //         nodeCanvas.renderAll();

//     //         img.set({
//     //             scaleX: 1,
//     //             scaleY: 1
//     //         });

//     //         nodeCanvas.centerObject(img);
//     //         nodeCanvas.renderAll();

//     //         img.filters = [];

//     //         var brightness = new fabric.Image.filters.Brightness({
//     //             brightness: parseInt(imageFilters.brightness) / 100
//     //         });
        
//     //         img.filters.push(brightness);
        
//     //         var contrast = new fabric.Image.filters.Contrast({
//     //             contrast: parseInt(imageFilters.contrast) / 100
//     //         });
        
//     //         img.filters.push(contrast);
        
        
//     //         if (imageFilters.sharpen) {
//     //             var sharpen = new fabric.Image.filters.Convolute({
//     //                 matrix: [0, -1, 0, -1, 5, -1, 0, -1, 0]
//     //             })
        
//     //             img.filters.push(sharpen);
//     //         }
        
//     //         img.applyFilters();
//     //         nodeCanvas.centerObject(img);
//     //         nodeCanvas.renderAll();

//     //         try {
//     //             fs.writeFile(filePath, nodeCanvas.toDataURL().replace('data:image/png;base64,', ""), 'base64')
//     //                 .then(function (err) {
//     //                     if (err) {
//     //                         throw new Error(err);
//     //                     }
//     //                     else {
//     //                         deferred.resolve();
//     //                     }
//     //                 })
//     //         }
//     //         catch (err) {
//     //             deferred.reject(err);
//     //         }
//     //     }
//     //     else{
//     //         logger.error("The image did not load")
//     //         deferred.reject();
//     //     }
//     // })

//     return deferred.promise;
// }

// function applyImageFilters(data, fImage, fCanvas) {
//     fImage.filters = [];

//     var brightness = new fabric.Image.filters.Brightness({
//         brightness: parseInt(data.brightness) / 100
//     });

//     fImage.filters.push(brightness);

//     var contrast = new fabric.Image.filters.Contrast({
//         contrast: parseInt(data.contrast) / 100
//     });

//     fImage.filters.push(contrast);


//     if (data.sharpen) {
//         var sharpen = new fabric.Image.filters.Convolute({
//             matrix: [0, -1, 0, -1, 5, -1, 0, -1, 0]
//         })

//         fImage.filters.push(sharpen);
//     }

//     fImage.applyFilters();

//     fCanvas.renderAll();
// }
