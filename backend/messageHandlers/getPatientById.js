
// const q = require("q");
const t32RestServices = require('../t32RestServices');
const t32RestEndPoints = require('../t32RestEndPoints');
const logger = require('electron-log');


module.exports.handleRequest = handleRequest;


/**
* Get patient full info given id.
*
* @param studioContext our studio context (see t32XrayStudioMain) 
* @param searchObject : {
            clinic_id : 
            patient_id : 
         }
*/
async function handleRequest( studioContext, requestId, searchObject ){

   try {
      logger.debug('... getPatientById : clinic_id/patient_id = ' + searchObject.clinic_id + '/' + searchObject.patient_id);

      let urlParam = { clinic_id: searchObject.clinic_id, patient_id: searchObject.patient_id };
      let headers = { 'clinic-id': searchObject.clinic_id };
      let postBody = null;

      let patient = await t32RestServices.execForResult(
         t32RestEndPoints.findPatientByIdForXrayStudio.routePath,
         t32RestEndPoints.findPatientByIdForXrayStudio.method,
         urlParam,
         postBody,
         headers);

      return { requestId: requestId, result: patient }
   } catch (e) {
      logger.info("error getPatientById ", e)
      throw { requestId: requestId, error: e }
   }
}
