'use strict';
// const q = require("q");
const request = require('request');
const UrlPattern = require('url-pattern');
const logger = require('electron-log');
// const moment = require('moment');
const t32GoogleAuthApi = require('./t32GoogleAuthApi');
const fs = require('fs-extra');
const path = require("path");
// const https = request("https");


module.exports.initialize = initialize;
module.exports.execForResult = execForResult;
module.exports.mockRestCallSuccess = mockRestCallSuccess
/* Public Interface */
var studioContext;
var requestFuncMap;

function initialize( theContext = this)
{
   logger.info('... initialize t32RestServices ...');

   studioContext = theContext;

   requestFuncMap = {};
   requestFuncMap['POST'] = request.post;
   requestFuncMap['GET'] = request.get;
   requestFuncMap['PUT'] = request.put;
}

async function execForResult(endPoint, method, urlParam, postBody, headers, formData, resultType = "json", timeout = 120000) {
  logger.debug('... handling t32RestServices call...');

  try {
    // if (resultType == 'json') {
      return await makeRestCall(endPoint, method, urlParam, postBody, headers, formData, resultType, timeout);
    // } else {
    //   let host = studioContext.setting.ehrServer;
    //   host = host.replace(/\/$/, "")

    //   let options = {
    //     segmentNameCharset: 'a-zA-Z0-9_-'
    //   };

    //   let pattern = new UrlPattern(endPoint, options);
    //   let resolvedEndPoint = pattern.stringify(urlParam);
    //   let url =  host + resolvedEndPoint;

    //   return request(url);
    // }
  } catch (e) {
    if (e == 401) {
      logger.info("401 error checking further")
      /**
       * Updated 5/2/19 
       * We will take the inactivity time logic out and try to get refresh token and if still there is 401, then we will ask user to relogin
       */
      if (studioContext.ehrToken) {
        delete studioContext.ehrToken
        if (!studioContext.googleTokens) {
          sendAuthFailedMessage(studioContext);
          throw 'Authentication Failed - Invalid Ehr Token'
        }
      };

      logger.info('... refresh google token and retry...')
      let tokens = await t32GoogleAuthApi.refreshGoogleAccessToken(studioContext.googleTokens)
      studioContext.googleTokens = tokens;

      try {
        return await makeRestCall(endPoint, method, urlParam, postBody, headers, formData, resultType, timeout);
      } catch (e) {
        if (e == 401) {
          logger.info("401 error on retry ", e);
          delete studioContext.googleTokens;
          delete studioContext.authCode;
          sendAuthFailedMessage(studioContext);
        } else {
          throw e;
        }
      }
    } else {
      throw e;
    }
  }
}


async function makeRestCall(endPoint, method, urlParam, postBody, headers, formData, resultType, timeout ) {
  try {
    let host = studioContext.setting.ehrServer;
    host = host.replace(/\/$/, "")

    let options = {
      segmentNameCharset: 'a-zA-Z0-9_-'
    };

    let pattern = new UrlPattern(endPoint, options);
    let resolvedEndPoint = pattern.stringify( urlParam );

    let opts = {
      // make sure no double slash after the host name.
      url: host + resolvedEndPoint, 
      method: method,
      headers: headers || {},
      form: postBody,
      timeout: timeout
    };

    // instead of passing in stream, passing in the file name 
    //  so we can re-initialize the file stream on retry.
    if ( formData ) {
      opts.formData = {
        file: fs.createReadStream(formData)
      }
    }

    // insert google token and mac address into header
    opts.headers['google-tokens'] = JSON.stringify(studioContext.googleTokens);
    if(studioContext.ehrToken){
      opts.headers['ehr-token'] = studioContext.ehrToken.token;
      opts.headers['clinic-id'] = studioContext.ehrToken.clinicId;
    }
    opts.headers['license'] = studioContext.setting.license;
    opts.headers['machine-id'] = studioContext.setting.machineId;
    opts.headers['app-version'] = studioContext.theApp.app.getVersion();

    logger.info('Submiting rest request :')
    // logger.info(opts);

    var func = requestFuncMap[method];

    if (resultType == 'fileStream') {
      return func.call(this, opts);
    } else if (resultType == 'json') {
      return await new Promise((resolve, reject) => {
        func.call(this, opts, function (err, response, body) {
          // logger.info('... t32RestServices.. err/response');
          // logger.info(err);
          // logger.info(response);
  
          if (err) {
            // logger.error(opts);
            logger.info('Error received for request : ');
            logger.error(err);
            reject(err);
          } else {
            logger.debug('... response.statusCode = ' + response ? response.statusCode : 'N/A');
  
            if (response && response.statusCode == 401) {
              reject(401);
            }
            else if (response && response.statusCode == 404) {
              reject('not found');
            }
            else if (response && response.statusCode == 503) {
              reject('Service is temporarily not available, please try again later')
            } else if (response && response.statusCode == 500){
              reject('operation failed with error from server')
            } else {
              body = body || {};
              try {
                let result = JSON.parse(body);
                if (result.status) {
                  resolve(result.data);
                } else {
                  if (result.data) {
                    reject(result.data);
                  } else {
                    resolve(result)
                  }
                }
              } catch (err) {
                logger.error(err);
                reject(err);
              }
            }
          }
        })
      })
    }

  } catch (e) {
    throw e;
  }
}

function sendAuthFailedMessage(studioContext){
  let msg = {
    msgType: 'Authentication',
    authCode: ''
  };

  studioContext.browserWindow.notifyClient(msg);

  if (studioContext.browserWindow.mainWindow) {
  studioContext.browserWindow.notifyClient('Your google Authentication session has expired, please login again', 'update-message');
  }
}

async function mockRestCallSuccess(callTime) {
  try {
    return await new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve()
      }, callTime)
    })
  } catch (e) {
    throw e;
  }
}