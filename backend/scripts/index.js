const child = require('child_process');
const path = require('path');
const logger = require('electron-log');

module.exports.runScript = runScript;

/**
 * Runs scripts inside of the ./scripts folder.
 * @param {*} scriptName name of action that is also the name of the script to run
 * @param {*} dbConfig 
 * @param {*} additionalArgs 
 */
async function runScript(scriptName, args, ext='bat') {
    try {
        logger.info("running File cmd ", scriptName)
        let filePath = path.resolve(`./externalScripts/${scriptName}.${ext}`);
        // logger.info("the file path ", filePath);
        // let args = [dbConfig.username, dbConfig.password, dbConfig.database].concat(additionalArgs);
        // logger.info("args are ", args);
        return await runExecFile(filePath, args);
    } catch (e) {
        logger.error("error running file cmd  " + scriptName);
        throw e;
    }
}

/**
 * Node Exectue command wrapped in promise.
 * @param {*} filePath 
 * @param {*} args 
 */
function runExecFile(filePath, args = []) {
    return new Promise((resolve, reject) => {

        let callback = (error, stdout, stderr) => {
            if (error) {
                logger.info("error for exec file ", error)
                reject(error)
            } else if (stderr) {
                logger.info("stderr from exec file ", stderr);
                reject(stderr)
            } else {
                logger.info("stdout from exec file ", stdout);
                resolve(stdout)
            }
        }

        child.execFile(filePath, args, callback)

    })
}

