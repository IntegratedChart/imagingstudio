(function() {

	angular
		.module('app', ['ngRoute', 'ui.bootstrap', 'ui.mask', 'toaster', 'oitozero.ngSweetAlert', 'ngAnimate'])
		.config([ '$routeProvider', configure])
		.run(['$location', startApp])

	function configure($routeProvider) {
		// Configure a dark theme with primary foreground yellow
		// $mdThemingProvider
		// 	.theme('docs-dark', 'default')
		// 	.primaryPalette('yellow')
		// 	.dark()
		// 	.foregroundPalette['3'] = 'yellow';

		$routeProvider.
		    when('/', {
		        controller: 'landingCtrl',
		        templateUrl: 'pages/landing.html'
		    }).
		    otherwise({redirectTo: '/'});
	}

	function startApp($location) {
		console.log("SENDING APP TO LANDING");
  		$location.path('/');		
	}
})();