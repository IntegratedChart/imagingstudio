(function (window, angular, undefined) {
    'use strict';
    // angular.module('app').directive('t32XrayCreateAutoRotateSettings', ['$modal',
    //     function ($modal) {
    //         return {
    //             restrict: 'AE',
    //             scope: {
    //                 clinicId: '=',
    //                 afterEdit: '&',
    //             },
    //             link: function (scope, element, attr) {
    //                 element.bind('click', function (event) {
    //                     console.log("EVENT CLICKED");
    //                     var modalInstance = $modal.open({
    //                         templateUrl: 'widgets/autoRotate/t32XrayCreateAutoRotateSettings.html',
    //                         controller: 't32XrayEditAutoRotateSettingsCtrl',
    //                         windowClass: 'largest-Modal',
    //                         backdrop: 'static',
    //                         resolve: {
    //                             clinicId: function () {
    //                                 return scope.clinicId;
    //                             }
    //                         }
    //                     });
    //                     modalInstance.result.then(function (result) {
    //                         // alert("we have the results");
    //                         console.log(result)
    //                         scope.$eval(scope.afterEdit({ "result": result }));
    //                     });
    //                 })
    //             }
    //         };
    //     }
    // ]);

    angular.module('app').controller('t32XrayCreateAutoRotateSettingsCtrl', [
        '$q',
        '$scope',
        '$modalInstance',
        'logger',
        't32XrayImageService',
        '$timeout',
        'SweetAlert',
        't32AlertService',
        't32RotationSettingsService',
        'rotationSettings',
        't32CommonService',
        function (
            $q,
            $scope,
            $modalInstance,
            logger,
            t32XrayImageService,
            $timeout,
            SweetAlert,
            t32AlertService,
            t32RotationSettingsService,
            rotationSettings,
            t32CommonService) {
            function init() {
                $scope.rotationSettings = rotationSettings || [];
                $scope.currentRotationSetting = _.cloneDeep(t32RotationSettingsService.getEmptySettings());

                $scope.isChanged = false;
                $scope.formErrors = true;

                console.log("Inside rotate settings, currentRotationSetting");
                console.log($scope.currentRotationSetting);
                console.log("Inside rotate settings, rotationSettings");
                console.log($scope.rotationSettings);

                var initialSubSettingKey = Object.keys($scope.currentRotationSetting.setting)[0];
                $scope.selectedSubSetting = $scope.currentRotationSetting.setting[initialSubSettingKey].setting;

                console.log(initialSubSettingKey);
                console.log($scope.selectedSubSetting);
            }

            $scope.cancel = function () {
                if(!$scope.isChanged){
                    $modalInstance.close({
                        modified: false
                    })
                }
                else if(!$scope.formErrors){
                    t32CommonService.askToSave('rotation settings')
                    .then(function (answer) {
                        if (answer) {
                            if (t32CommonService.doesExist($scope.currentRotationSetting.name, $scope.rotationSettings, 'name')) {
                                var errorObj = {
                                    type: "error",
                                    title: "Name already exists.",
                                    text: "The rotation setting name is already taken, please enter another name",
                                    timeout: 5000,
                                    showCloseButton: true
                                }
                                console.log("About to show name error");

                                $timeout(function(){
                                    t32AlertService.showToaster(errorObj)
                                }, 500)
                            }
                            else {
                                $scope.rotationSettings.push($scope.currentRotationSetting);

                                $modalInstance.close({
                                    modified: true,
                                    action: 'save',
                                    currentRotationSetting: $scope.currentRotationSetting,
                                    rotationSettings: $scope.rotationSettings
                                })
                            }
                        }
                        else {
                            $modalInstance.close({
                                modified: false
                            })
                        }
                    })
                }
                else{
                    var title = "Form Error"
                    var error = "The settings name is invalid, please try again.";

                    var errorObj = {
                        title: title,
                        text: error,
                        confirmText: "Close Anyways",
                        showCancel: true,
                        cancelText: "Retry"
                    }

                    t32AlertService.showError(errorObj)
                    .then(function (err) {
                        if (err.error == 'cancel') {
                            // do nothing and let them retry
                        }
                        else {
                            $modalInstance.close({
                                modified: false,
                                rotationSettings: $scope.unEditedRotationSettings,
                                currentRotationSetting: $scope.unEditedCurrentRotationSetting,
                            })
                        }
                    })
                }
            }

            $scope.saveRotationSetting = function () {

                if (t32CommonService.doesExist($scope.currentRotationSetting.name, $scope.rotationSettings, 'name')){
                    var errorObj = {
                        type: "error",
                        title: "Name already exists.",
                        text: "The rotation setting name is already taken, please enter another name",
                        showCloseButton: true,
                        timeout: 5000
                    }
                    t32AlertService.showToaster(errorObj)
                }
                else{
                    $scope.rotationSettings.push($scope.currentRotationSetting);
                    
                    $modalInstance.close({
                        modified: true,
                        action: 'save',
                        currentRotationSetting: $scope.currentRotationSetting,
                        rotationSettings: $scope.rotationSettings
                    })
                }
            }

            $scope.showSubSetting = function (setting) {
                $scope.selectedSubSetting = setting;
            }

            $scope.isSelected = function (data) {
                if ($scope.selectedSubSetting == data) {
                    return 'selected-list-item';
                }
                else {
                    return ''
                }
            }

            $scope.setIsChanged = function(){
                if($scope.isChanged){
                    return
                }
                else{
                    $scope.isChanged = true;
                }
            }

            $scope.rotate = function (key) {
                var deg = $scope.selectedSubSetting[key];
                return { "transform": "rotate(" + deg + "deg)" };
            }

            // $scope.$watch("currentRotationSetting.name", function(newVal, oldVal){
            //     if(newVal && newVal !== "" && newVal !== oldVal){
            //         $scope.formErrors = false;
            //         $scope.currentRotationSetting.name = t32CommonService.toTitleCase(newVal);
            //         $scope.currentRotationSetting.name = _.camelCase(newVal);
            //     }
            //     else if (newVal == ""){
            //         $scope.formErrors = true;
            //     }
            // })

            $scope.handleNameChange = function(){
                if ($scope.rotationSettingsCreateForm.name.$valid){
                    console.log("There are no errors");
                    $scope.setIsChanged();
                    $scope.formErrors = false;
                    $scope.currentRotationSetting.name = t32CommonService.toTitleCase($scope.currentRotationSetting.name);
                    // $scope.currentRotationSetting.name = _.camelCase($scope.currentRotationSetting.name);
                }
                else{
                    console.log("There is error");
                    $scope.setIsChanged();
                    $scope.formErrors = true;
                }
            }

            $scope.titleText = 'Rotation Setting'
            $scope.titleStyle = {};
            $scope.titleChildDivClass = 'branding';
            $scope.menuBarLen = 3;

            init();
        }
    ])
})(window, window.angular);