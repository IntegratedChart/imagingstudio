(function (window, angular) {
    'use strict';
    angular.module('app').directive('t32XrayAutoRotateDropdown', ['$modal',
        function ($modal) {
            return {
                restrict: 'AE',
                scope: {
                    clinicId: '=',
                    settings: '=',
                    currentRotationSetting: '=',
                    rotationSettings: '=',
                    deletedRotationSettings: '=',
                    onSelectionChange: '&',
                    show: "="
                },
                templateUrl: 'widgets/autoRotate/t32XrayAutoRotateDropdown.html',
                controller: 't32XrayAutoRotateDropdownCtrl',
            };
        }
    ]);

    angular.module('app').controller('t32XrayAutoRotateDropdownCtrl', [
        '$rootScope',
        '$scope',
        '$modal',
        '$q',
        '$timeout',
        't32AlertService',
        'logger',
        function (
            $rootScope,
            $scope,
            $modal,
            $q,
            $timeout,
            t32AlertService,
            logger) {
            function init() {
                // syncRotationSettings();
                // setupRotationSettings();
            }

            $scope.inSettingsSave = false;
            // $scope.settings = null;

            function setupRotationSettings(){
                // $scope.inSettingsSave = true;
                // logger.info("WE ARE LOGGED IN SO SYNCING SETTINGS");
                // t32RotationSettingsService.syncRotationSettings($scope.clinicId)
                // .then(function (settings) {
                    // $scope.settings = _.cloneDeep(settings);
                    logger.info("THE SETTINGS AFTER SYNC");
                    // logger.info(settings);
                    // logger.info($scope.settings);
                    // logger.info($scope.settings.rotationSettings);
                    
                    $scope.deletedRotationSettings = $scope.settings.deletedRotationSettings ? JSON.parse($scope.settings.deletedRotationSettings) : [];

                    if ($scope.settings.rotationSettings) {
                        if ( Array.isArray(JSON.parse($scope.settings.rotationSettings)) ) {
                            $scope.rotationSettings = JSON.parse($scope.settings.rotationSettings);

                            logger.info("THE ROTATION SETTING ARE DEFINED");
                            // logger.info($scope.rotationSettings);
                            
                            // logger.info('THE CURRENT ROTAITON SETTINGS');
                            // logger.info(JSON.parse($scope.settings.currentRotationSetting));

                            var currentRotationSettings = JSON.parse($scope.settings.currentRotationSetting);
                            

                            if($scope.rotationSettings.length > 0){
                                // $scope.rotationSettings = filterSettingsByClinicId($scope.rotationSettings, $scope.settings);
                                if(currentRotationSettings){
                                    var index = _.findIndex($scope.rotationSettings, function(setting){
                                        return _.isEqual(currentRotationSettings, setting);
                                    })

                                    if(index > -1){
                                        $scope.currentRotationSetting = $scope.rotationSettings[index]; 
                                    }
                                    else{
                                        $scope.currentRotationSetting = $scope.rotationSettings[0];
                                    }
                                }
                                else{
                                    $scope.currentRotationSetting = $scope.rotationSettings[0];
                                }
                            }
                            else{
                                setDefaultRotationSettings();
                            }

                        }
                        else {
                            setDefaultRotationSettings();
                        }

                    }
                    else {
                        logger.info("THE ROTATION SETTINGS ARE NOT DEFINED, SETTING DEFAULT");
                        setDefaultRotationSettings();
                        logger.info("DEFAULT SETTINGS");
                        logger.info($scope.rotationSettings)
                    }
                    // $scope.inSettingsSave = false;
                // })
                // .catch(function (err) {
                //     console.error(err);
                //     var title = "Fatal Error"
                //     var error = "There was a problem getting the current rotation settings, please try agian later. "

                //     var errorObj = {
                //         type: "error",
                //         title: title, 
                //         text: error,
                //         timeout: 5000,
                //         showCloseButton: true
                //     }

                //     t32AlertService.showToaster(errorObj)
                //     $modalInstance.close(result);
                // })
            }

            function isNotEmpty(str) {
                if (str !== "" && str != null && str != undefined && str !== {}) {
                    return true;
                }
                else {
                    return false;
                }
            }

            function setDefaultRotationSettings() {
                $scope.$emit("setAndSaveDefaultSettings")
            }

            $scope.onChange = function () {
                if($scope.currentRotationSetting != "" || $scope.currentRotationSetting != null){
                    $scope.$eval($scope.onSelectionChange);
                }
                logger.info($scope.currentRotationSetting);
            };

            $scope.onEdit = function (evt) {
                if (evt) {
                    evt.preventDefault();
                }
                // evt.preventDefault();
                var modalInstance = $modal.open({
                    templateUrl: 'widgets/autoRotate/t32XrayEditAutoRotateSettings.html',
                    controller: 't32XrayEditAutoRotateSettingsCtrl',
                    windowClass: 'sixtyPercent-Modal',
                    backdrop: 'static',
                    resolve: {
                        rotationSettings: function(){
                            return $scope.rotationSettings;
                        },
                        currentRotationSetting: function(){
                            return $scope.currentRotationSetting;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if(result.modified){
                        logger.info("We need to save our rotation settings");
                        // alert("Need to save these settings")
                        // $scope.rotationSettings = result.rotationSettings,
                        // $scope.currentRotationSetting = result.currentRotationSetting;
                        if(result.action == 'save'){
                            $scope.$emit('updateRotationSettings', result);
                        }
                        else if(result.action == 'delete'){
                            $scope.$emit('deleteRotationSettings');
                        }
                    }
                    else{
                        $scope.rotationSettings = result.rotationSettings;
                        $scope.currentRotationSetting = result.currentRotationSetting;
                        logger.info($scope.rotationSettings);
                        logger.info($scope.currentRotationSetting.name);
                    }
                });
            }

            $scope.onCreate = function(evt){
                if (evt) {
                    evt.preventDefault();
                }
                // evt.preventDefault();
                var modalInstance = $modal.open({
                    templateUrl: 'widgets/autoRotate/t32XrayCreateAutoRotateSettings.html',
                    controller: 't32XrayCreateAutoRotateSettingsCtrl',
                    windowClass: 'sixtyPercet-Modal',
                    backdrop: 'static',
                    resolve: {
                        rotationSettings: function () {
                            return $scope.rotationSettings;
                        },
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.modified) {
                        logger.info("We need to save our rotation settings");
                        // alert("Need to save these settings")
                        // $scope.rotationSettings = result.rotationSettings,
                        // $scope.currentRotationSetting = result.currentRotationSetting;
                        $scope.$emit('saveRotationSettings', result);
                    }
                    else {
                        // do nothing, we did not create anything
                    }
                });
            }

            $scope.onDelete = function(evt){
                // check to see if defualt, if it is then we cannot delete them.
                if (evt) {
                    evt.preventDefault();
                }
                // evt.preventDefault();
                var confirm = {
                    title: "Delete Setting",
                    text: "Are you sure you want to delete this rotation setting?",
                    confirmColor: '#d9534f',
                    showCancel: true,
                    confirmText: "Yes",
                    cancelText: "No",
                }

                t32AlertService.showAlert(confirm)
                .then(function(res){
                    if(res.alert == 'confirm'){
                        if ($scope.rotationSettings.length > 0 && $scope.currentRotationSetting.name !== 'Default') {
                            $scope.$emit('deleteRotationSettings');
                        }
                        else {
                            var alertObj = {
                                type: "error",
                                title: "Cannot Delete",
                                text: "This setting is not allowed to be deleted.",
                                timeout: 5000,
                                showCloseButton: true
                            }
                            t32AlertService.showToaster(alertObj)
                        }

                        // logger.info("The setttings after delete");
                        // logger.info($scope.rotationSettings);
                        // logger.info($scope.deletedRotationSettings);
                    }
                })
                .catch(function(err){
                    var alertObj = {
                        type: "error",
                        title: "Delete Failed",
                        text: "Failed to delete rotation setting, please try again later.",
                        timeout: 5000,
                        showCloseButton: true
                    }
                    t32AlertService.showToaster(alertObj)
                })
            }
            
            $scope.$on("setupRotationSettings", function(){
                // init();
                setupRotationSettings();
            })

            $scope.isFromLoggedInClinic = function(item){
                if (!item.clinic_id || item.clinic_id === $scope.settings.clinic_id) {
                    return item;
                }
            }

            init();
        }
    ]);
})(window, window.angular);