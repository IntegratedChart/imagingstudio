(function (window, angular, undefined) {
    'use strict';
    // angular.module('app').directive('t32XrayEditAutoRotateSettings', ['$modal',
    //     function ($modal) {
    //         return {
    //             restrict: 'AE',
    //             scope: {
    //                 clinicId: '=',
    //                 afterEdit: '&',
    //             },
    //             link: function (scope, element, attr) {
    //                 element.bind('click', function (event) {
    //                     console.log("EVENT CLICKED");
    //                     var modalInstance = $modal.open({
    //                         templateUrl: 'widgets/autoRotate/t32XrayEditAutoRotateSettings.html',
    //                         controller: 't32XrayEditAutoRotateSettingsCtrl',
    //                         windowClass: 'largest-Modal',
    //                         backdrop: 'static',
    //                         resolve: {
    //                             clinicId: function () {
    //                                 return scope.clinicId;
    //                             }
    //                         }
    //                     });
    //                     modalInstance.result.then(function (result) {
    //                         // alert("we have the results");
    //                         console.log(result)
    //                         scope.$eval(scope.afterEdit({ "result": result }));
    //                     });
    //                 })
    //             }
    //         };
    //     }
    // ]);

    angular.module('app').controller('t32XrayEditAutoRotateSettingsCtrl', [
        '$q',
        '$scope',
        '$modalInstance',
        'logger',
        't32XrayImageService',
        '$timeout',
        'SweetAlert',
        't32AlertService',
        'rotationSettings',
        'currentRotationSetting',
        't32CommonService',
        function (
            $q,
            $scope,
            $modalInstance,
            logger,
            t32XrayImageService,
            $timeout,
            SweetAlert,
            t32AlertService,
            rotationSettings,
            currentRotationSetting,
            t32CommonService) {
            function init() {
                $scope.rotationSettings = rotationSettings || [];
                $scope.currentRotationSetting = currentRotationSetting || {};
                $scope.unEditedRotationSettings = _.cloneDeep(rotationSettings);
                $scope.unEditedCurrentRotationSetting = _.cloneDeep(currentRotationSetting);
                // $scope.unEditedCurrentRotationSetting = JSON.parse(JSON.stringify(currentRotationSetting));
                console.log("Inside rotate settings, currentRotationSetting");
                console.log(currentRotationSetting);
                console.log("Inside rotate settings, rotationSettings");
                console.log(rotationSettings);

                var initialSubSettingKey = Object.keys($scope.currentRotationSetting.setting)[0];

                console.log(initialSubSettingKey);

                $scope.selectedSubSetting = $scope.currentRotationSetting.setting[initialSubSettingKey].setting;
                console.log($scope.selectedSubSetting);
            }

            // $scope.selectedSubSetting = null;
            $scope.isEdited = false;
            $scope.formErrors = false;

            $scope.cancel = function(){
                if(!$scope.isEdited){
                    $modalInstance.close({
                        modified: false,
                        currentRotationSetting: $scope.currentRotationSetting,
                        rotationSettings: $scope.rotationSettings
                    })
                }
                else if(!$scope.formErrors){
                    t32CommonService.askToSave('rotation settings')
                    .then(function (answer) {
                        if (answer) {
                            //since we are manipulating the original object, we do not need to do any save operation, that will be handled by the parent directive
                            if (t32CommonService.doesExist($scope.currentRotationSetting.name, $scope.rotationSettings, 'name') && $scope.currentRotationSetting.name !== $scope.unEditedCurrentRotationSetting.name) {
                                console.log($scope.currentRotationSetting.name );
                                console.log($scope.unEditedCurrentRotationSetting.name)
                                var errorObj = {
                                    type: "error",
                                    title: "Name already exists.",
                                    text: "The rotation setting name is already taken, please enter another name",
                                    timeout: 5000,
                                    showCloseButton: true
                                }
                                $timeout(function () {
                                    t32AlertService.showToaster(errorObj)
                                }, 500)
                            }
                            else {
                                $modalInstance.close({
                                    modified: true,
                                    action: 'save',
                                    currentRotationSetting: $scope.currentRotationSetting,
                                    rotationSettings: $scope.rotationSettings
                                })
                            }
                            // $modalInstance.close({
                            //     modified: true,
                            //     currentRotationSetting: $scope.currentRotationSetting,
                            //     rotationSettings: $scope.rotationSettings
                            // })
                        }
                        else {
                            console.log("We do not have anything edited");
                            console.log($scope.unEditedCurrentRotationSetting);
                            console.log($scope.unEditedRotationSettings);
                            $modalInstance.close({
                                modified: false,
                                rotationSettings: $scope.unEditedRotationSettings,
                                currentRotationSetting: $scope.unEditedCurrentRotationSetting,
                            })
                        }
                    })
                }
                else{
                    var title = "Form Error"
                    var error = "The settings name is invalid, please try again.";

                    var errorObj = {
                        title: title, 
                        text: error,
                        confirmText: "Close Anyways",
                        showCancel: true,
                        cancelText: "Retry"
                    }
                    
                    t32AlertService.showError(errorObj)
                    .then(function(err){
                        if(err.error == 'cancel'){
                            // do nothing and let them retry
                        }
                        else{
                            $modalInstance.close({
                                modified: false,
                                rotationSettings: $scope.unEditedRotationSettings,
                                currentRotationSetting: $scope.unEditedCurrentRotationSetting,
                            })
                        }
                    })

                }
            }

            $scope.deleteRotationSettings = function () {
                // check to see if defualt, if it is then we cannot delete them.
                var confirm = {
                    title: "Delete Setting",
                    text: "Are you sure you want to delete this rotation setting?",
                    confirmColor: '#d9534f',
                    showCancel: true,
                    confirmText: "Yes",
                    cancelText: "No",
                }

                t32AlertService.showAlert(confirm)
                .then(function (res) {
                    if (res.alert == 'confirm') {
                        if ($scope.rotationSettings.length > 0 && $scope.currentRotationSetting.name !== 'default' && $scope.currentRotationSetting.name !== 'Default') {
                            $scope.$emit('deleteRotationSettings');
                            $modalInstance.close({
                                modified: true,
                                action: "delete",
                                currentRotationSetting: $scope.currentRotationSetting,
                                rotationSettings: $scope.rotationSettings
                            })
                        }
                        else {
                            var alertObj = {
                                type: "error",
                                title: "Cannot Delete",
                                text: "This setting is not allowed to be deleted.",
                                timeout: 5000,
                                showCloseButton: true
                            }
                            t32AlertService.showToaster(alertObj)
                        }

                        // console.log("The setttings after delete");
                        // console.log($scope.rotationSettings);
                        // console.log($scope.deletedRotationSettings);
                    }
                })
                .catch(function (err) {
                    var alertObj = {
                        type: "error",
                        title: "Delete Failed",
                        text: "Failed to delete rotation setting, please try again later.",
                        timeout: 5000,
                        showCloseButton: true
                    }
                    t32AlertService.showToaster(alertObj)
                })
            }

            $scope.saveRotationSetting = function(){
                console.log($scope.currentRotationSetting.name);
                console.log($scope.unEditedCurrentRotationSetting.name);
                // console.log(t32CommonService.doesExist($scope.currentRotationSetting.name, $scope.rotationSettings, 'name'));
                if (t32CommonService.doesExist($scope.currentRotationSetting.name, $scope.unEditedRotationSettings, 'name') && $scope.currentRotationSetting.name !== $scope.unEditedCurrentRotationSetting.name) {
                    var errorObj = {
                        type: "error",
                        title: "Name already exists.",
                        text: "The rotation setting name is already taken, please enter another name",
                        timeout: 5000,
                        showCloseButton: true
                    }
                    t32AlertService.showToaster(errorObj)
                }
                else {
                    $modalInstance.close({
                        modified: true,
                        action: "save",
                        currentRotationSetting: $scope.currentRotationSetting,
                        rotationSettings: $scope.rotationSettings
                    })
                }

            }

            $scope.showSubSetting = function(setting){
                $scope.selectedSubSetting = setting;
            }

            $scope.isSelected = function (data) {
                if ($scope.selectedSubSetting == data) {
                    return 'selected-list-item';
                }
                else {
                    return ''
                }
            }

            $scope.setIsEdited = function(){
                if($scope.isEdited){
                    return 
                }
                else{
                    $scope.isEdited = true;
                }
            }

            $scope.rotate = function(key){
                var deg = $scope.selectedSubSetting[key];
                return {"transform": "rotate(" + deg + "deg)"};
            }

            $scope.handleNameChange = function () {
                if ($scope.rotationSettingsEditForm.name.$valid) {
                    $scope.setIsEdited();
                    console.log("There are no errors")
                    $scope.currentRotationSetting.name = t32CommonService.toTitleCase($scope.currentRotationSetting.name);
                    // $scope.currentRotationSetting.name = _.camelCase($scope.currentRotationSetting.name);
                    $scope.formErrors = false;
                }
                else {
                    $scope.setIsEdited();
                    console.log("There is error");
                    $scope.formErrors = true;
                    console.log($scope.isEdited);
                    console.log($scope.formErrors)
                }
            }

            $scope.titleText = 'Rotation Setting'
            $scope.titleStyle = {};
            $scope.titleChildDivClass = 'branding';
            $scope.menuBarLen = 3;

            init();
        }
    ])
})(window, window.angular);