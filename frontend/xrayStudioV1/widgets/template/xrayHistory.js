(function (window, angular, undefined) { 'use strict';

    angular.module('app').controller('XrayHistoryCtrl', [
        '$scope', 
        '$window',
        '$document',
        '$timeout',
        '$rootScope',
        '$modalStack',
        'SweetAlert',
        'logger',
        '$modalInstance',
        't32AlertService',
        't32SystemService',
        'currentPatient',
        '$q',
    function (
        $scope, 
        $window,
        $document,
        $timeout,
        $rootScope,
        $modalStack,
        SweetAlert,
        logger,
        $modalInstance,
        t32AlertService,
        t32SystemService,
        currentPatient,
        $q) 
    {
        function init(){
            $scope.theModalInstance = $modalInstance;
            $scope.dates = [];
            $scope.currentXrayDate = null;
            $scope.currentPatient = currentPatient;
            logger.debug($scope.currentPatient);
            if($scope.currentPatient){
                $scope.clinicId = currentPatient.clinic_id;
                $scope.patientId = currentPatient._id;
            }
            $scope.currentXraysHash = null;
            $scope.overFlowImages = [];
            $scope.appliedImages = [];
            $scope.groupedDates = {};
            $scope.setting = null;
            //go get the dates from backend with given patient 
            if($scope.currentPatient){

                logger.debug("There is a patient here");

                var requestType = 'getPatientXrayTakenDates';

                var requestObject = {
                    clinic_id: currentPatient.clinic_id,
                    patient_id: currentPatient._id
                }

                t32SystemService.submitSystemRequest(requestType, requestObject)
                .then(function(dates){
                    groupByDate(dates);
                    return t32SystemService.getXrayStudioSetting()
                    // logger.debug($scope.currentXrayDate);
                })
                .then(function(setting){
                    $scope.setting = setting;
                    logger.debug($scope.setting);

                    $scope.currentXrayDate = Object.keys($scope.groupedDates)[0];

                    return getPatientXraysByDate($scope.clinicId, $scope.patientId, $scope.groupedDates[$scope.currentXrayDate])

                })
                .then(function (xrays) {
                    putImagesOnTemplate(xrays);
                })
                .catch(function(err){
                    logger.info(err);
                    var alertObj = {
                        type: "error",
                        title: "Error initializing, please try again later.",
                        text: "",
                        timeout: 5000,
                        showCloseButton: true
                    }
                    t32AlertService.showToaster(alertObj);
                });
            }

        }

        function groupByDate(dates){
            var uniqDates = [];
            // alert("WE ARE INSIDE OF THE FUNCTION")
            _.each(dates, function(date){
                var formatedDate = moment(date).format("MM/DD/YYYY");
                logger.debug("WE HAVE THE FORMATED DATE")
                logger.debug(formatedDate);
                if(!$scope.groupedDates[formatedDate]){
                    $scope.groupedDates[formatedDate] = [];
                }
                $scope.groupedDates[formatedDate].push(date);
                uniqDates.push(moment(date).format("MM/DD/YYYY"));
            });


            $scope.dates =  _.uniq(uniqDates).sort(function(a, b) {
                var aa = new Date(a),
                    bb = new Date(b);
            
                if (aa !== bb) {
                    if (aa > bb) { return 1; }
                    if (aa < bb) { return -1; }
                }
                return aa - bb;
            }).reverse();

            console.log($scope.groupedDates);
            // return uniqDates;
        };

        var xrayPositionList = [ "max_right_iop_1", "max_right_iop_2","max_right_post_pa_3", "max_right_post_pa_4",  "max_anterior_iop_1", "max_anterior_iop_2", "max_anterior_iop_3", "max_anterior_pa_4", "max_anterior_pa_5", "max_anterior_pa_6", "max_left_iop_1", "max_left_iop_2", "max_left_post_pa_3", "max_left_post_pa_4", "right_bw_1", "right_bw_2", "left_bw_1", "left_bw_2", 
        "mandibular_right_post_pa_1", "mandibular_right_post_pa_2", "mandibular_right_iop_3", "mandibular_right_iop_4","mandibular_anterior_pa_1", "mandibular_anterior_pa_2", "mandibular_anterior_pa_3", "mandibular_anterior_iop_4", "mandibular_anterior_iop_5", "mandibular_anterior_iop_6","mandibular_left_post_pa_1", "mandibular_left_post_pa_2", "mandibular_left_iop_3", "mandibular_left_iop_4", "pano", "ceph", "profile_1", "profile_2", "profile_3", "jaw_inside_1", "jaw_inside_2", "jaw_outside_1", "jaw_outside_2", "jaw_outside_3" ];

        // $scope.photosHash = {
        //     "max_right_iop_1": [], "max_right_iop_2": [], "max_right_ost_pa_3": [], "max_right_post_pa_4": [], 

        //     "max_anterior_iop_1": [], "max_anterior_iop_2": [], "max_anterior_iop_3": [], 
        //     "max_anterior_pa_4": [], "max_anterior_pa_5": [], "max_anterior_pa_6": [], 
            
        //     "max_left_iop_1": [], "max_left_iop_2": [], "max_left_post_pa_3": [], "max_left_post_pa_4": [], 
            
        //     "right_bw_1": [], "right_bw_2": [], "left_bw_1": [], "left_bw_2": [], 
            
        //     "mandibular_right_post_pa_1": [], "mandibular_right_post_pa_2": [], "mandibular_right_iop_3": [], "mandibular_right_iop_4": [],

        //     "mandibular_anterior_pa_1": [], "mandibular_anterior_pa_2": [], "mandibular_anterior_pa_3": [], "mandibular_anterior_iop_4": [], "mandibular_anterior_iop_5": [],"mandibular_anterior_iop_6": [],

        //     "mandibular_left_post_pa_1": [], "mandibular_left_post_pa_2": [], "mandibular_left_iop_3": [], "mandibular_left_iop_4": [],

        //     "profile_1": [], "profile_2": [], "profile_3": [], "jaw_inside_1": [],  "jaw_inside_2": [], "jaw_outside_1": [],
        //     "jaw_outside_2": [], "jaw_outside_3": [],

        //     "ceph": [],

        //     "pano": []
        // }

        $scope.photosHash = {};


        // $scope.$watch('currentXrayDate', function(newVal, oldVal){
        //     logger.info("current xray date change New Val -->", newVal);
        //     logger.info("current xray date old Val --> ", oldVal);
        //     console.log("current xray date change New Val -->", newVal);
        //     console.log("current xray date old Val --> ", oldVal);
        //     if(newVal !== oldVal && newVal !== null){
        //         //alert("inside of the currentXray Change ")
        //         getPatientXraysByDate($scope.clinicId, $scope.patientId, $scope.groupedDates[$scope.currentXrayDate])
        //         .then(function(xrays){
        //             putImagesOnTemplate(xrays);
        //         }); 
        //     }
        // });

        $scope.onHistoryDateChange = function (val) {
            $scope.currentXrayDate = val;
            getPatientXraysByDate($scope.clinicId, $scope.patientId, $scope.groupedDates[$scope.currentXrayDate])
            .then(function (xrays) {
                putImagesOnTemplate(xrays);
            })
            .catch (function(err) {
                logger.info('errror getting xray history for ' + $scope.currentXrayDate);
                logger.info(err);
                var alertObj = {
                    type: "error",
                    title: "Failed to get Xrays for this date. Please try again.",
                    text: "",
                    timeout: 5000,
                    showCloseButton: true
                }
                t32AlertService.showToaster(alertObj);
            })
        }

        function putImagesOnTemplate(xrays){
            //clear all id's of the background-image
            clearAllImages();

            // var regExp = '.jpg'
            _.each(xrays, function(obj){
                // var filename = obj.filename.replace(regExp, '');
                var filename = obj.metadata.image_pos;
                // logger.debug(obj.filename);

                obj.src = $scope.setting.ehrServer +'/api/xrayStudio/fileUpload/getXrayStudioXrayFile/'  + obj._id +'/jpeg';

                // getImage(obj._id, 'jpeg')
                // .then(function(img){
                    if(!$scope.photosHash[filename]){
                        $scope.photosHash[filename] = [];
                        var id = '#' + filename;
                        // logger.debug(id);
                        var urlImg = "url(" + obj.src + ")";
                        $(id).css("background-image", urlImg);
                        // $(id).css("background-color", 'red');
                        $(id).addClass("applied-image");
                        $scope.appliedImages.push(obj);
                    }
                    else{
                        $scope.overFlowImages.push(obj);
                    }
                    $scope.photosHash[filename].push(obj);
                // })
                // .catch(function(err){
                //     console.log(err);
                //     alert("There was an error getting the xray image.")
                // })
            });
            logger.debug($scope.photosHash);
        }

        // function getImage(file_id, file_type){
        //     var deferred  = $q.defer();

        //     logger.debug(file_id)

        //     var requestType = 'getPatientXrayFile'
        //     var requestObject = {
        //         clinic_id: $scope.currentPatient.clinic_id,
        //         file_id: file_id,
        //         file_type: file_type
        //     }

        //     t32SystemService.submitSystemRequest(requestType, requestObject)
        //     .then(function(img){
        //         deferred.resolve(img);
        //     })
        //     .catch(function(err){
        //         deferred.reject(err);
        //     });
        // }

        function clearAllImages(){
            $scope.photosHash = {};
            $scope.appliedImages = [];
            $scope.overFlowImages = [];

            _.each(xrayPositionList, function(id){
                var formatedId = '#' + id;
                if($(formatedId).hasClass("applied-image")){
                    //alert(id);
                    $(formatedId).css({
                        "background-image":"none",
                    });
                    $(formatedId).removeClass("applied-image");
                }

            })
        }
        function getPatientXraysByDate(clinicId, patientId, uploadDate){
            var deferred = $q.defer();

            var requestType = 'getPatientXraysByDate';

            var requestObject = {
                clinic_id: clinicId,
                patient_id: patientId,
                date:  uploadDate
            }

            t32SystemService.submitSystemRequest(requestType, requestObject)
            .then(function(xrays){
                deferred.resolve(xrays);
            })
            .catch(function(err){
                deferred.reject(err);
            })

            return deferred.promise
        }

        $scope.titleImg = "img/xray-clipboard-alt.svg";

        $scope.titleStyle = {
        };

        $scope.titleImgStyle = {
            "width": "100px",
            "position": "absolute",
            "left": "25px"
        }

        
        init();
    }
]);
})(window, window.angular);