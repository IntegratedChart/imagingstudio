(function(window, angular, undefined){
  'use strict';
    angular.module('app').controller('XrayTemplateCtrl', [
        '$scope',
        '$modalInstance',
        '$modal',
        'patientContext',
        'logger',
        't32SystemService',
        '$q',
        'SweetAlert',
        't32PatientSelectionService',
        't32WorkflowService',
        '$timeout',
        't32ImageManipulation',
        't32SyncTemplate',
        't32XrayImageService',
        't32RotationSettingsService',
        't32AlertService',
        't32XraySensorSettingsService',
        't32XrayDriverService',
    function(
        $scope,
        $modalInstance,
        $modal,
        patientContext,
        logger,
        t32SystemService,
        $q,
        SweetAlert,
        t32PatientSelectionService,
        t32WorkflowService,
        $timeout,
        t32ImageManipulation,
        t32SyncTemplate,
        t32XrayImageService,
        t32RotationSettingsService,
        t32AlertService,
        t32XraySensorSettingsService,
        t32XrayDriverService)
    {
        function init(){ 
            //Call service to get information on user's xray templates and $scope.templates to the returned array;
            $scope.templates = [];
            $scope.startXray = false;
            $scope.template = null;
            $scope.newTemplate = {};
            $scope.newTemplate.templateName = '';
            $scope.newTemplate.imagePositions = [];
            $scope.photos = [];
            $scope.photosHash = {};
            $scope.appliedPhotos = [];
            $scope.setting = null;
            $scope.imageSettings = null;
            $scope.currentChartView = 'xray';
            $scope.takeXrayMode = false;
            $scope.hasAppliedPhotos =  $scope.appliedPhotos.length ? true :  false;
            $scope.activeImageCell = '';
            $scope.viewMode = 'xray';
            $scope.nextIndex = null;

            $scope.rotationSettings = [];
            $scope.currentRotationSetting = {};
            $scope.deletedRotationSettings = [];
            $scope.currentRotationSettingObj = {};


            $scope.selectedPatient = patientContext.selectedPatient || null;
            $scope.selectedFolder = patientContext.selectedFolder || null;

            console.log('-----INSIDE OF XRAY TEMPLATE-----')
            // console.log("The selectedPatient");
            // console.log($scope.selectedPatient);
            // console.log("The selectedFolder");
            // console.log($scope.selectedFolder);

            $scope.clinic_id = $scope.selectedPatient ? $scope.selectedPatient.clinic_id : null;

            $scope.patientInfoString = getPatientInfoByFolder();

            //for xray studio driver list directive
            $scope.selectedDriver = null;

            //clinicId check lets us know if the patient is pulled from online or is local(not connected to network)
            if($scope.clinic_id){
                logger.info("Syncing templates with ehr server")
                //make call to db to get the xray-templates and then save that to the settings 
                t32SyncTemplate.syncXrayTemplates($scope.clinic_id)
                .then(function(){
                    return t32RotationSettingsService.syncRotationSettings($scope.clinic_id)
                })
                .then(function(setting){
                    return t32XraySensorSettingsService.syncXraySensorSettings($scope.clinic_id)
                })
                .then(function (setting) {
                    $scope.setting = setting;
                    
                    if(!$scope.setting.selectedDriver){
                        $scope.setting.selectedDriver = null;
                    }

                    $scope.selectedDriver = $scope.setting.selectedDriver;
                    //make sure that we are in the xray view. Its flag used by chokidar watcher.
                    $scope.setting.viewMode = 'xray'
                    $scope.setting.currentSelectedFolder = $scope.selectedFolder;
                    var requestObject = $scope.setting;

                    // return t32SystemService.submitSystemRequest('saveXrayStudioSetting', requestObject)
                    return t32SystemService.saveXrayStudioSetting($scope.setting)
                })
                .then(function(result){
                    $scope.templates = $scope.setting.xrayTemplates ? JSON.parse($scope.setting.xrayTemplates) : [];

                    if ($scope.templates !== []) {
                        $scope.template = filterTemplateByClinicId($scope.templates, $scope.setting);
                    }
                    return t32SystemService.submitSystemRequest('getImageSettings')
                })
                .then(function(settings){
                    $scope.imageSettings = settings;
                    console.log("we have the Image settings ");
                    // console.log($scope.imageSettings);
                    $scope.$broadcast("setupRotationSettings");
                    $scope.loadFolderImages($scope.selectedFolder);
                })
                .catch(function(err){
                    console.error(err);
                    logger.error(err);
                    var errorObj = { 
                        type: "error",
                        title: "FatalError",
                        text: "Something went wrong, exiting the patient view.",
                        showCloseButton: true,
                        timeout: 5000
                        }
                    t32AlertService.showToaster(errorObj);
                    $modalInstance.close();
                })
            }
            else{
                logger.info("Getting local template settings")
                t32SystemService.getXrayStudioSetting() 
                .then(function(setting){
                    $scope.setting = setting;

                    $scope.setting.viewMode = 'xray';

                    $scope.setting.currentSelectedFolder = $scope.selectedFolder;

                    if (!$scope.setting.selectedDriver) {
                        $scope.setting.selectedDriver = null;
                    }

                    $scope.selectedDriver = $scope.setting.selectedDriver;
                    
                    // console.log("The setting before settings save in xray template");
                    // console.log ($scope.setting);

                    // var requestObject = $scope.setting;
                    
                    // return t32SystemService.submitSystemRequest('saveXrayStudioSetting', requestObject)
                    return t32SystemService.saveXrayStudioSetting($scope.setting)
                })
                .then(function(result){
                    $scope.templates = $scope.setting.xrayTemplates ? JSON.parse($scope.setting.xrayTemplates) : [];

                    // alert("template");
                    // console.log($scope.templates);

                    if ($scope.templates.length > 0) {
                        $scope.template = filterTemplateByClinicId($scope.templates, $scope.setting)
                        // alert($scope.template)
                    }
                    return t32SystemService.submitSystemRequest('getImageSettings')
                })
                .then(function (settings) {
                    $scope.$broadcast("setupRotationSettings")
                    $scope.imageSettings = settings;
                    $scope.loadFolderImages($scope.selectedFolder);
                })
                .catch(function(err){
                    console.error(err);
                    logger.error(err);
                    var errorObj = {
                        type: "error",
                        title: "FatalError",
                        text: "Something went wrong, exiting the patient view.",
                        showCloseButton: true,
                        timeout: 5000
                        }
                    t32AlertService.showToaster(errorObj);
                    $modalInstance.close();
                })
            }
        }

            // when receive backend notification.
        $scope.$on('DirectoryUpdated', function (evt, result) {          
            $scope.nextIndex = getNextIndex($scope.activeImageCell);
            $scope.loadFolderImages($scope.selectedFolder);
        });  
        
        /* ********for Testing, do not remove *********** */
        // $scope.$on('testing-backendSettings', function(){
        //     logger.info("THE BACKEND HAS UPDATED SETTINGS CHECKING FRONT END ", $scope.setting)
        // })


        //matches the templates stored inside settings with current clinic
        function filterTemplateByClinicId (templates, settings) {
            var filteredTemplates = null;
            filteredTemplates = _.filter(templates, function (templateObj) {
                return templateObj.clinic_id === settings.clinic_id || !templateObj.clinic_id
            })

            console.log("Here is the templates after filter");
            // console.log(filteredTemplates);

            if (filteredTemplates && filteredTemplates.length > 0) {
                var index = _.findIndex(filteredTemplates, function(template){
                    if (settings.currentTemplateObj){
                        return _.isEqual(template, JSON.parse(settings.currentTemplateObj))
                    }
                    else{
                        return false;
                    }
                })
                
                console.log("the index ", index)

                return index > -1 ? filteredTemplates[index] : filteredTemplates[0];
            }
            else{
                console.log("The filtered templates does not have length")
                return null;
            }
        }

        //flags for various operations that we can do inside of the page. 
        $scope.inEditMode = false;
        $scope.inCreateMode = false;
        $scope.disabledClick = true;

        //hardcoded xray chart with all of the image poistions 
        var xrayChart = [
            {1: "id_max_right_iop_1"}, {2: "id_max_right_iop_2"}, {3: "id_max_right_post_pa_3"}, {4: "id_max_right_post_pa_4"}, 

            {5: "id_max_anterior_iop_1"}, {6: "id_max_anterior_iop_2"}, {7: "id_max_anterior_iop_3"}, 
            {8: "id_max_anterior_pa_4"}, {9: "id_max_anterior_pa_5"}, {10: "id_max_anterior_pa_6"}, 
            
            {11: "id_max_left_iop_1"}, {12: "id_max_left_iop_2"}, {13: "id_max_left_post_pa_3"}, {14: "id_max_left_post_pa_4"}, 
            
            {15: "id_right_bw_1"}, {16: "id_right_bw_2"}, {17: "id_left_bw_1"}, {18: "id_left_bw_2"}, 
            
            {19: "id_mandibular_right_post_pa_1"}, {20: "id_mandibular_right_post_pa_2"}, {21: "id_mandibular_right_iop_3"}, {22: "id_mandibular_right_iop_4"},

            {23: "id_mandibular_anterior_pa_1"}, {24: "id_mandibular_anterior_pa_2"}, {25: "id_mandibular_anterior_pa_3"}, {26: "id_mandibular_anterior_iop_4"}, {27: "id_mandibular_anterior_iop_5"},{28: "id_mandibular_anterior_iop_6"},

            {29: "id_mandibular_left_post_pa_1"}, {30: "id_mandibular_left_post_pa_2"}, {31: "id_mandibular_left_iop_3"}, {32: "id_mandibular_left_iop_4"}
        ];

        var panoChart = [
            {1: "id_pano"}
        ]

        var cephChart = [
            {1: "id_ceph"}
        ]

        var orthoChart = [
            {1: "id_profile_1"}, {2: "id_profile_2"}, {3: "id_profile_3"}, {4: "id_jaw_inside_1"}, {5: "id_jaw_inside_2"}, {6: "id_jaw_outside_1"},
            {7: "id_jaw_outside_2"}, {8: "id_jaw_outside_3"}
        ]

        var selectedXrayChart = [];

        var selectedCount = 0;
        var count = -1;
        var id;
        var element;

        function getPatientInfoByFolder() {
            console.log("Getting patient info")
            var result = '';
            if ( $scope.selectedFolder )
            {
               result = $scope.selectedFolder.replace(/-/g, ' ');
            }
            console.log("We have the result")
            return result;
        }

        /**
         * logic to handle all the changes that happen after editing, creating or deleting xray images/tempaltes. 
         */
        $scope.onTemplateChange = function(){
            var imgPos = $scope.template ? $scope.template.imagePositions : null;

            var id;

            var chart = $scope.getCurrentChart();

            $scope.removeAllSelections(chart);
            
            $scope.takeXrayMode = false;

            if(imgPos){
                _.forEach(imgPos, function(posId){
                    $scope.toggleSelection(posId);
                    if($scope.selectedFolder && $scope.photos.length){
                        //place image inside of the proper position
                        putImgsOnTemplate(posId);
                        $scope.hasAppliedPhotos = true;
                    }
                });
                if($scope.hasAppliedPhotos){
                    //alert("WE have applied photos ");
                    //alert(imgPos[($scope.appliedPhotos.length)]);
                    // $scope.activeImageCell = imgPos[($scope.appliedPhotos.length)] ? imgPos[($scope.appliedPhotos.length)] : imgPos[($scope.appliedPhotos.length - 1)]
                    // alert("This is the next index " + $scope.nextIndex);
                    $scope.activeImageCell = $scope.nextIndex ? imgPos[$scope.nextIndex] : imgPos[0];

                    // $scope.activeImageCell = $scope.nextIndex || imgPos[0];
                    console.log('on template change active image cell', $scope.activeImageCell);
                    removeAllActiveImages();
                    setActive($scope.activeImageCell);
                    // saveTemplateToSettings(id);
                }
                else{
                    //alert("WE have no applied images");
                    removeAllActiveImages();
                    $scope.activeImageCell = imgPos[0];
                    setActive($scope.activeImageCell);
                    // saveTemplateToSettings(id);
                }

                $scope.hideAllUnselected(chart); 
                $scope.takeXrayMode = true;
                // saveTemplateToSettings(id);
            }
        }


        $scope.isFromLoggedInClinic = function(item){
            if(item.clinic_id === $scope.setting.clinic_id || !item.clinic_id){
                return item;
            }
            else{
                return false;
            }
        }

        /**
         * increment the selection box to the next cell when image in taken or images are loaded. 
         * @param {*} id the name of the photo 
        */
        function getNextIndex(id){
            var index = _.findIndex($scope.template.imagePositions, function(pos){
                return id === pos
            });
            index ++ 
            if(index >= $scope.template.imagePositions.length){
                index = 0;
            }

            return index;
        }

        /**
         * deincrement the selection box to the next cell when image in taken or images are loaded. 
         * @param {*} id the name of the photo 
        */
        function getPreviousIndex(id){
            var index = _.findIndex($scope.template.imagePositions, function (pos) {
                return id === pos
            });
            index --
            if(index < 0){
                index = $scope.template.imagePositions.length - 1;
            }

            return index;
        }

        /**
         * 
         * @param {String} activeImageCell the current active image position name
         * 
         */
        function saveTemplateToSettings(activeImageCell) {
            if($scope.template !== null){
                $scope.setting.currentTemplate = $scope.template.imagePositions;
                $scope.setting.currentTemplateObj = angular.toJson($scope.template);
            }
            //alert($scope.setting.currentTemplate);
            $scope.setting.activeImageCell = activeImageCell;
            $scope.setting.frontEndVal = "activeImageCellChange"
            // var requestObject =  $scope.setting

            // t32SystemService.submitSystemRequest('saveXrayStudioSetting', requestObject)
            t32SystemService.saveXrayStudioSetting($scope.setting)
            .then(function(result){
                //alert("Save finished");
                //alert($scope.setting);
            });
        }

        //handles changes to the template dropdown.
        $scope.$watch("template", function(newVal, oldVal){
            console.log("The new template");
            console.log(newVal);
            console.log($scope.template);
            if($scope.template == null){
                //do nothing
            }

            if(newVal !== oldVal && $scope.template !== null && $scope.isFromLoggedInClinic(newVal) !== false){
                console.log("Here is the templates list")
                console.log($scope.templates);
                // alert("the template changed");
                $scope.nextIndex = null;
                $scope.onTemplateChange();
                saveTemplateToSettings($scope.activeImageCell);
            }
            else{
                $scope.template = null;
                $scope.nextIndex = null;
                $scope.onTemplateChange();
                saveTemplateToSettings($scope.activeImageCell);
            }
        });

        $scope.$watch("activeImageCell", function(newVal, oldVal){
            if(newVal !== oldVal && $scope.activeImageCell !== null){
                //alert("The active image cell has changed");
                saveTemplateToSettings(newVal);
            }
        })

        function setActive(activeImageCell){
            makeActive(selectedXrayChart, activeImageCell);
        }

        /**
         * 
         * @param {Array} xrayChart 
         * give black background to hide the unselected image cells inside of the template
         */
        $scope.hideAllUnselected = function(xrayChart){
            _.forEach(xrayChart, function(value, key){
                var id = "#" + xrayChart[key][key+1];
                if($(id).hasClass("selected") === false){
                    $(id).addClass("hide-xray-panels");
                }
            })
        }

        
        $scope.getCurrentChart = function(){
            if($scope.currentChartView === 'xray'){
                return xrayChart;
            }
            else if($scope.currentChartView === 'panoView'){
                return panoChart;
            }
            else if($scope.currentChartView === 'orthoView'){
                return orthoChart;
            }
            else{
                return cephChart;
            }
        }

        /**
         * 
         * @param {String} newChart The name of the chart that we are switching to (xray, ortho, ceph, etc.)
         */
        $scope.onChartChange = function(newChart){
            $scope.currentChartView = newChart;
            // $scope.inCreateMode = false;
            // $('#create-mode').removeClass("highlight-active-box");
            // console.log($('#create-mode'));
            
            //remove edit button ui 
            if($scope.inEditMode){
                $scope.inEditMode = false;
                // $('#edit-mode').removeClass("highlight-active-box");
                $scope.disabledClick = true;
            }

            //remove create button ui
            if($scope.inCreateMode){
                $scope.inCreateMode = false;
                // $('#create-mode').removeClass("highlight-active-box");
                $scope.removeAllSelections();
                $scope.disabledClick = true;
            }

            $scope.activeImageCell = null;
            $scope.onTemplateChange();
        }

        /**
         * 
         * @param {String} id The image position
         */
        function putImgsOnTemplate(id){
            var img;

            if($scope.photosHash[id]){
                img = $scope.photosHash[id][0];
            }
            else{
                img = null;
            }

            if(img){
                var src = img.src;
                var formatedId = '#' + id;
                var urlImg = 'url(' + src + ')';
                
                //will help when needing to figure out which images need to go to overflow.
                $scope.appliedPhotos.push(img);

                var index = _.findIndex($scope.photos, function(obj){
                    return obj.desc === img.desc
                });

                $scope.photos.splice(index, 1);
                // $(formatedId).css("background-image", urlImg);
                $(formatedId).css({
                    "color": "white"
                });
                var imageHtml = "<img " + 
                "fileName=" + img.fileName + " fileDesc=" + img.fileDesc + " originalFileName=" + img.originalFileName + " src=' " + src + "'" + "style='z-index:1; width:100%; height: 100%;'>"
                console.log("The selected id: " + selectedCount);

                $(formatedId).append(imageHtml);

                $(formatedId).find("span").addClass("rearrage-position-name");

                $(formatedId).addClass("applied-image");
                // getNextIndex(id);
            }
        }

        //event being caught form the drag directive
        $scope.$on("handleClick", function(evt, id){
            console.log(id);
            var targetId = id.toString();
            $scope.handleClickEvent(targetId);
        })


        $scope.showAllSelections = function(xrayChart){
            _.forEach(xrayChart, function(value, key){
                var id = "#" + xrayChart[key][key+1];
                $(id).removeClass("hide-xray-panels");
            })
        }

        $scope.$on('XrayDriverExit', function(evt, result) {
           $scope.startXray = false;
        });    
        
        // $scope.$on('ImageReady', function(){
        //     $scope.startXray = false;
        // })

        /**
         * Triggers proper command to start xray taking from twain or from simulation
         * @param {*} command 
         */
        $scope.onNewXray = function(command) {
            var targetDir = $scope.setting.tab32Root + '/' + $scope.selectedFolder;

            if(command === 'start'){
                $scope.startXray = true;
                // console.log("The setting ", $scope.setting);

                if ($scope.setting.simulation) {
                    var requestType = 'startXraySimulation'
                    var requestObject = {
                        targetDir: targetDir
                    }

                    t32WorkflowService.simulationModePrompt()
                    .then(function () {
                        return t32SystemService.submitSystemRequest(requestType, requestObject)
                    })
                    .then(function () {

                    })
                    .catch(function (err) {
                        logger.info('Error with take xray in sim ', err);
                        var alertObj = {
                            type: 'error',
                            title: 'Xray Simulation Failed',
                            text: '',
                            timeout: 5000,
                            showCloseButton: true
                        }
                        return t32AlertService.showToaster(alertObj)
                        
                    })
                }
                else{
                    t32XrayDriverService.startScan($scope.selectedDriver, targetDir)
                }
            }
            else{
                t32XrayDriverService.stopScan($scope.selectedDriver);
            }

            // t32WorkflowService.simulationModePrompt()
            // .then(function(){
            //   var requestType = 'envokeXrayDriver';
            //    var myCommandString = $scope.setting.twainDriverPath || '';
            //    var targetDir = $scope.setting.tab32Root + '/' + $scope.selectedFolder;
            //    var reInitializeDriver = $scope.setting.reInitializeDriver;
            //    var disableSource = $scope.setting.disableSourceAfterAcquire;
            //    var showUi = $scope.setting.showUi;
            //    var cmd = command

            //    var args = [
            //       targetDir, 
            //       'cmd='+command, 
            //       'reInitializeDriver=' + $scope.setting.reInitializeDriver, 
            //       'disableSourceAfterAcquire=' + $scope.setting.disableSourceAfterAcquire,
            //       'showUi=' + $scope.setting.showUi ];

            //    var requestObject = {
            //       command : myCommandString,
            //       args    : args,
            //       cmd     : cmd
            //    }

            //     t32SystemService.submitSystemRequest(requestType, requestObject).then(function(result){
            //        alert("Xray Taken");
            //     });
            // });
        };

        function findActiveCell(xrayChart){
            var foundId
            _.each(xrayChart, function(val, key){
                var id = xrayChart[key][(key+1)];
                var formatedId = '#' +  id;
                if($(formatedId).hasClass("active-image")){
                    foundId = id;
                    //alert(foundId);
                };
            });

            return foundId;
        }

        /**
         * Handle changes after image has been edited.
         * @param {*} result Object with current image id of active cell.
         *
         */
        $scope.afterEdit = function(result){
            // console.log("Here is the result");
            // console.log(result);
            // console.log($scope.imageSettings);
            // if(result && result.hasOwnProperty('error')){
            //     $scope.loadFolderImages($scope.selectedFolder);
            //     return
            // }
            // getNextIndex(result.currentImageId);
            $scope.nextIndex = getCurrentIndex($scope.activeImageCell, $scope.template);

            t32SystemService.submitSystemRequest('getImageSettings')
            .then(function (settings) {
                $scope.imageSettings = settings;
                console.log($scope.imageSettings);
                // alert("We have response");
                $scope.loadFolderImages($scope.selectedFolder);
            })
            .catch(function (err) {
                logger.debug(err);
            })
        }

        function getCurrentIndex(id, template){
            var index = _.findIndex(template.imagePositions, function(pos){
                return id === pos;
            })
            
            return index;
        }   

        /**
         * Get the the images from the patient local directory 
         * the folder is defined in the settings.json file.
         */
        $scope.loadFolderImages = function(selectedFolder) {

            if (selectedFolder ){
                t32XrayImageService.loadFolderImages(selectedFolder)
                .then(function(result){
                    $scope.photos = result.photos;
                    $scope.photosHash = result.photosHash;
                    // console.log("Here is the photos hash");
                    // console.log($scope.photosHash);
                    // console.log($scope.photos)
                    addRandomQueryStringToPhotoFiles();
                    $scope.appliedPhotos = [];
                    $scope.onTemplateChange();
                })
            }
            else
            {
                $scope.photos = [];
                $scope.onTemplateChange();
            }
        }

        /**
         * get the id from the filename
         */
        function parseId(file){
            var reg = /(id_\w+)/g
            var id = file.match(reg)[0].substring(0, file.length -  1);
            logger.debug(id);
            // alert(id);
            return id;
        }   

        /**
         * 
         * @param {Array} photosArray 
         * Create hash of photos by photo id's
         */
        function groupPhotosById(photosArray){
            var hash = {};
            _.each(photosArray, function(photo){
                var id = parseId(photo.desc);
                logger.debug("HERE is the id inside of hash " + id);
                if(hash[id] && hash[id].length > 0){
                    hash[id].push(photo);
                }
                else{
                    hash[id] = [];
                    hash[id].push(photo);
                }
            });
            // logger.debug("THE HASH IS")
            // logger.debug(hash)
            return hash;
        }

        /**
         * Add random string to photo files to get local image url.
         */
        function addRandomQueryStringToPhotoFiles() {
            $scope.photos = _.each($scope.photos, function(photo){
                photo.src = "http://localhost:" + $scope.setting.localTcpPort + photo.fileName + '?r=' + Math.round(Math.random() * 999999);

                photo.origSrc = "http://localhost:" + $scope.setting.localTcpPort + photo.originalFileName + '?r=' + Math.round(Math.random() * 999999);
            });
        }

        // $scope.showPhoto = function (index) {
        //     $scope._Index = index;
        //     // setCurrentImage();            
        // };

        /**
         * Runs logic to handle what happens when xray template box is clicked. 
         * @param {*} eventId 
         */
        $scope.handleClickEvent = function(eventId){
            //handle clicks when we are trying to make selections to take xray.
            if($scope.disabledClick){
                //alert("we are here in disabled click");
                if($scope.takeXrayMode){
                    //make sure that all other selections are removed
                    //alert("We are in take xray mode");
                    removeAllActiveImages();
                    var formatedId = '#' + eventId
                    makeActive(selectedXrayChart , eventId);
                    // $scope.activeImageCell = eventId;
                    $scope.nextIndex = getNextIndex(eventId);
                    // alert("This is next index " + $scope.nextIndex);

                    //Save the active image cell in settings
                    // saveTemplateToSettings(eventId);
                }
            }
            else{
                $scope.toggleSelection(eventId);
            }
        }

        function makeActive(xrayChart, elementId){
            _.each(xrayChart, function(val, key){
                var id = xrayChart[key][(key+1).toString()];
                if(id = elementId){
                    var formatedId = '#' + id;
                    $(formatedId).addClass("active-image");
                    $scope.activeImageCell = id;
                }
            });
        }

        $scope.nextTemplateCell = function(){
            console.log("next index ", $scope.nextIndex)
            var index = getNextIndex($scope.activeImageCell);
            var nextImageId = $scope.template.imagePositions[index];
            removeAllActiveImages();
            makeActive(selectedXrayChart, nextImageId);
            $scope.nextIndex = getNextIndex($scope.activeImageCell);
        }

        $scope.previousTemplateCell = function(){
            var index = getPreviousIndex($scope.activeImageCell);
            var perviousIamgeId = $scope.template.imagePositions[index];
            removeAllActiveImages();
            makeActive(selectedXrayChart, perviousIamgeId);
            $scope.nextIndex = getNextIndex($scope.activeImageCell);
        }
        
        /**
         * 
         * Handle logic to set view that will select and unselect cells. 
         * @param {*} id 
         */
        $scope.toggleSelection = function(id){
            // console.log(element.target.id);
            // console.log("The id : ", id);
            var formatedId = "#" + id;

                $(formatedId).toggleClass("selected");

            //if the image cell is selected 
                if($(formatedId).hasClass("selected")) {
                    selectedCount++

                    var newElement = "<div style='position: absolute' " + "id=selected_" + id + " class='selected-count-number'>" + selectedCount.toString() + "</div>";
                    // angular.element(formatedId).append($compile(newElement)($scope));
                    $(formatedId).append(newElement);
                    var obj = {};
                    obj[selectedCount] = id;
                    selectedXrayChart.push(obj);
                    // console.log(selectedXrayChart);
                }
                else if( !$(formatedId).hasClass("selected") && selectedCount > 0){
                    var slicedXrayChartArray;
                    var matchedKey = findMatchingKey(selectedXrayChart, id);
                    // console.log(matchedKey);
                    if(matchedKey >= 0 && selectedXrayChart.length > 1){
                        slicedXrayChartArray = selectedXrayChart.splice(matchedKey, selectedXrayChart.length);
                        // console.log(slicedXrayChartArray);
                        formatSelectedXrayChart(matchedKey, slicedXrayChartArray, selectedXrayChart);
                    }
                    else{
                        slicedXrayChartArray = selectedXrayChart.splice(0, 1);
                    }
                    // console.log(slicedXrayChartArray);
                    // console.log(selectedXrayChart);
                    $(formatedId).find("div.selected-count-number").remove();
                    remakeSelections(selectedXrayChart);
                    selectedCount -- ;
                }
        }

        /**
         * removes the active image border from the xrayChart
         * used when going into edit and create template
         * @param {Array} xrayChart 
         */
        function removeAllActiveImages(xrayChart){
            if(!xrayChart){
                xrayChart = $scope.getCurrentChart();
            }

            _.each(xrayChart, function(val, key){
                var id = xrayChart[key][(key+1).toString()];
                var formatedId = '#' + id;

                $(formatedId).removeClass("active-image");
            })
        }

        //removes the all the selecitons classes.
        function remakeSelections(arrayMap){
            _.forEach(arrayMap, function(value, key){
                var id = arrayMap[key][(key+1).toString()];
                var formatedId = '#' + id;
                $(formatedId).find("div.selected-count-number").remove();
                // $(formatedId).removeClass("selected");
                // $(formatedId).addClass("selected");
                var newElement = "<div class='selected-count-number'>" + (key+1).toString() + "</div>";
                $(formatedId).append(newElement);
            })
        }

        function formatSelectedXrayChart(matchedKey, slicedArray, selectedArray){
            slicedArray.shift();
            var selectedObjectKey = matchedKey + 1;
            // console.log(selectedObjectKey);
            // console.log('This is the sliced Array ');
            // console.log(slicedArray);
            _.forEach(slicedArray, function(value, key){
                var currentKey = Object.keys(slicedArray[key]);
                var obj = {}
                obj[currentKey - 1] = slicedArray[key][currentKey];
                selectedArray.push(obj);
            })
        }

        var findMatchingKey = function(chart, id){
            var matchedKey
            _.forEach(chart, function(value, key){
                if(chart[key][(key+1).toString()] === id){
                    matchedKey = key;
                }
            });
            return matchedKey;
        }

        $scope.selectAllSelections = function(xrayChart){
            if(xrayChart === undefined){
                var xrayChart = $scope.getCurrentChart();
            }
            selectedCount = 0;
            selectedXrayChart = [];
            _.forEach(xrayChart, function(value, key){
                var id = xrayChart[key][(key+1).toString()];
                selectedXrayChart.push(xrayChart[key]);
                var formatedId = '#' + id;
                $(formatedId).find("div.selected-count-number").remove();
                if(!$(formatedId).children().hasClass("selected-count-number")){
                    $(formatedId).removeClass("selected");
                    $(formatedId).addClass("selected");
                    selectedCount++;
                    var newElement = "<div class='selected-count-number'>" + selectedCount.toString() + "</div>";
                    $(formatedId).append(newElement);
                }
            })
        }

        $scope.removeAllSelections = function(xrayChart){
            if(xrayChart === undefined){
                var xrayChart = $scope.getCurrentChart();
            }
            _.forEach(xrayChart, function(value, key){
                var id = xrayChart[key][(key+1).toString()];
                var formatedId = '#' + id;
                $(formatedId).removeClass("selected");
                $(formatedId).removeClass("hide-xray-panels");
                $(formatedId).find("div.selected-count-number").remove();
                $(formatedId).find("img").remove();
                $(formatedId).find("span").removeClass("rearrage-position-name");
                $(formatedId).css({
                    "background-image":"none",
                    "color": "black",
                });
            });
            removeAppliedImages();
            moveImagesToPhotosArray()
            selectedCount = 0;
            selectedXrayChart = [];
        }

        function moveImagesToPhotosArray(){
            while($scope.appliedPhotos.length >= 1){
                var img = $scope.appliedPhotos.pop();
                $scope.photos.unshift(img);
            }
            // logger.debug("This the photos array");
            // logger.debug($scope.photos);
            $scope.hasAppliedPhotos = false;
        }

        function removeAppliedImages(xrayChart){
            _.forEach(xrayChart, function(value, key){
                var id = xrayChart[key][(key+1).toString()];
                var formatedId = '#' + id;
                $(formatedId).find("img").remove();
                $(formatedId).find("span").removeClass("rearrage-position-name");
                $(formatedId).css({
                    "background-image":"none",
                    "color": "black"
                });
            })
        }   

        $scope.editTemplate = function(evt){
            if(evt){
                evt.preventDefault();
            }
            var chart = $scope.getCurrentChart();
            
            removeAllActiveImages(chart);
            $scope.takeXrayMode = false;

            removeAppliedImages(chart);
            moveImagesToPhotosArray();

            $scope.showAllSelections(chart);

            $scope.inCreateMode = false;
            // $('#create-mode').removeClass("highlight-actvie-box");

            toggleEditMode();
            
            _.forEach(chart, function(value, key){
                var id = chart[key][(key+1).toString()];
                var formatedId = '#' + id;
                $(formatedId).toggleClass("add-cursor");
            })
            if($scope.disabledClick){
              $scope.disabledClick = false;
            }
            else{
              $scope.disabledClick = true;
            }

            if(!$scope.inEditMode){
                $scope.removeAllSelections(chart);
                $scope.onTemplateChange();
            }
        }

        $scope.createTemplate = function(evt){
            if(evt){
                evt.preventDefault();
            }

            var chart = $scope.getCurrentChart();
            removeAllActiveImages(chart);
            $scope.takeXrayMode = false;

            $scope.inEditMode = false;
            // $('#edit-mode').removeClass("highlight-active-box");
            toggleCreateMode();

            $scope.removeAllSelections(chart);

            if($scope.inCreateMode){    
                $scope.newTemplate = {};
            }

            // console.log($scope.newTemplate);

            _.forEach(chart, function(value, key){
                var id = chart[key][(key+1).toString()];
                var formatedId = '#' + id;
                $(formatedId).toggleClass("add-cursor");
            })

            if($scope.disabledClick){
                $scope.disabledClick = false;
            }
              else{
                $scope.disabledClick = true;
            }

            if(!$scope.inCreateMode){
                $scope.onTemplateChange();
            }

        }

        function toggleEditMode(){
            if($scope.inEditMode){
                $scope.inEditMode = false;
                // $('#edit-mode').removeClass("highlight-active-box");
            }
            else{
                $scope.inEditMode = true;
                // $('#edit-mode').addClass("highlight-active-box");
            }
        }

        function toggleCreateMode(){
            if($scope.inCreateMode){
                $scope.inCreateMode = false;
                // $('#create-mode').removeClass("highlight-active-box");
            }
            else{
                $scope.inCreateMode = true;
                // $('#create-mode').addClass("highlight-active-box");
            }
        }

        $scope.saveTemplate = function(){

            //alert("these are the settings inside of save template");

            //alert($scope.setting);
            
            // console.log("This is the template " + $scope.template);
            if($scope.inCreateMode){
                if(!$scope.newTemplate){

                    var alertObj = {
                        type: 'error',
                        title: 'No Name Selected',
                        text: 'The template name is not defined, please enter a template name.',
                        timeout: 5000,
                        showCloseButton: true
                        }
                    t32AlertService.showToaster(alertObj)
                    return
                }

                if(checkForDuplicateNames($scope.newTemplate.templateName) === true){
                    console.log("DUPLICATE NAME")
                    
                    var alertObj = {
                        type: 'error',
                        title: 'Name Already Exists',
                        text: 'The template name is already taken, please enter a new one.',
                        timeout: 5000,
                        showCloseButton: true
                    }
                    t32AlertService.showToaster(alertObj)
                            return
                        }
                else{
                    // console.log($scope.newTemplate.templateName);

                    $scope.newTemplate.imagePositions = [];
                    
                    _.each(selectedXrayChart, function(val, key){
                        var value = selectedXrayChart[key][key+1]
                        // console.log(value);
                        $scope.newTemplate.imagePositions.push(value);
                    });

                    //save tempaltes to settings json file until the user uploads the images. 
                    // $scope.template = $scope.newTemplate;

                    // if($scpoe.clinic_id){
                    //     $scope.template.clinic_id = $scope.clinic_id
                    // }
                    console.log("We have image positions ")
                    // console.log($scope.newTemplate.imagePositions);

                    if($scope.newTemplate.imagePositions.length){
                        $scope.template = $scope.newTemplate;
                        $scope.setting.currentTemplate = $scope.template.imagePositions;
                        $scope.setting.currentTemplateObj = angular.toJson($scope.newTemplate);

                        $scope.templates.push($scope.template);
                        $scope.setting.xrayTemplates = angular.toJson($scope.templates);
                        
                        $scope.activeImageCell = $scope.template.imagePositions[0];
                        $scope.setting.activeImageCell = $scope.activeImageCell;
    
                        // var requestObject = $scope.setting
                        // t32SystemService.submitSystemRequest('saveXrayStudioSetting', requestObject);
                        t32SystemService.saveXrayStudioSetting($scope.setting)
    
                        //toggle off create button
                        $scope.createTemplate();
                    }
                    else{

                        var alertObj = {
                            type: 'error',
                            title: 'No Positions Selected',
                            text: 'Please define template positions.',
                            timeout: 5000,
                            showCloseButton: true
                        }
                        t32AlertService.showToaster(alertObj)
                        return
                    }
                }   
            }
            else{
                $scope.template.imagePositions = [];

                _.each(selectedXrayChart, function(val, key){
                    var value = selectedXrayChart[key][key+1]
                    // console.log(value);
                    $scope.template.imagePositions.push(value);
                })

                //alert("this is the settings ");

                //alert($scope.setting);
                // console.log("We have image positions " + $scope.newTemplate.imagePositions);
                // go get settings if the settings template is not defined.
                if($scope.template.imagePositions.length){
                    $scope.activeImageCell = $scope.template.imagePositions[0];
                    $scope.setting.activeImageCell = $scope.activeImageCell;
                    // if($scope.template.version){
                    //     $scope.template.version += 1;
                    // }
                    $scope.setting.xrayTemplates = angular.toJson($scope.templates);
                    $scope.setting.currentTemplate = $scope.template.imagePositions;
                    $scope.setting.currentTemplateObj = angular.toJson($scope.template);
    
                    // var requestObject = $scope.setting
                    // t32SystemService.submitSystemRequest('saveXrayStudioSetting', requestObject);

                    t32SystemService.saveXrayStudioSetting($scope.setting)
    
                    //toggle off edit 
                    $scope.editTemplate();   
                }
                else{

                    var alertObj = {
                        type: 'error',
                        title: 'No Positions Selected',
                        text: 'Please define template positions.',
                        timeout: 5000,
                        showCloseButton: true
                    }
                    t32AlertService.showToaster(alertObj)
                    return
                }
            }
        }


        function checkForDuplicateNames(templateName){
            var duplicate = false;
            var templates  = $scope.setting.xrayTemplates ? JSON.parse($scope.setting.xrayTemplates) : [];
            _.each(templates, function(obj){
                if(obj.templateName === templateName){
                    duplicate = true;
                }
            });
            return duplicate;
        }

        $scope.deleteTemplate = function(evt){
            if(evt){
                evt.preventDefault();
            }

            if(!$scope.inCreateMode){
                var alertObj = {
                    title: "Delete Template",
                    text: "Are you sure you want to delete this xray template?",
                    confirmColor: '#d9534f', 
                    confirmText: "Yes",
                    showCancel: true,
                    cancelText: "No"
                }
                t32AlertService.showAlert(alertObj)
                .then(function(res){
                    if(res.alert === 'confirm'){
                        var index = _.findIndex($scope.templates, function(obj){
                            return $scope.template === obj;
                        });
            
                        //remove the current template in the scope and put it into a deleted templates array.
                        if(!$scope.deletedTemplates){
                            $scope.deletedTemplates = [];
                        };

                        if($scope.template._id){
                            var deleted = $scope.templates.splice(index, 1)[0];
                            deleted.isToBeDeleted = true;
                            $scope.deletedTemplates.push(deleted);
                        }
                        else{
                            $scope.templates.splice(index, 1);
                        }

                        // $scope.templates.splice(index, 1);
            
                        $scope.template = $scope.templates.length ? $scope.templates[0] : null;
                        $scope.setting.currentTemplate = $scope.template ? $scope.template.imagePositions : null;
                        $scope.setting.currentTemplateObj = $scope.template ? angular.toJson($scope.template) : null;
                        $scope.setting.xrayTemplates = angular.toJson($scope.templates);
                        $scope.setting.deletedTemplates = angular.toJson($scope.deletedTemplates);
                        $scope.nextIndex = null;
                        $scope.activeImageCell = null;
                        $scope.setting.activeImageCell = $scope.activeImageCell;

                        // var requestObject = $scope.setting

                        // t32SystemService.submitSystemRequest('saveXrayStudioSetting', requestObject)
                        t32SystemService.saveXrayStudioSetting($scope.setting)
                        .then(function(){
                            //make sure we get out of edit template;
                            if($scope.inEditMode){
                                $scope.editTemplate();
                            }
                        });
                    }
                })
            }

        }

        $scope.isTemplateNameEntered = function(){
            if($scope.inCreateMode){
                if($scope.newTemplate.templateName){
                    return true;
                }
                else{
                    return false;
                }
            }
            else if(!$scope.inCreateMode){
                if($scope.template.templateName){
                    return true;
                }
                else{
                    return false;
                }
            }
        }

        $scope.onUpload = function() {
            ensurePatient()
            .then(function(patient){
                return showConfirmationModal(patient);
            })
            .then(function(recordData){
                var patient = recordData.patient;
                var imageTakenDate = recordData.recordDate;
                $scope.doUpload(patient, imageTakenDate);
                t32XrayDriverService.stopScan($scope.selectedDriver);
                $modalInstance.close();
            })
            // showConfirmationModal({name: 'Test'})
            // .then(function(recordDate){
            //     // alert("We have the upload record date");
            //     return $sccope.doUpload
            //     logger.debug(recordDate);
            // })  
            .catch(function(err){
                logger.debug(err);
            })
        };

        function showConfirmationModal(patient){
            var deferred = $q.defer();

            var modalInstance = $modal.open({
                templateUrl:'widgets/patient/t32PatientXrayRecordDate.html',
                controller: 't32PatientXrayRecordDateCtrl',
                windowClass : 'fiftyPercent-Modal',
                backdrop: 'static',
                resolve: {
                    patient: function(){
                        return patient;
                    },
                }
            });

            modalInstance.result.then(function (recordDate) {      
                deferred.resolve(recordDate)                     
            }, function (err) {   
                deferred.reject(err);                   
            });
                
            return deferred.promise     
        }

        /**
         * make sure patient selected
         */
        function ensurePatient() {
            var deferred = $q.defer();
            if ( $scope.selectedPatient )
            {
                deferred.resolve($scope.selectedPatient);
            }
            else
            {
                var fromEhrOnly = true;
                t32PatientSelectionService.selectPatient( fromEhrOnly )
                .then(function(patientContext){
                    // alert(patientContext.selectedPatient);
                    $scope.selectedPatient = patientContext.selectedPatient;
                    deferred.resolve(patientContext.selectedPatient);
                })
            }
            return deferred.promise;
        }

        $scope.doUpload = function(patient, imageTakenDate) {
            var requestType = 'uploadPatientXrays';

            var requestObject = { 
                clinic_id       : patient.clinic_id,
                patient_id      : patient._id,
                folder          : $scope.selectedFolder,
                originalsFolder : $scope.selectedFolder + "/originals",
                type            : 'xray',
                imageTakenDate  : imageTakenDate,
                imageSettings   : $scope.imageSettings
            };

            $scope.xrayUploading = true;

            // logger.info("Submitting request to upload xrays from client side ", requestObject)
            t32SystemService.submitSystemRequest(requestType, requestObject)
            .then(function(result){
                console.log("We have the results from uploading");
                $scope.xrayUploading = false;
                $modalInstance.close({action: ''});

                var swalOpts = {
                    title: "File Uploaded",
                    text: "Xrays are uploaded into tab32 EHR app.",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#d9534f",
                    confirmButtonText: "OK",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: true,
                    allowOutsideClick: true,
                    closeOnCancel: true
                };
                SweetAlert.swal(
                    swalOpts,
                    function (isConfirm) {
                    }
                );
            })
            .catch(function(err){
                // logger.debug("We have recieved the error from the system service")
                $scope.xrayUploading = false;

                var alertObj = {
                    type: 'error',
                    title: 'Upload Failed',
                    text: 'Xray uploading failed, please try again later.',
                    timeout: 0,
                    showCloseButton: true
                    }
                t32AlertService.showToaster(alertObj)

                console.error(err);
                logger.error(err);
            })
        };

        function confirmXrayUpload(patient) {
            var deferred = $q.defer();
            var swalOpts = {
                title: "For " + patient.name,
                text: "Upload images into EHR.",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#d9534f",
                confirmButtonText: "OK",
                cancelButtonText: "Cancel",
                closeOnConfirm: true,
                allowOutsideClick: true,
                closeOnCancel: true
            };
            SweetAlert.swal(
                swalOpts,
                function(isConfirm){
                    if ( isConfirm )
                    {
                        deferred.resolve(patient);
                    }
                }
            );
            return deferred.promise;            
        }

        $scope.showPreviousXrays = function(){
            var modalInstance = $modal.open({
                templateUrl: "widgets/template/xrayHistory.html",
                controller: "XrayHistoryCtrl",
                windowClass: "largest-Modal",
                backdrop: "static",
                resolve:{ 
                    currentPatient: function(){
                        return $scope.selectedPatient;
                    }
                }
            });
        }

        $scope.checkForPhotos = function(){
            // alert("Inside of check for photos");
            logger.debug(Object.keys($scope.photosHash).length);
            if(Object.keys($scope.photosHash).length === 0){
                return false;
            }
            else{
                return true;
            }
        }

        // $scope.rotateActiveImage = function(deg){
        //     var index = findAppliedImageIndex();

        //     keepCurrentActiveIndex();
            
        //     if(index !== null){
        //         var imageUrl = $scope.appliedPhotos[index].fileName;
        //         // alert(imageUrl);
        //         if(imageUrl){
        //             t32ImageManipulation.rotate(imageUrl, deg)
        //             .then(function(result){
        //                 $scope.loadFolderImages($scope.selectedFolder);
        //             })
        //         }
        //     }
        // }

        // function keepCurrentActiveIndex(){
        //     var currentIndex = _.findIndex($scope.template.imagePositions, function(pos){
        //         return pos === $scope.activeImageCell
        //     });

        //     // alert(currentIndex);
        //     if(currentIndex !== -1){
        //         $scope.nextIndex = currentIndex
        //     }
        // }

        // $scope.deleteActiveImage = function(){
        //     var index = findAppliedImageIndex();
        //     // alert(index);
        //     keepCurrentActiveIndex();
        //     if(index !== null){
        //         var filePath = $scope.appliedPhotos[index].fileName;
        //         t32ImageManipulation.delete(false, filePath)
        //         .then(function(result){
        //             if(result === 'ok'){
        //                 $scope.loadFolderImages($scope.selectedFolder);
        //             }
        //         })
        //         .catch(function(err){
        //             conosle.log(err);
        //         })
        //     }

        // }

        // $scope.deleteAllImages = function(){
        //     t32ImageManipulation.delete(true, $scope.selectedFolder)
        //     .then(function(result){
        //         if(result === 'ok'){
        //             $scope.nextIndex = null;
        //             $scope.loadFolderImages($scope.selectedFolder);
        //         }
        //     })
        //     .catch(function(err){
        //         console.log(err)
        //     })
        // }

        // function findAppliedImageIndex(){
        //     var index = _.findIndex($scope.appliedPhotos, function(photo){
        //         var id = parseId(photo.desc);
        //         logger.debug(id);
        //         return id === $scope.activeImageCell
        //     });

        //     if(index !== -1){
        //         return index;
        //     }
        //     else{
        //         return null;
        //     }

        // }

        $scope.$on('swappedXrayImages', function(evt, data){
            console.log('swapping images');

            var requestObject = {
                fileDesc: getImageName(data.fileDesc, $scope.selectedFolder),
                newFileDesc:getImageName( data.newFileDesc, $scope.selectedFolder),
                keepBothNames: data.keepBothNames,
                settings: $scope.imageSettings
            }

            // alert(requestObject);
            // console.log(requestObject);

            t32SystemService.submitSystemRequest('swapImageSettings', requestObject)
            .then(function(settings){
                $scope.imageSettings = settings;
                $scope.loadFolderImages($scope.selectedFolder);
            })
            .catch(function(err){
                // logger.error(err);
                $scope.loadFolderImages($scope.selectedFolder);
            })
        })

        function getImageName(fileName, currentFolder) {
            var regex = "/" + currentFolder + "/";
            var newName = fileName.replace(regex, "");
            return newName
        }

        /**
         * Handle rotation setting defualt settings save
         */
        $scope.$on('setAndSaveDefaultSettings', function(){
            
            var defaultSettingObj = t32RotationSettingsService.getDefualtSettings()
            $scope.rotationSettings.push(defaultSettingObj);
            console.log("Saving and setting default");
            // console.log(t32RotationSettingsService.getSettingFromSettingsObj(defaultSettingObj));
            $scope.currentRotationSetting = $scope.rotationSettings[0];
            $scope.setting.rotationSettings = angular.toJson($scope.rotationSettings);
            // $scope.setting.rotationSettings = JSON.stringify($scope.rotationSettings, null, 4);;
            // $scope.setting.rotationSettings = $scope.rotationSettings;
            $scope.setting.currentRotationSetting = angular.toJson($scope.currentRotationSetting);
            $scope.currentRotationSettingObj = t32RotationSettingsService.getSettingFromSettingsObj($scope.currentRotationSettingObj.setting);
            $scope.setting.currentRotationSettingObj = $scope.currentRotationSettingObj;

            // console.log($scope.rotationSettings);
            // console.log($scope.currentRotationSetting);
            // console.log($scope.setting);

            t32SystemService.saveXrayStudioSetting($scope.setting)
            .then(function(){
            })
        })

        $scope.$on('saveRotationSettings', function(evt, result){
            if(result){
                $scope.rotationSettings = result.rotationSettings;
                $scope.currentRotationSetting = result.currentRotationSetting;
            }

            saveRotationSettings();
        })

        $scope.$on('updateRotationSettings', function(evt, result){
            if(result){
                // if(result.currentRotationSetting.version){
                //     result.currentRotationSetting.version += 1;
                // }
                $scope.rotationSettings = result.rotationSettings;
                $scope.currentRotationSetting = result.currentRotationSetting;
            }

            saveRotationSettings();
        })

        $scope.$on('deleteRotationSettings', function(){
            _.find($scope.rotationSettings, function (obj, index) {
                if (obj.name == $scope.currentRotationSetting.name) {
                    $scope.rotationSettings.splice(index, 1);
                    $scope.currentRotationSetting = $scope.rotationSettings[0];
                    if(obj._id){
                        $scope.deletedRotationSettings.push(obj);
                    }
                }
            });

            saveRotationSettings();
        })

        $scope.$on('saveDriverSeleciton', function(evt, driver){
            console.log("the selected driver is ", $scope.selectedDriver);

            $scope.setting.selectedDriver = $scope.selectedDriver;

            t32SystemService.saveXrayStudioSetting($scope.setting)
            .then(function(setting){
                $scope.setting = setting;
            })
        })

        // $scope.$on('recordConflict', function(evt, msg){
        //     openConflictResolutionModal(msg)
        //     .then(function(){
        //         console.log("We have resolved the conflict")
        //     })
        //     .catch(function(err){
        //         var errorObj = {
        //             type: "error",
        //             title: "FatalError",
        //             text: "Something went wrong, exiting the patient view.",
        //             showCloseButton: true,
        //             timeout: 5000
        //         }
        //         t32AlertService.showToaster(errorObj);
        //         $modalInstance.close();
        //     })
        // })

        // function openConflictResolutionModal (msg) {
        //     var deferred = $q.defer();
        //     console.log("Opening modal")
        //     var modalInstance = $modal.open({
        //         templateUrl: 'widgets/conflictResolution/t32RecordConflictResolution.html',
        //         controller: 't32RecordConflictResolutionCtrl',
        //         size: 'md',
        //         backdrop: 'static',
        //         resolve: {
        //             message: function () {
        //                 return msg;
        //             }
        //         }
        //     });

        //     modalInstance.result.then(function (resolvedRecords) {
        //         deferred.resolve(resolvedRecords);
        //     }, function () {
        //         deferred.resolve();
        //     });

        //     return deferred.promise;
        // };

        function saveRotationSettings(){
            console.log("save rotation setting");
            // console.log(t32RotationSettingsService.getSettingFromSettingsObj($scope.currentRotationSetting.setting));

            $scope.setting.rotationSettings = angular.toJson($scope.rotationSettings);
            $scope.setting.currentRotationSetting = angular.toJson($scope.currentRotationSetting);
            $scope.setting.deletedRotationSettings = angular.toJson($scope.deletedRotationSettings);
            $scope.setting.currentRotationSettingObj = t32RotationSettingsService.getSettingFromSettingsObj($scope.currentRotationSetting.setting);

            // console.log($scope.rotationSettings);
            // console.log($scope.currentRotationSetting);
            // console.log($scope.setting);
            // var requestObject = $scope.setting;

            // t32SystemService.submitSystemRequest('saveXrayStudioSetting', requestObject)
            t32SystemService.saveXrayStudioSetting($scope.setting)
            .then(function () {
            })
        }

        //handle change for dropdown select change for rotaiton settings
        $scope.onSelecitonChange = function(){
            // alert("We are about to save after selection change");
            $timeout(function(){
                saveRotationSettings();
            }, 100);
        }

        $scope.cancel = function(){
            if($scope.xrayUploading){
                $modalInstance.close({action: "upload"});
            }
            $modalInstance.close({action: "close"});
        }

        $scope.titleImg = "img/xray-clipboard-alt.svg";

        $scope.titleStyle = {
        };

        $scope.titleImgStyle = {
            "width": "100px",
            "position": "absolute",
            "left": "25px"
        }

        init();
    }]);
})(window, window.angular);