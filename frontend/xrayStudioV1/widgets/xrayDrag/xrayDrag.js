(function (window, angular, undefined) {
  'use strict';
	angular.module('app').directive('xrayDrag', [ '$rootScope', 't32XrayDragService', '$timeout', 't32XrayImageService',
		function ($rootScope, t32XrayDragService, $timeout, t32XrayImageService) {
			return {
				restrict: 'A',
				scope: {
					targetId: '@',
					overflowImg: '@'
				},
				link: function (scope, element, attr) {
					element.bind('click', function(e){
						console.log("EVENT CLICKED");
						console.log(scope.targetId);
						if(scope.targetId){
							scope.$emit("handleClick", scope.targetId);
						}
					})
					element.bind('dragstart', function(e){
						console.log("We have started dragging " + scope.targetId);
						console.log("*****DRAG EVENT STARTED***");
						
						var fileName = element.find("img").attr("fileName");
						console.log(fileName);
						var originalFileName = element.find("img").attr("originalFileName");
						var fileDesc = element.find("img").attr("fileDesc");

						t32XrayDragService.setFileDesc(fileDesc);
						t32XrayDragService.setFileName(fileName);
						t32XrayDragService.setOriginalFileName(originalFileName);
						t32XrayDragService.setIsOverflowImg(scope.overflowImg);
						// alert(scope.overflowImg);

						t32XrayDragService.setStartingDragId(scope.targetId);
						// e.preventDefault();
					})
					element.bind('dragover',function ( e ) {
                        // console.log('detect dragover event....');
                        // logger.debug("***DRAGOVER EVENT FIRING*****")
						e.preventDefault();
					});
					element.bind('dragleave',function ( e ) {
                        // console.log('detect dragleave event....');
                        // logger.debug("***DRAGLEAVE EVENT FIRING*****")
						e.preventDefault();
					});
					element.bind('dragenter',function ( e ) {
                        // console.log('detect draglENTER event....');
                        // logger.debug("***DRAGENTER EVENT FIRING*****")
						e.preventDefault();
					});
					element.bind('drop',function ( e ) {
                        // console.log('detect drop event....');
						// logger.debug("***DRAGDROP EVENT FIRING*****");
						e.preventDefault();
						// alert("dropped")
						console.log(scope.targetId);
						t32XrayDragService.setEndingDragId(scope.targetId);

						var endingFileName = element.find("img").attr("fileName");
						var endingOriginalFileName = element.find("img").attr("originalFileName");
						var endingFileDesc = element.find("img").attr("fileDesc");

						console.log(endingFileName);
						console.log(endingFileDesc);
						console.log(endingOriginalFileName);
						
						t32XrayDragService.setEndingFileName(endingFileName);
						t32XrayDragService.setEndingOriginalFileName(endingOriginalFileName);
						t32XrayDragService.setEndingFileDesc(endingFileDesc);

						var fileName = t32XrayDragService.getFileName();
						var originalFileName = t32XrayDragService.getOriginalFileName();
						var endingFileName = t32XrayDragService.getEndingFileName();
						var endingOriginalFileName = t32XrayDragService.getEndingOriginalFileName();
						var startingDragId = t32XrayDragService.getStartingDragId();
						var endingDragId = t32XrayDragService.getEndingDragId();
						var isOverflowImg = t32XrayDragService.getIsOverflowImg();

						//if our drop location is not the overflow images section
						if(!scope.overflowImg){
							console.log("Here is the scope.");
							console.log(scope);
							if(!scope.$parent.inEditMode && !scope.$parent.inCreateMode){
								t32XrayDragService.swapXrayImages(startingDragId, endingDragId, fileName, originalFileName, endingFileName, endingOriginalFileName, isOverflowImg)
								.then(function(result){
									if (result == 'matchingIds') {
										var data = {
											fileDesc: t32XrayDragService.getFileDesc(),
											newFileDesc: t32XrayDragService.getEndingFileDesc(),
											keepBothNames: true,
											getImageName: false
										}
									}
									else{

										var data = {
											fileDesc: t32XrayDragService.getFileDesc(),
											newFileDesc: t32XrayImageService.removeExt(result),
											keepBothNames: false,
											getImageName: true
										}
									}
										
									scope.$emit('swappedXrayImages', data);
								})
								.catch(function(err){
									console.log(err);
								})
							}
						}
						else{
							
						}
						// alert('dropped onto ' + scope.targetId);
					});
				}
			};
		}
	]);   
})(window, window.angular);