(function (window, angular) {
    'use strict';
    angular.module('app').directive('t32XraySensorSettingsDropdown', ['$modal',
        function ($modal) {
            return {
                restrict: 'AE',
                scope: {
                    clinicId: '=',
                    settings: '=',
                    currentXraySensorSetting: '=',
                    xraySensorSettings: '=',
                    deletedXraySensorSettings: '=',
                    onSelectionChange: '&',
                    show: "="
                },
                templateUrl: 'widgets/xraySensor/t32XraySensorSettingsDropdown.html',
                controller: 't32XraySensorSettingsDropdownCtrl',
            };
        }
    ]);

    angular.module('app').controller('t32XraySensorSettingsDropdownCtrl', [
        '$rootScope',
        '$scope',
        '$modal',
        '$q',
        '$timeout',
        't32AlertService',
        'logger',
        function (
            $rootScope,
            $scope,
            $modal,
            $q,
            $timeout,
            t32AlertService,
            logger) {
            function init() {
                setupXraySensorSettings();
                // syncXraySensorSettings();
            }

            $scope.inSettingsSave = false;
            // $scope.settings = null;

            // function syncXraySensorSettings() {
            //     $scope.inSettingsSave = true;
            //     // console.log("WE ARE LOGGED IN SO SYNCING SETTINGS");
            //     t32XraySensorSettingsService.syncXraySensorSettings($scope.clinicId)
            //         .then(function (settings) {
            //             $scope.settings = _.cloneDeep(settings);
            //             console.log("THE SETTINGS AFTER SYNC");
            //             console.log(settings);
            //             console.log($scope.settings);
            //             console.log($scope.settings.xraySensorSettings)

            //             $scope.deletedXraySensorSettings = $scope.settings.deletedXraySensorSettings ? JSON.parse($scope.settings.deletedXraySensorSettings) : [];

            //             if ($scope.settings.xraySensorSettings) {
            //                 if (Array.isArray(JSON.parse($scope.settings.xraySensorSettings))) {
            //                     $scope.xraySensorSettings = JSON.parse($scope.settings.xraySensorSettings);

            //                     console.log("THE SENSOR SETTING ARE DEFINED");
            //                     console.log($scope.xraySensorSettings);

            //                     if ($scope.xraySensorSettings.length > 0) {
            //                         // $scope.xraySensorSettings = filterSettingsByClinicId($scope.xraySensorSettings, $scope.settings);
            //                         $scope.currentXraySensorSetting = $scope.xraySensorSettings[0];
            //                         $scope.onChange();
            //                     }
            //                 }
            //             }
            //             else {
            //                 console.log("THE ROTATION SETTINGS ARE NOT DEFINED");
            //                 logger.info("THE ROTATION SETTINGS ARE NOT DEFINED");
            //             }
            //             $scope.inSettingsSave = false;
            //         })
            //         .catch(function (err) {
            //             console.error(err);
            //             var title = "Fatal Error"
            //             var error = "There was a problem getting the xray sensor settings, please try agian later. "

            //             var errorObj = {
            //                 type: "error",
            //                 title: title,
            //                 text: error,
            //                 timeout: 5000,
            //                 showCloseButton: true
            //             }

            //             t32AlertService.showToaster(errorObj)
            //             $modalInstance.close(result);
            //         })
            // }

            function setupXraySensorSettings(){
                // $scope.settings = _.cloneDeep(settings);
                // console.log("THE SETTINGS AFTER SYNC");
                // console.log(settings);
                console.log("In setup Xray sensor settings")
                // console.log($scope.settings);
                // console.log($scope.settings.xraySensorSettings)

                $scope.deletedXraySensorSettings = $scope.settings.deletedXraySensorSettings ? JSON.parse($scope.settings.deletedXraySensorSettings) : [];

                if ($scope.settings.xraySensorSettings) {
                    if (Array.isArray(JSON.parse($scope.settings.xraySensorSettings))) {
                        $scope.xraySensorSettings = JSON.parse($scope.settings.xraySensorSettings);

                        console.log("THE SENSOR SETTING ARE DEFINED");
                        // console.log($scope.xraySensorSettings);

                        if ($scope.xraySensorSettings.length > 0) {
                            // $scope.xraySensorSettings = filterSettingsByClinicId($scope.xraySensorSettings, $scope.settings);
                            $scope.currentXraySensorSetting = $scope.xraySensorSettings[0];
                            $scope.onChange();
                        }
                    }
                }
                else {
                    console.log("THE ROTATION SETTINGS ARE NOT DEFINED");
                    logger.info("THE ROTATION SETTINGS ARE NOT DEFINED");
                }
            }

            function isNotEmpty(str) {
                if (str !== "" && str != null && str != undefined && str !== {}) {
                    return true;
                }
                else {
                    return false;
                }
            }


            $scope.$on("setupXraySensorSettings", function(){
                setupXraySensorSettings();
            })
            
            $scope.onChange = function () {
                if ($scope.currentXraySensorSetting != "" || $scope.currentXraySensorSetting != null) {
                    $scope.$eval($scope.onSelectionChange);
                }
                console.log('handling sensor change')
                // console.log($scope.currentXraySensorSetting);
            };

            $scope.onEdit = function (evt) {
                if (evt) {
                    evt.preventDefault();
                }
                // evt.preventDefault();
                if($scope.xraySensorSettings.length > 0){
                    var modalInstance = $modal.open({
                        templateUrl: 'widgets/xraySensor/t32EditXraySensorSettings.html',
                        controller: 't32EditXraySensorSettingsCtrl',
                        windowClass: 'seventyPercent-Modal',
                        backdrop: 'static',
                        resolve: {
                            xraySensorSettings: function () {
                                return $scope.xraySensorSettings;
                            },
                            currentXraySensorSetting: function () {
                                return $scope.currentXraySensorSetting;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        if (result.modified) {
                            console.log("We need to save our rotation settings");
                            // alert("Need to save these settings")
                            // $scope.xraySensorSettings = result.xraySensorSettings,
                            // $scope.currentXraySensorSetting = result.currentXraySensorSetting;
                            if (result.action == 'save') {
                                $scope.$emit('updateXraySensorSettings', result);
                            }
                            else if (result.action == 'delete') {
                                $scope.$emit('deleteXraySensorSetting');
                            }
                        }
                        else {
                            $scope.xraySensorSettings = result.xraySensorSettings;
                            $scope.currentXraySensorSetting = result.currentXraySensorSetting;
                            // console.log($scope.xraySensorSettings);
                            // console.log($scope.currentXraySensorSetting.name);
                        }
                    });
                }
            }

            $scope.onCreate = function (evt) {
                if (evt) {
                    evt.preventDefault();
                }
                // evt.preventDefault();
                var modalInstance = $modal.open({
                    templateUrl: 'widgets/xraySensor/t32CreateXraySensorSettings.html',
                    controller: 't32CreateXraySensorSettingsCtrl',
                    windowClass: 'seventyPercent-Modal',
                    backdrop: 'static',
                    resolve: {
                        xraySensorSettings: function () {
                            return $scope.xraySensorSettings;
                        },
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.modified) {
                        console.log("We need to save our rotation settings");
                        // alert("Need to save these settings")
                        // $scope.xraySensorSettings = result.xraySensorSettings,
                        // $scope.currentXraySensorSetting = result.currentXraySensorSetting;
                        $scope.$emit('saveXraySensorSettings', result);
                    }
                    else {
                        // do nothing, we did not create anything
                    }
                });
            }

            $scope.onDelete = function (evt) {
                if (evt) {
                    evt.preventDefault();
                }
                // evt.preventDefault();
                // check to see if defualt, if it is then we cannot delete them.
                if($scope.xraySensorSettings.length > 0){                    
                    var confirm = {
                        title: "Delete Setting",
                        text: "Are you sure you want to delete this xray sensor setting?",
                        confirmColor: '#d9534f',
                        showCancel: true,
                        confirmText: "Yes",
                        cancelText: "No",
                    }
                    
                    t32AlertService.showAlert(confirm)
                    .then(function (res) {
                        if (res.alert == 'confirm') {
                            if ($scope.xraySensorSettings.length > 0) {
                                $scope.$emit('deleteXraySensorSetting');
                            }
                            else {
                                // var alertObj = {
                                //     type: "error",
                                //     title: "Cannot Delete",
                                //     text: "This setting is not allowed to be deleted.",
                                //     timeout: 5000,
                                //     showCloseButton: true
                                // }
                                // t32AlertService.showToaster(alertObj)
                            }
    
                            // console.log("The setttings after delete");
                            // console.log($scope.xraySensorSettings);
                            // console.log($scope.deletedXraySensorSettings);
                        }
                    })
                    .catch(function (err) {
                        var alertObj = {
                            type: "error",
                            title: "Delete Failed",
                            text: "Failed to delete rotation setting, please try again later.",
                            timeout: 5000,
                            showCloseButton: true
                        }
                        t32AlertService.showToaster(alertObj)
                    })
                }
            }

            $scope.isFromLoggedInClinic = function (item) {
                if (!item.clinic_id || item.clinic_id === $scope.settings.clinic_id) {
                    return item;
                }

            }

            init();
        }
    ]);
})(window, window.angular);