(function (window, angular, undefined) {
    'use strict';

    angular.module('app').controller('t32EditXraySensorSettingsCtrl', [
        '$q',
        '$scope',
        't32SystemService',
        '$modalInstance',
        'logger',
        't32XrayImageService',
        '$timeout',
        'SweetAlert',
        't32AlertService',
        'xraySensorSettings',
        'currentXraySensorSetting',
        't32CommonService',
        function (
            $q,
            $scope,
            t32SystemService,
            $modalInstance,
            logger,
            t32XrayImageService,
            $timeout,
            SweetAlert,
            t32AlertService,
            xraySensorSettings,
            currentXraySensorSetting,
            t32CommonService) {
            function init() {
                $scope.xraySensorSettings = xraySensorSettings || [];
                $scope.currentXraySensorSetting = currentXraySensorSetting || {};
                $scope.unEditedXraySensorSettings = _.cloneDeep(xraySensorSettings);
                $scope.unEditedCurrentXraySensorSetting = _.cloneDeep(currentXraySensorSetting);
                // $scope.unEditedCurrentXraySensorSetting = JSON.parse(JSON.stringify(currentXraySensorSetting));
                console.log("Inside edit xray sensor settings, currentXraySensorSetting");
                // console.log(currentXraySensorSetting);
                console.log("Inside edit xray sensor settings, xraySensorSettings");
                // console.log(xraySensorSettings);
            }

            $scope.isEdited = false;
            $scope.formErrors = false;

            $scope.cancel = function () {
                if (!$scope.isEdited) {
                    $modalInstance.close({
                        modified: false,
                        currentXraySensorSetting: $scope.currentXraySensorSetting,
                        xraySensorSettings: $scope.xraySensorSettings
                    })
                }
                else{
                    t32CommonService.askToSave('Xray Sensor Settings')
                        .then(function (answer) {
                            if (answer) {
                                if($scope.formErrors || $scope.xraySensorSettingsEditForm.$invalid){
                                    var title = "Form Error"
                                    var error = "Please correct the form error and try again.";

                                    var errorObj = {
                                        title: title,
                                        text: error,
                                        confirmText: "Close Anyways",
                                        showCancel: true,
                                        cancelText: "Retry"
                                    }

                                    t32AlertService.showError(errorObj)
                                    .then(function (err) {
                                        if (err.error == 'cancel') {
                                            // do nothing and let them retry
                                        }
                                        else {
                                            $modalInstance.close({
                                                modified: false,
                                                xraySensorSettings: $scope.unEditedXraySensorSettings,
                                                currentXraySensorSetting: $scope.unEditedCurrentXraySensorSetting,
                                            })
                                        }
                                    })
                                }
                                $modalInstance.close({
                                    modified: true,
                                    action: 'save',
                                    currentXraySensorSetting: $scope.currentXraySensorSetting,
                                    xraySensorSettings: $scope.xraySensorSettings
                                })
                            }
                            else {
                                console.log("We do not have anything edited");
                                // console.log($scope.unEditedCurrentXraySensorSetting);
                                // console.log($scope.unEditedXraySensorSettings);
                                $modalInstance.close({
                                    modified: false,
                                    xraySensorSettings: $scope.unEditedXraySensorSettings,
                                    currentXraySensorSetting: $scope.unEditedCurrentXraySensorSetting,
                                })
                            }
                        })
                }
                // else {
                //     var title = "Form Error"
                //     var error = "Please correct the form error and try again.";

                //     var errorObj = {
                //         title: title,
                //         text: error,
                //         confirmText: "Close Anyways",
                //         showCancel: true,
                //         cancelText: "Retry"
                //     }

                //     t32AlertService.showError(errorObj)
                //         .then(function (err) {
                //             if (err.error == 'cancel') {
                //                 // do nothing and let them retry
                //             }
                //             else {
                //                 $modalInstance.close({
                //                     modified: false,
                //                     xraySensorSettings: $scope.unEditedXraySensorSettings,
                //                     currentXraySensorSetting: $scope.unEditedCurrentXraySensorSetting,
                //                 })
                //             }
                //         })

                // }
            }

            $scope.deleteXraySensorSetting = function () {
                // check to see if defualt, if it is then we cannot delete them.
                var confirm = {
                    title: "Delete Setting",
                    text: "Are you sure you want to delete this xray sensor setting?",
                    confirmColor: '#d9534f',
                    showCancel: true,
                    confirmText: "Yes",
                    cancelText: "No",
                }

                t32AlertService.showAlert(confirm)
                    .then(function (res) {
                        if (res.alert == 'confirm') {
                            if ($scope.xraySensorSettings.length > 0) {
                                $modalInstance.close({
                                    modified: true,
                                    action: "delete",
                                    currentXraySensorSetting: $scope.currentXraySensorSetting,
                                    xraySensorSettings: $scope.xraySensorSettings
                                })
                            }
                            else {
                                var alertObj = {
                                    type: "error",
                                    title: "Cannot Delete",
                                    text: "There are no settings to be deleted",
                                    timeout: 5000,
                                    showCloseButton: true
                                }
                                t32AlertService.showToaster(alertObj)
                            }
                        }
                    })
                    .catch(function (err) {
                        var alertObj = {
                            type: "error",
                            title: "Delete Failed",
                            text: "Failed to delete rotation setting, please try again later.",
                            timeout: 5000,
                            showCloseButton: true
                        }
                        t32AlertService.showToaster(alertObj)
                    })
            }

            $scope.saveXraySensorSetting = function () {

                if(!$scope.formErrors && $scope.xraySensorSettingsEditForm.$valid){
                    $modalInstance.close({
                        modified: true,
                        action: "save",
                        currentXraySensorSetting: $scope.currentXraySensorSetting,
                        xraySensorSettings: $scope.xraySensorSettings
                    })
                }
                else{
                    var errorObj = {
                        type: "error",
                        title: "Form Error",
                        text: "Please correct the form error and try again..",
                        confirmText: "Okay"
                    }

                    t32AlertService.showError(errorObj)
                    .then(function (err) {
                        // if (err.error == 'cancel') {
                        //     // do nothing and let them retry
                        // }
                        // else {
                        //     $modalInstance.close({
                        //         modified: false,
                        //         xraySensorSettings: $scope.unEditedXraySensorSettings,
                        //         currentXraySensorSetting: $scope.unEditedCurrentXraySensorSetting,
                        //     })
                        // }
                    })
                }
            }

            // $scope.showSubSetting = function (setting) {
            //     $scope.selectedSubSetting = setting;
            // }

            $scope.showSetting = function(setting){
                $scope.currentXraySensorSetting = setting;
            }

            $scope.isSelected = function (data) {
                if ($scope.selectedSubSetting == data) {
                    return 'selected-list-item';
                }
                else {
                    return ''
                }
            }

            $scope.setIsEdited = function () {
                if ($scope.isEdited) {
                    return
                }
                else {
                    $scope.isEdited = true;
                }
            }

            $scope.handleInputChange = function (type) {
                // if ($scope.xraySensorSettingsEditForm[type].$valid) {
                    $scope.setIsEdited();
                    // console.log("There are no errors")
                    // $scope.currentXraySensorSetting[type] = t32CommonService.toTitleCase($scope.currentXraySensorSetting[type]);

                    $scope.formErrors = $scope.xraySensorSettingsEditForm.$invalid;
                // }
                // else {
                //     $scope.setIsEdited();
                //     console.log("There is error");
                //     $scope.formErrors = true;
                //     console.log($scope.isEdited);
                //     console.log($scope.formErrors)
                // }
            }

            $scope.titleText = 'Xray Sensor'
            $scope.titleStyle = {};
            $scope.titleChildDivClass = 'branding';
            $scope.menuBarLen = 3;

            init();
        }
    ])
})(window, window.angular);