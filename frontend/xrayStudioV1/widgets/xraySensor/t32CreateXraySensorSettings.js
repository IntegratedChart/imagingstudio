(function (window, angular, undefined) {
    'use strict';

    angular.module('app').controller('t32CreateXraySensorSettingsCtrl', [
        '$q',
        '$scope',
        't32SystemService',
        '$modalInstance',
        'logger',
        '$timeout',
        'SweetAlert',
        't32AlertService',
        'xraySensorSettings',
        't32CommonService',
        function (
            $q,
            $scope,
            t32SystemService,
            $modalInstance,
            logger,
            $timeout,
            SweetAlert,
            t32AlertService,
            xraySensorSettings,
            t32CommonService) {
            function init() {
                $scope.xraySensorSettings = xraySensorSettings || [];
                $scope.currentXraySensorSetting = {
                    name: null,
                    manufacturer: null,
                    modelNumber: null,
                    prodName: null,
                    height: null,
                    width: null
                };

                $scope.isChanged = false;
                $scope.formErrors = true;

                console.log("Inside create xray sensor settings, currentXraySensorSetting");
                // console.log($scope.currentXraySensorSetting);

                console.log("Inside create xray sensor settings, xraySensorSettings");
                // console.log($scope.xraySensorSettings);

                // console.log($scope.selectedSubSetting);
            }

            $scope.cancel = function () {
                // alert($scope.isChanged);
                if (!$scope.isChanged) {
                    $modalInstance.close({
                        modified: false
                    })
                }
                else{
                    t32CommonService.askToSave('Xray Sensor Settings')
                    .then(function (answer) {
                        if (answer) {
                            if($scope.formErrors || $scope.xraySensorSettingsCreateForm.$invalid){
                                var title = "Form Error"
                                var error = "There are errors with the submission, or missing information.";
        
                                var errorObj = {
                                    title: title,
                                    text: error,
                                    confirmText: "Close Anyways",
                                    showCancel: true,
                                    cancelText: "Retry"
                                }
                                $timeout(function(){
                                    t32AlertService.showError(errorObj)
                                    .then(function (err) {
                                        if (err.error == 'cancel') {
                                            // do nothing and let them retry
                                        }
                                        else {
                                            $modalInstance.close({
                                                modified: false,
                                                xraySensorSettings: null,
                                                currentXraySensorSetting: null,
                                            })
                                        }
                                    })
                                }, 300)
                            }
                            else{
                                $scope.xraySensorSettings.push($scope.currentXraySensorSetting);
        
                                $modalInstance.close({
                                    modified: true,
                                    action: 'save',
                                    currentXraySensorSetting: $scope.currentXraySensorSetting,
                                    xraySensorSettings: $scope.xraySensorSettings
                                })
                            }
                        }
                        else {
                            $modalInstance.close({
                                modified: false
                            })
                        }
                    })
                }
            }

            $scope.saveXraySensorSettings = function () {
                $scope.xraySensorSettings.push($scope.currentXraySensorSetting);

                $modalInstance.close({
                    modified: true,
                    action: 'save',
                    currentXraySensorSetting: $scope.currentXraySensorSetting,
                    xraySensorSettings: $scope.xraySensorSettings
                })
            }

            $scope.setIsChanged = function () {
                if ($scope.isChanged) {
                    return
                }
                else {
                    $scope.isChanged = true;
                }
            }

            $scope.handleInputChange = function (type) {
                // alert('change');
                // $scope.currentXraySensorSetting[type] = t32CommonService.toTitleCase($scope.currentXraySensorSetting[type]);

                console.log('handling input change inside of sensor settings', type);
                // console.log($scope.currentXraySensorSetting[type]);
                $scope.setIsChanged();
                // alert($scope.isChanged);
                $scope.formErrors = $scope.xraySensorSettingsCreateForm.$invalid;
            }

            $scope.titleImg = "img/sensor.svg";

            $scope.titleStyle = {
                "margin-left": "-15px",
                "margin-right": "-15px"
            };
            $scope.titleImgStyle = {
                "width": "100px",
                "position": "absolute",
                "top": "5px",
                "left": "25px"
            }


            init();
        }
    ])
})(window, window.angular);