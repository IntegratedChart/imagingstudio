(function (window, angular, undefined) {
    'use strict';
    angular.module('app').directive('t32XrayStudioDriverList', [
        function () {
            return {
                restrict: 'EA',
                scope: {
                    selectedDriver: '='
                },
                controller: 't32XrayStudioDriverListCtrl',
                templateUrl: 'widgets/xrayDriver/t32XrayStudioDriverList.html'
            };
        }
    ]);

    angular.module('app').controller('t32XrayStudioDriverListCtrl', [
        '$rootScope',
        '$scope',
        '$q',
        '$interval',
        '$timeout',
        'logger',
        't32XrayDriverService',
        't32SystemService',
        function (
            $rootScope,
            $scope,
            $q,
            $interval,
            $timeout,
            logger,
            t32XrayDriverService,
            t32SystemService) {
            function init() {
                logger.info("studio drvier init ");
                logger.info("getting winName");

                $scope.winName = t32SystemService.getWinName();

                t32SystemService.getSystemOS()
                .then( function(sysOs) {
                    os = sysOs;
                    getDriverList(true);
                })
                .catch( function(err) {
                    getDriverList(true)
                })
            }

            $scope.driverList = [];
            $scope.timeElapsed = 0;
            var initFinised = false;
            var os = null;

            var driverReq = $interval(function(){
                getDriverList();
                $scope.timeElapsed += 2;
            }, 2000);

            logger.info('the driver req interval ', driverReq);

            function getDriverList(initCall){
                var deferred = $q.defer();
                
                if (!initFinised && !initCall) {
                    deferred.resolve();
                } else if (os !== 'win32'){
                    console.log("on mac, ignore driver request");
                    $interval.cancel(driverReq);
                } 
                else if ($scope.driverList && $scope.driverList.length > 0) {
                    logger.info("we have driver list already, about to cancel call")
                    $interval.cancel(driverReq);
                    deferred.resolve();
                } else {
                    if ($scope.timeElapsed >= 60) {
                        logger.info("dirver connection failed after retry exausted");
                        $interval.cancel(driverReq);
                    }
                    t32XrayDriverService.getDriverList($scope.winName)
                    .then(function (driverList) {
                        if (driverList && driverList.length > 0) {
                            $scope.driverList = driverList;
                            if(!$scope.selectedDriver){
                                $scope.selectedDriver = $scope.driverList[0];
                            }
                            deferred.resolve($scope.driverList);
                        } else {
                            initFinised = true;
                            deferred.resolve();
                        }
                    })
                    .catch(function(err){
                        deferred.reject(err);
                    })
                }

                return deferred.promise;
            }

            $scope.updateDriverSelection = function(){
                $timeout(function(){
                    $rootScope.$broadcast('saveDriverSeleciton');
                }, 200)
            }

            init();
        }
    ]);
})(window, window.angular);