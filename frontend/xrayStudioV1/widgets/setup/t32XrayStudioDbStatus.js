(function (window, angular, undefined) {
    'use strict';
    angular.module('app').directive('t32XrayStudioDbStatus', [function () {
        return {
            restrict: 'E',
            scope: {
                afterSetup: '&'
            },
            controller: 't32XrayStudioDbStatusCtrl',
            templateUrl: 'widgets/setup/t32XrayStudioDbStatus.html'
        };
    }]);

    angular.module('app').controller('t32XrayStudioDbStatusCtrl', [
        '$q',
        '$modal',
        '$scope',
        't32SystemService',
        't32WorkflowService',
        't32XrayStudioDbService',
        function (
            $q,
            $modal,
            $scope,
            t32SystemService,
            t32WorkflowService,
            t32XrayStudioDbService) {

            function init() {
                $scope.dbInfo = {
                    username: '',
                    database: '',
                    isTab32Admin: false,
                    isConnected: false
                }
                $scope.connectedClass = ['connecting', 'blinking'];
                $scope.status = 'Connecting to db';
                $scope.hasConnectionUpdated = false;
            }

            $scope.$on('dbConnectionChange', function (evt, dbInfo) {
                $scope.dbInfo = dbInfo;
                $scope.hasConnectionUpdated = true;
                updateConnection($scope.dbInfo.isConnected);
            });

            function updateConnection(isConnected) {
                $scope.status = isConnected ? 'Db connected' : 'Db not connected'
            }

            $scope.getConnectionClass = function () {
                if (!$scope.hasConnectionUpdated) {
                    return ['connecting', 'blinking']
                } else if ($scope.dbInfo.isConnected) {
                    return 'connected'
                } else {
                    return 'disconnected'
                }
            }

            $scope.onSetup = function () {
                // only show the collect db info component, if we are not connected and are not the tab32Admin account
                if (!$scope.hasConnectionUpdated || $scope.dbInfo.isConnected || $scope.isTab32Admin) {
                    console.log("db is still connecting or already connected or use is tab32 admin, will not send for credentials collection");
                    return
                }

                return t32XrayStudioDbService.collectDbInfo($scope.dbInfo)
            }

            init();
        }
    ]);
})(window, window.angular);   
