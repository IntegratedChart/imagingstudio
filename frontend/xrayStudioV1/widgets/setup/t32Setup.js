(function (window, angular, undefined) { 'use strict';
   angular.module('app').controller('t32SetupCtrl', [ 
            '$q',
            't32SystemService',
            '$modalInstance',
            '$location',
            '$scope',
      function (
            $q,
            t32SystemService,
            $modalInstance,
            $location,
            $scope ) {

         function init()
         {
            $scope.theModalInstance = $modalInstance;
            
            $scope.showAdvanced = true;

            $scope.logLevels = ['warn', 'info','debug' ];

            // $scope.boolVals = ["True", "False"];
            
            var requestType = 'getXrayStudioSetting';

            t32SystemService.getXrayStudioSetting()
            .then(function(setting){
               $scope.setting = setting;
            //    if(!$scope.setting.appUpdateUrl){
            //       $scope.setting.appUpdateUrl = 'https://images.tab32.com/xrayStudio2/updates/latest/'
            //    }
            });
         }  

         $scope.update = function() {
            // var requestType = 'saveXrayStudioSetting';

            // t32SystemService.submitSystemRequest(requestType, $scope.setting)
            t32SystemService.saveXrayStudioSetting($scope.setting)
            .then(function(setting){
            //    t32SystemService.setXrayStudioSetting(setting);
               $modalInstance.close('OK');
               $location.path('/');
            });
         };

         $scope.resetLicense = function() {
            $scope.setting.license = '';
            $scope.update();
         };

         $scope.titleImg = "img/setting.svg";
         $scope.titleStyle = {
            "margin-left": "-15px",
            "margin-right": "-15px"
         };
         $scope.titleImgStyle = {
            "width": "100px",
            "position": "absolute",
            "top": "5px",
            "left": "35px"
         }
         
         init();
      }
   ]);
})(window, window.angular);   