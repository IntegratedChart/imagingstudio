(function (window, angular, undefined) { 'use strict';
   angular.module('app').directive('t32SetupButton', [function () {
      return {
         restrict: 'E',
         scope: {
            afterSetup : '&'
         },
         controller: 't32SetupButtonCtrl',
         templateUrl: 'widgets/setup/t32SetupButton.html'
      };
   }]);   

   angular.module('app').controller('t32SetupButtonCtrl', [ 
            '$q',
            't32SystemService',
            't32WorkflowService',
            '$modal',
            '$scope',
      function (
            $q,
            t32SystemService,
            t32WorkflowService,
            $modal,
            $scope ) {

         function init()
         {
         }  

         $scope.onSetup = function() {
            t32WorkflowService.openSettingPage()
            .then(function(){
               $scope.$eval($scope.afterSetup);                                           
            });
         }

         init();
      }
   ]);
})(window, window.angular);   
