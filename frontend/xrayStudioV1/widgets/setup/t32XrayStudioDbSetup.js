(function (window, angular, undefined) {
    'use strict';
    angular.module('app').controller('t32XrayStudioDbSetupCtrl', [
        '$q',
        '$scope',
        '$modalInstance',
        't32SystemService',
        't32AlertService',
        'username',
        'database',
        function (
            $q,
            $scope,
            $modalInstance,
            t32SystemService,
            t32AlertService,
            username,
            database) {

            function init() {
                // Set the default value of inputType
                $scope.inputType = 'password';
                $scope.passwordIconClass = 'fa fa-eye'
                $scope.theModalInstance = $modalInstance;

                $scope.username = username || null;
                $scope.password = '';
                $scope.errors = [];
                $scope.database = database

                t32SystemService.getXrayStudioSetting()
                .then(function (setting) {
                    $scope.setting = setting;
                });
            }

            // Hide & show password function
            $scope.togglePassword = function () {

                if ($scope.inputType == 'password') {
                    // $scope.typePassword = true;
                    $scope.inputType = 'text';
                    $scope.passwordIconClass = 'fa fa-eye-slash'
                } else {
                    // $scope.typePassword = false;
                    $scope.passwordIconClass = 'fa fa-eye'
                    $scope.inputType = 'password';
                }
            };

            $scope.submit = function() {
                $scope.errors = [];

                if (!$scope.username) {
                    $scope.errors.push('username is required.')
                } else if (!$scope.password) {
                    $scope.errors.push('password is requried')
                }

                if ($scope.errors.length > 0) {
                    return
                }

                var requestType = 'submitDbCredentials'
                var requestMsg = {
                    database: $scope.database,
                    username: $scope.username,
                    password: $scope.password,
                    isTab32Admin: false
                }

                t32SystemService.submitSystemRequest(requestType, requestMsg)
                .then(function() {
                    var toast = {
                        type: "success",
                        title: "Submission successfull",
                        timeout: 2000,
                    }
                    t32AlertService.showToaster(toast);
                    $modalInstance.close()
                })
                .catch(function(err) {
                    // console.log("error submitting db credentials ", err);
                    var toast = {
                        type: "error",
                        title: "Submission failed, please try again.",
                        timeout: 2000,
                    }
                    t32AlertService.showToaster(toast);
                })
            }

            $scope.cancel = function() {
                console.log("command cancelled, db will stay disconnected.");
                $modalInstance.dismiss();
            }



            init();
        }
    ]);
})(window, window.angular);   