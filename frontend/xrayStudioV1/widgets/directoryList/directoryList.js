(function (window, angular, undefined) {
    'use strict';
    angular.module('app').directive('directoryList', ['$modal', function ($modal) {
        return {
            restrict: 'EA',
            scope: {
                onFolderSelect: '&'
            },
            templateUrl: 'widgets/directoryList/directoryList.html',
            controller: 'directoryListCtrl'
        };
    }]);

    angular.module('app').controller('directoryListCtrl', [
        '$timeout',
        '$scope',
        't32SystemService',
        function (
            $timeout,
            $scope,
            t32SystemService) {

            function init() {
                $scope.ext = "\\";

                var requestType = 'getImageDirectoryList';
                t32SystemService.submitSystemRequest(requestType, null)
                .then(function (result) {
                    console.log('directory list result=' + result);
                    $scope.directoryList = result;
                });

                var requestType1 = 'getOsInfo';

                t32SystemService.submitSystemRequest(requestType1, null)
                .then(function(os){
                    console.log('the os is ', os);
                    $scope.ext = os == 'win32' ? "\\" : "/";
                })

                var requestType2 = 'getRootPath'
                t32SystemService.submitSystemRequest(requestType2, null)
                .then(function (path) {
                    console.log('directory root path', path);
                    $scope.rootPath = path;
                })

            }

            $scope.selectFolder = function(folder){
                $scope.$eval($scope.onFolderSelect({ 'folder': folder }));
            }

            init();
        }
    ]);

})(window, window.angular);   
