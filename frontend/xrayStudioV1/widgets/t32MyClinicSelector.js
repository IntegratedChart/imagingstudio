(function (window, angular, undefined) {
   'use strict';
   angular.module('app').directive('t32MyClinicSelector', [function () {
      return {
         restrict: 'E',
         scope: {
            selectedClinic : '=',
            onSelectClinic : '&'
         },            
         controller: 't32MyClinicSelectorCtrl',
         templateUrl: 'widgets/t32MyClinicSelector.html'
      };
   }]);   

   angular.module('app').controller('t32MyClinicSelectorCtrl', [
            '$scope', 
            '$location',
            't32SystemService',
            '$timeout',
      function (
            $scope,
            $location,
            t32SystemService,
            $timeout ) 
      {
         function init()
         {
            $scope.authenticated = t32SystemService.isAuthenticated();

            $scope.$on('Authentication', function(evt, result) {
               $scope.authenticated = t32SystemService.isAuthenticated();
               $scope.reload();
            });                

            $scope.reload();
         }

         $scope.reload = function() {
            $scope.clinicList = null;
            if ($scope.authenticated ){
            //    var requestType = 'getMyClinicList';

            //    t32SystemService.submitSystemRequest(requestType, null)
            //    .then(function(clinicList){
            //       console.log('clinicList=' + clinicList );
            //       $scope.clinicList = clinicList;
            //       $scope.clinicList = _.sortBy(clinicList, function(clinic){
            //          return clinic.shortName || clinic.name;
            //       });

            //       if ( $scope.clinicList && $scope.clinicList.length > 0)
            //       {
            //          $scope.selectedClinic = $scope.clinicList[0];
            //          t32SystemService.setSelectedClinic( $scope.selectedClinic );
            //       }
            //    });               
               var requestType = 'getXrayStudioLicencedClinic';

               t32SystemService.submitSystemRequest(requestType, null)
               .then(function(clinic){
                  $scope.clinicList = [];
                  $scope.clinicList.push(clinic);
                  console.log('clinicList=' + $scope.clinicList );
                  // $scope.clinicList = _.sortBy(clinicList, function(clinic){
                  //    return clinic.shortName || clinic.name;
                  // });

                  if ( $scope.clinicList && $scope.clinicList.length > 0){
                     $scope.selectedClinic = $scope.clinicList[0];
                     t32SystemService.setSelectedClinic( $scope.selectedClinic );
                  }
               });               
            }
         }

         $scope.changeClinic = function() {
            $timeout( function() {
               $scope.$eval($scope.onSelectClinic({selectedClinic : $scope.selectedClinic}));
               t32SystemService.setSelectedClinic( $scope.selectedClinic );
            }, 100 ); 
         }
         init();
      }
   ]);
})(window, window.angular);