(function (window, angular, undefined) { 'use strict';
   angular.module('app').controller('t32FolderViewerCtrl', [ 
            '$q',
            't32SystemService',
            't32PatientSelectionService',
            't32WorkflowService',
            '$modalInstance',
            'patientContext',
            'SweetAlert',
            'logger',
            '$location',
            '$scope',
      function (
            $q,
            t32SystemService,
            t32PatientSelectionService,
            t32WorkflowService,
            $modalInstance,
            patientContext,
            SweetAlert,
            logger,
            $location,
            $scope ) {

         function init()
         {
            $scope.theModalInstance = $modalInstance;
            $scope.selectedPatient = patientContext.selectedPatient;
            $scope.selectedFolder = patientContext.selectedFolder;
            $scope.setting = null;

            t32SystemService.getXrayStudioSetting() 
            .then(function(setting){
               $scope.setting = setting;

               $scope.$watch('selectedFolder', function(newVal, oldVal ) {
                  loadFolderImages();
               });

               // when receive backend notification.
               $scope.$on('DirectoryUpdated', function(evt, result) {
                  loadFolderImages();
               });                
            })
            loadFolderImages();

            $scope.patientInfoString = $scope.getPatientInfoByFolder();
         }  

         $scope.getPatientInfoByFolder = function() {
            var result = '';
            if ( $scope.selectedFolder )
            {
               result = $scope.selectedFolder.replace(/-/g, ' ');
            }
            return result;
         }

         function loadFolderImages() {
            if ( $scope.selectedFolder )
            {
               var requestType = 'getDirectoryFileList';

               t32SystemService.submitSystemRequest(requestType, $scope.selectedFolder )
               .then(function(fileList){
                  // fileList = _.filter(fileList, function(file){
                  //    return file.endsWith('.jpg');
                  // })

                  $scope.photos = _.map(fileList, function(file){
                     var fileName = "/" + $scope.selectedFolder + '/' + file ;
                     return {
                        desc     : file,
                        fileName : fileName
                     };
                  });

                  addRandomQueryStringToPhotoFiles();

                  $scope._Index = $scope._Index || 0;
                  setCurrentImage();
               });
            }
            else
            {
               $scope.photos = [];
               setCurrentImage();
            }
         }

         function setCurrentImage() 
         {
            $scope.currentFileName = null;
            if ( $scope.photos && $scope.photos.length )
            {
               $scope.currentFileName = $scope.photos[$scope._Index].fileName;
            }
         }

         function addRandomQueryStringToPhotoFiles() {
            $scope.photos = _.each($scope.photos, function(photo){
               photo.src = "http://localhost:" + $scope.setting.localTcpPort + photo.fileName + '?r=' + Math.round(Math.random() * 999999);
            });
         }

         $scope.onModifyImage = function(imageData) {
            var thePhoto = $scope.photos[$scope._Index];
            if ( thePhoto )
            {
               var fileName = thePhoto.fileName;
               var requestType = 'saveImage';
               var requestMsg = {
                  fileName : thePhoto.fileName,
                  imageData : imageData
               };

               t32SystemService.submitSystemRequest(requestType, requestMsg)
               .then(function(result){
                  addRandomQueryStringToPhotoFiles();
               });
            }
         }


         $scope.isActive = function (index) {
            return $scope._Index === index;
         };

         $scope.showPrev = function () {
            $scope._Index = ($scope._Index > 0) ? --$scope._Index : $scope.photos.length - 1;
            setCurrentImage();            
         };

         $scope.showNext = function () {
            $scope._Index = ($scope._Index < $scope.photos.length - 1) ? ++$scope._Index : 0;
            setCurrentImage();            
         };

         $scope.showPhoto = function (index) {
            $scope._Index = index;
            setCurrentImage();            
         };

         $scope.onDeleteAllImages = function() {
            deleteXray( true, $scope.selectedFolder )
            .then(function() {
               loadFolderImages();
            })
         };

         $scope.onDeleteActiveImage = function() {
            var thePhoto = $scope.photos[$scope._Index];
            if ( thePhoto )
            {
               deleteXray( false, thePhoto.fileName )
               .then(function() {
                  loadFolderImages();
               })               
            }
         };

         function deleteXray(isDirectory, filePath)
         {
            var requestType = 'deleteXrays';
            var requestObject = { 
               isDirectory : isDirectory,
               filePath : filePath
            };
            return t32SystemService.submitSystemRequest(requestType, requestObject )
         }

         $scope.onUpload = function() {
            ensurePatient()
            .then(function(patient){
               return confirmXrayUpload(patient);
            })
            .then(function(patient){
               $scope.doUpload(patient);
            });
         };

         function ensurePatient() {
            var deferred = $q.defer();
            if ( $scope.selectedPatient )
            {
               deferred.resolve($scope.selectedPatient);
            }
            else
            {
               var fromEhrOnly = true;
               t32PatientSelectionService.selectPatient( fromEhrOnly )
               .then(function(patientContext){
                  deferred.resolve(patientContext.selectedPatient);
               })
            }
            return deferred.promise;
         }

         $scope.doUpload = function(patient) {
            var requestType = 'uploadPatientXrays';
            var requestObject = { 
               clinic_id   : patient.clinic_id,
               patient_id  : patient._id,
               folder      : $scope.selectedFolder,
               imageTakenDate : new Date()
            };

            t32SystemService.submitSystemRequest(requestType, requestObject )
            .then(function(result){
               $scope.theModalInstance.close('done');

               var swalOpts = {
                  title: "File Uploaded",
                  text: "Xrays are uploaded into tab32 EHR app.",
                  type: "success",
                  showCancelButton: false,
                  confirmButtonColor: "#d9534f",
                  confirmButtonText: "OK",
                  cancelButtonText: "Cancel",
                  closeOnConfirm: true,
                  allowOutsideClick: true,
                  closeOnCancel: true
               };
               SweetAlert.swal(
                  swalOpts,
                  function(isConfirm){
                  }
               );
            })
            .catch(function(err){
               console.error(err);
               logger.warn(err);
            })
         };

         function confirmXrayUpload(patient) {
            var deferred = $q.defer();
            var swalOpts = {
               title: "For " + patient.name,
               text: "Upload images into EHR.",
               type: "info",
               showCancelButton: true,
               confirmButtonColor: "#d9534f",
               confirmButtonText: "OK",
               cancelButtonText: "Cancel",
               closeOnConfirm: true,
               allowOutsideClick: true,
               closeOnCancel: true
            };
            SweetAlert.swal(
               swalOpts,
               function(isConfirm){
                  if ( isConfirm )
                  {
                     deferred.resolve(patient);
                  }
               }
            );
            return deferred.promise;            
         }

         $scope.onNewXray = function() {
            t32WorkflowService.simulationModePrompt()
            .then(function(){
               var requestType = 'envokeXrayDriver';
               var myCommandString = $scope.setting.twainDriverPath || '';
               var targetDir = $scope.setting.tab32Root + '/' + $scope.selectedFolder;

               var args = [targetDir];

               var requestObject = {
                  command : myCommandString,
                  args    : args
               }

               t32SystemService.submitSystemRequest(requestType, requestObject);                                               
            });
         };


         init();
      }
   ]);
})(window, window.angular);   