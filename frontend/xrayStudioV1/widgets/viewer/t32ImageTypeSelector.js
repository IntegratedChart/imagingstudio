(function (window, angular, undefined) { 'use strict';
    angular.module('app').directive('t32ImageTypeSelector', [function () {
        return {
            restrict: 'E',
            scope: {
                folder             : '=',
                fileName           : '='
            },
            controller: 't32ImageTypeSelectorCtrl',
            templateUrl: 'widgets/viewer/t32ImageTypeSelector.html'
        };
    }]);   

    angular.module('app').controller('t32ImageTypeSelectorCtrl', [ 
            '$q',
            't32SystemService',
            'SweetAlert',
            '$filter',
            '$scope',
        function (
            $q,
            t32SystemService,
            SweetAlert,
            $filter,
            $scope ) {

            function init()
            {
                $scope.$watch('folder', function(newVal, oldVal ) {
                    loadImageType();
                });

                $scope.$watch('fileName', function(newVal, oldVal ) {
                    loadImageType();
                });

                $scope.types = [
                    { key : 'xray',   label : 'xray'},
                    { key : '1-creditApproval', label : 'Credit Approval'},
                    { key : '2-financial', label : 'Financial'},
                    { key : '3-insurance', label : 'Insurance'},
                    { key : '4-medicalHistory', label : 'Medical History'},
                    { key : '5-labslip', label : 'Lab Slip'},
                    { key : '6-extractionConsents', label : 'Extraction Consents'},
                    { key : '7-patientInfo', label : 'Patient Information'},
                    { key : '8-treatmentPlans', label : 'Treatment Plans'},
                    { key : '9-referrals', label : 'Referrals'},
                    { key : 'a-letters', label : 'Letters'},
                    { key : 'b-patientTreatment', label : 'Patient Treatment'},
                    { key : 'g-patientPictures', label : 'Patient Pictures'},
                    { key : 'h-statements', label : 'Statements'},
                    { key : 'i-chart', label : 'Tooth Charts'},
                    { key : 'j-other', label : 'Other'},
                ];
            }  

            function loadImageType() {
                $scope.selectedType = null;
                if ( $scope.folder && $scope.fileName )
                {
                    $scope.selectedType = getFileTypeByName($scope.fileName);
                }
            }

            /**
             * we will code the type into the file name
             * 
             * by default, files are for xray images.  For non-xray images
             * file name will be xxx_type_fileType_type.jpg.
             *
             */
            function getFileTypeByName(fileName)
            {
                var nameWithNoExt = fileName.substring(0, fileName.lastIndexOf('.'));
                var tokens = nameWithNoExt.split('_type_');
                var type = tokens.length > 1 ? tokens[1] : 'xray';
                return type;
            }

            $scope.onChangeType = function() {
                if ( $scope.selectedType != 'xray' )
                {
                    var msg = 'Upon upload, non-xray image will be stored in FILE UPLOAD section';

                    var swalOpts = {
                        title: 'Non Xray Image',
                        text: msg,
                        type: "info",
                        showCancelButton: false,
                        confirmButtonColor: "#d9534f",
                        confirmButtonText: 'OK',
                        cancelButtonText: 'Cancel',
                        closeOnConfirm: true,
                        allowOutsideClick: true,
                        closeOnCancel: true
                    };
                    SweetAlert.swal( swalOpts );
                }

                var requestType = 'changeFileType';
                var params = {
                    fileName : $scope.fileName,
                    fileType : $scope.selectedType
                };

                t32SystemService.submitSystemRequest(requestType, params )
                .then(function(){ 
                });                        
            }

            init();
        }
    ]);
})(window, window.angular);   
