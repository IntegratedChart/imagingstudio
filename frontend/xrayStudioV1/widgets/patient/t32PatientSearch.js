(function (window, angular, undefined) { 'use strict';
    angular.module('app').directive('t32PatientSearch', [function () {
        return {
            restrict: 'E',
            scope: {
                clinic             : '=',
                onSelectPatient    : '&'
            },
            controller: 't32PatientSearchCtrl',
            templateUrl: 'widgets/patient/t32PatientSearch.html'
        };
    }]);   

    angular.module('app').controller('t32PatientSearchCtrl', [ 
            '$q',
            't32SystemService',
            '$filter',
            '$scope',
        function (
            $q,
            t32SystemService,
            $filter,
            $scope ) {

            function init()
            {
                $scope.patients = null;
                $scope.dummyMoreRecordName = 'FOR MORE PATIENTS, CLICK HERE FOR ADVANCED SEARCH';

                $scope.$watch('clinic._id', function(newVal, oldVal ) {
                    if ( newVal )
                    {
                    }
                });

                $scope.$watch('searchInput', function(){
                    $scope.getMatchPatients($scope.searchInput)
                    .then(function(patients){
                        $scope.patients = patients;
                    })
                })
            }  


            $scope.getMatchPatients = function( searchInput )
            {
                var deferred = $q.defer();
                var result = [];
                if ( $scope.clinic && searchInput !== undefined && searchInput !== null && searchInput.trim().length > 0 )
                {

                    var requestType = 'patientSearch';
                    var searchObject = {
                        clinic_id : $scope.clinic._id,
                        searchInput : searchInput
                    }

                    t32SystemService.submitSystemRequest(requestType, searchObject)
                    .then(function(patients){
                        if ( patients && patients.length > 10)
                        {
                            patients[10] = { 'name' : $scope.dummyMoreRecordName };
                        }

                        deferred.resolve(patients);

                    });
                }
                else
                {
                    deferred.resolve([]);
                    // return [];
                }
                return deferred.promise;
            };

            $scope.displayPatient = function (patient)
            {
                // console.log("patientSearchWidget, displayPatient=")
                // console.log(patient)
                var title = "";
                if ( patient !== undefined && patient != null )
                {
                    if ( patient.name != $scope.dummyMoreRecordName ) 
                    {
                        title = $filter('t32Label')(patient.name, true);

                        var dob = $filter('t32Date')(patient.dob, 'MM/dd/yy');  

                        if ( dob )
                        {
                            title += ', ' + dob;
                        }

                        if ( patient.address )
                        {
                            var phone = $filter('t32Tel')(patient.address.phone);
                            if ( phone )
                            {
                                title += ', ' + phone;
                            }
                        }

                        if ( patient.clinicData && patient.clinicData.chartId )
                        {
                            title += ', Chart Id:' + patient.clinicData.chartId;
                        }

                        if ( patient.clinicData && patient.clinicData.foreignId )
                        {
                            title += ', Foreign Id:' + patient.clinicData.foreignId;
                        }

                        var email = $filter('t32Email')(patient.email);
                        if ( email )  
                        {
                            title += ', ' + email;
                        }

                    }
                    else
                    {
                        title = '<b>' + patient.name + '</b>';
                    }
                }
                return title;
            };


            $scope.onSelect = function(patient)
            {
                if ( patient )
                {
                    if ( patient.name != $scope.dummyMoreRecordName )
                    {
                        doSelectPatient(patient);
                    }
                    else
                    {
                        $scope.advancedSearch();
                    }
                }
            };

            function doSelectPatient( patient )
            {
                var requestType = 'getPatientById';
                var searchObject = {
                    clinic_id : $scope.clinic._id,
                    patient_id : patient._id
                }

                t32SystemService.submitSystemRequest(requestType, searchObject)
                .then(function(patient){
                    $scope.searchInput = null;
                    $scope.patients = null;
                    $scope.$eval($scope.onSelectPatient({patient : patient}));
                });
            }

            init();
        }
    ]);
})(window, window.angular);   
