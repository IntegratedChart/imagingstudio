(function (window, angular, undefined) { 'use strict';
   angular.module('app').directive('t32PatientProfile', [function () {
      return {
         restrict: 'E',
         scope: {
            clinic             : '=',
            patient            : '='
         },
         controller: 't32PatientProfileCtrl',
         templateUrl: 'widgets/patient/t32PatientProfile.html'
      };
   }]);   

   angular.module('app').controller('t32PatientProfileCtrl', [ 
            '$q',
            't32SystemService',
            '$filter',
            '$scope',
      function (
            $q,
            t32SystemService,
            $filter,
            $scope ) {

         function init()
         {
         }  

         init();
      }
   ]);
})(window, window.angular);   
