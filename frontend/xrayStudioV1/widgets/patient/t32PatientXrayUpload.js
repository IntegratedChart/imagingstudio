(function (window, angular, undefined) { 'use strict';
    angular.module('app').directive('t32PatientXrayUpload', [function () {
        return {
            restrict: 'E',
            scope: {
                clinic             : '=',
                patient            : '='
            },
            controller: 't32PatientXrayUploadCtrl',
            templateUrl: 'widgets/patient/t32PatientXrayUpload.html'
        };
    }]);   

    angular.module('app').controller('t32PatientXrayUploadCtrl', [ 
            '$q',
            't32SystemService',
            '$filter',
            '$scope',
        function (
            $q,
            t32SystemService,
            $filter,
            $scope ) {

            function init()
            {
            }  

            init();
        }
    ]);
})(window, window.angular);   
