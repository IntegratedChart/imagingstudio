(function (window, angular, undefined) { 'use strict';
    angular.module('app').directive('t32EditPatientTrigger', [
            'SweetAlert', 
            '$modal',
        function (
            SweetAlert,
            $modal ) {
        return {
            restrict: 'A',
            scope : {
                patient       :  '=',
                afterEdit     :  '&',
            },
            link: function (scope, element, attr) {
                element.bind('click',function () {
                    var modalInstance = $modal.open({
                      templateUrl: 'widgets/patient/t32PatientEdit.html',
                      controller: 't32PatientEditCtrl',
                      size: 'lg',
                      backdrop : 'static',
                      resolve : {
                        patient : function() {
                            return scope.patient;
                        },
                        readOnly : function() {
                            return false;
                        },
                        familyMember: function(){
                            return null;
                        }
                      }
                    });

                    modalInstance.result.then(function (result) {
                        if ( result )
                        {
                            scope.$eval(scope.afterEdit({'updatedPatient' : result}));
                        }
                    }, function () {
                    });
                });
            }
        };
    }]);   
})(window, window.angular);   
