(function (window, angular, undefined) { 'use strict';
    angular.module('app').directive('t32NewPatientIcon', [function () {
        return {
            restrict: 'E',
            scope: {
               clinic    : '=',
               afterCreate : '&', // callback function callback(newPatient)  
            },
            controller: 't32NewPatientIconCtrl',
            templateUrl: 'widgets/patient/t32NewPatientIcon.html'
        };
    }]);  

    angular.module('app').controller('t32NewPatientIconCtrl', [ 
            '$modal',
            '$route',
            '$scope', 
        function (
            $modal,
            $route,
            $scope ) {

            $scope.onCreate = function()
            {
                var modalInstance = $modal.open({
                  templateUrl: 'widgets/patient/t32PatientEdit.html',
                  controller: 't32PatientEditCtrl',
                  size: 'lg',
                  backdrop : 'static',
                  resolve : {
                    patient : function() {
                        return null;
                    },
                    readOnly : function() {
                        return false;
                    },
                    familyMember: function(){
                        return null;
                    }
                  }
                });

                modalInstance.result.then(function (newPatient) {
                    if ( newPatient )
                    {
                        $scope.$eval($scope.afterCreate({'newPatient' : newPatient}));
                    }
                }, function () {
                });
            };
        }
    ]);

})(window, window.angular);   