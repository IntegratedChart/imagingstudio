(function (window, angular, undefined) { 'use strict';
    angular.module('app').controller('t32PatientXrayRecordDateCtrl', [
            '$scope', 
            'patient',
            'MomentService',
            '$modalInstance',
            'SweetAlert',
            'logger',
        function (
            $scope, 
            patient,
            MomentService,
            $modalInstance,
            SweetAlert,
            logger) 
        {
            $scope.theModalInstance = $modalInstance;
            $scope.patient = patient;

            $scope.selectedDate = moment().startOf('day').format();


            $scope.onSubmit = function(){
                logger.debug($scope.selectedDate);
                if(!Date.parse($scope.selectedDate)){
                    var swalOpts = {
                        title: "Invalid Record Date",
                        text: "Please select a valid record date.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#d9534f",
                        confirmButtonText: "OK",
                        cancelButtonText: "Cancel",
                        closeOnConfirm: true,
                        allowOutsideClick: true,
                        closeOnCancel: true
                    };
                    SweetAlert.swal(
                        swalOpts,
                        function(isConfirm){
                        }
                    );
                }
                else{
                    $modalInstance.close({
                        patient : $scope.patient,
                        recordDate : $scope.selectedDate
                    })
                }
            }

            $scope.titleImg = "img/calendar.svg";
            $scope.titleStyle = {
                "margin-left": "-15px",
                "margin-right": "-15px"
            };
            $scope.titleImgStyle = {
                "width": "100px",
                "position": "absolute",
                "top": "5px",
                "left": "25px"
            }
        }
    ]);
})(window, window.angular);
