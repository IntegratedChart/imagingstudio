(function (window, angular, undefined) { 'use strict';
    angular.module('app').controller('t32PatientEditCtrl', [ 
                '$rootScope', 
                '$route',
                '$scope', 
                '$q',
                'patient', 
                'readOnly',
                '$filter', 
                't32SystemService',
                // 'SharedDataService', 
                '$modalInstance', 
                // 'toaster',
                // 't32PatientServices', 
                // 'ClinicApiService', 
                'SweetAlert',
                'familyMember',
        function (
                $rootScope, 
                $route,
                $scope, 
                $q,
                patient, 
                readOnly,
                $filter, 
                t32SystemService,
                // SharedDataService, 
                $modalInstance, 
                // toaster, 
                // t32PatientServices,
                // ClinicApiService, 
                SweetAlert,
                familyMember){

            function init() {
                console.log("Entering PatientEditCtrl" );
                $scope.readOnly = readOnly;
                $scope.theModalInstance = $modalInstance;
                $scope.applyToAllMembers = false;
                $scope.showUpdateFamilyMembersOption = true;
                $scope.updateFamilyMemberObject = {};
                $scope.apllyToAllOptions = [
                    {type: 'updatedAddress',  label: 'Update Address',    selected: false},
                    {type: 'updatedPhone',    label: 'Update Phone',      selected: false},
                    {type: 'updatedSmsPhone', label: 'Update Cell',       selected: false},
                    {type: 'updatedEmail',    label: 'Update Email',      selected: false},
                ]


                $scope.toggleAll = function() {
                    var toggleStatus = !$scope.applyToAllMembers;
                    _.each($scope.apllyToAllOptions, function(itm){ 
                        itm.selected = toggleStatus; 
                    });
                 }
                 
                $scope.states = ["AL","AK","AZ","AR","CA","CO","CT","DE","DC","FL","GA","HI","ID","IL","IN","IA",
                                "KS","KY","LA","ME","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","MD",
                                "MA","MI","MN","MS","MO","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY"];

                $scope.newPatient = false;
                $scope.patient = patient;
                $scope.clinic = t32SystemService.getSelectedClinic();

                if($scope.patient && patient._id ){
                    // when user does not enter DOB, scheduler is set dob to be 07/01/1776  
                    //  In TP, we need catch that and blank out dob.  use ha
                    var dob = null;
                    if ( $scope.patient.dob )
                    {
                        if ( moment($scope.patient.dob).isBefore('1800-01-01', 'year') )
                        {
                            $scope.patient.dob = "";
                        }
                        else
                        {
                            // $scope.patient.dob = moment($scope.patient.dob).format("YYYY-MM-DD");                
                        }
                    }
                }else{
                    console.log("Creating patient object");

                    if(familyMember){
                        $scope.patient = {};
                        $scope.patient.lname = familyMember.lname;
                        $scope.patient.email = familyMember.email;
                        $scope.patient.emailConsent = familyMember.emailConsent;
                        $scope.patient.address = familyMember.address;
                        $scope.patient.city = familyMember.city;
                        $scope.patient.state = familyMember.state;
                        $scope.patient.smsPhone = familyMember.smsPhone;
                        $scope.patient.smsConsent = familyMember.smsConsent;
                        $scope.patient.guarantor_id = familyMember.guarantor_id;
                    }
                    else{
                        $scope.showUpdateFamilyMembersOption = false;
                    }

                    if ( ! $scope.patient )
                    {
                        $scope.patient = {};
                    }
                    if ( ! $scope.patient.clinicData )
                    {
                        $scope.patient.clinicData = {};
                    }

                    $scope.newPatient = true;
                }
            }

            $scope.savePatient = function(){
                console.log("saving Patient TODO:do dao.upsertdoc here");

                if(!$scope.patient.address){
                    $scope.patient.address = {};
                }
                if(!$scope.patient.address.add_line1){
                    $scope.patient.address.add_line1 = "To be collected";
                }
                if(!$scope.patient.address.city){
                    $scope.patient.address.city = "To be collected";
                }
                if(!$scope.patient.address.state && $scope.clinic.address ){
                    $scope.patient.address.state = $scope.clinic.address.state;
                }
                if(!$scope.patient.address.zipcode){
                    $scope.patient.address.zipcode = 99999;
                }

                if ( !$scope.patient.inactive )
                {
                    $scope.patient.inactiveReason = '';
                }

                console.log("This is the scope apply all " + $scope.applyToAllMembers)
                if($scope.applyToAllMembers){
                    $scope.updateFamilyMemberObject.updateFamilyMembers = true;
                    $scope.updateFamilyMemberObject.updatedAddress = isOptionSelected('updatedAddress');
                    $scope.updateFamilyMemberObject.updatedEmail = isOptionSelected('updatedEmail');
                    $scope.updateFamilyMemberObject.updatedPhone = isOptionSelected('updatedPhone');
                    $scope.updateFamilyMemberObject.updatedSmsPhone = isOptionSelected('updatedSmsPhone');
                }
                
                if($scope.newPatient){
                    console.log("Adding clinic_id");
                    var clinic = t32SystemService.getSelectedClinic();
                    $scope.patient.clinic_id = clinic._id;
                    doSavePatient();
                }else{
                    doSavePatient();
                }
            };

            function isOptionSelected(type){
                console.log("inside of option selected");
                var found = false;
                _.each($scope.apllyToAllOptions, function(option){
                    if(option.type === type){
                        console.log(option.selected);
                        if(option.selected === true){
                            found = true;
                        }
                    }
                });
                return found
            }

            var doSavePatient = function () {

                $scope.patient.fname = $scope.patient.fname ? $scope.patient.fname.trim() : '';
                $scope.patient.mname = $scope.patient.mname ? $scope.patient.mname.trim() : '';
                $scope.patient.lname = $scope.patient.lname ? $scope.patient.lname.trim() : '';
                $scope.patient.name = $scope.patient.fname;
                if($scope.patient.mname){
                    $scope.patient.name += " " + $scope.patient.mname;
                }
                if($scope.patient.lname){
                    $scope.patient.name += " " + $scope.patient.lname;
                }

                $scope.patient.dob = moment($scope.patient.dob).utc().hour(12).format();

                // generate chart id if necessary for new patient logic.
                if($scope.patient.clinicData && $scope.patient.clinicData.chartId){
                    $scope.upsertPatient();
                } else {
                    generateNewChartId()
                    .then(function(newChartId){
                        if ( !$scope.patient.clinicData ) {
                            $scope.patient.clinicData = {};
                        }
                        $scope.patient.clinicData.chartId = newChartId;

                        $scope.upsertPatient();
                    })
                }
            };

            function generateNewChartId() {
                var requestType = 'generateNewChartId';
                var requestObject = { clinic_id : $scope.clinic._id };

                return t32SystemService.submitSystemRequest(requestType, requestObject);
            }

            $scope.upsertPatient = function () {

                var requestType = 'upsertBusinessObject';
                var searchObject = {
                    clinic_id : $scope.clinic._id,
                    schemaName : 'Patient',
                    doc : $scope.patient
                }

                t32SystemService.submitSystemRequest(requestType, searchObject)
                .then(function(patient){
                    $modalInstance.close(patient);
                });


                // $scope.request_wrapper = {
                //     patient: {},
                //     updateFamilyMemberObject: {}
                // };
                // $scope.request_wrapper.patient = $scope.patient;
                // $scope.request_wrapper.updateFamilyMemberObject = $scope.updateFamilyMemberObject;

                // PatientApiService.upsert($scope.request_wrapper,
                //     function (result) {
                //         console.log("on client side controllers.js - savePatient return. result=");
                //         console.log(result);
                //         if(result.status === 0){
                //             toaster.pop("error", "Error Saving Record", result.data + " please refresh the page and try again");
                //         }
                //         else{
                //             result.$promise.then(function (res) {
                //                 console.log(res);
                //                 toaster.pop("success", null, "Patient Record Saved");
                //                 $modalInstance.close(res);
                //             });
                //         }
                //     }
                // );
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

            $scope.titleImg = "img/patient-profile.svg";
            $scope.titleStyle = {
              "margin-left": "-15px",
              "margin-right": "-15px"
            };
            $scope.titleImgStyle = {
                "width": "100px",
                "position": "absolute",
                "top": "5px",
                "left": "35px"
            }

            init();
        }
    ]);
})(window, window.angular);   
