(function (window, angular, undefined) {
    'use strict';
    angular.module('app').directive('t32XrayImageEdit', ['$rootScope', '$timeout', '$modal', '$window', 't32XrayDragService', 't32XrayImageService',
        function ($rootScope, $timeout, $modal, $window, t32XrayDragService, t32XrayImageService) {
            return {
                restrict: 'AE',
                scope: {
                    activeImageCell: '@',
                    xrayImages: '=',
                    appliedImages: '=',
                    overflowImages: '=',
                    currentTemplate: '=',
                    selectedFolder: '=',
                    selectedPatient: '=',
                    clinicId: '=',
                    imageSettings: '=',
                    afterEdit: '&',
                    settings: '='
                },
                link: function (scope, element, attr, $window) {
                    element.bind('click', function (event) {
                        console.log("EVENT CLICKED");
                        console.log(scope.appliedImages);
                        var fileMapByCreation = {}
                        t32XrayImageService.getPhotosInfo(scope.selectedFolder, 'createTime')
                        .then(function(fileMap){
                            console.log("Here is the file map by create time");
                            console.log(fileMap);
                            fileMapByCreation = fileMap;

                            var modalInstance = $modal.open({
                                templateUrl: 'widgets/editor/t32XrayImageEdit.html',
                                controller: 't32XrayImageEditCtrl',
                                windowClass: 'xray-edit-window',
                                backdrop: 'static',
                                resolve: {
                                    activeImageCell: function () {
                                        return scope.activeImageCell;
                                    },
                                    xrayImages: function () {
                                        return scope.xrayImages;
                                    },
                                    appliedImages: function(){
                                        return scope.appliedImages;
                                    },
                                    overflowImages: function(){
                                        return scope.overflowImages
                                    },
                                    currentTemplate: function(){
                                        return scope.currentTemplate;
                                    },
                                    selectedFolder: function () {
                                        return scope.selectedFolder;
                                    },
                                    selectedPatient: function(){
                                        return scope.selectedPatient;
                                    },
                                    clinicId: function(){
                                        return scope.clinicId;
                                    },
                                    imageSettings: function(){
                                        return scope.imageSettings;
                                    },
                                    settings: function(){
                                        return scope.settings;
                                    },
                                    fileMapByCreation: function(){
                                        return fileMapByCreation;
                                    }
                                }
                            });
                            modalInstance.result.then(function (result) {
                                // alert("we have the results");
                                console.log(result)
                                scope.$eval(scope.afterEdit({"result": result}));
                            });
                        })
                        .catch(function(err){
                            console.log("failed to get file info map ", err);
                        })
                    })
                }
            };
        }
    ]);

    /**
     * injected params: xrayImages is a hash that comprizes of all the images 
     */
    angular.module('app').controller('t32XrayImageEditCtrl', [
        '$q',
        '$scope',
        't32SystemService',
        'activeImageCell',
        'xrayImages',
        'appliedImages',
        'overflowImages',
        'currentTemplate',
        'selectedFolder',
        'selectedPatient',
        'clinicId',
        'settings',
        'imageSettings',
        '$modalInstance',
        'logger',
        't32ImageManipulation',
        't32XrayImageService',
        '$timeout',
        't32AlertService',
        '$window',
        'SweetAlert',
        '$modal',
        'fileMapByCreation',
        function(
            $q,
            $scope,
            t32SystemService,
            activeImageCell,
            xrayImages,
            appliedImages,
            overflowImages,
            currentTemplate,
            selectedFolder,
            selectedPatient,
            clinicId,
            settings,
            imageSettings,
            $modalInstance,
            logger,
            t32ImageManipulation,
            t32XrayImageService,
            $timeout,
            t32AlertService,
            $window,
            SweetAlert,
            $modal,
            fileMapByCreation){
                function init(){
                    $scope.activeImageCell = activeImageCell;
                    $scope.xrayImages = xrayImages;
                    // $scope.appliedImages = appliedImages;
                    $scope.overflowImages = overflowImages;
                    $scope.currentTemplate = currentTemplate;
                    $scope.selectedFolder  = selectedFolder;
                    $scope.selectedPatient = selectedPatient;
                    $scope.theModalInstance = $modalInstance;
                    $scope.imageSettings = imageSettings;
                    $scope.settings = settings;
                    $scope.currentXraySensorSetting = {};
                    $scope.xraySensorSettings = [];
                    $scope.deletedXraySensorSettings = [];
                    $scope.fileMapByCreation = fileMapByCreation;

                    //with active image cell we can figure out which image we need to load initially.
                    console.log($scope.settings);
                    
                    console.log("The applied images after map sort ", $scope.appliedImages);

                    $scope.appliedImages = appliedImages.map(function (imgObj) {
                        var id = imgObj.desc.toString();

                        if ($scope.fileMapByCreation[id]) {
                            imgObj.created = $scope.fileMapByCreation[id];
                        }
                        else {
                            imgObj.created = null;
                        }

                        return imgObj;
                    }).sort(function (a, b) {
                        console.log("here is a ", a);
                        console.log("here is b ", b);
                        return new Date(a.created) - new Date(b.created);
                    })

                    var index = _.findIndex($scope.appliedImages, function (image) {
                        console.log(image);
                        var id = t32XrayImageService.parseId(image.desc);
                        return id === $scope.activeImageCell
                    });

                    console.log(index);

                    $scope.currentIndex = index > -1 ? index : 0;

                    console.log('currentIndex ', $scope.currentIndex);
                    console.log('appliedImages ', $scope.appliedImages);
                    $scope.currentImage = $scope.appliedImages[$scope.currentIndex];
                    console.log('currentImage ', $scope.currentImage);

                    if(index <= -1){
                        $scope.activeImageCell = t32XrayImageService.parseId($scope.currentImage.desc);
                    }
                    $scope.activeImageName = parseName($scope.activeImageCell);

                    $scope.currentImageNum = $scope.currentIndex + 1;
                    $scope.totalImageNum = $scope.appliedImages.length;

                    $scope.inImageManipulation = false;
                    $scope.manipulated = false;
                    $scope.flipXCounter = false;
                    $scope.flipYCounter = false;
                    $scope.currentImageSettings = $scope.imageSettings[$scope.currentImage.fileDesc];
                    $scope.os = 'darwin';

                    //load the os info to apply sharpness based on what system we have
                    getOsInfo().then(function(os){
                        $scope.os = os;
                    })
                    .catch(function(err){
                        var errorObj = {
                            type: "error",
                            title: "Fatal Error!",
                            text: "OS information not found",
                            timeout: 5000,
                            showCloseButton: true
                        };

                        t32AlertService.showToaster(errorObj)
                        $modalInstance.close();
                    })
                }

                var appWindow = angular.element($window);

                // appWindow.bind('resize', function () {
                //     // clearCurrentFabricImage();
                //     setupCanvasDimensions();
                //     setupFabricImage();
                // });

                var fCanvas = '';
                var fImage;
                var xrayEditContainer ;
                var xrayContainerElem;
                var sidebarContainerElem;
                var xrayContainerHeight;
                var xrayContainerWidth;
                
                angular.element(document).ready(function(){
                    // var context = canvas.getContext('2d');
                    xrayEditContainer = angular.element(document.querySelector('#xray-canvas-container'));
                    xrayContainerElem = angular.element(document.querySelector('#xray-image-container'));
                    
                    sidebarContainerElem = angular.element(document.querySelector('#sidebar'));

                    // xrayContainerHeight = 0.8 * $window.innerHeight - 40;
                    // xrayContainerWidth = parseInt($window.innerWidth) - parseInt(sidebarContainerElem.innerWidth()) - 110;
                    xrayContainerHeight = xrayEditContainer.innerHeight() - 40;
                    xrayContainerWidth = xrayEditContainer.innerWidth() - 40;
                    // if ($window.innerWidth < 768) {
                    //     console.log(xrayEditContainer.innerHeight())
                    //     console.log(xrayEditContainer.innerWidth())
                    //     xrayContainerHeight = xrayEditContainer.innerHeight() - 40;
                    //     xrayContainerWidth = xrayEditContainer.innerWidth() - 40;
                    // }
                    
                    fCanvas = new fabric.StaticCanvas('canvas');
                    fCanvas.setHeight(xrayContainerHeight);
                    fCanvas.setWidth(xrayContainerWidth);

                    setupFabricImage();
                })

                function setupCanvasDimensions(){
                    // if ($window.innerWidth < 768) {
                    //     // console.log(xrayEditContainer.innerHeight())
                    //     // console.log(xrayEditContainer.innerWidth())
                    //     xrayContainerHeight = xrayEditContainer.innerHeight() - 40;
                    //     xrayContainerWidth = xrayEditContainer.innerWidth() - 40;
                    // }
                    // else {
                    //     xrayContainerHeight = 0.8 * $window.innerHeight - 40;
                    //     xrayContainerWidth = parseInt($window.innerWidth) - parseInt(sidebarContainerElem.innerWidth()) - 110;
                    // }

                    xrayContainerHeight = xrayEditContainer.innerHeight() - 40;
                    xrayContainerWidth = xrayEditContainer.innerWidth() - 40;

                    if(fCanvas && fCanvas.get("height") > fCanvas.get("width")){
                        // fCanvas.setHeight(xrayContainerHeight);
                        // fCanvas.setWidth(xrayContainerWidth);
                        // console.log("We do not mess with canvas dimensions")
                    }
                    else{
                        fCanvas.setHeight(xrayContainerHeight);
                        fCanvas.setWidth(xrayContainerWidth);
                    }
                }

                /**
                 * setupFabricIamge - get current image info and scale it to fit the parent container, apply filters accordingly.
                 */
                function setupFabricImage(){
                    var renderableHeight;
                    var renderableWidth;
                    var canvasHeight = fCanvas.get("height");
                    var canvasWidth = fCanvas.get("width");
                    var canvasRatio = canvasWidth / canvasHeight;
                    
                    if(fImage != undefined && fImage != null && fImage != "") {
                        
                        var imgRatio = fImage.get("width") / fImage.get("height");
                        // If image's aspect ratio is less than canvas's we fit on height
                        // and place the image centrally along width
                        if (imgRatio < canvasRatio) {
                            renderableHeight = canvasHeight;
                            renderableWidth = fImage.width * (renderableHeight / fImage.height);
                            // xStart = (canvasWidth - renderableWidth) / 2;
                            // yStart = 0;
                        }
                        // If image's aspect ratio is greater than canvas's we fit on width
                        // and place the image centrally along height
                        else if (imgRatio > canvasRatio) {
                            renderableWidth = canvasWidth;
                            renderableHeight = fImage.height * (renderableWidth / fImage.width);
                            // xStart = 0;
                            // yStart = (canvasHeight - renderableHeight) / 2;
                        }
                        // Happy path - keep aspect ratio
                        else {
                            renderableHeight = canvasHeight
                            renderableWidth = canvasWidth
                            // xStart = 0;
                            // yStart = 0;
                        }


                        fImage.scaleToHeight(renderableHeight);
                        fImage.scaleToWidth(renderableWidth);
                        fCanvas.centerObject(fImage);

                        fCanvas.add(fImage).renderAll();
                    }
                    else {
                        $scope.inImageManipulation = true;
                        console.log($scope.currentImage.fileName);
                        console.log($scope.currentImage.originalFileName);

                        ensureOriginalFile($scope.currentImage.fileName, $scope.currentImage.originalFileName)
                        .then(function(result){

                            fabric.Image.fromURL($scope.currentImage.origSrc, function (image) {
                                console.log("THE IMAGE OBJ FROM URL")
                                console.log(image);
                                // var imageHeight = image.height;
                                // var imageWidth = image.width;

                                var imgRatio = image.width / image.height;
                                
                                fImage = image;
                                // If image's aspect ratio is less than canvas's we fit on height
                                // and place the image centrally along width
                                if (imgRatio < canvasRatio) {
                                    renderableHeight = canvasHeight;
                                    renderableWidth = image.width * (renderableHeight / image.height);
                                    // xStart = (canvasWidth - renderableWidth) / 2;
                                    // yStart = 0;
                                }
                                // If image's aspect ratio is greater than canvas's we fit on width
                                // and place the image centrally along height
                                else if (imgRatio > canvasRatio) {
                                    renderableWidth = canvasWidth;
                                    renderableHeight = image.height * (renderableWidth / image.width);
                                    // xStart = 0;
                                    // yStart = (canvasHeight - renderableHeight) / 2;
                                }
                                // Happy path - keep aspect ratio
                                else {
                                    renderableHeight = canvasHeight
                                    renderableWidth = canvasWidth
                                    // xStart = 0;
                                    // yStart = 0;
                                }


                                image.scaleToHeight(renderableHeight);
                                image.scaleToWidth(renderableWidth);

                                fCanvas.centerObject(image);

                                fCanvas.add(image).renderAll();

                                console.log(image);
                                console.log(fImage);

                                if ($scope.currentImageSettings) {
                                    console.log("There are settings here");
                                    console.log($scope.currentImageSettings);
                                    applySettingsToImage()
                                }
                            });
                        })
                        .catch(function(err){
                            console.error(err);

                            $scope.inImageManipulation = false;
                            var title = "Fatal Error"
                            var error = "Something went wrong getting the images, please try again later."

                            var errorObj = {
                                type: "error",
                                title: title,
                                text: error,
                                timeout: 5000,
                                showCloseButton: true
                            }

                            t32AlertService.showToaster(errorObj)
                            $modalInstance.close(); 
                        })
                    }
                }

                function getOsInfo(){
                    var deferred = $q.defer();

                    var requestObject = {};

                    t32SystemService.submitSystemRequest('getOsInfo', requestObject)
                        .then(function (platform) {
                            deferred.resolve(platform);
                        })
                        .catch(function (err) {
                            deferred.reject(err);
                        });

                    return deferred.promise;
                }

                function ensureOriginalFile(fileName, originalFileName){
                    var deferred = $q.defer();

                    var requestObject = {
                        fileName: fileName,
                        originalFileName: originalFileName
                    }

                    t32SystemService.submitSystemRequest('ensureOriginalFile', requestObject)
                    // .then(function(){
                    //     return refresh($scope.selectedFolder, false)
                    // })
                    .then(function(){
                        $scope.inImageManipulation = false;
                        deferred.resolve();
                    })
                    .catch(function(err){
                        deferred.reject(err);
                    });
                    
                    return deferred.promise;
                }


                function applySettingsToImage(){
                    var flipX = $scope.currentImageSettings.flipX;
                    var flipY = $scope.currentImageSettings.flipY;
                    var angle = $scope.currentImageSettings.angle;
                    
                    fImage.set({
                        angle: $scope.currentImageSettings.angle,
                        flipX: $scope.currentImageSettings.flipX,
                        flipY: $scope.currentImageSettings.flipY
                    });

                    fCanvas.centerObject(fImage);

                    var scaleX = fImage.get("scaleX");
                    var scaleY = fImage.get("scaleY");
                    var imageHeight = fImage.get("height");
                    var imageWidth = fImage.get("width");
                    var imageHeightToSacle = imageHeight * scaleY;
                    var imageWidthToScale = imageWidth * scaleX;

                    console.log(scaleX);
                    console.log(scaleY);
                    
                    if(angle == 90  || angle == 270 || angle == -90 || angle == -270){
                        fCanvas.setHeight(imageWidthToScale);
                        fCanvas.setWidth(imageHeightToSacle);
                    }

                    fCanvas.centerObject(fImage);
                    fCanvas.renderAll();

                    applyImageFilters($scope.currentImageSettings, fImage, fCanvas);
                }


                function applyImageFilters(data, image, canvas){
                    image.filters = [];

                    var brightness = new fabric.Image.filters.Brightness({
                        brightness: parseInt(data.brightness) / 100
                    });

                    image.filters.push(brightness);

                    var contrast = new fabric.Image.filters.Contrast({
                        contrast: parseInt(data.contrast) / 100
                    });

                    image.filters.push(contrast);


                    if (data.sharpen) {                        
                        var sharpen = new fabric.Image.filters.Convolute({
                            matrix: [0, -2, 0, -1, 5, -1, 0, 0, 0]
                        })

                        if ($scope.os == 'win32') {
                            sharpen = new fabric.Image.filters.Convolute({
                                matrix: [0, -1, 0, -1, 5, -1, 0, -1, 0]
                            })
                        }

                        image.filters.push(sharpen);
                    }

                    image.applyFilters();

                    canvas.renderAll();
                }

                function clearCurrentFabricImage(){
                    fCanvas.clear();
                    fCanvas.set({
                        height: 0,
                        width: 0
                    });
                    fImage = null;
                    $scope.manipulated = false;
                }

                $scope.rotateImage = function(deg){
                    var currentAngle = fImage.get('angle') + deg;
                    var degree = currentAngle % 360;

                    fImage.set({angle: degree});

                    var imageHeightToSacle = fImage.get("height") * fImage.get("scaleY");
                    var imageWidthToScale = fImage.get("width") * fImage.get("scaleX");

                    if (degree == 90 || degree == 270 || degree == -90 || degree == -270) {
                        fCanvas.setHeight(imageWidthToScale);
                        fCanvas.setWidth(imageHeightToSacle);
                    }
                    else{
                        fCanvas.setWidth(imageWidthToScale);
                        fCanvas.setHeight(imageHeightToSacle);
                    }

                    fCanvas.centerObject(fImage);
                    fCanvas.renderAll();

                    $scope.currentImageSettings.angle = degree;

                    $scope.manipulated = true;
                    console.log(fImage);
                }

                $scope.flipX = function(){
                    
                    var currentAngle = fImage.get('angle');
                    if (currentAngle == 90 || currentAngle == 270 || currentAngle == -90 || currentAngle == -270) {
                        $scope.flipYCounter = !$scope.flipYCounter;
                        fImage.set({ flipY: $scope.flipYCounter });

                        fCanvas.centerObject(fImage);
                        fCanvas.renderAll();

                        $scope.currentImageSettings.flipY = $scope.flipYCounter;
                    }
                    else {
                        $scope.flipXCounter = !$scope.flipXCounter;
                        fImage.set({ flipX: $scope.flipXCounter });
                        
                        fCanvas.centerObject(fImage);
                        fCanvas.renderAll();

                        $scope.currentImageSettings.flipX = $scope.flipXCounter;
                    }

                    $scope.manipulated = true;
                    console.log(fImage);
                }

                $scope.flipY = function(){

                    var currentAngle = fImage.get('angle');
                    if (currentAngle == 90 || currentAngle == 270 || currentAngle == -90 || currentAngle == -270) {
                        $scope.flipXCounter = !$scope.flipXCounter;
                        fImage.set({ flipX: $scope.flipXCounter });

                        fCanvas.centerObject(fImage);
                        fCanvas.renderAll();

                        $scope.currentImageSettings.flipX= $scope.flipXCounter;
                    }
                    else {
                        $scope.flipYCounter = !$scope.flipYCounter;
                            fImage.set({ flipY: $scope.flipYCounter });

                        fCanvas.centerObject(fImage);
                        fCanvas.renderAll();

                        $scope.currentImageSettings.flipY = $scope.flipYCounter;
                    }

                    $scope.manipulated = true;
                    console.log(fImage);
                }

                $scope.$on('manipulateImage', function(evt, data){
                    applyImageFilters(data, fImage, fCanvas);

                    console.log($scope.currentImageSettings);
                    $scope.currentImageSettings.brightness = data.brightness;
                    $scope.currentImageSettings.contrast = data.contrast;
                    $scope.currentImageSettings.sharpen = data.sharpen;

                    $scope.manipulated = true;
                });

                $scope.$on('zoomIn', function(evt, data){
                    var zoom = data.zoom;
                    zoomIn(zoom);
                })

                function zoomIn(zoom){
                    var zoomPercent = zoom + "%";
                    $('#xray-canvas-container').css({
                        "zoom": zoomPercent
                    })
                }

                $scope.deleteImage = function(file){
                    $scope.inImageManipulation = true;
                    var imageObj = Object.assign({}, file);
                    var imgDesc = imageObj.fileDesc;
                    var imageUrl = imageObj.fileName;
                    var originalImageUrl = imageObj.originalFileName;

                    //we know that the last image is about to be deleted, so we want to close the modal after delete. 
                    if($scope.appliedImages.length <= 1){
                        t32ImageManipulation.delete(false, imageUrl, originalImageUrl)
                        .then(function (result) {
                            console.log("Delete passed");
                            if ($scope.imageSettings[imgDesc]) {
                                try {
                                    delete $scope.imageSettings[imgDesc];
                                }
                                catch (err) {
                                    // alert("delete failed");
                                    throw new Error(err);
                                    console.error(err);
                                }
                                
                                $scope.cancel();
                            }
                            else {
                                $scope.cancel();
                            }
                        })
                        .catch(function (err) {
                            console.error(err);
                            $scope.inImageManipulation = false;
                            $scope.nextImage();
                        })
                    }
                    else{
                        t32ImageManipulation.delete(false, imageUrl, originalImageUrl)
                        .then(function(result){
                            console.log("Delete passed");
                            if($scope.imageSettings[imgDesc]){
                                try{
                                    delete $scope.imageSettings[imgDesc];
                                }
                                catch(err){
                                    // alert("delete failed");
                                    throw new Error(err);
                                    console.error(err);
                                }
                                console.log("The applied image length is: " + $scope.appliedImages);
                                if($scope.appliedImages.length > 0){
                                    return refresh($scope.selectedFolder, true)
                                }
                                else{
                                    $scope.cancel();
                                }
                            }
                            else{
                                return refresh($scope.selectedFolder, true)
                            }
                        })
                        .then(function(result){
                            console.log("REFRESH PASSED")
                            if(result == 'close'){
                                // do nothng, we are closing 
                            }
                            else{
                                clearCurrentFabricImage();
                                setupFabricImage();
                                $scope.inImageManipulation = false;
                                $scope.nextImage();
                            }
                        })
                        .catch(function(err){
                            console.error(err);
                            $scope.inImageManipulation = false;
                            $scope.nextImage();
                        })
                    }
                }


                $scope.saveImage = function(){
                    // we need to send call to backend with image json settings, and have the backend save, after that we can call refresh.
                    // alert(imageFilters);
                    var deferred = $q.defer();

                    // console.log(JSON.stringify(fImage));
                    // console.log(fImage.toObject());
                    // console.log(JSON.stringify(fCanvas));
                    // console.log(fImage.toObject());
                    // console.log(fabric.textureSize);

                    var canvasDimensions = {
                        height: fImage.height,
                        width: fImage.width
                    }

                    var imageObj = fImage.toObject();

                    $scope.inImageManipulation = true;

                    // var newCanvasEl = document.getElementById('newCanvas');

                    var newFabricCanvas = new fabric.StaticCanvas('newCanvas');
                    newFabricCanvas.setHeight(canvasDimensions.height)
                    newFabricCanvas.setWidth(canvasDimensions.width);

                    var newFabricImage;
                    var dataUri;

                    fabric.Image.fromObject(imageObj, function(img){
                        console.log("In here")
                        newFabricImage = img;
                        newFabricCanvas.add(img);

                        console.log(newFabricCanvas);
                        console.log(Object.getPrototypeOf(newFabricCanvas));

                        if (imageObj.hasOwnProperty('angle')) {
                            if (imageObj.angle == 90 || imageObj.angle == 270 || imageObj.angle == -90 || imageObj.angle == -270) {
                                newFabricCanvas.setHeight(imageObj.width);
                                newFabricCanvas.setWidth(imageObj.height);
                            }
                        }

                        img.set({
                            scaleX: 1,
                            scaleY: 1,
                        });

                        newFabricCanvas.centerObject(img);
                        newFabricCanvas.renderAll();

                        applyImageFilters($scope.currentImageSettings, img, newFabricCanvas);

                        dataUri = newFabricCanvas.toDataURL();

                        // console.log(dataUri);

                        var requestObject = {
                            filePath: $scope.currentImage.fileName,
                            // imageObj: fImage.toObject(),
                            // canvasDimensions: canvasDimensions,
                            // imageFilters: $scope.currentImageSettings
                            dataUri: dataUri
                        }

                        t32SystemService.submitSystemRequest('saveManipulatedXrayImage', requestObject)
                        .then(function () {
                            $scope.imageSettings[$scope.currentImage.fileDesc] = $scope.currentImageSettings;

                            return t32SystemService.submitSystemRequest('saveImageSettings', $scope.imageSettings)
                        })
                        .then(function (settings) {
                            $scope.imageSettings = settings;
                            return refresh($scope.selectedFolder, true)
                        })
                        .then(function () {
                            $scope.inImageManipulation = false;
                            $scope.manipulated = false;
                            $scope.currentImageSettings = $scope.imageSettings[$scope.currentImage.fileDesc];
                            deferred.resolve();
                        })
                        .catch(function (err) {
                            var title = "Fatal Error"
                            var error = "There was an error saving the image changes, please try again later. " + err;

                            var errorObj = {
                                type: "error",
                                title: title,
                                text: error,
                                timeout: 5000,
                                showCloseButton: true
                            }
                            
                            t32AlertService.showToaster(errorObj)
                            deferred.reject();
                        })
                    });

                    return deferred.promise;
                }

                function parseName(imageId){
                    var underscore = /_/gi
                    var splitName = imageId.replace("id_", "").replace(underscore, " ").split(" ");
                    var capitalizedStr = "";
                    _.each(splitName, function(word){
                        // alert(word[0]);
                        capitalizedStr += word[0].toUpperCase() + word.substring(1, word.length) + " ";
                    })

                    return capitalizedStr
                }

                // t32SystemService.getXrayStudioSetting()
                // .then(function (setting) {
                //     $scope.settings = setting;
                // })
                // .catch(function (err) {
                //     $modalInstance.close();
                // })

                function next(idx, appliedImages){
                    idx += 1;

                    if(idx < 0){
                        idx = appliedImages.length - 1;
                    }

                    if (idx >= appliedImages.length) {
                        idx = 0;
                    }

                    return idx;
                }

                function prev(idx, appliedImages){
                    idx -= 1;

                    // alert("going back to " +  idx);

                    if (idx < 0) {
                        idx = appliedImages.length - 1;
                    }

                    if (idx >= appliedImages.length) {
                        idx = 0;
                    }

                    return idx;
                }

                $scope.nextImage = function(){
                    if($scope.manipulated){
                        askToSave()
                        .then(function(){

                            $scope.currentIndex = next($scope.currentIndex, $scope.appliedImages);

                            $scope.currentImage = $scope.appliedImages[$scope.currentIndex];

                            $scope.activeImageCell = t32XrayImageService.parseId($scope.currentImage.desc)
                            
                            $scope.activeImageName = parseName($scope.activeImageCell);
                            
                            $scope.currentImageSettings = $scope.imageSettings[$scope.currentImage.fileDesc];
                            
                            $scope.currentImageNum += 1;
                            if ($scope.currentImageNum > $scope.totalImageNum) {
                                $scope.currentImageNum = 1;
                            }

                            clearCurrentFabricImage();
                            
                            setupCanvasDimensions();
                            
                            setupFabricImage();

                            $timeout(function () {
                                $scope.$broadcast("newImageSelected");
                            }, 10); 
                        })
                        .catch(function(err){
                            var title = "Fatal Error"
                            var error = err;

                            var errorObj = {
                                type: 'error',
                                title: title,
                                text: error,
                                timeout: 5000,
                                showCloseButton: true
                            }

                            t32AlertService.showToaster(errorObj)
                            $modalInstance.close();
                        })
                    }
                    else{
                        $scope.currentIndex = next($scope.currentIndex, $scope.appliedImages);

                        $scope.currentImage = $scope.appliedImages[$scope.currentIndex];

                        $scope.activeImageCell = t32XrayImageService.parseId($scope.currentImage.desc)

                        $scope.activeImageName = parseName($scope.activeImageCell);
                        
                        $scope.currentImageSettings = $scope.imageSettings[$scope.currentImage.fileDesc];

                        $scope.currentImageNum += 1;
                        if ($scope.currentImageNum > $scope.totalImageNum) {
                            $scope.currentImageNum = 1;
                        }

                        clearCurrentFabricImage();

                        setupCanvasDimensions();
                        
                        setupFabricImage();

                        $timeout(function () {
                            $scope.$broadcast("newImageSelected");
                        }, 10);                   
                    }
                }

                $scope.previousImage = function(){
                    if ($scope.manipulated) {
                        askToSave()
                        .then(function () {
                            $scope.currentIndex = prev($scope.currentIndex, $scope.appliedImages);

                            $scope.currentImage = $scope.appliedImages[$scope.currentIndex];

                            $scope.activeImageCell = t32XrayImageService.parseId($scope.currentImage.desc)

                            $scope.activeImageName = parseName($scope.activeImageCell);

                            $scope.currentImageSettings = $scope.imageSettings[$scope.currentImage.fileDesc];

                            $scope.currentImageNum -= 1;
                            if ($scope.currentImageNum <= 0) {
                                $scope.currentImageNum = $scope.totalImageNum;
                            }

                            clearCurrentFabricImage();

                            setupCanvasDimensions();
                            
                            setupFabricImage();

                            $timeout(function () {
                                $scope.$broadcast("newImageSelected");
                            }, 10);    
                        })
                        .catch(function (err) {
                            
                        })
                    }
                    else{
                        $scope.currentIndex = prev($scope.currentIndex, $scope.appliedImages);
    
                        $scope.currentImage = $scope.appliedImages[$scope.currentIndex];
    
                        $scope.activeImageCell = t32XrayImageService.parseId($scope.currentImage.desc)
    
                        $scope.activeImageName = parseName($scope.activeImageCell);

                        $scope.currentImageSettings = $scope.imageSettings[$scope.currentImage.fileDesc];

                        $scope.currentImageNum -= 1;
                        if ($scope.currentImageNum <= 0) {
                            $scope.currentImageNum = $scope.totalImageNum;
                        }

                        clearCurrentFabricImage();

                        setupCanvasDimensions();
                        
                        setupFabricImage();
                        
                        $timeout(function () {
                            $scope.$broadcast("newImageSelected");
                        }, 10);                         
                    }
                }

                function askToSave(msg){
                    var deferred = $q.defer();

                    var swalOpts = {
                        title: msg ? msg.title : "Unsaved Changes",
                        text: msg ? msg.text : "Its seems like there are changes made to the current image that have not been saved, would you like to save them?",
                        type: msg ? msg.type : "warning",
                        showCancelButton: msg ? msg.showCancelButton : true,
                        confirmButtonColor: msg ? msg.confirmButtonColor : "#449d44",
                        cancelButtonColor: msg ? msg.cancelButtonColor : "#d9534f",
                        confirmButtonText: msg ? msg.confirmButtonText : "Save",
                        cancelButtonText: msg ? msg.cancelButtonText : "Do Not Save",
                        closeOnConfirm: msg ? msg.closeOnConfirm : true,
                        allowOutsideClick: msg ? msg.allowOutsideClick : false,
                        closeOnCancel: msg ? msg.closeOnCancel : true
                    };
                    SweetAlert.swal(swalOpts, function (isConfirm) {
                        if (isConfirm) {
                            $scope.saveImage()
                            .then(function(){
                                deferred.resolve();
                            })
                            .catch(function(err){
                                deferred.reject(err);
                            })
                        }
                        else{
                            console.log("We did not confirm the save");
                            t32SystemService.submitSystemRequest('getImageSettings')
                            .then(function (settings) {
                                $scope.imageSettings = settings;
                                console.log($scope.imageSettings);
                                deferred.resolve();
                            })
                            .catch(function (err) {
                                logger.debug(err);
                            })
                        }
                    });

                    return deferred.promise;
                }

                function refresh(folder, isDelete){
                    var deferred = $q.defer();
                    console.log($scope.appliedImages.length);
                    $timeout(function(){
                        t32XrayImageService.loadFolderImages(folder)
                        .then(function (result) {
                            console.log("THE PHOTOS LENGTH");
                            console.log(result.photos.length);

                            if(result.photos.length){
                                $scope.photos = result.photos;
                                $scope.xrayImages = result.photosHash;
                                $scope.photos = addRandomQueryStringToPhotoFiles($scope.photos, $scope.settings);

                                //sets the applied images array
                                applyImages($scope.photos);

                                $scope.totalImageNum = $scope.appliedImages.length;
                                $scope.currentImage = $scope.appliedImages[$scope.currentIndex];
                                $scope.activeImageCell = t32XrayImageService.parseId($scope.currentImage.desc)
                                $scope.activeImageName = parseName($scope.activeImageCell);
                                deferred.resolve();
                            }
                            else{
                                $scope.cancel()
                                deferred.resolve('close');
                            }
                        })
                        .catch(function (err) {
                            deferred.reject();
                            logger.error(err);
                        })
                    }, 50)

                    return deferred.promise;
                }

                function addRandomQueryStringToPhotoFiles(photos, setting) {
                    photos = _.each(photos, function (photo) {
                        photo.src = "http://localhost:" + setting.localTcpPort + photo.fileName + '?r=' + Math.round(Math.random() * 999999);

                        photo.origSrc = "http://localhost:" + $scope.settings.localTcpPort + photo.originalFileName + '?r=' + Math.round(Math.random() * 999999);
                    });
                    console.log(photos);
                    return photos;
                }

                function applyImages() {
                    var imgPos = $scope.currentTemplate ? $scope.currentTemplate.imagePositions : null;

                    console.log(imgPos);

                    if (imgPos) {
                        $scope.appliedImages = [];
                        _.forEach(imgPos, function (posId) {
                            if ($scope.selectedFolder && $scope.photos.length) {
                                //place image inside of the proper position
                                addImage(posId);
                            }
                        });
                    }

                    $scope.appliedImages =  $scope.appliedImages.slice().map(function (imgObj) {
                        var id = imgObj.desc.toString();

                        if ($scope.fileMapByCreation[id]) {
                            imgObj.created = $scope.fileMapByCreation[id];
                        }
                        else {
                            imgObj.created = null;
                        }

                        return imgObj;
                    }).sort(function (a, b) {
                        console.log("here is a ", a);
                        console.log("here is b ", b);
                        return new Date(a.created) - new Date(b.created);
                    })

                    console.log($scope.appliedImages);
                }

                /**
                 * addImage - take images out of the photos array and put them into the appliedImages
                 * @param {String} id The image position
                 */
                function addImage(id){
                    var img = null;

                    if($scope.xrayImages[id]){
                        img = $scope.xrayImages[id][0];
                    }

                    if(img){
                        $scope.appliedImages.push(img);

                        var index = _.findIndex($scope.photos, function(obj){
                            return obj.desc === img.desc
                        });

                        $scope.photos.splice(index, 1);
                    }

                    $scope.appliedImages
                }

                $scope.cancel = function(){
                    var currentImageId = $scope.currentImage ? t32XrayImageService.parseId($scope.currentImage.desc) : null;
                    // alert("closing");
                    if($scope.manipulated){
                        askToSave()
                        .then(function(){
                            $modalInstance.close({ currentImageId: currentImageId });
                        })
                        .catch(function(err){
                            var title = "Fatal Error"
                            var error = err;

                            var errorObj = {
                                type: "error",
                                title: title,
                                text: error,
                                timeout: 5000,
                                showCloseButton: true
                            }

                            t32AlertService.showToaster(errorObj)
                            $modalInstance.close({ currentImageId: currentImageId });
                        })
                    }
                    else{
                        $modalInstance.close({ currentImageId: currentImageId });
                    }

                }

                // $scope.$on("imagesAdjusted", function(evt, data){
                //     $scope.imageSettings[$scope.currentImage.desc] = data.currentImageSettings
                //     refresh($scope.selectedFolder, false)
                // });

                $scope.$on("showError", function(evt, message){
                    var title = "Fatal Error";
                    var error = message.msg;

                    var errorObj = {
                        type: "error",
                        title: title,
                        text: error,
                        timeout: 5000,
                        showCloseButton: true
                    }

                    t32AlertService.showToaster(errorObj)
                    $modalInstance.close(result);
                })

                // $scope.$on('saveXraySensorSettings', function (evt, result) {
                //     console.log("save event caught inside of image edit")
                //     if (result) {
                //         $scope.xraySensorSettings = result.xraySensorSettings;
                //         $scope.currentXraySensorSetting = result.currentXraySensorSetting;
                //     }

                //     saveXraySensorSettings();
                // })

                // $scope.$on('deleteXraySensorSetting', function () {
                //     _.find($scope.xraySensorSettings, function (obj, index) {
                //         if (obj.manufacturer == $scope.currentXraySensorSetting.manufacturer &&obj.modelName == $scope.currentXraySensorSetting.modelName && obj.prodName == $scope.currentXraySensorSetting.prodName) {
                //             $scope.xraySensorSettings.splice(index, 1);
                //             $scope.currentXraySensorSetting = $scope.xraySensorSettings[0];
                //             if (obj._id) {
                //                 $scope.deletedXraySensorSettings.push(obj);
                //             }
                //         }
                //     });

                //     saveXraySensorSettings();
                // })

                // function saveXraySensorSettings() {
                //     // console.log("the new rotation setting");
                //     // console.log(t32RotationSettingsService.getSettingFromSettingsObj($scope.currentXraySensorSetting.setting));

                //     $scope.settings.xraySensorSettings = angular.toJson($scope.xraySensorSettings);
                //     $scope.settings.currentXraySensorSetting = angular.toJson($scope.currentXraySensorSetting);
                //     $scope.settings.deletedRotationSettings = angular.toJson($scope.deletedRotationSettings);

                //     console.log($scope.xraySensorSettings);
                //     console.log($scope.currentXraySensorSetting);
                //     console.log($scope.settings);
                //     var requestObject = $scope.settings;

                //     t32SystemService.submitSystemRequest('saveXrayStudioSetting', requestObject)
                //     .then(function () {
                //         console.log("Setting save for sensors")
                //     })
                // }

                // //handle change for dropdown select change for sensor settings
                // $scope.onXraySensorSettingChange = function () {
                //     // alert("We are about to save after selection change");
                //     $timeout(function () {
                //         saveXraySensorSettings();
                //     }, 100);
                // }

                $scope.openMeasureTool = function(){
                    if ($scope.manipulated){
                        var saveMsg = {
                            title: "Unsaved Changes",
                            text: "The measurment tool will not show the current image changes, would you like to save them first?",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#449d44",
                            cancelButtonColor: "#d9534f",
                            confirmButtonText: "Save",
                            cancelButtonText: "Do Not Save",
                            closeOnConfirm: true,
                            allowOutsideClick: false,
                            closeOnCancel: true
                        }
                        askToSave(saveMsg)
                        .then(function(){
                            openMeasureToolModal();
                        })
                    }
                    else{
                        openMeasureToolModal();
                    }
                }

                function openMeasureToolModal (){
                    var modalInstance = $modal.open({
                        templateUrl: 'widgets/editor/t32XrayRuler.html',
                        controller: 't32XrayRulerCtrl',
                        windowClass: 'xray-ruler-modal',
                        backdrop: 'static',
                        resolve: {
                            clinicId: function () {
                                return $scope.clinicId;
                            },
                            currentXraySensorSetting: function () {
                                return $scope.currentXraySensorSetting
                            },
                            xraySensorSettings: function () {
                                return $scope.xraySensorSettings
                            },
                            deletedXraySensorSettings: function () {
                                return $scope.deletedXraySensorSettings
                            },
                            settings: function () {
                                return $scope.settings
                            },
                            imageSource: function () {
                                return $scope.currentImage.src
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                    });
                }

                init();
            }
    ])
})(window, window.angular);