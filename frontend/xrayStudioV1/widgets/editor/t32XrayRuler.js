(function (window, angular, undefined) {
    'use strict';
    angular.module('app').controller('t32XrayRulerCtrl', [
        '$scope',
        '$modalInstance',
        't32XrayRulerService',
        'clinicId',
        'imageSource',
        'currentXraySensorSetting',
        'xraySensorSettings',
        'deletedXraySensorSettings',
        'settings',
        '$timeout',
        '$window',
        't32SystemService',
        't32AlertService',
        function ($scope,
                  $modalInstance,
                  t32XrayRulerService,
                  clinicId,
                  imageSource,
                  currentXraySensorSetting,
                  xraySensorSettings,
                  deletedXraySensorSettings,
                  settings,
                  $timeout,
                  $window,
                  t32SystemService,
                  t32AlertService ) {

            function init() {
                $scope.theModalInstance = $modalInstance;
                $scope.clinicId = clinicId;
                $scope.imageSource = imageSource;
                $scope.showSensorDropdown = true;
                $scope.settings = settings
                $scope.currentXraySensorSetting = currentXraySensorSetting;
                $scope.xraySensorSettings = xraySensorSettings;
                $scope.deletedXraySensorSettings = deletedXraySensorSettings;
                console.log($scope.imageSource);
                $scope.showAngle = true;
                $scope.showDeltas = false;

                $scope.imageWidth = $scope.currentXraySensorSetting ? $scope.currentXraySensorSetting.width : 31.1;

                $scope.imageHeight = $scope.currentXraySensorSetting ? $scope.currentXraySensorSetting.height : 20.2;

                console.log($scope.imageHeight);
                console.log($scope.imageWidth);
                $scope.$watch('imageWidth', function(newVal, oldVal) {
                    if ( newVal != oldVal )
                    {
                        // alert("Image width changed: " + oldVal + ", " + newVal);
                        initializeImage();
                    }
                });
                $scope.$watch('imageHeight', function(newVal, oldVal) {
                    if ( newVal != oldVal )
                    {
                        // alert("Image width changed: " + oldVal + ", " + newVal);
                        initializeImage();
                    }
                });
                
                $scope.showLoupe = false;
                initializeImage();

                // $timeout(function(){
                //     if($scope.xraySensorSettings.length <= 0){
                //         var error = {
                //             type: "warning",
                //             title: "Missing Sensor Information",
                //             text: "The images will not be measured accurately without the sensor information, please add it.",
                //             showCloseButton: true,
                //             timeout: 5000
                //         }
                //         t32AlertService.showToaster(error)
                //     }
                // }, 1000)
            }

            function initializeImage() {
                $timeout( function() {
                    $scope.onImageLoad();
                }, 1000 ); 

            }

            
            $(document).ready(function(){
                var appWindow = angular.element($window);
                var openModal = angular.element(document.getElementsByClassName("modal-open")[0]);
                var modals = openModal.find('.modal');
                var currentModal = modals[modals.length - 1];
                console.log('openModal', openModal);
                console.log(currentModal);

                var isScrolling;
                var isResizing;

                $(currentModal).scroll(function (event) {
                    console.log('scrolling');

                    $timeout.cancel(isScrolling);

                    isScrolling = $timeout(function () {
                        $scope.onClear();
                        $scope.onImageLoad();
                    }, 10)

                    // $scope.onImageLoad();
                    // initializeImage();
                })

                appWindow.bind('resize', function () {
                    // clearCurrentFabricImage();
                    $timeout.cancel(isResizing);

                    isResizing = $timeout(function () {
                        $scope.onClear();
                        $scope.onImageLoad();
                    }, 100)
                    // $scope.onImageLoad();
                    console.log(appWindow);
                });

                $(currentModal).css("overflow-x", "auto");
            })


            

            /**
             * a call back function to pass to t32XrayRulerService 
             *   callback when mouse moves on the image.
             *   This is so we could control the loupe display
             */
            function imageCanvasListener( mouseInFlag ) {
                var delay = mouseInFlag ? 10 : 1000;
                $timeout( function() {
                    $scope.showLoupe = false;
                }, delay ); 
            }

            $scope.onSaveImage = function(imageData) {
                $modalInstance.close(imageData);
            };

            $scope.onImageLoad = function( ) {
                var modalContainerId = 'xray-ruler-modal-container';
                var parentContainerId = 'xray-ruler-container';

                console.log($scope.imageWidth)
                if(!$scope.imageWidth || !$scope.imageHeight){
                    return 
                }
                else{
                    // call server to get the image content.
                    t32XrayRulerService.initialize( imageCanvasListener, $scope.imageSource,  $scope.imageWidth, $scope.imageHeight, modalContainerId, parentContainerId );
                }

            };

            $scope.hasLines = function() {
                return t32XrayRulerService.hasLines();
            };

            $scope.onClear = function() {
                return t32XrayRulerService.clear();
            };

            $scope.onResetAdjustment = function() {
                // reset the adjustment alone might not restore back to
                // the original state due to image loss.
                // let's just reload image.
                initializeImage();
            };

            // $scope.afterDeleteXray = function() {
            //     $scope.theModalInstance.close('delete');
            // };

            $scope.$on('saveXraySensorSettings', function (evt, result) {
                console.log("save event caught inside of ruler")
                if (result) {
                    $scope.xraySensorSettings = result.xraySensorSettings;
                    $scope.currentXraySensorSetting = result.currentXraySensorSetting;
                }

                $scope.imageWidth = $scope.currentXraySensorSetting.width;
                $scope.imageHeight = $scope.currentXraySensorSetting.height;

                saveXraySensorSettings();
            })

            $scope.$on('updateXraySensorSettings', function (evt, result) {
                console.log("save event caught inside of ruler")
                if (result) {
                    // if(result.currentXraySensorSetting.version){
                    //     result.currentXraySensorSetting.version += 1;
                    // }
                    $scope.xraySensorSettings = result.xraySensorSettings;
                    $scope.currentXraySensorSetting = result.currentXraySensorSetting;
                }

                $scope.imageWidth = $scope.currentXraySensorSetting.width;
                $scope.imageHeight = $scope.currentXraySensorSetting.height;

                saveXraySensorSettings();
            })

            $scope.$on('deleteXraySensorSetting', function () {
                _.find($scope.xraySensorSettings, function (obj, index) {
                    if (obj.name == $scope.currentXraySensorSetting.name && obj.manufacturer == $scope.currentXraySensorSetting.manufacturer && obj.modelName == $scope.currentXraySensorSetting.modelName && obj.prodName == $scope.currentXraySensorSetting.prodName) {
                        $scope.xraySensorSettings.splice(index, 1);
                        $scope.currentXraySensorSetting = $scope.xraySensorSettings[0];
                        if (obj._id) {
                            $scope.deletedXraySensorSettings.push(obj);
                        }
                    }
                });

                saveXraySensorSettings();
            })

            function saveXraySensorSettings() {
                // console.log("the new rotation setting");
                // console.log(t32RotationSettingsService.getSettingFromSettingsObj($scope.currentXraySensorSetting.setting));

                $scope.settings.xraySensorSettings = angular.toJson($scope.xraySensorSettings);
                $scope.settings.currentXraySensorSetting = angular.toJson($scope.currentXraySensorSetting);
                // $scope.settings.deletedRotationSettings = angular.toJson($scope.deletedRotationSettings);

                console.log($scope.xraySensorSettings);
                console.log($scope.currentXraySensorSetting);
                console.log($scope.settings);
                // var requestObject = $scope.settings;

                if ($scope.currentXraySensorSetting) {
                    $scope.imageWidth = $scope.currentXraySensorSetting.width;
                    $scope.imageHeight = $scope.currentXraySensorSetting.height;
                    $scope.sensorWarning = false;
                }
                else {
                    $scope.imageWidth = 31.1;
                    $scope.imageHeight = 20.2;
                    $scope.sensorWarning = true;
                    // var error = {
                    //     type: "warning",
                    //     title: "Sensor settings not defined!",
                    //     text: "The images will not be measured accurately without the sensor information, please add it.",
                    //     showCloseButton: true,
                    //     timeout: 5000
                    // }
                    // t32AlertService.showToaster(error)
                }

                console.log($scope.imageHeight);
                console.log($scope.imageWidth)

                // t32SystemService.submitSystemRequest('saveXrayStudioSetting', requestObject)
                t32SystemService.saveXrayStudioSetting(setting)
                .then(function () {
                    console.log("Setting saved for sensors")
                })
            }

            $scope.$on('sensorWarning', function (evt, msg) {
                $scope.sensorWarning = msg.warning;
            })

            //handle change for dropdown select change for sensor settings
            $scope.onXraySensorSettingChange = function () {
                // alert("We are about to save after selection change");
                $timeout(function () {
                    console.log($scope.currentXraySensorSetting);
                    if($scope.currentXraySensorSetting){
                        $scope.imageWidth = $scope.currentXraySensorSetting.width;
                        $scope.imageHeight = $scope.currentXraySensorSetting.height;

                        $scope.sensorWarning = false;
                    }
                    else{
                        $scope.imageWidth = 31.1;
                        $scope.imageHeight = 20.2;

                        $scope.sensorWarning = true;
                    }
                
                    saveXraySensorSettings();
                }, 100);
            }

            $scope.titleImg = "img/pencil.svg";
            $scope.titleStyle = {
                "margin-left": "-30px",
                "margin-right": "-30px"
            };
            $scope.titleImgStyle = {
                "width": "100px",
                "position": "absolute",
                "top": "5px",
                "left": "25px"
            }

            init();
        }
    ]);
})(window, window.angular);