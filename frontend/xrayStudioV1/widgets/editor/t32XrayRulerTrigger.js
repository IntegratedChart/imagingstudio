(function (window, angular, undefined) {
    'use strict';
    angular.module('app').directive('t32XrayRulerTrigger', [
            '$modal', 
            '$sce',
        function (
            $modal,
            $sce ) {
        return {
            restrict: 'A',
            scope: {
                imageSource  : "=",
                onModifyImage    : "&"
            },

            link: function (scope, element, attr) {
                element.bind('click',function () {
                    var modalInstance = $modal.open({
                        templateUrl: 'widgets/editor/t32XrayRuler.html',
                        controller: 't32XrayRulerCtrl',
                        windowClass : 'ninetyPercent-Modal',
                        backdrop: 'static',
                        resolve: {
                            imageSource : function() {
                                return scope.imageSource;
                            },
                            clinicId : function() {
                                return null;
                            },
                            patientId : function() {
                                return null;
                            },
                            hideDeleteButton : function() {
                                return false;
                            },

                        }
                    });

                    modalInstance.result.then(function (imageData) {
                        if ( imageData )
                        {
                            scope.$eval(scope.onModifyImage({imageData : imageData}));                                                        
                        }
                    }, function () {
                    });
                });
            }
        };
    }]);   
})(window, window.angular);