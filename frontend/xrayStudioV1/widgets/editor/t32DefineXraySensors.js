(function (window, angular, undefined) { 'use strict';

    angular.module('app').controller('t32DefineXraySensorsCtrl', [
            '$scope', 
            '$modalInstance',
            't32SystemService',
        function (
            $scope, 
            $modalInstance,
            t32SystemService ) 
        {

            function init()
            {
                $scope.theModalInstance = $modalInstance;

                t32SystemService.getXrayStudioSetting()
                .then(function(setting){
                    $scope.setting = setting;

                    $scope.sensorIdList = [];
                    if ( setting.sensorList && setting.sensorList.length )
                    {
                        _.each(setting.sensorList, function(sensor) {
                            $scope.sensorIdList.push(sensor._id.toString() );
                        });                        
                    }

                    var requestType = 'getMasterSensorList';
                    t32SystemService.submitSystemRequest(requestType, null)
                    .then(function(masterSensorList){
                        _.each(masterSensorList, function(xraySensor){
                            if ( _.contains($scope.sensorIdList,  xraySensor._id.toString() ))
                            {
                                xraySensor.selected = true;
                            }
                        });

                        $scope.masterSensorList = masterSensorList;                        
                    });
                });
            }

            $scope.onUpdate = function() {
                var newSensorList = [];

                var selectedXrayIdList = [];
                _.each($scope.masterSensorList, function(xraySensor){
                    if ( xraySensor.selected )
                    {
                        newSensorList.push(xraySensor);
                    }
                });

                $scope.setting.sensorList = newSensorList;
                var requestType = 'saveXrayStudioSetting';

                t32SystemService.saveXrayStudioSetting($scope.setting)
                // t32SystemService.submitSystemRequest(requestType, $scope.setting)
                .then(function(setting){
                   $modalInstance.close('OK');
                });
            };


            init();
        }
    ]);

})(window, window.angular);   