(function (window, angular, undefined) { 'use strict';
    // angular.module('app').directive('xrayImageEditor', ['$window', function ($window) {
    //     var canvas, canvasContainer
    //     return {
    //         restrict: 'EA',
    //         scope: {
    //             height: '@',
    //             width: '@'
    //         },
    //         templateUrl: '/widgets/editor/xrayImageEditor.html',
    //         controller: 'XrayImageEditorCtrl',
    //         // link: function(scope, element){
    //         //     // canvas = element.find('canvas')[0];
    //         //     // canvasContainer = angular.element(document.getElementsByClassName("canvas-container"))[0];
    //         //     // canvasContainer.style.display = 'inline';
    //         //     // scope.signaturePad = new SignaturePad(canvas);
    //         // }
    //     };
    // }]);   

    angular.module('app').controller('XrayImageEditorCtrl', [
            '$scope', 
            '$window',
            '$document',
            '$timeout',
            '$rootScope',
            '$modalStack',
            'SweetAlert',
            'logger',
            'selectedFolder',
            'setting',
            '$modalInstance',
            't32SystemService',
        function (
            $scope, 
            $window,
            $document,
            $timeout,
            $rootScope,
            $modalStack,
            SweetAlert,
            logger,
            selectedFolder,
            setting,
            $modalInstance,
            t32SystemService) 
        {
            function init(){
                $scope.selectedFolder = selectedFolder;
                $scope.setting = setting;
                $scope.theModalInstance = $modalInstance;
                $scope.photos = [];
                $scope.currentFileName = null;
                loadFolderImages();

                $scope.paintBrushDimensions = {
                    display: "inline-block",
                    textAlign: "right",
                    color: "green"
                };
                $scope.inPaintMode = true;
    
            }
            function loadFolderImages() {
                if ( $scope.selectedFolder ){
                   var requestType = 'getDirectoryFileList';
    
                   t32SystemService.submitSystemRequest(requestType, $scope.selectedFolder )
                   .then(function(fileList){
                        $scope.photos = _.map(fileList, function(file){
                            var fileName = "/" + $scope.selectedFolder + '/' + file;
                            var id = parseId(file);
                            return {
                                desc     : file,
                                fileName : fileName,
                                id       : id
                            };
                        });
    
                        logger.debug($scope.photos);
                        addRandomQueryStringToPhotoFiles();
                        // $scope.appliedPhotos = [];
                        // $scope.photosHash = makePhotosHash($scope.photos);
    
                        // logger.debug($scope.photosHash);
                        $scope._Index = $scope._Index || 0;
                        setCurrentImage();
                   });
                }
                else
                {
                    $scope.photos = [];
                    setCurrentImage();
                }
            }

            function parseId(file){
                var reg = (/_\w+/g);
                var id = file.match(reg)[0].substring(1, file.length -  1);
                logger.debug(id);
                return id;
            }   

            function addRandomQueryStringToPhotoFiles() {
                $scope.photos = _.each($scope.photos, function(photo){
                    photo.src = "http://localhost:" + $scope.setting.localTcpPort + photo.fileName + '?r=' + Math.round(Math.random() * 999999);
                });
            }

            
            function setCurrentImage() {
                $scope.currentFileName = null;
                if ( $scope.photos && $scope.photos.length ){
                    $scope.currentFileName = $scope.photos[$scope._Index].fileName;
                }
            }

            $scope.isActive = function (index) {
                return $scope._Index === index;
            };

            $scope.showPrev = function () {
                $scope._Index = ($scope._Index > 0) ? --$scope._Index : $scope.photos.length - 1;
                setCurrentImage();            
            };
    
             $scope.showNext = function () {
                $scope._Index = ($scope._Index < $scope.photos.length - 1) ? ++$scope._Index : 0;
                setCurrentImage();            
            };

            $scope.showPhoto = function (index) {
                $scope._Index = index;
                setCurrentImage();            
            };

            $scope.onDeleteAllImages = function() {
                deleteXray( true, $scope.selectedFolder )
                .then(function() {
                    loadFolderImages();
                })
            };
    
             $scope.onDeleteActiveImage = function() {
                var thePhoto = $scope.photos[$scope._Index];
                if ( thePhoto ){
                    deleteXray( false, thePhoto.fileName )
                    .then(function() {
                        loadFolderImages();
                    })               
                }
            };

            function deleteXray(isDirectory, filePath){
               var requestType = 'deleteXrays';
               var requestObject = { 
                    isDirectory : isDirectory,
                    filePath : filePath
                };
               return t32SystemService.submitSystemRequest(requestType, requestObject )
            }

            $scope.onModifyImage = function(imageData) {
                var thePhoto = $scope.photos[$scope._Index];
                if ( thePhoto ){
                   var fileName = thePhoto.fileName;
                   var requestType = 'saveImage';
                   var requestMsg = {
                      fileName : thePhoto.fileName,
                      imageData : imageData
                   };
    
                   t32SystemService.submitSystemRequest(requestType, requestMsg)
                   .then(function(result){
                      addRandomQueryStringToPhotoFiles();
                   });
                }
            }

            $scope.togglePaintMode = function(){
                $scope.inPaintMode = !$scope.inPaintMode;
                if(!$scope.inPaintMode){
                    $scope.paintBrushDimensions.color = "grey";
                    $scope.$broadcast("paintBrushToggled", "false");
                }
                else{
                    $scope.paintBrushDimensions.color = "green";
                    $scope.$broadcast("paintBrushToggled", "true");
                }
            }
    
            
            init();
        }
    ]);
})(window, window.angular);