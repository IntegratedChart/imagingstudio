(function (window, angular, undefined) {
    'use strict';
    angular.module('app').directive('t32BrightnessContrastControls', [
        '$modal',
        function (
            $modal) {
            return {
                restrict: 'EA',
                scope: {
                    currentImageSettings: '=',
                    disabled: '='
                },
                templateUrl: 'widgets/editor/t32BrightnessContrastControls.html',
                controller: 't32BrightnessContrastControlsCtrl'
            };
        }
    ]);



    angular.module('app').controller('t32BrightnessContrastControlsCtrl', [
        '$scope',
        '$modal',
        't32SystemService',
        'logger',
        't32ImageManipulation',
        '$rootScope',
        '$timeout',
        function (
            $scope,
            $modal,
            t32SystemService,
            logger,
            t32ImageManipulation,
            $rootScope,
            $timeout) {
            function init() {
                // t32SystemService.getXrayStudioSetting()
                // .then(function (settings) {
                //     $scope.setting = settings;
                // })

                $scope.zoom = 100;
                setAdjustmentLevels();
            }
            console.log('brightness controls init');
            $scope.setAdjustment;

            function setAdjustmentLevels(){
                console.log('setting brightness/contrast levels');
                $scope.setAdjustment = true;
                if ($scope.currentImageSettings) {
                    $scope.brightness = $scope.currentImageSettings.brightness;
                    $scope.contrast = $scope.currentImageSettings.contrast;
                    $scope.sharpen = $scope.currentImageSettings.sharpen;
                }
                else {
                    $scope.currentImageSettings = {
                        brightness: 0,
                        contrast: 0,
                        sharpen: false,
                        angle: 0,
                        flipX: false,
                        flipY: false
                    }

                    $scope.brightness = $scope.currentImageSettings.brightness;
                    $scope.contrast = $scope.currentImageSettings.contrast;
                    $scope.sharpen = $scope.currentImageSettings.sharpen;
                }

                $timeout(function(){
                    $scope.setAdjustment = false;
                }, 10)
            }   

            $scope.$watch('brightness', function(newVal, oldVal){
                console.log("brightness watcher wakes");
                if(newVal && newVal !== oldVal && !$scope.setAdjustment){
                    $scope.manipulate();
                }
            })
            $scope.$watch('contrast', function(newVal, oldVal){
                console.log("contrast watcher wakes");
                if(newVal && newVal !== oldVal && !$scope.setAdjustment){
                    $scope.manipulate();
                }
            })

            $scope.manipulate = function(){
                var data = {
                    brightness : $scope.brightness,
                    contrast: $scope.contrast,
                    sharpen: $scope.sharpen
                }
                // alert("emit")
                $scope.$emit('manipulateImage', data);
            }

            $scope.zoomIn = function(){
                var data = {
                    zoom: $scope.zoom
                }

                $scope.$emit('zoomIn', data);
            }

            $scope.$on('newImageSelected', function () {
                setAdjustmentLevels();
                resetZoom();
            })

            function resetSliders() {
                $scope.brightness = 0;
                $scope.contrast = 0;
                $scope.sharpen = false;
                resetZoom()
            }

            function resetZoom(){
                $scope.zoom = 100;
                $scope.zoomIn();
            }

            $scope.resetManipulation = function () {
                resetSliders();
                $scope.manipulate();
            }

            init();
        }
    ]);

})(window, window.angular);   
