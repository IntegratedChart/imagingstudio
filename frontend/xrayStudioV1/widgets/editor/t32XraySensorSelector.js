(function (window, angular, undefined) { 'use strict';
    angular.module('app').directive('t32XraySensorSelector', [
            '$modal',
        function (
                $modal ) {
            return {
                restrict: 'E',
                scope : {
                    selectedSensorSize  : '='
                },
                templateUrl: 'widgets/editor/t32XraySensorSelector.html',
                controller: 't32XraySensorSelectorCtrl'
            };
        }
    ]); 

    angular.module('app').controller('t32XraySensorSelectorCtrl', [
            '$scope', 
            '$modal',
            't32SystemService',
        function (
            $scope, 
            $modal,
            t32SystemService ) 
        {

            function init()
            {
                t32SystemService.getXrayStudioSetting()
                .then(function(setting){
                    $scope.sensorOptionList = [];

                    $scope.sensorOptionList.push({
                        desc : '--- Define New Sensor ---',
                        size : 100
                    });

                    if ( setting.sensorList && setting.sensorList.length )
                    {
                        _.each(setting.sensorList, function(xraySensor){
                            $scope.sensorOptionList.push( {
                                desc : buildSensorLabel(xraySensor, true),
                                size : xraySensor.width
                            });
                            $scope.sensorOptionList.push( {
                                desc : buildSensorLabel(xraySensor, false),
                                size : xraySensor.height
                            });
                        });

                        if ( $scope.sensorOptionList.length > 1)
                        {
                            $scope.selectedSensorOption = $scope.sensorOptionList[1];
                        }
                        else
                        {
                            $scope.selectedSensorOption = null;                        
                        }                        
                    }
                    $scope.onChangeOption();
                })
            }

            function buildSensorLabel(xraySensor, isHorizontal) 
            {
                var label = xraySensor.manufacturer + ' - ' + xraySensor.prodName + ' - (' + xraySensor.modelNumber + ') ';
                if ( isHorizontal )
                {
                    label += 'BW - ' + xraySensor.width;
                }
                else
                {
                    label += 'PA - ' + xraySensor.height;
                }
                return label;
            }

            $scope.onChangeOption = function () {
                if ( $scope.selectedSensorOption && $scope.selectedSensorOption.desc == '--- Define New Sensor ---')
                {
                    var modalInstance = $modal.open({
                        templateUrl: 'widgets/editor/t32DefineXraySensors.html',
                        controller: 't32DefineXraySensorsCtrl',
                        windowClass : 'seventyPercent-Modal',
                        backdrop: 'static',
                        resolve: {
                        }
                    });

                    modalInstance.result.then(function (modified) {
                        init();                            
                    }, function () {
                        init();                            
                    });
                }
                $scope.selectedSensorSize = $scope.selectedSensorOption ? $scope.selectedSensorOption.size : 100;  
            };

            init();
        }
    ]);

})(window, window.angular);   