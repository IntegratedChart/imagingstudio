(function (window, angular, undefined) {
    'use strict';
    angular.module('app').controller('t32RecordConflictResolutionCtrl', [
        '$q',
        '$modal',
        '$modalInstance',
        '$rootScope',
        '$timeout',
        'logger',
        'message',
        '$scope',
        function (
            $q,
            $modal,
            $modalInstance,
            $rootScope,
            $timeout,
            logger,
            message,
            $scope) {
            
            $scope.conflictsArray = message.conflictArr;
            $scope.title = message.recordType;

            $scope.theModalInstance = $modalInstance;

            $scope.confirm = function(){
                $modalInstance.close();
            }
        }
    ]);
})(window, window.angular);