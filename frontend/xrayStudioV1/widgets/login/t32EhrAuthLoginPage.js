(function (window, angular, undefined) {
    'use strict';
    angular.module('app').directive('t32EhrAuthLogin', [function () {
        return {
            restrict: 'EA',
            scope: {
            },
            controller: 't32EhrAuthLoginPageCtrl',
            templateUrl: 'widgets/login/t32EhrAuthLoginPage.html'
        };
    }]);   

    angular.module('app').controller('t32EhrAuthLoginPageCtrl', [
        '$scope',
        't32SystemService',
        '$location',
        '$window',
        function (
            $scope,
            t32SystemService,
            $location,
            $window) {
            function init() {
                $scope.authenticated = t32SystemService.isAuthenticated();

                $scope.$on('Authentication', function (evt, result) {
                    $scope.authenticated = t32SystemService.isAuthenticated();
                });  
            }

            $scope.onLogin = function () {
                var requestType = 'getGoogleAuthUrl';
                t32SystemService.submitSystemRequest(requestType, null)
                .then(function (authUrl) {
                                                  
                })
            }

            init();
        }
    ]);
})(window, window.angular);