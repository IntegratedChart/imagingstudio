(function (window, angular, undefined) {
   'use strict';

   angular.module('app').controller('t32EhrLoginCtrl', [
         '$scope', 
         '$modalInstance',
         't32SystemService',
         '$location',
         '$window',
      function (
         $scope,
         $modalInstance,
         t32SystemService,
         $location,
         $window ) 
      {
         function init()
         {
            $scope.theModalInstance = $modalInstance;
         }

         $scope.onLogin = function() {
            var requestType = 'getGoogleAuthUrl';
            t32SystemService.submitSystemRequest(requestType, null)
            .then(function(authUrl){
               $modalInstance.close('OK');

               // const {shell} = require('electron')
               // shell.openExternal(authUrl)                                        
            })               
         }

         init();
      }
   ]);
})(window, window.angular);