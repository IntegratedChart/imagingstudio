(function (window, angular, undefined) {
   'use strict';
   angular.module('app').directive('t32EhrLoginButton', [function () {
      return {
         restrict: 'E',
         scope: {
         },            
         controller: 't32EhrLoginButtonCtrl',
         templateUrl: 'widgets/login/t32EhrLoginButton.html'
      };
   }]);   

   angular.module('app').controller('t32EhrLoginButtonCtrl', [
         '$scope', 
         '$modal',
         't32SystemService',
         '$location',
         '$timeout',
      function (
         $scope,
         $modal,
         t32SystemService,
         $location,
         $timeout ) 
      {
         function init()
         {
            $scope.authenticated = t32SystemService.isAuthenticated();

            $scope.$on('Authentication', function(evt, result) {
               $scope.authenticated = t32SystemService.isAuthenticated();
            });                

         }

         $scope.onLogin = function() {
            var modalInstance = $modal.open({
               templateUrl: 'widgets/login/t32EhrLogin.html',
               controller: 't32EhrLoginCtrl',
               // windowClass : 'ninetyPercent-Modal',
               size: 'md',
               backdrop: 'static',
               resolve: {
               // clinic : function() {
               //     return scope.clinic;
               // }
               }
            });

            modalInstance.result.then(function (modified) {
               $scope.$eval($scope.afterSetup);                            
            }, function () {
               $scope.$eval($scope.afterSetup);                            
            });  
         }

         init();
      }
   ]);
})(window, window.angular);