(function (window, angular, undefined) {
    'use strict';
    angular.module('app').directive('t32PageHeader', ['$modal', function ($modal) {
        return {
            restrict: 'EA',
            scope: {
                titleText: '=',
                titleImg: '=',
                titleImgStyle: '=',
                titleContainerStyle: '=',
                titleChildDivClass: '=',
                menuTarget: '@',
                menuBarLen: '@'
            },
            templateUrl: 'widgets/general/t32PageHeader.html',
            controller: 't32PageHeaderCtrl'
        };
    }]);

    angular.module('app').controller('t32PageHeaderCtrl', [
        '$timeout',
        '$scope',
        function (
            $timeout,
            $scope) {

            function init() {
                // $timeout(function(){
                //     $scope.titleImg = $scope.titleImg || null;
                //     if($scope.titleImg){
                        
                //     }
                // }, 100)
            }

            init();
        }
    ]);

})(window, window.angular);   
