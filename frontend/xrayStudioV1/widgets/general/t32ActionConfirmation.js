(function (window, angular, undefined) { 'use strict';
    angular.module('app').directive('t32ActionConfirmation', ['SweetAlert', function (SweetAlert) {
        return {
            restrict: 'A',
            scope : {
                confirmationTitle       :  '@',
                confirmationMsg         :  '@',
                confirmationLabel       :  '@',
                confirmationCancelLabel :  '@',
                onConfirm               :  '&' 
            },
            link: function (scope, element, attr) {
                element.bind('click',function () {
                    var swalOpts = {
                        title: scope.confirmationTitle,
                        text: scope.confirmationMsg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#d9534f",
                        confirmButtonText: scope.confirmationLabel,
                        cancelButtonText: scope.confirmationCancelLabel,
                        closeOnConfirm: true,
                        allowOutsideClick: true,
                        closeOnCancel: true
                    };
                    SweetAlert.swal(
                        swalOpts,
                        function(isConfirm){
                            if (isConfirm) {
                                scope.$eval(scope.onConfirm);
                            }
                        }
                    );
                });
            }
        };
    }]);   
})(window, window.angular);   
