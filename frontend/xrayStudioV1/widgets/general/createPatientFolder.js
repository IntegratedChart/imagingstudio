(function (window, angular, undefined) {
    'use strict';
    angular.module('app').directive('createPatientFolder', ['$modal', function ($modal) {
        return {
            restrict: 'EA',
            scope: {
                onCreateFolder: '&'
            },
            templateUrl: 'widgets/general/createPatientFolder.html',
            controller: 'createPatientFolderCtrl'
        };
    }]);

    angular.module('app').controller('createPatientFolderCtrl', [
        '$timeout',
        '$scope',
        't32SystemService',
        function (
            $timeout,
            $scope,
            t32SystemService) {

            function init() {
                $scope.ext = "\\";

                var requestType1 = 'getOsInfo';

                t32SystemService.submitSystemRequest(requestType1, null)
                .then(function (os) {
                    console.log('the os is ', os);
                    $scope.ext = os == 'win32' ? "\\" : "/";
                })

                var requestType2 = 'getRootPath';

                t32SystemService.submitSystemRequest(requestType2, null)
                .then(function (path) {
                    console.log('directory root path', path);
                    $scope.rootPath = path;
                })

            }

            $scope.folderName ='';
            var folderImg = 'img/dental-folder.svg';
            var emptyFolderImg = 'img/folder-empty.svg';
            var folderImgStyle = {
                "width": "60px"
            };
            var emptyFolderImgStyle = {
                "width": "60px"
            };

            $scope.imgSrc = emptyFolderImg;
            $scope.imgStyle = emptyFolderImgStyle;



            $scope.createFolder = function () {
                $scope.$eval($scope.onCreateFolder({ 'name': $scope.folderName }));
            }

            $scope.$watch('folderName', function(newVal, oldVal){
                if(newVal !== oldVal){
                    if(!newVal){
                        $scope.imgSrc = emptyFolderImg;
                        $scope.imgStyle = emptyFolderImgStyle;
                    }
                    else{
                        $scope.imgSrc = folderImg;
                        $scope.imgStyle = folderImgStyle;
                    }
                }
            })

            init();
        }
    ]);

})(window, window.angular);   
