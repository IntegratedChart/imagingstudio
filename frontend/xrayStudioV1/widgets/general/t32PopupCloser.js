/**
 * Widget for close the modal popup window
 *      <t32-popup-closer
 *          the-modal-instance=""   // the modal instance to close, 
 *          special-close="">       // if controller wants to handle the close event, 
 *                                       provide a callback function instead of theModalInstance
 *      </t32-popup-closer>
 */
(function (window, angular, undefined) { 'use strict';
    angular.module('app').directive('t32PopupCloser', [function () {
        return {
            restrict: 'AE',
            scope: {
                theModalInstance  : '=',
                specialClose      : '&'
            },
            controller: 't32PopupCloser',            
            templateUrl: 'widgets/general/t32PopupCloser.html'
        };
    }]);  

    angular.module('app').controller('t32PopupCloser', [ 
            '$scope',
        function (
            $scope ) {

            $scope.onClose = function() 
            {
                if ($scope.theModalInstance )
                {
                    console.log("in t32PopupCloser.onClose, close modal..");
                    $scope.theModalInstance.close();
                }
                else
                {
                    console.log("in t32PopupCloser.onClose, eval specialClose..");
                    $scope.$eval($scope.specialClose);
                }
            };
        }
    ]);

})(window, window.angular);   
