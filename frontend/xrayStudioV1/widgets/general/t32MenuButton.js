(function (window, angular, undefined) {
    'use strict';
    angular.module('app').directive('t32MenuButton', ['$modal', function ($modal) {
        return {
            restrict: 'E',
            scope: {
                btnBg: '@',
                bars: '@',
                barBg: '@',
                targetId: '@'
            },
            templateUrl: 'widgets/general/t32MenuButton.html',
            controller: 't32MenuButtonCtrl'
        };
    }]);

    angular.module('app').controller('t32MenuButtonCtrl', [
        '$timeout',
        '$scope',
        function (
            $timeout,
            $scope) {

            function init() {
                console.log('menu button init');
                // console.log('btnBg ', $scope.btnBg);
                // console.log('bars ', $scope.bars);
                // console.log('barBg ', $scope.barBg);
                // console.log('targetId ', $scope.targetId);

                $scope.barStyle = {
                    'background': $scope.barBg || '#ddd'
                }

                $scope.btnStyle = {
                    'background': $scope.btnBg || 'white',
                    'margin': 0
                }

                // $scope.iconBars = [];

                $scope.bars = $scope.bars ? parseInt($scope.bars) : 3;
                // console.log($scope.bars)
                $scope.iconBars = Array.from(Array($scope.bars).keys());

                console.log($scope.iconBars)
                // for(var i = 0; i < $scope.bars; i++){
                //     $scope.iconBars.push(i);
                // }

            }

            init();
        }
    ]);

})(window, window.angular);   
