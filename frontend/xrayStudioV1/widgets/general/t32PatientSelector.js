(function (window, angular, undefined) {
   'use strict';

   angular.module('app').controller('t32PatientSelectorCtrl', [
         '$scope',
         'fromEhrOnly',
         't32SystemService',
         '$modalInstance',
         'logger',
      function ($scope,
         fromEhrOnly,
         t32SystemService,
         $modalInstance,
         logger) {

         function init() {
            $scope.barLen = 3;
            $scope.theModalInstance = $modalInstance;
            $scope.theOption = 'byEHR';
            $scope.fromEhrOnly = fromEhrOnly;

            $scope.authenticated = t32SystemService.isAuthenticated();

            // $scope.directoryList = [];
            // var requestType = 'getImageDirectoryList';
            // t32SystemService.submitSystemRequest(requestType, null)
            // .then(function(result){
            //    console.log('result=' + result );
            //    $scope.directoryList = result;
            //    $scope.selectedFolder = null;
            // });

            $scope.$watch('theOption', function(newVal, oldVal) {
               $scope.selectedFolder = null;
               $scope.selectedPatient = null;
            })

            $scope.$watch('manualInput', function(newVal, oldVal) {
               $scope.selectedFolder = replaceSpace($scope.manualInput, '-' );
               $scope.selectedFolder = _.capitalize($scope.selectedFolder);
            })
         };

         $scope.onSelectPatient = function(patient){
             $scope.selectedPatient = patient;
             onChangePatient();
             $scope.onConfirm('xray');
         }

         function onChangePatient() {
            $scope.selectedFolder = null;
            if ( $scope.selectedPatient && $scope.selectedPatient._id )
            {
               var folder = replaceSpace($scope.selectedPatient.fname, '') + '-' +
                            replaceSpace($scope.selectedPatient.lname, '');
               if ( $scope.selectedPatient.clinicData && $scope.selectedPatient.clinicData.chartId )
               {
                  folder += '-' + $scope.selectedPatient.clinicData.chartId;
               }

               $scope.selectedFolder = _.capitalize( folder );
            }
         }

         function replaceSpace(str, replacement) {
            return str ? str.replace(/\s/g, replacement) : '';
         }

         $scope.onConfirm = function(typeOfUpload) {
            var patientContext = {
               selectedFolder  : $scope.selectedFolder,
               selectedPatient : $scope.selectedPatient,
               uploadType: typeOfUpload
            };

            $modalInstance.close(patientContext);
         };

         $scope.onCancel = function() {
            $modalInstance.dismiss();
         };

         $scope.selectOpt = function(opt){
            $scope.theOption = opt;
         }

         $scope.isSelected = function (opt) {
            if ($scope.theOption == opt) {
               return 'selected-list-item';
            }
            else {
               return ''
            }
         }

         $scope.onLocalFolderSelect = function(folder){
            $scope.selectedFolder = folder;
            $scope.onConfirm('xray');
         }

         $scope.onManualFolderSelect = function(name){
            console.log('making manual entry')
            $scope.selectedFolder = replaceSpace(name, '-');
            $scope.selectedFolder = _.capitalize($scope.selectedFolder);
            $scope.onConfirm('xray');
         }

         $scope.titleText = 'Patient Search'
         $scope.titleStyle = {};
         $scope.titleChildDivClass = 'branding';
         $scope.menuBarLen = 3;

         init();
      }
   ]);
})(window, window.angular);