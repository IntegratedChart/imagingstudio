(function (window, angular, undefined) {
   'use strict';

   angular.module('app').controller('t32SimpleAnswerCollectorCtrl', [
         '$scope',
         'title',
         'noteList',
         '$modalInstance',
      function ($scope,
         title,
         noteList,
         $modalInstance) {

         function init() {
            $scope.theModalInstance = $modalInstance;
            $scope.title = title;
            $scope.noteList = noteList;
            $scope.theAnswer = null;
         };

         $scope.onConfirm = function() {
            $modalInstance.close($scope.theAnswer);
         };

         $scope.onCancel = function() {
            $modalInstance.dismiss();
         };

         init();
      }
   ]);
})(window, window.angular);