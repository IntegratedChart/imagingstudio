(function (window, angular, undefined) { 'use strict';
    angular.module('app').directive('t32DatePicker', [function () {
        return {
            restrict: 'E',
            scope: {
                selectedDate        : '=',
                isDisabled          : '=',
                hideTodayButton     : '=',
                hideInputWidget     : '=',
                inline              : '=',
                afterChange         : '&'
            },
            controller: 't32DatePickerCtrl',
            templateUrl: 'widgets/general/t32DatePicker.html'

        };
    }]);   

    angular.module('app').value('t32DatePickerOptions', {
        formatYear: 'yy',
        showWeeks: false,
        startingDay: 1
    });

    angular.module('app').controller('t32DatePickerCtrl', [
            '$scope', 
            't32DatePickerOptions',
        function (
            $scope, 
            t32DatePickerOptions )
        {
            // myDatePickerOptions is defined in factory as global value
            $scope.t32DatePickerOptions = t32DatePickerOptions;

            $scope.toggle = function( $event )
            {
                $event.preventDefault();
                $event.stopPropagation();

                $scope.opened = !$scope.opened;
            };

            $scope.today = function()
            {
                $scope.selectedDate = moment().toDate();
            };

            $scope.onChange = function() {
                $scope.$eval($scope.afterChange);
            };

            if (!$scope.inline)
                $scope.opened = false;
            else
                $scope.opened = true;

        }
    ]);
})(window, window.angular);
