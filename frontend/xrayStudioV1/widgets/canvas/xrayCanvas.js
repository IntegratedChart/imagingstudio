(function (window, angular, undefined) { 'use strict';
    angular.module('app').directive('xrayCanvas', ['$window', function ($window) {
        var canvas, canvasContainer
        return {
            restrict: 'EA',
            scope: {
                height: '@',
                width: '@',
                // inPaintMode: "="
            },
            templateUrl: 'widgets/canvas/xrayCanvas.html',
            controller: 'XrayCanvasCtrl',
            // link: function(scope, element){
            //     // canvas = element.find('canvas')[0];
            //     // canvasContainer = angular.element(document.getElementsByClassName("canvas-container"))[0];
            //     // canvasContainer.style.display = 'inline';
            //     // scope.signaturePad = new SignaturePad(canvas);
            // }
        };
    }]);   

    angular.module('app').controller('XrayCanvasCtrl', [
            '$scope', 
            '$window',
            '$document',
            '$timeout',
            '$rootScope',
            '$modalStack',
            'SweetAlert',
            'logger',
        function (
            $scope, 
            $window,
            $document,
            $timeout,
            $rootScope,
            $modalStack,
            SweetAlert,
            logger) 
        {
            function init(){
                // var canvas = angular.element(document.getElementsByTagName("canvas"));

                console.log("Running init")
                // $scope.clinicId = SharedDataService.get_selected_clinic()._id;
                // $scope.patientId = SharedDataService.get_selected_patient()._id;
                $scope.patientToothCanvas = {};
                var canvasContainer = angular.element(document.getElementsByClassName("canvas-container"))[0];

                $scope.canvas = angular.element(document.getElementById("canvas-pad"))[0];
                console.log($scope.canvas)
                $scope.signaturePad = new SignaturePad($scope.canvas);
                // var ctx = canvas.getContext('2d');
                // ctx.globalCompositeOperation = 'source-over';
                $scope.signaturePad._ctx.globalCompositeOperation = 'source-over';
                canvasContainer.style.display = 'inline';
                $scope.EMPTY_IMAGE = 'data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA=';
                $scope.penColor = 'black';
                $scope.dataUrl = $scope.EMPTY_IMAGE;
                $scope.lineThicknessScaleValue = 25;
                $scope.eraserThicknessScaleValue = 25;
                $scope.lineWidth = $scope.lineThicknessScaleValue/10;
                $scope.imageArray = [];
                $scope.imageRedoArray = [];
                $scope.$broadcast('imageRedoArrayChange');
                $scope.eraserMode = false;
                $scope.enterView = true;
                $scope.signaturePad.penColor = $scope.penColor
                $scope.signaturePad.minWidth = $scope.lineWidth;
                $scope.signaturePad.maxWidth = $scope.lineWidth;
                $scope.canvasDimensions = {
                    height : $scope.height.toString() + 'px',
                    width: $scope.width.toString() + 'px',
                    display: 'inline'
                }

                $scope.imageArray.push($scope.dataUrl);
                $scope.$broadcast('imageArrayChange');

                //maybe in futrue give user the ability to store the canvas on the local settings

                $scope.signaturePad.onEnd = function(){
                    console.log($scope.eraserMode)
                    // if($scope.eraserMode === false){
                    console.log("The on end function runs");
                    $scope.dataUrl = $scope.signaturePad.toDataURL();
                    $scope.imageArray.push($scope.dataUrl);
                    $scope.$broadcast('imageArrayChange');
                    $scope.enterView = false;
                    // console.log($scope.imageArray);
                    // }
                    // else{
                    //     $scope.dataUrl = $scope.signaturePad.toDataURL();
                    //     $scope.imageArray.push($scope.dataUrl);
                    //     $scope.enterView = false;
                    //     console.log($scope.imageArray);
                    // }
                }
            }

            // $scope.$watch("inPaintMode", function(){
            //     if(!$scope.inPaintMode){
            //         // alert("The paint mode is false")
            //         $scope.signaturePad.clear();
            //         $scope.signaturePad.off();
            //     }
            //     else{
            //         $scope.signaturePad.on();
            //     }
            // });
            
            $scope.$on('imageArrayChange', function(){
                console.log("Image Array Changed");
                if($scope.imageArray.length <= 1){
                    $scope.changeUndoStatus = {
                        color: 'gray',
                        cursor: 'defualt',
                        pointerEvents: 'none'
                    }
                }
                else{
                    $scope.changeUndoStatus = {}
                }
            });

            $scope.$on('pageReloaded', function(){
                console.log("page reloaded");
                $scope.canvas.remove();
            })

            $scope.$on('imageRedoArrayChange', function(){
                console.log("Redo Image Array Changed");
                if($scope.imageRedoArray.length === 0){
                    $scope.changeRedoStatus = {
                        color: 'gray',
                        cursor: 'defualt',
                        pointerEvents: 'none'
                    }
                }
                else{
                    $scope.changeRedoStatus = {};
                }
            })

            $scope.clear = function(){
                var swalOpts = {
                    title: "Clear All Markings",
                    text: "Are you sure you want you clear all markings?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#d9534f",
                    confirmButtonText: "Clear All",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: true,
                    allowOutsideClick: true,
                    closeOnCancel: true 
                };
                SweetAlert.swal(
                swalOpts, 
                function(isConfirm){ 
                    if (isConfirm) {
                        $scope.signaturePad.clear();
                        $scope.imageArray = [];
                        $scope.imageRedoArray = [];
                        $scope.dataUrl = $scope.EMPTY_IMAGE;
                        $scope.imageArray.push($scope.dataUrl);
                        $scope.$broadcast('imageArrayChange');
                        $scope.$broadcast('imageRedoArrayChange');
                    }
                });
                // $scope.signaturePad.clear();
                // $scope.dataUrl = $scope.EMPTY_IMAGE;
                // $scope.imageArray.push($scope.dataUrl);
                // $scope.imageArray = [];
            }

            $scope.$watch('penColor', function(){
                console.log("pen color watcher wakes")
                $scope.signaturePad.penColor = $scope.penColor;
                $scope.color = {color: $scope.penColor};
            });


            $scope.selectColor = function(color){
                console.log("pen color change");
                // $scope.color = {color: color};
                $scope.penColor = color;
                $scope.eraserMode = false;
                
                $scope.lineWidth = ($scope.lineThicknessScaleValue/ 10);
                $scope.signaturePad.minWidth = $scope.lineWidth;
                $scope.signaturePad.maxWidth = $scope.lineWidth;

                // ctx.globalCompositeOperation = 'source-over';
                $scope.signaturePad._ctx.globalCompositeOperation = 'source-over';
            }

            $scope.$watch('lineThicknessScaleValue', function(){
                console.log("Line Thickness value changed");
                console.log($scope.lineThicknessScaleValue);
                $scope.lineWidth = ($scope.lineThicknessScaleValue/ 10);
                $scope.signaturePad.minWidth = $scope.lineWidth;
                $scope.signaturePad.maxWidth = $scope.lineWidth;
            });

            $scope.$watch('eraserThicknessScaleValue', function(){
                console.log("Eraser Thickness value changed");
                console.log($scope.eraserThicknessScaleValue);
                $scope.lineWidth = ($scope.eraserThicknessScaleValue/ 10);
                $scope.signaturePad.minWidth = $scope.lineWidth;
                $scope.signaturePad.maxWidth = $scope.lineWidth;
            });


            $scope.$watch("dataUrl", function(dataUrl){
                console.log("Data changed")
                if(dataUrl){
                    // var ctx = canvas.getContext('2d');
                    // ctx.globalCompositeOperation = 'source-over';
                    // alert($scope.signaturePad._ctx.globalCompositeOperation);
                    if(window.devicePixelRatio !== 1){
                        window.devicePixelRatio = 1;
                    }
                    $scope.signaturePad._ctx.globalCompositeOperation = 'source-over';
                    // alert($scope.signaturePad._ctx.globalCompositeOperation);
                    $scope.signaturePad.clear();
                    // var image = new Image();
                    var width = $scope.width;
                    var height = $scope.height;
                    console.log("This is the canvas width " + width);
                    console.log("This is the height " + $scope.height);
                    // image.src = dataUrl;
                    // image.onload = function(){
                    //     $scope.signaturePad._ctx.drawImage(image, 0, 0, width, height);
                    //     console.log($scope.signaturePad);
                    //     $scope.$broadcast('imageDrawn');
                    //     console.log("Image drawn event");
                    // }
                    $scope.signaturePad.fromDataURL(dataUrl);
                    // $scope.$broadcast('imageDrawn');
                    // console.log("Image drawn event");
                }
            });

            // $scope.$on('imageDrawn', function(){
            //     if($scope.eraserMode){
            //         console.log("Image drawn knows we are in erase mode");
            //         // var ctx = $scope.canvas.getContext('2d');
            //         // ctx.globalCompositeOperation = 'destination-out';
            //         $scope.signaturePad._ctx.globalCompositeOperation = 'destination-out';
            //     }
            // })

            $scope.undo = function(){
                console.log("clicked Undo");
                var undoImage;

                if($scope.imageArray.length > 1){
                    undoImage = $scope.imageArray.pop();
                    $scope.imageRedoArray.push(undoImage);
                    $scope.$broadcast('imageArrayChange');
                    $scope.$broadcast('imageRedoArrayChange');
                    $scope.dataUrl = $scope.imageArray[$scope.imageArray.length - 1];
                }
                // else if($scope.imageArray.length === 1){
                //     undoImage = $scope.imageArray.pop();
                //     $scope.imageRedoArray.push(undoImage);
                //     $scope.$broadcast('imageArrayChange');
                //     $scope.$broadcast('imageRedoArrayChange');
                //     $scope.signaturePad.clear();
                //     $scope.dataUrl = $scope.EMPTY_IMAGE;
                // }
                else{
                    console.log("No data to undo");
                }
                console.log($scope.imageArray);
                $scope.enterView = false;
            }

            $scope.redo = function(){
                var redoImage
                if($scope.imageRedoArray.length > 0){
                    console.log("clicked Redo");
                    var redoImage = $scope.imageRedoArray.pop();
                    $scope.imageArray.push(redoImage);
                    $scope.$broadcast('imageArrayChange');
                    $scope.$broadcast('imageRedoArrayChange');
                    $scope.dataUrl = $scope.imageArray[$scope.imageArray.length - 1];
                }
            }

            // $scope.erase = function(){
            //     console.log("Inside erase");
            //     $scope.eraserMode = true;
            //     $scope.lineWidth = ($scope.eraserThicknessScaleValue/ 10);
            //     $scope.signaturePad.minWidth = $scope.lineWidth;
            //     $scope.signaturePad.maxWidth = $scope.lineWidth;
            //     // var ctx = $scope.canvas.getContext('2d');
            //     // ctx.globalCompositeOperation = 'destination-out';
            //     $scope.signaturePad._ctx.globalCompositeOperation = 'destination-out';
            //     // console.log(ctx.globalCompositeOperation);
            // }
            init();
        }
    ]);
})(window, window.angular);