(function (window, angular, undefined) { 'use strict';

    angular.module('app').filter('xrayName',
    function () {
        return function (imageId) {
            if (imageId) {
                var underscore = /_/gi
                var splitName = imageId.replace("id_", "").replace(underscore, " ");
                // var capitalizedStr = "";
                // _.each(splitName, function (word) {
                //     // alert(word[0]);
                //     capitalizedStr += word[0].toUpperCase() + word.substring(1, word.length) + " ";
                // })

                return splitName.toUpperCase();
            }
        }
    });

    angular.module('app').filter('numString',
        function () {
            return function (num) {
                if (num) {
                    return num.toString();
                }
            }
        });

    angular.module('app').filter('t32BenefitPlanType', function () {
        return function (planType) {
            switch(planType) {
                case 0: 
                    return "PPO or By Percentage";
                case 1:
                    return "MEDICAID or By Amount";
                default:
                    return planType;
            }
        };
    });  

    angular.module('app').filter('t32Tel', function () {
        return function (tel) {
            if (!tel) { return ''; }

            var value = tel.toString().trim().replace(/^\+/, '');

            if (value.match(/[^0-9]/)) {
                return tel;
            }

            var country, city, number;

            if ( value === '0000000000')
            {
                return "";
            }

            switch (value.length) {
                case 10: // +1PPP####### -> C (PPP) ###-####
                    country = 1;
                    city = value.slice(0, 3);
                    number = value.slice(3);
                    break;

                case 11: // +CPPP####### -> CCC (PP) ###-####
                    country = value[0];
                    city = value.slice(1, 4);
                    number = value.slice(4);
                    break;

                case 12: // +CCCPP####### -> CCC (PP) ###-####
                    country = value.slice(0, 3);
                    city = value.slice(3, 5);
                    number = value.slice(5);
                    break;

                default:
                    return tel;
            }

            if (country == 1) {
                country = "";
            }

            number = number.slice(0, 3) + '-' + number.slice(3);

            return (country + " (" + city + ") " + number).trim();
        };
    });

    angular.module('app').filter('t32Email', function(){
        return function (email){
            if(!email || email=="noemail@tab32.com"){
                return "";
            }else{
                return email;
            }
        };
    });

    angular.module('app').filter('t32Trim', function(){
        return function (value, length){
            if(!value){
                return "";
            }else{
                if(value.length > length) {
                    return value.substring(0, length) + "...";
                } else
                    return value;
            }
        };
    });

    angular.module('app').filter('t32Date',['$filter', function($filter){
        return function (dateToFormat, formatString){
            if(!dateToFormat){
                return "";
            }
            if(moment(dateToFormat).isSame("1776-07-04", "year")){
                return "";
            }else{
                return $filter('date')(dateToFormat, formatString);
            }
        };
    }]);

    angular.module('app').filter('t32TimeDisplay', function(){
        return function (time){
            if(!time){
                return "00";
            }
            var display = time + '';
            if(display.length < 2)
                display = '0' + display;
            return display;
        };
    });

    angular.module('app').filter('t32AppointmentTimeDisplay', function(){
        return function (time){
            if(!time){
                return "00";
            }
            var minute = moment(time).minute();
            var hours = moment(time).hour();
            var am_pm = "AM";
            if(hours > 12) {
               am_pm = "PM";
               hours = hours - 12;
            }
            if(minute.toString().length == 1) {
                minute = "0"+ minute;
            }
            var display = hours + ":" + minute + " " + am_pm;
            return display;
        };
    });

    angular.module('app').filter('t32AppointmentTimeOnlyDisplay', function () {
        return function (time) {
            var result = "";
            if(time){
                result = moment(time, "HH:mm").format("h:mm A");
            }
            return result;
        };
    });

    angular.module('app').filter('t32Gender', function () {
        return function (genderCode) {
            switch(genderCode) {
                case 0: 
                    return "Female";
                case 1:
                    return "Male";
                default:
                    return genderCode;
            }
        };
    });  

    angular.module('app').filter('t32ClaimFeeSource', function () {
        return function (source) {
            switch(source) {
                case 1 :
                    return 'UCR Fee';
                case 2:
                    return "Actual Fee";
                case 3:
                    return "Zero Amount";
                default:
                    return source;
            }
        };
    });  

    /**
     * Need to display our userType 'dentist' as 'provider'
     */
    angular.module('app').filter('t32ClinicUserType', function () {
        return function (userType) {
            switch(userType) {
                case 'dentist': 
                    return "provider";
                default:
                    return userType;
            }
        };
    });  


    angular.module('app').filter('t32CallStatus', function () {
        return function (status) {
            var result = "";
            if(status == 'ringing'){
                result = "Incoming Call";
            }else if(status == 'connected'){
                result = "Call Connected";
            }
            return result;
        };
    });  

    angular.module('app').filter('t32CurrentAge', function(){
        return function (date, addParen){
            if(!date){
                return "";
            }

            var result = moment().diff(date, 'years');
            // result += " y/o";
            if(addParen){
                result = "("+result+")";
            }
            return result;
        };
    });

    angular.module('app').filter('t32Label', function () {
        return function (wordsStr, capitalized) {
            if (!wordsStr) return wordsStr;

            var words = wordsStr.split(/[ \-_]/);
            var finalWords = '';
            for (var i = 0; i < words.length; i++) {
                words[i] = words[i].trim();
                if (!capitalized) {
                    finalWords += words[i] + ' ';
                }
                else if (capitalized == 'firstWord') {
                    if (i > 0) {
                        finalWords += words[i] + ' ';
                    }
                    else {
                        finalWords += words[i].substring(0, 1).toUpperCase() +  words[i].substring(1) + ' ';
                    }
                }
                else if ( capitalized == 'initial')
                {
                    finalWords += words[i].substring(0, 1).toUpperCase() + ' ';                    
                }
                else {
                    finalWords += words[i].substring(0, 1).toUpperCase() + words[i].substring(1) + ' ';
                }
            }

            return finalWords.trim();
        };
    });

    angular.module('app').filter('t32Dollar', function () {
        return function (amount) {
            return amount.toFixed(2);
        };
    });

    angular.module('app').filter('t32Choices', function () {
        return function (choices) {
            var result = "";
            _.each(choices, function (choice) {
                result += choice;
                result += ", ";
            });
            result = result.slice(0, -2);
            return result;
        };
    });

    angular.module('app').filter('t32FormPreview', function () {
        return function (preview) {
            var displayEntries = [];
            _.each(preview, function (value) {
                // if(field.dataType=="checkbox"){
                //     displayEntries.push(field.label);
                // }else{
                    displayEntries.push(value);
                // }
            });
            return displayEntries.join(', ');
        };
    });

    angular.module('app').filter('t32FirstCharUpperCase', function () {
        return function (text) {
            if(!text)
                return;
            else
                return text.charAt(0).toUpperCase() + text.slice(1);
        };
    });

    angular.module('app').filter('t32Address', function () {
        return function (address) {
            var addString = "";
            
            if ( address )
            {
                if(!!address && !!address.add_line1 && !!address.city && !!address.state && !!address.zipcode){
                    addString += address.add_line1;
                    if (address.add_line2){
                        addString += " " + address.add_line2;
                    }
                    addString += " " + address.city;
                    addString += ", " + address.state;
                    addString += " " + address.zipcode;
                }else{
                    if(address.add_line1){
                        addString += address.add_line1;
                    }
                    if(address.add_line2){
                        addString += " " + address.add_line2;
                    }
                    if(address.city){
                        addString += " " + address.city;
                    }
                    if(address.state){
                        addString += ", " + address.state;
                    }
                    if(address.zipcode){
                        addString += " " + address.zipcode;
                    }
                }                
            }

            return addString;
        };
    });

    // angular.module('app').filter('t32ClinicGeoInfo', [
    //         't32PaymentMethodLookupService',
    //     function (
    //         t32PaymentMethodLookupService )
    //     {
    //         return function (clinic) {
    //             var formatted = '';
    //             if ( clinic && clinic.regGeoInfo )
    //             {
    //                 if ( clinic.regGeoInfo.country )
    //                 {
    //                     formatted += clinic.regGeoInfo.country + ', ';
    //                 } 
    //                 if ( clinic.regGeoInfo.city )
    //                 {
    //                     formatted += clinic.regGeoInfo.city + ', ';
    //                 } 
    //                 if ( clinic.regGeoInfo.region )
    //                 {
    //                     formatted += clinic.regGeoInfo.region + ', ';
    //                 } 
    //             }
    //             return formatted;
    //         };
    //     }
    // ]);

    // angular.module('app').filter('t32ClaimStatusForBiller', [
    //         't32ClaimStatusLookupService',
    //     function (
    //         t32ClaimStatusLookupService )
    //     {
    //         return function (statusCode) {
    //             var status = t32ClaimStatusLookupService.getClaimStatus(statusCode);
    //             return status ? status.label : '';
    //         };
    //     }
    // ]);

    // angular.module('app').filter('t32PaymentMethod', [
    //         't32PaymentMethodLookupService',
    //     function (
    //         t32PaymentMethodLookupService )
    //     {
    //         return function (paymentMethodCode) {
    //             var paymentMethod = t32PaymentMethodLookupService.getPaymentMethodByCode(paymentMethodCode);
    //             return paymentMethod ? paymentMethod.desc : '';
    //         };
    //     }
    // ]);

    // angular.module('app').filter('t32CheckType', [
    //         't32CheckTypeLookupService',
    //     function (
    //         t32CheckTypeLookupService )
    //     {
    //         return function (checkTypeCode) {
    //             var checkType = t32CheckTypeLookupService.getCheckTypeByCode(checkTypeCode);
    //             return checkType ? checkType.label : '';
    //         };
    //     }
    // ]);

    angular.module('app').filter('t32AttachmentPosition', [
        function ()
        {
           // def table copied from revenue fileUploadServices.
            var map = {};
            map.B4 = 'Referral Form';
            map.DA = 'Dental Models';
            map.DG = 'Diagnostic Report';
            map.EB = 'Explanation of Benefits';
            map.OB = 'Operative Note';
            map.OZ = 'Support Data for Claim';
            map.P6 = 'Periodontal Charts';
            map.RB = 'Radiology Films';
            map.RR = 'Radiology Reports';

            return function (attachmentPosition) {
               var decode = map[ attachmentPosition ];
               if ( !decode )
               {
                  decode = attachmentPosition;
               }
                return decode ? decode : attachmentPosition;
            };   
        }
    ]);
})(window, window.angular);



