(function (window, angular, undefined) {'use strict';
    angular.module('app').service('t32XrayRulerService', [
                '$q',
                '$timeout',
                't32AlertService',
                '$rootScope',
        function (
                $q,
                $timeout,
                t32AlertService,
                $rootScope) {
            // t32 customization
            var theMouseEventCallback;

            var parentContainer;
            var parentBoundingRect;
            var parentOffsetY;

            var modalContainer;
            var modalBoundingRect;
            var modalBoundingRectTop;
            var imageShrinkRatio;
            var wholeImageWidth;
            var wholeImageHeight;


            /**
             * @imageSrc the URL to the image.
             * @imageWidth the xray image width.
             * @modalContainerId
             * @parentContainerId
             */
            this.initialize = function( mouseMoveEventCallback, imageSrc, imageWidth, imageHeight, modalContainerId, parentContainerId ) {
                theMouseEventCallback = mouseMoveEventCallback;

                console.log(imageWidth);
                console.log(imageHeight);
                // if(!imageWidth || !imageHeight){
                //     var error = {
                //         type: "warning",
                //         title: "Sensor settings not defined!",
                //         text: "The ruler will not measure without the sensor information, please add it and try again.",
                //         showCloseButton: true,
                //         timeout: 5000
                //     }
                //     t32AlertService.showToaster(error)
                //     return
                // }

                wholeImageWidth = imageWidth;
                wholeImageHeight = imageHeight;

                parentContainer = document.getElementById(parentContainerId);
                parentOffsetY = parentContainer.offsetTop;

                parentBoundingRect = parentContainer.getBoundingClientRect();

                modalContainer = document.getElementById(modalContainerId);

                modalBoundingRect = modalContainer.getBoundingClientRect();
                modalBoundingRectTop = modalBoundingRect.top;

                var bodyRect = document.body.getBoundingClientRect();

                help_system = new HelpSystem(document.getElementById('helptext'));
                aug_view = new AugenmassView(document.getElementById('t32-xray-ruler-measure'));

                var show_delta_checkbox = document.getElementById('show-deltas');
                show_delta_checkbox.addEventListener("change", function(e) {
                    aug_view.setShowDeltas(show_delta_checkbox.checked);
                    aug_view.drawAll();
                });

                var show_angle_checkbox = document.getElementById('show-angles');

                show_angle_checkbox.addEventListener("change", function(e) {
                    aug_view.setShowAngles(show_angle_checkbox.checked);
                    aug_view.drawAll();
                });

                loupe_canvas = document.getElementById('t32-xray-ruler-loupe');
                loupe_canvas.style.left = document.body.clientWidth - loupe_canvas.width - 10;
                loupe_ctx = loupe_canvas.getContext('2d');
                // We want to see the pixels:
                loupe_ctx.imageSmoothingEnabled = false;
                loupe_ctx.mozImageSmoothingEnabled = false;
                //loupe_ctx.webkitImageSmoothingEnabled = false;
                loupe_ctx.imageSmoothingEnabled = false;
                aug_view.resetWithSize(10, 10); // Some default until we have an image.

                // var chooser = document.getElementById("file-chooser");
                // chooser.addEventListener("change", function(e) {
                //   load_background_image(chooser);
                // });

                // var download_link = document.getElementById('download-result');
                // download_link.addEventListener('click', function() {
                //   download_result(download_link)
                // }, false);
                // download_link.style.opacity = 0; // not visible at first.
                // download_link.style.cursor = "default";

                t32_load_image(imageSrc);
            };

            this.hasLines = function() {
                return  aug_view && 
                        aug_view.getModel() && 
                        aug_view.getModel().hasLines()  && 
                        !aug_view.getModel().hasEditLine()? true : false;
            };

            this.clear = function() {
                if ( aug_view && aug_view.getModel() ){
                    aug_view.getModel().init();
                    aug_view.drawAll();
                }
            };

            this.brighter = function( percentage ) {
                Caman("#t32-xray-ruler-background-img", function () {
                    this.brightness(percentage).render();
                });          
            };

            function round(value, decimals) {
                return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
            }

            function t32_load_image(imageSrc) {
                var imageFactor = wholeImageWidth > wholeImageHeight ? wholeImageWidth : wholeImageHeight;
                var imageFactorRatio = imageFactor == wholeImageWidth ? wholeImageWidth / wholeImageHeight : wholeImageHeight / wholeImageWidth;
                var new_img = new Image();
                var wholeLength;
                // Image loading in the background canvas. Once we have the image, we
                // can size the canvases to a proper size.
                var background_canvas = document.getElementById('t32-xray-ruler-background-img');
                background_canvas.height = 500;
                background_canvas.width = 500;
                new_img.onload = function() {
                    var imageSize = figureImageSize( new_img );
                    var imageRatio = imageSize.width / imageSize.height;
                    
                    console.log("The image ratio", imageRatio);

                    var bg_context = background_canvas.getContext('2d');
                    console.log("background_canvas", background_canvas);
                    // Yang
                    // adjust the original image based on the canvas size
                    var newWidth = background_canvas.width;
                    var newHeight = newWidth / imageRatio;

                    if ( newHeight > background_canvas.height){
                        newHeight = background_canvas.height;
                        newWidth = newHeight * imageRatio;
                    }

                    imageShrinkRatio = newWidth / new_img.width;
                    new_img.width = newWidth;
                    new_img.height = newHeight;

                    // set the unit
                    var wholeLength = Math.sqrt( new_img.width * new_img.width );

                    if (imageSize.height > imageSize.width) {
                        console.log("Image Factor is changing");
                        wholeLength = Math.sqrt(new_img.height * new_img.height);
                        imageRatio = imageSize.height / imageSize.width;
                    }

                    var wholeImageRatio = round(imageRatio, 3);
                    var wholeImageFactorRatio = round(imageFactorRatio, 3);

                    console.log(wholeImageRatio);
                    console.log(wholeImageFactorRatio);
                    if (wholeImageRatio !== wholeImageFactorRatio) {
                        // var error = {
                        //         type: "warning",
                        //         title: "Sensor setting does not match image",
                        //         text: "It seems that the sensor dimensions do not match the xray image dimensions. This can cause the measurement to be inaccurate.",
                        //         showCloseButton: true,
                        //         timeout: 5000
                        //     }

                        // t32AlertService.showToaster(error)
                        if(Math.abs(wholeImageRatio - wholeImageFactorRatio) > 0.1){
                            $rootScope.$broadcast('sensorWarning', { warning: true });
                        }
                    }
                    else {
                        $rootScope.$broadcast('sensorWarning', { warning: false });
                    }
                    console.log(wholeLength);
                    // Yang - End of mods.
                    console.log("Changing background width and height");
                    background_canvas.width = newWidth;
                    background_canvas.height = newHeight;

                    console.log("background canvas width " + background_canvas.width);
                    console.log("background canvas height " + background_canvas.height);

                    bg_context.clearRect(0, 0, newWidth, newHeight);
                    bg_context.drawImage(new_img, 0, 0, newWidth, newHeight);

                    console.log("about to reset with size" + newWidth + " " + newHeight);
                    aug_view.resetWithSize(newWidth, newHeight);

                    // need to set the unit after the above reset
                    console.log("The unites per pixel " + (imageFactor / wholeLength));
                    aug_view.setUnitsPerPixel(imageFactor / wholeLength );
                    
                    help_system.achievementUnlocked(HelpLevelEnum.DONE_FILE_LOADING);
                    backgroundImage = new_img;
                    // init_download(chooser.files[0].name);
                }
                new_img.src = imageSrc;
            }            

            function figureImageSize( img ){
                var t32HiddenMeasure = document.getElementById('t32HiddenMeasure');

                // if there is no printing section, create one
                if (!t32HiddenMeasure) {
                    t32HiddenMeasure = document.createElement('div');
                    t32HiddenMeasure.id = 't32HiddenMeasure';
                    var body = document.body;
                    body.insertBefore(t32HiddenMeasure, body.firstChild);
                }

                t32HiddenMeasure.appendChild(img);

                var result = {};
                result.width = img.width;
                result.height = img.height;

                console.log("the width of image", img.width);
                console.log("the height of image", img.height);
                return result;
            }
            // end of t32 customization

            // augenmass-main
            // Some constants.

            // How lines usually look like (blue with yellow background should make
            // it sufficiently distinct in many images).
            var line_style = "#00f";
            var background_line_style = 'rgba(255, 255, 0, 0.4)';
            var background_line_width = 7;

            // On highlight.
            var highlight_line_style = "#f00";
            var background_highlight_line_style = 'rgba(0, 255, 255, 0.4)';

            var length_font_pixels = 12;
            var angle_font_pixels = 10;
            var loupe_magnification = 7;
            var end_bracket_len = 5;

            // These variables need to be cut down and partially be private
            // to the modules.
            var help_system;
            var aug_view;
            var backgroundImage; // if loaded. Also used by the loupe.

            // Init function. Call once on page-load.
            function augenmass_init() {
                help_system = new HelpSystem(document.getElementById('helptext'));
                aug_view = new AugenmassView(document.getElementById('measure'));

                var show_delta_checkbox = document.getElementById('show-deltas');
                show_delta_checkbox.addEventListener("change", function(e) {
                    aug_view.setShowDeltas(show_delta_checkbox.checked);
                    aug_view.drawAll();
                });

                var show_angle_checkbox = document.getElementById('show-angles');
                show_angle_checkbox.addEventListener("change", function(e) {
                    aug_view.setShowAngles(show_angle_checkbox.checked);
                    aug_view.drawAll();
                });

                loupe_canvas = document.getElementById('loupe');
                loupe_canvas.style.left = document.body.clientWidth - loupe_canvas.width - 10;
                loupe_ctx = loupe_canvas.getContext('2d');
                // We want to see the pixels:
                loupe_ctx.imageSmoothingEnabled = false;
                loupe_ctx.mozImageSmoothingEnabled = false;
                loupe_ctx.webkitImageSmoothingEnabled = false;
                aug_view.resetWithSize(10, 10); // Some default until we have an image.

                var chooser = document.getElementById("file-chooser");
                chooser.addEventListener("change", function(e) {
                    load_background_image(chooser);
                });

                var download_link = document.getElementById('download-result');
                download_link.addEventListener('click', function() {
                    download_result(download_link)
                }, false);
                download_link.style.opacity = 0; // not visible at first.
                download_link.style.cursor = "default";
            }

            function AugenmassController(canvas, view) {
                // This doesn't have any public methods.
                this.start_line_time_ = 0;

                var self = this;
                canvas.addEventListener("mousedown", function(e) {
                    extract_event_pos(e, function(e, x, y) {
                        self.onClick(e, x, y);
                    });
                });
                canvas.addEventListener("contextmenu", function(e) {
                    e.preventDefault();
                });
                canvas.addEventListener("mousemove", function(e) {
                    theMouseEventCallback(true);
                    extract_event_pos(e, onMove);
                });

                canvas.addEventListener ("mouseout", function(e) {
                    theMouseEventCallback(false);
                });

                // canvas.addEventListener("dblclick", function(e) {
                //   extract_event_pos(e, onDoubleClick);
                // });
                document.addEventListener("keydown", onKeyEvent);

                function extract_event_pos(e, callback) {
                // browser and scroll-independent extraction of mouse cursor in canvas.
                    var x, y;
                    if (e.pageX != undefined && e.pageY != undefined) {
                        x = e.pageX;
                        y = e.pageY;
                    } else {
                        x = e.clientX + scrollLeft();
                        y = e.clientY + scrollY();
                    }
                    x -= canvas.offsetLeft;
                    y -= canvas.offsetTop;

                    // Yang 
                    //  Adjustment for showing it in modal window.
                    x -= parentBoundingRect.left;
                    y -= parentOffsetY;
                    y -= modalBoundingRectTop;

                    callback(e, x, y);
                }

                function getModel() {
                    return view.getModel();
                }

                function getView() {
                    return view;
                }

                function cancelCurrentLine() {
                    if (getModel().hasEditLine()) {
                        getModel().forgetEditLine();
                        getView().drawAll();
                    }
                }

                function onKeyEvent(e) {
                    if (e.keyCode == 27) { // ESC key.
                        cancelCurrentLine();
                    }
                }

                function onMove(e, x, y) {
                    if (backgroundImage === undefined){
                        return;
                    }
                    var has_editline = getModel().hasEditLine();
                    if (has_editline) {
                        getModel().updateEditLine(x, y);
                    }
                    // showFadingLoupe(x, y);
                    showLoupe(x, y);

                    if (!has_editline){
                        return;
                    }
                    getView().drawAll();
                }

                this.onClick = function(e, x, y) {
                    if (e.which != undefined && e.which == 3) {
                        // right mouse button.
                        cancelCurrentLine();
                        return;
                    }
                    var now = new Date().getTime();
                    if (!getModel().hasEditLine()) {
                        getModel().startEditLine(x, y);
                        this.start_line_time_ = now;
                        help_system.achievementUnlocked(HelpLevelEnum.DONE_START_LINE);
                    } else {
                        var line = getModel().updateEditLine(x, y);
                        // Make sure that this was not a double-click event.
                        // (are there better ways ?)
                        if (line.length() > 50 || (line.length() > 0 && (now - this.start_line_time_) > 500)) {
                            getModel().commitEditLine();
                            help_system.achievementUnlocked(HelpLevelEnum.DONE_FINISH_LINE);
                        } else {
                            getModel().forgetEditLine();
                        }
                    }
                    getView().drawAll();
                }

                function onDoubleClick(e, x, y) {
                    cancelCurrentLine();
                    var selected_line = getModel().findClosest(x, y);

                    if (selected_line == undefined){
                        return;
                    }

                    getView().highlightLine(selected_line);
                    
                    var orig_len_txt = (getView().getUnitsPerPixel() * selected_line.length()).toPrecision(4);
                    var new_value_txt = prompt("Length of selected line ?", orig_len_txt);
                    if (orig_len_txt != new_value_txt) {
                        var new_value = parseFloat(new_value_txt);
                        if (new_value && new_value > 0) {
                        getView().setUnitsPerPixel(new_value / selected_line.length());
                        }
                    }
                    help_system.achievementUnlocked(HelpLevelEnum.DONE_SET_LEN);
                    getView().drawAll();
                }
            }

            function scrollTop() {
                return document.body.scrollTop + document.documentElement.scrollTop;
            }

            function scrollLeft() {
                return document.body.scrollLeft + document.documentElement.scrollLeft;
            }

            function init_download(filename) {
                var pos = filename.lastIndexOf(".");
                if (pos > 0) {
                    filename = filename.substr(0, pos);
                }
                var download_link = document.getElementById('download-result');
                download_link.download = "augenmass-" + filename + ".png";
                download_link.style.cursor = "pointer";
                download_link.style.opacity = 1;
            }

            function download_result(download_link) {
                if (backgroundImage === undefined){
                    return;
                }
                aug_view.drawAll();
                download_link.href = aug_view.getCanvas().toDataURL('image/png');
            }

            function load_background_image(chooser) {
                if (chooser.value == "" || !chooser.files[0].type.match(/image.*/)){
                    return;
                }

                var img_reader = new FileReader();
                img_reader.readAsDataURL(chooser.files[0]);
                img_reader.onload = function(e) {
                    var new_img = new Image();
                    // Image loading in the background canvas. Once we have the image, we
                    // can size the canvases to a proper size.
                    var background_canvas = document.getElementById('background-img');
                    new_img.onload = function() {
                        var bg_context = background_canvas.getContext('2d');
                        background_canvas.width = new_img.width;
                        background_canvas.height = new_img.height;
                        bg_context.drawImage(new_img, 0, 0);

                        aug_view.resetWithSize(new_img.width, new_img.height);

                        help_system.achievementUnlocked(HelpLevelEnum.DONE_FILE_LOADING);
                        backgroundImage = new_img;
                        init_download(chooser.files[0].name);
                    }
                    new_img.src = e.target.result;
                }
            }            
            // end of augenmass-main

            // augenmass-element
            // Some useful function to have :)
            function euklid_distance(x1, y1, x2, y2) {
                return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
            }

            function Point(x, y) {
                this.update = function(x, y) {
                    this.x = x;
                    this.y = y;
                }

                // key to be used in hash tables.
                this.get_key = function() {
                    return this.x + ":" + this.y;
                }

                this.update(x, y);
            }

            function Line(x1, y1, x2, y2) {
                // The canvas coordinate system numbers the space _between_ pixels
                // as full coordinage. Correct for that.
                this.p1 = new Point(x1 + 0.5, y1 + 0.5);
                this.p2 = new Point(x2 + 0.5, y2 + 0.5);

                // While editing: updating second end of the line.
                this.updatePos = function(x2, y2) {
                    this.p2.update(x2 + 0.5, y2 + 0.5);
                }

                // Helper for determining selection: how far is the given position from the
                // center text.
                this.distanceToCenter = function(x, y) {
                    var centerX = (this.p1.x + this.p2.x)/2;
                    var centerY = (this.p1.y + this.p2.y)/2;
                    return euklid_distance(centerX, centerY, x, y);
                }

                this.length = function() {
                    return euklid_distance(this.p1.x, this.p1.y, this.p2.x, this.p2.y);
                }
            }

            // Represents the (CCW) angle of the line between "center_p" center point and
            // point "remote_p" with respect to the horizontal line.
            function Angle(center_p, remote_p, line) {
                this.center = center_p;
                this.p = remote_p;
                this.line = line;
                this.angle = undefined;
                this.is_valid = false;

                this.arm_length = function() {
                    return euklid_distance(this.center.x, this.center.y, this.p.x, this.p.y);
                }

                // Whenever points change, this needs to be called to re-calculate the
                // angle.
                this.notifyPointsChanged = function() {
                    var len = euklid_distance(this.center.x, this.center.y, this.p.x, this.p.y);
                    if (len == 0.0) {
                        this.is_valid = false;
                        return;
                    }
                    var dx = this.p.x - this.center.x;
                    var angle = Math.acos(dx/len);
                    if (this.p.y > this.center.y) {
                        angle = 2 * Math.PI - angle;
                    }
                    this.angle = angle;
                    this.is_valid = true;
                }

                this.notifyPointsChanged();
            }

            // Represents an arc with the given start and end-angle. Does not react
            // on updates in the angle object.
            function Arc(angle1, angle2) {
                this.center = angle1.center;
                this.start = angle1.angle;
                this.end = angle2.angle;
                if (this.end < this.start) {
                this.end += 2 * Math.PI;
                }

                // Hint for the drawing.
                // We want the drawn arc use at maximum half the lenght of the arm. That
                // way, two adjacent arcs on each endpoint of the arm are rendered
                // without overlap.
                this.max_radius = Math.min(angle1.arm_length(), angle2.arm_length()) / 2;

                // Printable value in the range [0..360)
                this.angleInDegrees = function() {
                var a = 180.0 * (this.end - this.start) / Math.PI;
                if (a < 0) a += 360;
                    return a;
                }
            }            
            // end of augenmass-element

            // augenmass-help
            // We show different help levels. After each stage the user successfully
            // performs, the next level is shown. Once the user managed all of them,
            // we're fading into silency.
            var HelpLevelEnum = {
                DONE_FILE_LOADING:  0,
                DONE_START_LINE:    1,
                DONE_FINISH_LINE:   2,
                DONE_ADD_ANGLE:     3,
                DONE_SET_LEN:       4,
            };

            // Constructor function.
            function HelpSystem(helptext_span) {
                this.last_help_level_ = -1;

                // Initial text.
                printHelp("(Only your browser reads the image. It is not uploaded anywhere.)");

                this.achievementUnlocked = function(level) {
                    if (level < this.last_help_level_){
                        return;
                    }
                    this.last_help_level_ = level;
                    printLevel(level);
                }

                function printLevel(requested_level) {
                    switch (requested_level) {
                    case HelpLevelEnum.DONE_FILE_LOADING:
                        printHelp("Click somewhere to start a line.");
                        break;
                    case HelpLevelEnum.DONE_START_LINE:
                        printHelp("A second click finishes the line. Or Cancel with 'Esc'.");
                        break;
                    case HelpLevelEnum.DONE_FINISH_LINE:
                        printHelp("TIP: starting a line where another ends measures their angles.");
                        break;
                    case HelpLevelEnum.DONE_ADD_ANGLE:
                        printHelp("Double click on center of line to set relative size.");
                        break;
                    case HelpLevelEnum.DONE_SET_LEN:
                        printHelp("Congratulations - you are an expert now!", true);
                        break;
                    }
                }

                function printHelp(help_text, fade_away) {
                    // if (help_text == undefined) return;
                    // while (helptext_span.firstChild) {
                    //     helptext_span.removeChild(helptext.firstChild);
                    // }
                    // helptext_span.appendChild(document.createTextNode(help_text));
                    
                    // if (fade_away != undefined && fade_away) {
                    //     helptext_span.style.transition = "opacity 10s";
                    //     helptext_span.style.opacity = 0;
                    // }
                }
            }            
            // end of augenmass-help


            // augenmass-loupe
            // TODO: make this an object. Needs to have access to the
            // background image as well.
            var loupe_canvas;
            var loupe_ctx;
            var loupe_fading_timer;

            // Helper to show the 'corner hooks' in the loupe display.
            function showQuadBracket(loupe_ctx, loupe_size, bracket_len) {
                loupe_ctx.moveTo(0, bracket_len);                 // top left.
                loupe_ctx.lineTo(bracket_len, bracket_len);
                loupe_ctx.lineTo(bracket_len, 0);
                loupe_ctx.moveTo(0, loupe_size - bracket_len);   // bottom left.
                loupe_ctx.lineTo(bracket_len, loupe_size - bracket_len);
                loupe_ctx.lineTo(bracket_len, loupe_size);
                loupe_ctx.moveTo(loupe_size - bracket_len, 0);     // top right.
                loupe_ctx.lineTo(loupe_size - bracket_len, bracket_len);
                loupe_ctx.lineTo(loupe_size, bracket_len);         // bottom right.
                loupe_ctx.moveTo(loupe_size - bracket_len, loupe_size);
                loupe_ctx.lineTo(loupe_size - bracket_len, loupe_size - bracket_len);
                loupe_ctx.lineTo(loupe_size, loupe_size - bracket_len);
            }

            function drawLoupeLine(ctx, line, off_x, off_y, factor) {
                // these 0.5 offsets seem to look inconclusive on Chrome and Firefox.
                // Need to go deeper.
                ctx.beginPath();
                var x1pos = (line.p1.x - off_x), y1pos = (line.p1.y - off_y);
                var x2pos = (line.p2.x - off_x), y2pos = (line.p2.y - off_y);
                ctx.moveTo(x1pos * factor, y1pos * factor);
                ctx.lineTo(x2pos * factor, y2pos * factor);
                ctx.stroke();
                // We want circles that circumreference the pixel in question.
                ctx.beginPath();
                ctx.arc(x1pos * factor + 0.5, y1pos * factor + 0.5,
                    1.5 * factor/2, 0, 2*Math.PI);
                ctx.stroke();
                ctx.beginPath();
                ctx.arc(x2pos * factor + 0.5, y2pos * factor + 0.5,
                    1.5 * factor/2, 0, 2*Math.PI);
                ctx.stroke();
            }

            function showLoupe(x, y) {
                if (backgroundImage === undefined || loupe_ctx === undefined){
                    return;
                }

                // Yang.
                // We have adjusted the original image based on canvis max size.
                // the following adjustment is to compensate for the original image adjustment
                x = x / imageShrinkRatio;
                y = y / imageShrinkRatio;


                // if we can fit the loupe right of the image, let's do it. Otherwise
                // it is in the top left corner, with some volatility to escape the cursor.
                var cursor_in_frame_x = x - scrollLeft();
                var cursor_in_frame_y = y - scrollTop() + aug_view.getCanvas().offsetTop;

                // Let's see if we have any overlap with the loupe - if so, move it
                // out of the way.
                var top_default = 10;
                var left_loupe_edge = document.body.clientWidth - loupe_canvas.width - 10;
                if (backgroundImage.width + 40 < left_loupe_edge)
                left_loupe_edge = backgroundImage.width + 40;
                loupe_canvas.style.left = left_loupe_edge;

                // Little hysteresis while moving in and out
                if (cursor_in_frame_x > left_loupe_edge - 20 && cursor_in_frame_y < loupe_canvas.height + top_default + 20) {
                    loupe_canvas.style.top = loupe_canvas.height + top_default + 60;
                } else if (cursor_in_frame_x < left_loupe_edge - 40 || cursor_in_frame_y > loupe_canvas.height + top_default + 40) {
                    loupe_canvas.style.top = top_default;
                }

                var loupe_size = loupe_ctx.canvas.width;

                var img_max_x = (backgroundImage.width / imageShrinkRatio )- 1;
                var img_max_y = (backgroundImage.height / imageShrinkRatio )- 1;
                var crop_size = loupe_size/loupe_magnification;

                var start_x = x - crop_size/2;
                var start_y = y - crop_size/2;

                var off_x = 0, off_y = 0;
                if (start_x < 0) { 
                    off_x = -start_x; start_x = 0; 
                }
                if (start_y < 0) { 
                    off_y = -start_y; start_y = 0; 
                }
                var end_x = x + crop_size/2;
                var end_y = y + crop_size/2;
                end_x = end_x < img_max_x ? end_x : img_max_x;
                end_y = end_y < img_max_y ? end_y : img_max_y;
                var crop_w = (end_x - start_x) + 1;
                var crop_h = (end_y - start_y) + 1;
                loupe_ctx.fillStyle = "#777";
                loupe_ctx.fillRect(0, 0, loupe_size, loupe_size);
                off_x -= 0.5;
                off_y -= 0.5;

                loupe_ctx.drawImage(backgroundImage,
                    start_x, start_y, crop_w, crop_h,
                    off_x * loupe_magnification, off_y * loupe_magnification,
                    loupe_magnification * crop_w,
                    loupe_magnification * crop_h);

                loupe_ctx.beginPath();
                loupe_ctx.strokeStyle = 'rgba(255, 255, 255, 0.3)';
                loupe_ctx.lineWidth = 1;
                // draw four brackets enclosing the pixel in question.
                var bracket_len = (loupe_size - loupe_magnification)/2;
                showQuadBracket(loupe_ctx, loupe_size, bracket_len);
                loupe_ctx.stroke();
                loupe_ctx.beginPath();
                loupe_ctx.strokeStyle = 'rgba(0, 0, 0, 0.3)';
                showQuadBracket(loupe_ctx, loupe_size, bracket_len - 1);
                loupe_ctx.stroke();

                loupe_ctx.beginPath();
                loupe_ctx.fillStyle = "#000";
                loupe_ctx.fillText("(" + x + "," + y + ")", 10, 30);
                loupe_ctx.stroke();

                // TODO: we want the loupe-context be scaled anyway, and have this a 
                // translation.
                var l_off_x = x - crop_size/2 + 0.5
                var l_off_y = y - crop_size/2 + 0.5;
                // Draw all the lines in the loupe; better 'high resolution' view.
                for (var style = 0; style < 2; ++style) {
                    switch (style) {
                        case 0:
                            loupe_ctx.strokeStyle = background_line_style;
                            loupe_ctx.lineWidth = loupe_magnification;
                            break;
                        case 1:
                            loupe_ctx.strokeStyle = line_style;
                            loupe_ctx.lineWidth = 1;
                            break;
                    }
                    var model = aug_view.getModel();
                    model.forAllLines(function(line) {
                        drawLoupeLine(loupe_ctx, line, l_off_x, l_off_y, loupe_magnification);
                    });
                    if (model.hasEditLine()) {
                        drawLoupeLine(loupe_ctx, model.getEditLine(), l_off_x, l_off_y, loupe_magnification);
                    }
                }
            }

            function showFadingLoupe(x, y) {
                if (loupe_fading_timer != undefined){
                    clearTimeout(loupe_fading_timer);   // stop scheduled fade-out.
                }
                loupe_canvas.style.transition = "top 0.3s, opacity 0s";
                loupe_canvas.style.opacity = 1;
                showLoupe(x, y);
                // Stay a couple of seconds, then fade away.
                loupe_fading_timer = setTimeout(function() {
                    loupe_canvas.style.transition = "top 0.3s, opacity 5s";
                    loupe_canvas.style.opacity = 0;
                }, 8000);
            }            
            // end of augenmass-loupe

            // augenmass-model
            // Conctructor function.
            function AugenmassModel() {
                "use strict";

                // Yang 
                this.hasLines = function() {
                    return this.lines_.length;
                };

                this.init = function() {
                    this.lines_ = new Array();
                    this.point_angle_map_ = {};  // points to lines originating from it.
                    this.current_line_ = undefined;
                    this.current_angle_ = undefined;

                };

                this.init();

                // -- editing operation. We start a line and eventually commit or forget it.

                // Start a new line but does not add it to the model yet.
                this.startEditLine = function(x, y) {
                    var line = new Line(x, y, x, y);
                    this.current_line_ = line;
                    this.current_angle_ = this.addAngle(line.p1, line.p2, line);
                }
                this.hasEditLine = function() { 
                    return this.current_line_ != undefined; 
                }
                this.getEditLine = function() { 
                    return this.current_line_; 
                }

                this.updateEditLine = function(x, y) {
                    if (this.current_line_ == undefined){
                        return;
                    }
                    this.current_line_.updatePos(x, y);
                    this.current_angle_.notifyPointsChanged();
                    return this.current_line_;
                }

                this.commitEditLine = function() {
                    var line = this.current_line_;
                    this.lines_[this.lines_.length] = line;
                    this.addAngle(line.p2, line.p1, line);
                    this.current_line_ = undefined;
                }

                this.forgetEditLine = function() {
                    if (this.current_line_ == undefined){
                        return;
                    }
                    this.removeAngle(this.current_line_.p1, this.current_line_);
                    this.current_line_ = undefined;
                }

                this.addAngle = function(center, p2, line) {
                    var key = center.get_key();
                    var angle_list = this.point_angle_map_[key];
                    if (angle_list === undefined) {
                        angle_list = new Array();
                        this.point_angle_map_[key] = angle_list;
                    }
                    var angle = new Angle(center, p2, line);
                    angle_list[angle_list.length] = angle;
                    return angle;
                }

                this.removeAngle = function(center_point, line) {
                    var key = center_point.get_key();
                    var angle_list = this.point_angle_map_[key];
                    if (angle_list === undefined){
                        return;
                    } // shrug.
                    var pos = -1;
                    for (var i = 0; i < angle_list.length; ++i) {
                        if (angle_list[i].line == line) {
                            pos = i;
                            break;
                        }
                    }
                    if (pos >= 0) {
                        angle_list.splice(pos, 1);
                    }
                }

                // Remove a line
                this.removeLine = function(line) {
                    var pos = this.lines_.indexOf(line);
                    if (pos < 0){
                        alert("Should not happen: Removed non-existent line")
                    };
                    this.lines_.splice(pos, 1);
                }

                // Find the closest line to the given coordinate or 'undefined', if they
                // are all too remote.
                this.findClosest = function(x, y) {
                    var smallest_distance = undefined;
                    var selected_line = undefined;
                    this.forAllLines(function(line) {
                        var this_distance = line.distanceToCenter(x, y);
                        if (smallest_distance == undefined
                        || this_distance < smallest_distance) {
                        smallest_distance = this_distance;
                        selected_line = line;
                        }
                    })
                    if (selected_line && smallest_distance < 50) {
                        return selected_line;
                    }
                    return undefined;
                }

                // Iterate over all lines; Callback needs to accept a line.
                this.forAllLines = function(cb) {
                    for (var i = 0; i < this.lines_.length; ++i) {
                        cb(this.lines_[i]);
                    }
                }

                this.forAllArcs = function(cb) {
                    for (var key in this.point_angle_map_) {
                        if (!this.point_angle_map_.hasOwnProperty(key))
                        continue;
                        var angle_list = this.point_angle_map_[key];
                        if (angle_list.length < 2)
                        continue;
                        angle_list.sort(function(a, b) {
                        return a.angle - b.angle;
                        });
                        for (var i = 0; i < angle_list.length; ++i) {
                        var a = angle_list[i], b = angle_list[(i+1) % angle_list.length];
                        if (!a.is_valid || !b.is_valid)
                            continue;
                        var arc = new Arc(a, b);
                        if (arc.angleInDegrees() >= 180.0)
                            continue;
                        cb(arc)
                        }
                    }
                }
            }            
            // end of augenmass-model

            // augenmass-view
            // Constructor function.
            function AugenmassView(canvas) {
                "use strict";
                this.measure_canvas_ = canvas;
                this.measure_ctx_ = this.measure_canvas_.getContext('2d');
                this.model_ = undefined;
                this.controller_ = undefined;
                this.print_factor_ = 1.0;  // Semantically, this one should probably be in the model.
                this.show_deltas_ = false;
                this.show_angles_ = true;

                // Create a fresh measure canvas of the given size.
                this.resetWithSize = function(width, height) {
                    this.measure_canvas_.width = width;
                    this.measure_canvas_.height = height;
                    this.measure_ctx_.font = 'bold ' + length_font_pixels + 'px Sans Serif';

                    this.print_factor_ = 1.0;
                    // A fresh model.
                    this.model_ = new AugenmassModel();
                    if (this.controller_ == undefined) {
                        this.controller_ = new AugenmassController(canvas, this);
                    }
                }

                this.getUnitsPerPixel = function() { return this.print_factor_; }
                this.setUnitsPerPixel = function(factor) { this.print_factor_ = factor; }
                this.setShowDeltas = function(b) { this.show_deltas_ = b; }
                this.setShowAngles = function(b) { this.show_angles_ = b; }

                this.getModel = function() { return this.model_; }
                this.getCanvas = function() { return this.measure_canvas_; }

                // Draw all the lines!
                this.drawAll = function() {
                    this.measure_ctx_.clearRect(0, 0, this.measure_canvas_.width, this.measure_canvas_.height);
                    this.drawAllNoClear(this.measure_ctx_);
                }

                this.highlightLine = function(line) {
                    drawMeasureLine(this.measure_ctx_, line, this.print_factor_, this.show_deltas_, true);
                }

                this.drawAllNoClear = function(ctx) {
                    this.measure_ctx_.font = 'bold ' + length_font_pixels + 'px Sans Serif';
                    var length_factor = this.print_factor_;
                    var show_deltas = this.show_deltas_;
                    this.model_.forAllLines(function(line) {
                        drawMeasureLine(ctx, line, length_factor, show_deltas, false);
                    });
                    if (this.model_.hasEditLine()) {
                        var line = this.model_.getEditLine();
                        drawEditline(ctx, line, this.print_factor_);
                        if (this.show_deltas_) {
                        drawDeltaLine(ctx, line, this.print_factor_);
                        }
                    }
                    if (this.show_angles_) {
                        this.measure_ctx_.font = angle_font_pixels + "px Sans Serif";
                        // Radius_fudge makes the radius of the arc slighly different
                        // for all angles around one center so that they are easier
                        // to distinguish.
                        var radius_fudge = 0;
                        var current_point = undefined;
                        var any_arc = false;
                        this.model_.forAllArcs(function(arc) {
                        if (current_point == undefined
                            || current_point.x != arc.center.x
                            || current_point.y != arc.center.y) {
                            current_point = arc.center;
                            radius_fudge = 0;
                        }
                        drawArc(ctx, arc, (radius_fudge++ % 3) * 3);
                        any_arc = true;
                        });
                        if (any_arc) {
                        help_system.achievementUnlocked(HelpLevelEnum.DONE_ADD_ANGLE);
                        }
                    }
                }

                // Write rotated text, aligned to the outside.
                function writeRotatedText(ctx, txt, x, y, radius, angle) {
                    ctx.save();
                    ctx.beginPath();
                    ctx.translate(x, y);
                    ctx.strokeStyle = background_line_style;
                    ctx.lineWidth = angle_font_pixels;
                    ctx.lineCap = 'butt';   // should end flush with the arc.
                    ctx.moveTo(0, 0);
                    if (angle <= Math.PI/2 || angle > 3 * Math.PI/2) {
                        ctx.rotate(-angle);   // JavaScript, Y U NO turn angles left.
                        ctx.textAlign = 'right';
                        ctx.textBaseline = 'middle';
                        ctx.lineTo(radius, 0);
                        ctx.stroke();
                        ctx.fillText(txt, radius, 0);
                    } else {
                        // Humans don't like to read text upside down
                        ctx.rotate(-(angle + Math.PI));
                        ctx.textAlign = 'left';
                        ctx.textBaseline = 'middle';
                        ctx.lineTo(-radius, 0);
                        ctx.stroke();
                        ctx.fillText(txt, -radius, 0);
                    }
                    ctx.restore();
                }

                function drawArc(ctx, arc, radius_fiddle) {
                    var text_len = ctx.measureText("333.3\u00B0").width;
                    var radius = text_len + 2 * end_bracket_len + radius_fiddle;
                    ctx.beginPath();
                    ctx.lineWidth = background_line_width;
                    ctx.strokeStyle = background_line_style;
                    // Javascript turns angles right not left. Ugh.
                    ctx.arc(arc.center.x, arc.center.y, Math.min(radius, arc.max_radius),
                        2 * Math.PI - arc.end, 2 * Math.PI - arc.start);
                    ctx.stroke();

                    ctx.beginPath();
                    ctx.lineWidth = 1;
                    ctx.strokeStyle = "#000";
                    ctx.arc(arc.center.x, arc.center.y, Math.min(radius, arc.max_radius),
                        2 * Math.PI - arc.end, 2 * Math.PI - arc.start);
                    ctx.stroke();

                    var middle_angle = (arc.end - arc.start)/2 + arc.start;
                    writeRotatedText(ctx, arc.angleInDegrees().toFixed(1) + "\u00B0",
                            arc.center.x, arc.center.y,
                            radius - 2, middle_angle);
                }


                // Draw a perpendicular (to the line) end-piece ("T") at position x, y.
                function drawT(ctx, x, y, remote_x, remote_y, t_len, optional_gap) {
                    var len = euklid_distance(x, y, remote_x, remote_y);
                    if (len < 1){
                        return;
                    }
                    var dx = remote_x - x;
                    var dy = remote_y - y;
                    if (optional_gap === undefined) {
                        ctx.moveTo(x - t_len * dy/len, y + t_len * dx/len);
                        ctx.lineTo(x + t_len * dy/len, y - t_len * dx/len);
                    } else {
                        ctx.moveTo(x - t_len * dy/len, y + t_len * dx/len);
                        ctx.lineTo(x - optional_gap * dy/len, y + optional_gap * dx/len);
                        ctx.moveTo(x + t_len * dy/len, y - t_len * dx/len);
                        ctx.lineTo(x + optional_gap * dy/len, y - optional_gap * dx/len);
                    }
                }

                // Drawing the line while editing.
                // We only show the t-anchor on the start-side. Also the line is
                // 1-2 pixels shorter where the mouse-cursor is, so that we don't cover
                // anything in the target crosshair.
                function drawEditline(ctx, line, length_factor) {
                    var pixel_len = line.length();

                    // We want to draw the line a little bit shorter, so that the
                    // open crosshair cursor has 'free sight'
                    var dx = line.p2.x - line.p1.x;
                    var dy = line.p2.y - line.p1.y;
                    if (pixel_len > 2) {
                        dx = dx * (pixel_len - 2)/pixel_len;
                        dy = dy * (pixel_len - 2)/pixel_len;
                    }

                    // Background for t-line
                    ctx.beginPath();
                    ctx.strokeStyle = background_line_style;
                    ctx.lineWidth = background_line_width;
                    ctx.lineCap = 'round';
                    drawT(ctx, line.p1.x, line.p1.y, line.p2.x, line.p2.y,
                        end_bracket_len);
                    ctx.stroke();

                    // White background for actual line
                    ctx.beginPath();
                    ctx.lineCap = 'butt';  // Flat to not bleed into crosshair.
                    ctx.moveTo(line.p1.x, line.p1.y);
                    ctx.lineTo(line.p1.x + dx, line.p1.y + dy);
                    ctx.stroke();

                    // t-line ...
                    ctx.beginPath();
                    ctx.strokeStyle = '#000';
                    ctx.lineWidth = 0.5;
                    ctx.lineCap = 'butt';
                    drawT(ctx, line.p1.x, line.p1.y, line.p2.x, line.p2.y, 50);
                    // Leave a little gap at the line where the cursor is to leave
                    // free view to the surroundings.
                    drawT(ctx, line.p2.x, line.p2.y, line.p1.x, line.p1.y, 50, 10);
                    ctx.stroke();

                    // ... and actual line.
                    ctx.beginPath();
                    ctx.strokeStyle = '#00F';
                    ctx.lineWidth = 1;
                    ctx.moveTo(line.p1.x, line.p1.y);
                    ctx.lineTo(line.p1.x + dx, line.p1.y + dy);
                    ctx.stroke();

                    if (pixel_len >= 2) {
                        var print_text = (length_factor * pixel_len).toPrecision(4);
                        var text_len = ctx.measureText(print_text).width + 2 * length_font_pixels;
                        // Print label.
                        // White background for text. We're using a short line, so that we
                        // have a nicely rounded box with our line-cap.
                        var text_dx = -text_len/2;
                        var text_dy = -(length_font_pixels + 10)/2;
                        if (pixel_len > 0) {
                        text_dx = -dx * text_len/(2 * pixel_len);
                        text_dy = -dy * (length_font_pixels + 10)/(2 * pixel_len);
                        }
                        writeLabel(ctx, print_text, line.p1.x + text_dx, line.p1.y + text_dy,
                                "center");
                    }
                }

                // General draw of a measuring line.
                function drawMeasureLine(ctx, line, length_factor, show_deltas, highlight) {
                    var print_text = (length_factor * line.length()).toPrecision(4);
                    if (show_deltas && line.p1.x != line.p2.x && line.p1.y != line.p2.y) {
                        var dx = length_factor * (line.p2.x - line.p1.x);
                        var dy = length_factor * (line.p1.y - line.p2.y);
                        print_text += "; \u0394=(" + Math.abs(dx).toPrecision(4) + ", "
                        + Math.abs(dy).toPrecision(4) + ")";
                    }
                    ctx.beginPath();
                    // Some contrast background.
                    if (highlight) {
                        ctx.strokeStyle = background_highlight_line_style;
                    } else {
                        ctx.strokeStyle = background_line_style;
                    }
                    ctx.lineWidth = background_line_width;
                    ctx.lineCap = 'round';
                    ctx.moveTo(line.p1.x, line.p1.y);
                    ctx.lineTo(line.p2.x, line.p2.y);
                    drawT(ctx, line.p1.x, line.p1.y, line.p2.x, line.p2.y,
                        end_bracket_len);  
                    drawT(ctx, line.p2.x, line.p2.y, line.p1.x, line.p1.y,
                        end_bracket_len);  
                    ctx.stroke();

                    ctx.beginPath();
                    // actual line
                    if (highlight) {
                        ctx.strokeStyle = highlight_line_style;
                    } else {
                        ctx.strokeStyle = line_style;
                    }
                    ctx.lineWidth = 1;
                    ctx.moveTo(line.p1.x, line.p1.y);
                    ctx.lineTo(line.p2.x, line.p2.y);
                    drawT(ctx, line.p1.x, line.p1.y, line.p2.x, line.p2.y,
                        end_bracket_len);  
                    drawT(ctx, line.p2.x, line.p2.y, line.p1.x, line.p1.y,
                        end_bracket_len);
                    ctx.stroke();

                    // .. and text.
                    var a = new Angle(line.p1, line.p2, line);
                    var slope_upwards = ((a.angle > 0 && a.angle < Math.PI/2)
                                || (a.angle > Math.PI && a.angle < 3 * Math.PI/2));
                    var flat_angle = Math.PI/10;
                    var flat_slope = ((a.angle > 0 && a.angle < flat_angle)
                            || (a.angle > Math.PI/2 && a.angle < (Math.PI/2 + flat_angle)));
                    writeLabel(ctx, print_text,
                            (line.p1.x + line.p2.x)/2, (line.p1.y + line.p2.y)/2 - 10,
                            flat_slope ? "center" : (slope_upwards ? "right" : "left"));
                }

                // Write a label with a contrasty background.
                function writeLabel(ctx, txt, x, y, alignment) {
                    ctx.font = 'bold ' + length_font_pixels + 'px Sans Serif';
                    ctx.textBaseline = 'middle';
                    ctx.textAlign = alignment;

                    ctx.beginPath();
                    var dx = ctx.measureText(txt).width;
                    ctx.lineWidth = length_font_pixels + 5;  // TODO: find from style.
                    ctx.lineCap = 'round';
                    ctx.strokeStyle = background_line_style;
                    var line_x = x;
                    if (alignment == 'center') {
                        line_x -= dx/2;
                    }
                    else if (alignment == 'right') {
                        line_x -= dx;
                    }
                    ctx.moveTo(line_x, y);
                    ctx.lineTo(line_x + dx, y);
                    ctx.stroke();
                    ctx.fillStyle = '#000';
                    ctx.fillText(txt, x, y);
                }

                function drawDeltaLine(ctx, line, length_factor, highlight) {
                    if (line.p1.x == line.p2.x || line.p1.y == line.p2.y)
                        return;

                    var non_overlap_target = 30;
                    var dy = line.p2.y - line.p1.y;
                    var dx = line.p2.x - line.p1.x;
                    ctx.beginPath();
                    ctx.lineWidth = 0.5;
                    ctx.strokeStyle = "#000";
                    ctx.moveTo(line.p1.x, line.p1.y);
                    ctx.lineTo(line.p2.x, line.p1.y);
                    if (Math.abs(dy) > non_overlap_target) {
                        var line_y = line.p1.y + dy - ((dy < 0) ? -non_overlap_target : non_overlap_target);
                        ctx.lineTo(line.p2.x, line_y);
                    }
                    ctx.stroke();

                    var hor_len = length_factor * Math.abs(dx);
                    var hor_align = "center";
                    if (dx <= 0 && dx > -80) hor_align = "right";
                    else if (dx >= 0 && dx < 80) hor_align = "left";
                    writeLabel(ctx, hor_len.toPrecision(4), (line.p1.x + line.p2.x)/2,
                            line.p1.y + ((dy > 0) ? -20 : 20), hor_align);
                    var vert_len = length_factor * Math.abs(dy);
                    writeLabel(ctx, vert_len.toPrecision(4),
                            line.p2.x + (dx > 0 ? 10 : -10),
                            (line.p1.y + line.p2.y)/2,
                            line.p1.x < line.p2.x ? "left" : "right");
                }
            }            
            // end of angenmass-view
        }
    ]);
})(window, window.angular);