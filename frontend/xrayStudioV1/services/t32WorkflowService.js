(function (window, angular, undefined) {'use strict';
    angular.module('app').service('t32WorkflowService', [
                '$q',
                '$timeout',
                '$modal',
                'logger',
                'SweetAlert',
                't32SystemService',
        function (
                $q,
                $timeout,
                $modal,
                logger,
                SweetAlert,
                t32SystemService ) {

            /**
             */
            this.ensureLicense = function( ) {
                var deferred = $q.defer();

                var theSetting;
                t32SystemService.getXrayStudioSetting()
                .then(function(setting){
                    theSetting = setting;
                    if ( setting && setting.license )
                    {
                        deferred.resolve()
                    }
                    else
                    {
                        collectLicenseFromUser()
                        .then(function(license){
                            return activateLicense(theSetting,license );
                        })
                        .then(function(){
                            deferred.resolve();
                        })
                        .catch(function(err){
                            logger.error(err);
                            deferred.reject(err);
                        })
                    }
                });
                return deferred.promise;
            };

            function collectLicenseFromUser() {
                var title = 'Enter License Number';
                var noteList = [
                  'License can be acquired from tab32 EHR, by ',
                  'Login into http://www.tab32.com',
                  'Click on Setting',
                  'Click on Download Xray Software',
                  'Click on Manage Xray Studio License'
                ];

                return t32SystemService.collectSimpleAnswer(title, noteList);
            }

            function activateLicense( setting, license ) {
                var deferred = $q.defer();

                logger.debug('... about to activating license :' + license );
                var requestType = 'activateLicense';
                t32SystemService.submitSystemRequest(requestType, license )
                .then(function(errorMsg){
                    if ( errorMsg )
                    {
                       var swalOpts = {
                          title: "Ooops, little problem",
                          text: errorMsg,
                          type: "error",
                          showCancelButton: false,
                          confirmButtonColor: "#d9534f",
                          confirmButtonText: "Let Me Check",
                          cancelButtonText: "Cancel",
                          closeOnConfirm: true,
                          allowOutsideClick: true,
                          closeOnCancel: true
                       };
                       SweetAlert.swal(
                          swalOpts,
                          function(isConfirm){
                          }
                       );
                    }
                    else
                    {
                       // save the license 
                       setting.license = license;
                       logger.info("The setting after licenc activation, ", setting)
                    //    var requestType = 'saveXrayStudioSetting';
                       t32SystemService.saveXrayStudioSetting(setting)
                    //    t32SystemService.submitSystemRequest(requestType, setting)
                       .then(function(){
                         deferred.resolve();
                       })
                    }
                }) 
                .catch(function(err){
                    console.error(err);
                })          
                return deferred.promise;    
            }

            /**
             * Provide prompt to user if we are in the simulation mode.
             */
            this.simulationModePrompt = function( ) {
                var deferred = $q.defer();

                var theSetting;
                var that = this;
                t32SystemService.getXrayStudioSetting()
                .then(function(setting){
                    theSetting = setting;
                    if ( setting && setting.simulation )
                    {
                       var text = 'In Simulation mode, Click Setting to change.'
                       var swalOpts = {
                          title: "Xray Simulation",
                          text: text,
                          type: "info",
                          showCancelButton: true,
                          confirmButtonColor: "#d9534f",
                          confirmButtonText: "Proceed",
                          cancelButtonText: "Setting",
                          closeOnConfirm: true,
                          allowOutsideClick: true,
                          closeOnCancel: true
                       };
                       SweetAlert.swal(
                          swalOpts,
                          function(isConfirm){
                            if (isConfirm)
                            {
                              deferred.resolve();
                            }
                            else
                            {
                              that.openSettingPage();
                            }
                          }
                       );
                    }
                    else
                    {
                        deferred.resolve();
                    }
                });
                return deferred.promise;
            };

            this.openSettingPage = function() {
              var deferred = $q.defer();

              var modalInstance = $modal.open({
                 templateUrl: 'widgets/setup/t32Setup.html',
                 controller: 't32SetupCtrl',
                 size: 'lg',
                 backdrop: 'static',
                 resolve: {
                 }
              });

              modalInstance.result.then(function () {
                 deferred.resolve();
              }, function () {
                 deferred.resolve();
              });  
              return deferred.promise;
            };
        }
    ]);
})(window, window.angular);