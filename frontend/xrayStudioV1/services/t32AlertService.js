(function (window, angular, undefined) {
    'use strict';
    angular.module('app').service('t32AlertService', [
        '$q',
        'SweetAlert',
        'toaster',
        '$window',
        '$timeout',
        function (
            $q,
            SweetAlert,
            toaster,
            $window,
            $timeout) {
                
            this.showError = function(errorObj){
                var deferred = $q.defer();

                var swalOpts = {
                    title: errorObj.title || 'Fatal Error',
                    text: errorObj.text || "There was a fatal error",
                    type: "warning",
                    showCancelButton: errorObj.showCancel || false,
                    confirmButtonColor: errorObj.confirmColor || "#d9534f",
                    confirmButtonText: errorObj.confirmText || "Okay",
                    cancelButtonText: errorObj.cancelText || "Cancel",
                    closeOnConfirm: errorObj.closeOnConfirm || true,
                    allowOutsideClick: errorObj.outsideClick || false,
                    closeOnCancel: errorObj.closeOnCancel || true
                };
                SweetAlert.swal(swalOpts, function (isConfirm) {
                    if (isConfirm) {
                        deferred.resolve({ error: 'fatal' });
                    }
                    else{
                        deferred.resolve({ error: 'cancel'});
                    }
                });

                return deferred.promise;
            }

            this.showAlert = function(alertObj){
                var deferred = $q.defer();

                var swalOpts = {
                    title: alertObj.title || 'Alert',
                    text: alertObj.text || 'Something has happened',
                    type: "warning",
                    showCancelButton: alertObj.showCancel || false,
                    confirmButtonColor: alertObj.confirmColor || "#006400",
                    confirmButtonText: alertObj.confirmText || "Okay",
                    cancelButtonText: alertObj.cancelText || "Cancel",
                    closeOnConfirm: alertObj.closeOnConfirm || true,
                    allowOutsideClick: alertObj.outsideClick || false,
                    closeOnCancel: alertObj.closeOnCancel || true
                };
                SweetAlert.swal(swalOpts, function (isConfirm) {
                    if (isConfirm) {
                        deferred.resolve({ alert: 'confirm' });
                    }
                    else {
                        deferred.resolve({ alert: 'cancel' });
                    }
                });

                return deferred.promise;
            }

            this.showToaster = function(alertObj){

                toaster.pop({
                    type: alertObj.type || "info",
                    title: alertObj.title || null,
                    body: alertObj.text || "",
                    timeout: alertObj.timeout || 0,
                    showCloseButton: alertObj.showCloseButton || false,
                    closeHtml: alertObj.closeHtml || "",
                });
            }

            this.showLoadingAlert = function (alertTitle) {
                var swalOpts = {
                    html: true,
                    title: "<i class='fa fa-spinner fa-pulse fa-3x fa-fw' style='color: #5bc0de'></i>",
                    text: alertTitle,
                    type: null,
                    showCancelButton: false,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                };
                SweetAlert.swal(
                    swalOpts,
                    function (confirm) {}
                )
                return
            }

            this.closeLoadingAlert = function(){
                var deferred = $q.defer();

                $window.swal.close();  
                $timeout(function(){
                    deferred.resolve()
                }, 1000)


                return deferred.promise;
            }
        }
    ]);
})(window, window.angular);