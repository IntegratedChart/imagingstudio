(function (window, angular, undefined) {
    'use strict';
    angular.module('app').service('t32XrayStudioAppInstanceService', [
        '$rootScope',
        '$q',
        '$timeout',
        'logger',
        '$injector',
        't32AlertService',
        '$modalStack',
        't32SystemService',
        function (
            $rootScope,
            $q,
            $timeout,
            logger,
            $injector,
            t32AlertService,
            $modalStack,
            t32SystemService) {
            
            var channel = 't32XrayStudioAppInstance';
            var t32XrayStudioChannelMapperService = $injector.get('t32XrayStudioChannelMapperService');

            this.handleIncomingMsg = function(msg){
                console.log('trying to handle incoming message ', msg)
                if (msg.msgType === 'wakeUp') {
                    var alertObj = {
                        title: "Incoming Request",
                        text: "Allow to open " + msg.args + "?",
                        showCancel: true,
                        confirmText: "Open"
                    }

                    t32AlertService.showAlert(alertObj)
                    .then(function(result){
                        if(result.alert == 'confirm'){
                            $modalStack.dismissAll('clear');
                        }
                    })
                }
                else if(msg.msgType === 'appAlreadyRunning'){
                    var alertObj = {
                        confirmColor: '#ff0000',
                        title: 'Oops!!!',
                        text: msg.error,
                        showCancel: false,
                    }
                    t32AlertService.showAlert(alertObj)
                    .then(function (result) {
                        logger.info("Try to close the app now.")
                        t32SystemService.exitApp();
                    });
                }
                else if(msg.msgType === 'commandFailed'){
                    var alertObj = {
                        confirmColor: '#ff0000',
                        title: 'Oops!!!',
                        text: 'Ehr envocation command failed',
                        showCancel: false,
                    }
                    t32AlertService.showAlert(alertObj)
                    .then(function (result) {

                    });
                }

            }
            
        }
    ]);
})(window, window.angular);