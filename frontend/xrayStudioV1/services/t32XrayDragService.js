(function (window, angular, undefined) {'use strict';
    angular.module('app').service('t32XrayDragService', [
                '$q',
                '$timeout',
                't32SystemService',
        function (
                $q,
                $timeout,
                t32SystemService ) {

            var startingDragId
            var endingDragId
            var fileName
            var originalFileName
            var overflowImg
            var endingFileName
            var endingOriginalFileName
            var fileDesc
            var endingFileDesc
            
            this.getStartingDragId = function(){
                return startingDragId;
            }

            this.setStartingDragId = function(id){
                startingDragId = id;
            }

            this.getIsOverflowImg = function(){
                return overflowImg;
            }

            this.setIsOverflowImg = function(statement){
                overflowImg = statement;
            }

            this.getEndingDragId = function(){
                return endingDragId;
            }
            
            this.setEndingDragId = function(id){
                endingDragId = id;
            }

            this.getFileName = function(){
                return fileName
            }

            this.setFileName = function(name){
                fileName = name;
            }

            this.getOriginalFileName = function(){
                return originalFileName
            }

            this.setOriginalFileName = function(name){
                originalFileName = name;
            }

            this.setEndingFileName = function(name){
                endingFileName = name;
            }

            this.setEndingOriginalFileName = function(name){
                endingOriginalFileName = name;
            }

            this.getEndingFileName = function(){
                return endingFileName;
            }

            this.getEndingOriginalFileName = function(){
                return endingOriginalFileName;
            }

            this.setFileDesc = function(desc){
                fileDesc = desc;
            }

            this.getFileDesc = function(){
                return fileDesc;
            }

            this.setEndingFileDesc = function (desc) {
                endingFileDesc = desc;
            }
            

            this.getEndingFileDesc = function(){
                return endingFileDesc;
            }

            this.swapXrayImages = function(startingDragId, endingDragId, fileName, originalFileName, endingFileName, endingOriginalFileName, isOverflowImg){
                var deferred = $q.defer();

                var requestObject = {
                    originalId: startingDragId,
                    replacementId: endingDragId,
                    fileName: fileName,
                    originalFileName: originalFileName
                }

                // alert("overflowimage: " + isOverflowImg);

                if( (startingDragId !== endingDragId && !isOverflowImg) || isOverflowImg){
                    t32SystemService.submitSystemRequest('swapXrayImage', requestObject)
                    .then(function(newFileName){
                       deferred.resolve(newFileName);
                    })
                    .catch(function(err){
                       logger.debug(err);
                       deferred.reject(err);
                    })
                }
                // else if(isOverflowImg){
                //     t32SystemService.submitSystemRequest('swapXrayImage', requestObject)
                //     .then(function(result){
                //        deferred.resolve(result);
                //     //    alert("We have result " + result);
                //     })
                //     .catch(function(err){
                //        console.log(err);
                //        deferred.reject(err);
                //     })
                // }
                else{
                    deferred.reject();
                }

                return deferred.promise;
            }

        }
    ]);
})(window, window.angular);