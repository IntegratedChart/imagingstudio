(function (window, angular, undefined) {
    'use strict';
    angular.module('app').service('t32XrayStudioDbService', [
        '$rootScope',
        '$q',
        '$timeout',
        'logger',
        '$injector',
        't32AlertService',
        '$modalStack',
        '$modal',
        't32SystemService',
        't32XrayStudioUtilService',
        function (
            $rootScope,
            $q,
            $timeout,
            logger,
            $injector,
            t32AlertService,
            $modalStack,
            $modal,
            t32SystemService,
            t32XrayStudioUtilService) {

            var channel = 't32XrayStudioDb';
            var t32XrayStudioChannelMapperService = $injector.get('t32XrayStudioChannelMapperService');

            this.handleIncomingMsg = function (msg) {
                logger.info('trying to handle incoming message ', msg)
                let self = this;

                if (msg.msgType === 'credentialPrompt') {

                    var alertObj = getCredentialPromptAlert(msg.credentials.username)

                    t32AlertService.showAlert(alertObj)
                    .then(function (result) {
                        if (result.alert == 'confirm') {
                            $timeout(function () {
                                return self.collectDbInfo(msg.credentials)
                            }, 1000)
                        }
                    })
                    .catch(function (err) {
                        logger.info('error showing alert ', err)
                    })
                } else if (msg.msgType === 'dbConnectionChange') {
                    $rootScope.$broadcast('dbConnectionChange', msg.update)
                }
            }

            function getCredentialPromptAlert(user) {

                return {
                    title: "Db credentials needed!",
                    text: "The information provided for the database access is either missing or in-correct. Would you like to enter it now? (If you do not proceed, all functionality with regards to offline EHR will not work.)",
                    showCancel: true,
                    confirmText: "Proceed"
                }
            }

            this.collectDbInfo = function(credentials) {
                var modalInstance = $modal.open({
                    templateUrl: 'widgets/setup/t32XrayStudioDbSetup.html',
                    controller: 't32XrayStudioDbSetupCtrl',
                    // windowClass : 'ninetyPercent-Modal',
                    size: 'sm',
                    backdrop: 'static',
                    resolve: {
                        username : function() {
                            return credentials.username;
                        },
                        database: function() {
                            return credentials.database
                        }
                    }
                });

                modalInstance.result.then(function () {
                    console.log("Db info collection closed")
                }, function () {
                    console.log("Db info collection closed")
                });
            }

        }
    ]);
})(window, window.angular);