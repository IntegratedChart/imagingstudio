(function (window, angular, undefined) {
    'use strict';
    angular.module('app').service('t32XrayDriverService', [
        '$rootScope',
        '$q',
        '$timeout',
        'logger',
        '$injector',
        function (
            $rootScope,
            $q,
            $timeout,
            logger,
            $injector) {
            
            var driverChannel = 'tab32 x ray driver channel';
            var driverList = [];
            var lastCommunication = null;
            var t32XrayStudioChannelMapperService = $injector.get('t32XrayStudioChannelMapperService');
            var scanInProgress = false;
            var session = 1
            var globalWinName = null;

            this.getDriverList = function(winName){
                var deferred = $q.defer();
                globalWinName = winName
                logger.info("call to get driver list in " + winName + " " + driverList);

                if(driverList && driverList.length > 0){
                    logger.info("WE have the driver list already")
                    deferred.resolve(driverList);
                }
                else{
                    var message = {
                        msgType: 'ListDrivers'
                    } 
    
                    t32XrayStudioChannelMapperService.handleOutgoingMsg(driverChannel, message)
                    .then(function () {
                        deferred.resolve();
                    })
                    .catch(function (err) {
                        deferred.reject(err);
                    })
                }

                return deferred.promise;
            }

            this.startScan = function(driver, selectedPatientFolder){
                var deferred = $q.defer();

                if(scanInProgress){
                    deferred.resolve();
                }
                else{
                    var message = {
                        msgType: 'StartScan',
                        driver: driver,
                        sessionId: session,
                        selectedFolder: selectedPatientFolder,
                        timestamp: new Date()
                    }
    
                    t32XrayStudioChannelMapperService.handleOutgoingMsg(driverChannel, message)
                    .then(function () {
                        deferred.resolve();
                    })
                    .catch(function (err) {
                        deferred.reject(err);
                    })
                }
                
                return deferred.promise;
            }

            this.stopScan = function(){
                var deferred = $q.defer();

                var message = {
                    msgType: 'StopScan',
                    sessionId: session,
                    timestamp: new Date()
                }

                t32XrayStudioChannelMapperService.handleOutgoingMsg(driverChannel, message)
                .then(function () {
                    deferred.resolve();
                })
                .catch(function (err) {
                    deferred.reject(err);
                })

                return deferred.promise;
            }

            this.checkStatus = function(){
                var deferred = $q.defer();

                var message = {
                    msgType: 'HeartBeat',
                    sessionId: 'test',
                    timestamp: new Date()
                }

                t32XrayStudioChannelMapperService.handleOutgoingMsg(driverChannel, message)
                .then(function () {
                    deferred.resolve();
                })
                .catch(function (err) {
                    deferred.reject(err);
                })

                return deferred.promise;
            }

            this.handleIncomingMsg = function(msg){
                var msgType = msg.msgType;
                lastCommunication = new Date();

                if (msgType == 'AvailableDrivers') {
                    logger.info("we have list of available drivers in ", globalWinName);
                    driverList = msg.drivers;
                    logger.info ("driver list set ", driverList);
                }
                else if (msgType == 'HeartBeatFromDriver') {
                    logger.info("client side recieved heartbeat")
                }
                else if (msgType == 'ImageReady') {
                    logger.info("client side recieved The image is ready");
                    $rootScope.$broadcast('ImageReady')
                }
                else if (msgType == 'StopAck') {
                    logger.info('client side recieved stopAck');
                    $rootScope.$broadcast('XrayDriverExit');
                }
                else if (msgType == 'QuitAck') {
                    logger.info("Quiting driver...");
                    $rootScope.$broadcast('XrayDriverExit');
                    // driverList = [];
                }

            }
            
        }
    ]);
})(window, window.angular);