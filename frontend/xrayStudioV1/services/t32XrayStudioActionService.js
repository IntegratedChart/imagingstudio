(function (window, angular, undefined) {
    'use strict';
    angular.module('app').service('t32XrayStudioActionService', [
        '$rootScope',
        '$q',
        '$timeout',
        'logger',
        '$injector',
        't32AlertService',
        '$modalStack',
        't32SystemService',
        't32XrayStudioUtilService',
        function (
            $rootScope,
            $q,
            $timeout,
            logger,
            $injector,
            t32AlertService,
            $modalStack,
            t32SystemService,
            t32XrayStudioUtilService) {

            var channel = 't32XrayStudioAction';
            var t32XrayStudioChannelMapperService = $injector.get('t32XrayStudioChannelMapperService');
            var doNotTrigger = false;

            this.handleIncomingMsg = function (msg) {
                logger.info('trying to handle incoming message ', msg)

                if (msg.action === 'getPatient') {
                    var alertObj = getAlertObj("open Patient")

                    t32AlertService.showAlert(alertObj)
                    .then(function (result) {
                        if (result.alert == 'confirm') {
                            $timeout(function(){
                                return handleGetPatientAction(msg)
                            }, 1000)
                        }
                    })
                    .catch(function(err){
                        logger.info('error showing alert ', err)
                    })
                } else if (msg.action === 'triggerXrayTemplate') {
                    // if (doNotTrigger) {
                    //     logger.info("do not trigger is set ");
                    //     doNotTrigger = false;
                    // } else {
                        return this.handleTriggerXrayTemplate(msg);
                    // }
                } else if (msg.action === 'clearPatientContext') {
                    // doNotTrigger = true;
                    return this.handleClearPatientContext()
                }
            }


            this.handleClearPatientContext = function() {
                $rootScope.$broadcast('clearPatientContext');
            }

            this.handleTriggerXrayTemplate = function (msg) {
                t32AlertService.showLoadingAlert("Please Wait...");
                logger.info("info coming to trigger xray template ", msg);
                let patientFolder =  msg.info.patientInfo.fname + "-" + msg.info.patientInfo.lname + "-"  + msg.info.patientInfo.chartId;
                let patientContext = {
                    selectedPatient: null,
                    uploadType: 'xray',
                    selectedFolder: patientFolder,
                    secondWindowInstance: msg.info.winInstance
                } 
                ensurePatientFolder(patientFolder)
                .then( function() {
                    $rootScope.$broadcast('openXrayTemplate', patientContext);
                    t32SystemService.setWinName(patientContext.secondWindowInstance)
                    return t32AlertService.closeLoadingAlert()
                })
                // .then(function() {
                // })
                .catch(function (err) {
                    t32AlertService.closeLoadingAlert()
                    .then(function () {
                        var alertObj = {
                            type: "error",
                            title: "Error opening patient",
                            text: err
                        }

                        t32AlertService.showToaster(alertObj);
                    })
                })
            }

            function getAlertObj(actionText){

                return {
                    title: "Incoming Request From tab32 EHR",
                    text: "There is a request from tab32 EHR to " + actionText + "." + "Would you like to proceed? (keep in mind that all unsaved changes will be discarded)",
                    showCancel: true,
                    confirmText: "Proceed"
                }
            }

            function handleGetPatientAction(msg){
                t32AlertService.showLoadingAlert("Please Wait...");

                var patientContext = {}
                handleActionConfirmation('runGetPatientAction', msg)
                .then(function (patient) {
                    t32SystemService.setUserAuthCode({
                        token: msg.params.token,
                        clinicId: msg.params.clinicId
                    });

                    patientContext.selectedPatient = patient;
                    patientContext.uploadType = 'xray';
                    patientContext.selectedFolder = t32XrayStudioUtilService.formatPatientFolderString(patientContext.selectedPatient);

                    return ensurePatientFolder(patientContext.selectedFolder)
                })
                .then(function () {
                    return t32AlertService.closeLoadingAlert()
                })
                .then(function(){
                    $modalStack.dismissAll('clear');
                    $rootScope.$broadcast('openXrayTemplate', patientContext);
                })
                .catch(function (err) {
                    logger.info("Error recieved on frontend ", err);
                    t32AlertService.closeLoadingAlert()
                    .then(function(){
                        logger.info('handling error')
                        handleActionError(err, msg);
                    })
                })
            }

            function ensurePatientFolder(patientFolder) {
                var deferred = $q.defer();

                var requestType = 'ensureFolder';
                var requestObject = {
                    selectedFolder: patientFolder
                };

                t32SystemService.submitSystemRequest(requestType, requestObject)
                    .then(function () {
                        deferred.resolve();
                    })
                    .catch(function (err) {
                        console.error(err);
                        logger.warn(err);
                    })

                return deferred.promise;
            }

            function handleActionConfirmation(backendRequest, msg) {
                //send out broadcast to get patient and call 
                var requestType = backendRequest;
                var requestObj = {}
                requestObj.actionObj = msg;

                return t32SystemService.submitSystemRequest(requestType, requestObj)
            }

            function handleActionError(err, actionObj) {
                var errorObj = {
                    timeout: 5000,
                    type: 'error',
                    title: "Failed to get patient",
                    text: "There was an error while fetching the patient from EHR, please try again later."
                }

                if (err === "Public Id Registration Required") {
                    // ask user to register public key, if they cancel, go no further. if they agree then ask them to login. If they 
                    promptUserLogin(null, null, null)
                    .then ( function(result){
                        handleGetPatientAction(actionObj)
                    }).catch ( function(err){
                        logger.info("login prompt failed ", err);
                    })
                } else if (err === "No Auth Token") {
                    //ask user to login and if they agree then run the 
                    alert('pop open no auth token logic')
                } else {
                    $timeout(function () {
                        t32AlertService.showToaster(errorObj);
                    }, 100)
                }
            }


            function promptUserLogin(title, text, confirmText){
                var deferred = $q.defer();

                let alertObj = {
                    title: title || "Login Required",
                    text: text || "Please Login in to continue",
                    showCancel: true,
                    confirmText: confirmText || "Login"
                }

                t32AlertService.showAlert(alertObj)
                .then ( function(result) {
                    if (result.alert == 'confirm') {

                        var requestType = 'getGoogleAuthUrl';

                        t32SystemService.submitSystemRequest(requestType, null)
                        .then(function (authUrl) {
                            waitForLoginConfirmation()
                            .then (function(){
                                deferred.resolve();
                            }).catch (function(err){
                                deferred.reject(err);
                            })
                        })
                    }
                    else{
                        deferred.reject('cancelled');
                    }
                })
                .catch( function (err) {
                    deferred.reject(err);
                    logger.info("Error prompting user to login");
                })

                return deferred.promise;
            }

            function waitForLoginConfirmation(){
                var deferred = $q.defer();

                $timeout(function(){
                    t32AlertService.showLoadingAlert("Please Wait...");
                }, 100)

                var attempts = 0;
                var isLoggedIn = setInterval(function () {
                    var status = t32SystemService.isAuthenticated();

                    logger.info('waiting for user login...');

                    if (attempts > 120) {
                        logger.info("login listener limit reached, rejecting request..")
                        
                        t32AlertService.closeLoadingAlert()
                        .then(function(){
                            clearInterval(isLoggedIn);
                            deferred.reject("limit reached");
                        });
                    } else if (status === true) {
                        t32AlertService.closeLoadingAlert()
                        .then(function(){
                            clearInterval(isLoggedIn);
                            deferred.resolve();
                        })
                    } else {
                        attempts++;
                    }

                }, 1000);

                return deferred.promise;
            }

        }
    ]);
})(window, window.angular);