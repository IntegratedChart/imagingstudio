(function (window, angular, undefined) {'use strict';
   angular.module('app').service('t32PatientSelectionService', [
         '$q',
         '$modal',
         '$timeout',
         'logger',
         'SweetAlert',
         't32SystemService',
      function (
         $q,
         $modal,
         $timeout,
         logger,
         SweetAlert,
         t32SystemService ) {

         /**
         */
         this.selectPatient = function( fromEhrOnly ) {
            var deferred = $q.defer();

            var modalInstance = $modal.open({
               templateUrl: 'widgets/general/t32PatientSelector.html',
               controller: 't32PatientSelectorCtrl',
               windowClass : 'eightyPercent-Modal',
               // size: 'md',
               backdrop: 'static',
               resolve: {
                  fromEhrOnly : function() {
                     return fromEhrOnly;
                  }
               }
            });

            modalInstance.result.then(function (patientContext) {
               var requestType = 'ensureFolder';
               var requestObject = { selectedFolder : patientContext.selectedFolder};
               t32SystemService.submitSystemRequest(requestType, requestObject )
               .then(function(){
                  deferred.resolve(patientContext);
               })
               .catch(function(err){
                  console.error(err);
                  logger.warn(err);
               })
            }, function () {
               deferred.resolve();
            });

            return deferred.promise;
         };
      }
   ]);
})(window, window.angular);