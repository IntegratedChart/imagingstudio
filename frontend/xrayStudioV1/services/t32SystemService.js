(function (window, angular, undefined) {
    'use strict';
    angular.module('app').service('t32SystemService', [
        '$q',
        '$modal',
        '$modalStack',
        '$rootScope',
        '$timeout',
        'logger',
        't32XrayStudioChannelMapperService',
        't32AlertService',
        function (
            $q,
            $modal,
            $modalStack,
            $rootScope,
            $timeout,
            logger,
            t32XrayStudioChannelMapperService,
            t32AlertService) {
            var userAuthCode = null;
            var CACHEDXRAYSTUDIOSETTINGS = null;
            var myRequestId = 10;
            var deferredMapByRequestId = {};
            var SYSTEM_OS;
            // cache the selected clinic.
            var selectedClinic = null;
            var winName = 'main';
            const { ipcRenderer } = require('electron');
            // this.getUserAuthCode = function() {
            //     var deferred = $q.defer();
            //     return deferred.promise;                
            // };

            this.setUserAuthCode = function (authCode) {
                userAuthCode = authCode;
            }

            this.isAuthenticated = function () {
                return userAuthCode ? true : false;
            };

            this.getXrayStudioSetting = function () {
                var deferred = $q.defer();

                if (CACHEDXRAYSTUDIOSETTINGS) {
                    deferred.resolve(CACHEDXRAYSTUDIOSETTINGS);
                }
                else {
                    this.submitSystemRequest('getXrayStudioSetting')
                        .then(function (setting) {
                            CACHEDXRAYSTUDIOSETTINGS = {};
                            for (let key in setting) {
                                CACHEDXRAYSTUDIOSETTINGS[key] = setting[key];
                            }
                            deferred.resolve(CACHEDXRAYSTUDIOSETTINGS);
                        })
                }
                return deferred.promise;
            };

            // this.setXrayStudioSetting = function(setting) {
            //     CACHEDXRAYSTUDIOSETTINGS = setting;
            // };

            this.saveXrayStudioSetting = function (setting) {
                var deferred = $q.defer();

                this.submitSystemRequest('saveXrayStudioSetting', setting)
                    .then(function (returnSetting) {
                        for (let key in setting) {
                            CACHEDXRAYSTUDIOSETTINGS[key] = setting[key];
                        }
                        deferred.resolve(CACHEDXRAYSTUDIOSETTINGS);
                    })
                    .catch(function (err) {
                        logger.info('error saving studio settings ', err);
                        deferred.reject(err);
                    })

                return deferred.promise;
            }

            this.exitAppWindow = function () {
                this.submitSystemRequest('exitAppWindow', {})
            }

            //essentially same method as above, only it will not save any settings.
            this.exitApp = function () {
                this.submitSystemRequest('exitApp', {})
            }

            /**
             * function to submit a request from frontend to backend.
             */
            this.submitSystemRequest = function (requestType, requestMsg) {
                var deferred = $q.defer();

                var nextId = myRequestId++;

                var t32Request = {
                    requestType: requestType,
                    requestMsg: requestMsg,
                    requestId: nextId
                };

                deferredMapByRequestId[nextId] = deferred;

                ipcRenderer.send('asynchronous-message', t32Request);

                return deferred.promise;
            };

            /**
            * initialize the listener for backend notification
            */
            this.initializeListener = function () {
                var self = this;

                console.log("-------Listener registered------------");
                ipcRenderer.on('asynchronous-reply', (event, wrapper) => {
                    var requestId = wrapper.requestId;
                    // console.log("we recieved a reply back from backend");
                    // console.log(wrapper);

                    if (requestId && !wrapper.error) {
                        var theDeferred = deferredMapByRequestId[requestId];
                        // console.log("The deferred");
                        // console.log(theDeferred);
                        if (theDeferred) {
                            // console.log("We are resolving");
                            theDeferred.resolve(wrapper.result);
                        }
                        delete deferredMapByRequestId[requestId];
                    } else if (requestId && wrapper.error) {
                        var theDeferred = deferredMapByRequestId[requestId];
                        if (theDeferred) {
                            theDeferred.reject(wrapper.error);
                            delete deferredMapByRequestId[requestId];
                        }
                    }
                });

                ipcRenderer.removeListener('BackendPushNotification', backendPushNotificationListener);

                ipcRenderer.on('BackendPushNotification', backendPushNotificationListener);

                ipcRenderer.send('clientInitComplete');
            };

            this.getSystemOS = function () {
                var deferred = $q.defer();

                if (!SYSTEM_OS) {
                    var requestType1 = 'getOsInfo';

                    this.submitSystemRequest(requestType1, null)
                        .then(function (os) {
                            SYSTEM_OS = os;
                            deferred.resolve(os);
                        }).catch(function (err) {
                            SYSTEM_OS = 'win32'
                            console.log('error getting system os info, defaulting to win32')
                        })
                } else {
                    deferred.resolve(SYSTEM_OS)
                }

                return deferred.promise;
            }



            function backendPushNotificationListener(event, msg) {
                console.log('v1 backendpush')
                logger.info(" backend message recieved on frontend in " + winName);
                logger.info(msg);
                var doNotBroadcast = false;

                if (msg.msgType == 'Authentication') {
                    userAuthCode = msg.authCode;
                    if (!msg.authCode) {
                        // Google auth token expired.
                        // close all modal and bring user back to landing.
                        $modalStack.dismissAll("clear");
                        let error = {
                            title: 'Authentication Failed',
                            text: 'Your google Authentication session has expired, please login again.'
                        }
                        t32AlertService.showError(error);
                    }
                }
                else if (msg.msgType == 'uploadsPending') {
                    $modalStack.dismissAll("clear");
                }
                // else if (msg.msgType == 'updateXrayStudioSettings'){
                //     for(let key in msg.settings){
                //         CACHEDXRAYSTUDIOSETTINGS[key] = msg.settings[key];
                //     };
                // }
                else if (msg.msgType == 'updateXrayStudioSettingsProperty') {
                    for (let key in msg.keyValToChange) {
                        if (!CACHEDXRAYSTUDIOSETTINGS[key]) {
                            CACHEDXRAYSTUDIOSETTINGS[key] = null;
                        }
                        CACHEDXRAYSTUDIOSETTINGS[key] = msg.keyValToChange[key]
                    }
                } else if (msg.msgType == 'incomingCommsChannelMsg') {
                    doNotBroadcast = true;
                    t32XrayStudioChannelMapperService.handleIncomingMsg(msg.channel, msg.msg)
                }

                $timeout(function () {
                    if (doNotBroadcast) {
                        //do nothing
                    }
                    else {
                        logger.info("Sending broadcast ", msg.msgType);
                        $rootScope.$broadcast(msg.msgType, msg);
                    }
                }, 10);
            }

            this.collectSimpleAnswer = function (title, noteList) {
                var deferred = $q.defer();

                var modalInstance = $modal.open({
                    templateUrl: 'widgets/general/t32SimpleAnswerCollector.html',
                    controller: 't32SimpleAnswerCollectorCtrl',
                    // windowClass : 'ninetyPercent-Modal',
                    size: 'md',
                    backdrop: 'static',
                    resolve: {
                        title: function () {
                            return title;
                        },
                        noteList: function () {
                            return noteList;
                        }
                    }
                });

                modalInstance.result.then(function (theAnswer) {
                    deferred.resolve(theAnswer);
                }, function () {
                    deferred.resolve();
                });

                return deferred.promise;
            };

            this.getSelectedClinic = function () {
                return selectedClinic;
            };

            this.getWinName = function() {
                return winName
            }

            this.setWinName = function (name) {
                winName = name;
            }

            this.setSelectedClinic = function (theClinic) {
                selectedClinic = theClinic;
                var self = this;
                self.getXrayStudioSetting().then(function (setting) {
                    if (setting.clinic_id !== selectedClinic._id) {
                        setting.currentTemplate = null;
                        setting.activeImageCell = null;
                    }
                    setting.clinic_id = selectedClinic._id;
                    // set clinic on studio context
                    setting.clinic = selectedClinic;
                    self.saveXrayStudioSetting(setting)
                })
                    .catch(function (err) {
                        console.error(err);
                    })
            };
        }
    ]);
})(window, window.angular);