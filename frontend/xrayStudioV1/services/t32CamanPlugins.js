(function (window, angular, undefined) {'use strict';
   angular.module('app').service('t32CamanPlugins', [
            '$q',
            '$timeout',
      function (
            $q,
            $timeout ) {
         this.initialize = function() {
            Caman.Plugin.register("rotate", function(degrees) {
               var angle, canvas, ctx, height, to_radians, width, x, y;
               angle = degrees % 360;
               if (angle === 0) {
                  return this.dimensions = {
                     width: this.canvas.width,
                     height: this.canvas.height
                  };
               }
               to_radians = Math.PI / 180;
               if (typeof exports !== "undefined" && exports !== null) {
                  canvas = new Canvas();
               } else {
                  canvas = document.createElement('canvas');
                  copyAttributes(this.canvas, canvas);
               }
               if (angle === 90 || angle === -270 || angle === 270 || angle === -90) {
                  width = this.canvas.height;
                  height = this.canvas.width;
                  x = width / 2;
                  y = height / 2;
               } else if (angle === 180) {
                  width = this.canvas.width;
                  height = this.canvas.height;
                  x = width / 2;
                  y = height / 2;
               } else {
                  width = Math.sqrt(Math.pow(this.originalWidth, 2) + Math.pow(this.originalHeight, 2));
                  height = width;
                  x = this.canvas.height / 2;
                  y = this.canvas.width / 2;
               }
               canvas.width = width;
               canvas.height = height;
               ctx = canvas.getContext('2d');
               ctx.save();
               ctx.translate(x, y);
               ctx.rotate(angle * to_radians);
               ctx.drawImage(this.canvas, -this.canvas.width / 2, -this.canvas.height / 2, this.canvas.width, this.canvas.height);
               ctx.restore();
               return this.replaceCanvas(canvas);
            });

            Caman.Filter.register("rotate", function() {
               return this.processPlugin("rotate", Array.prototype.slice.call(arguments, 0));
            });     

            Caman.Plugin.register("mirror",function(vertical){
                var canvas, ctx, id;
                                 
                // Support NodeJS by checking for exports object
                if (typeof exports !== "undefined" && exports !== null) {
                  canvas = new Canvas(this.dimensions.width, this.dimensions.height);
                } else {
                  canvas = document.createElement('canvas');
                  copyAttributes(this.canvas, canvas);
                  canvas.width = this.dimensions.width;
                  canvas.height = this.dimensions.height;
                }                
                ctx = canvas.getContext('2d');
                
                if(vertical==true){
                    ctx.translate(0, canvas.height);
                    ctx.scale(1, -1)
                }else{
                    ctx.translate(canvas.width,0);
                    ctx.scale(-1, 1)
                }
                
                ctx.drawImage(this.canvas, 0, 0);
                return this.replaceCanvas(canvas);
            });

            Caman.Filter.register("mirror", function() {
              this.processPlugin("mirror", arguments);
            });                   
         }

         function copyAttributes(from, to, opts) {
            var attr, _i, _len, _ref, _ref1, _results;

            if (opts == null) {
               opts = {};
            }
            _ref = from.attributes;
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
               attr = _ref[_i];
               if ((opts.except != null) && (_ref1 = attr.nodeName, __indexOf.call(opts.except, _ref1) >= 0)) {
                  continue;
               }
               _results.push(to.setAttribute(attr.nodeName, attr.nodeValue));
            }
            return _results;
         };         
      }
   ]);
})(window, window.angular);