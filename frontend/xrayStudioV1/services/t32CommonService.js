(function (window, angular, undefined) {
    'use strict';
    angular.module('app').service('t32CommonService', [
        '$q',
        'SweetAlert',
        function (
            $q,
            SweetAlert) {

            this.askToSave = function(option) {
                var deferred = $q.defer();

                var swalOpts = {
                    title: "Unsaved Changes",
                    text: "Its seems like there are changes made to " + option + " that have not been saved, would you like to save them?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#449d44",
                    cancelButtonColor: "#d9534f",
                    confirmButtonText: "Save",
                    cancelButtonText: "Do Not Save",
                    closeOnConfirm: true,
                    allowOutsideClick: false,
                    closeOnCancel: true
                };
                SweetAlert.swal(swalOpts, function (isConfirm) {
                    if (isConfirm) {
                        deferred.resolve(true);
                    }
                    else {
                        deferred.resolve(false);
                    }
                });

                return deferred.promise
            }

            this.toTitleCase = function(str){
                if(typeof str == "string"){
                    return str.replace(/\w\S*/g, function (txt) {
                        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                    });
                }
                else{
                    return str;
                }
            }

            /**
             * see if a value exists in a array of objects given a fieldname
             * 
             */
            this.doesExist = function(val, arr, field){
                // console.log(val);
                // console.log(field);
                var exists = false;
                _.each(arr, function(obj){
                    // console.log(obj[field]);
                    // console.log(val);
                    if(val == obj[field]){
                        exists = true;
                    }
                });

                return exists
            }
        }
    ]);
})(window, window.angular);