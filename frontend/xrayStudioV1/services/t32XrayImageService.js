(function (window, angular, undefined) {
    'use strict';
    angular.module('app').service('t32XrayImageService', [
        'logger',
        't32SystemService',
        '$q',
        'SweetAlert',
        't32PatientSelectionService',
        't32WorkflowService',
        '$timeout',
        't32ImageManipulation',
        function (
            logger,
            t32SystemService,
            $q,
            SweetAlert,
            t32PatientSelectionService,
            t32WorkflowService,
            $timeout,
            t32ImageManipulation) {

            this.loadFolderImages = function(selectedFolder){
                var photos = [];
                var imageHash = {};
                var deferred = $q.defer();

                var requestType = 'getDirectoryFileList';
                var self = this;
                t32SystemService.submitSystemRequest(requestType, selectedFolder)
                .then(function(fileList){

                    photos = _.map(fileList, function (file) {
                        var fileName = "/" + selectedFolder + '/' + file;
                        var originalFileName = "/" + selectedFolder + '/originals/' + file;
                        var fileId = self.parseId(file);
                        // var id = parseId(file);
                        var fileMapId = self.removeExt(file);
                        return {
                            fileDesc: fileMapId,
                            desc: file,
                            fileName: fileName,
                            fileId: fileId,
                            originalFileName: originalFileName
                        };
                    });

                    imageHash = self.groupPhotosById(photos);
                    deferred.resolve({photos: photos, photosHash: imageHash});
                })
                .catch(function(err){
                    deferred.reject(err);
                })

                return deferred.promise;    
            }

            /**
             * 
             * @param {Array} photosArray 
             * Create hash of photos by photo id's
             */
            this.groupPhotosById = function(photosArray) {
                var hash = {};
                var self = this;
                _.each(photosArray, function (photo) {
                    var id = self.parseId(photo.desc);
                    logger.debug("HERE is the id inside of hash " + id);
                    if (hash[id] && hash[id].length > 0) {
                        hash[id].push(photo);
                    }
                    else {
                        hash[id] = [];
                        hash[id].push(photo);
                    }
                });
                // logger.debug("THE HASH IS")
                // logger.debug(hash)
                return hash;
            }

            /**
             * get the id from the filename
             */
            this.parseId = function (file) {
                var reg = /(id_\w+)/g
                var id = file.match(reg)
                if (id && id.length > 0) {
                    id = id[0].substring(0, file.length - 1);
                }
                else {
                    id = ""
                }

                logger.debug(id);
                // alert(id);
                return id;
            }  

            this.removeExt = function(file){
                var reg = /\.\w+/g
                console.log(file.match(reg));
                var match = file.match(reg);
                var id = "";

                if(match && match.length > 0){
                    var index = file.indexOf(file.match(reg)[0]);
                    id =  file.slice(0, index);
                }
                
                console.log(id);
                return id
            }

            this.getPhotosInfo = function(selectedFolder, infoNeeded){
                var deferred = $q.defer();

                var requestType = 'getDirectoryFileInfo';
                // var self = this;
                var requestObj = {
                    directoryPath: selectedFolder,
                    infoNeeded: infoNeeded
                }
                
                t32SystemService.submitSystemRequest(requestType, requestObj)
                .then(function (fileMap) {
                    deferred.resolve(fileMap);
                })
                .catch(function (err) {
                    deferred.reject(err);
                })

                return deferred.promise;
            }

        }
    ]);

})(window, window.angular);