(function (window, angular, undefined) {'use strict';

    angular.module('app').service('MomentService', [function () {
        this.inputDateFormat = 'YYYY-MM-DD';

        this.today = function () {
            return moment().local().hours(0).minutes(0).seconds(0).milliseconds(0).toDate();
        };
        this.local = function () {
            return moment().local().toDate();
        };

        this.isBeforeAndToday = function (date) {
            return this.getDatesDiff(date, this.today()) < 0 ? false : true;
        };
        this.isToday = function (date) {
            return this.getDatesDiff(date, this.today()) !== 0 ? false : true;
        };

        /**
         * Calculate the differences between two dates based on unit
         *
         * @param startDate
         * @param endDate
         * @param diffUnit "day" or "month"; "day" is the default, if nothing is passed in.
         * @returns A positive number if startDate is after endDate. Otherwise, a negative number.
         *          Zero if they are the same day.
         */
        this.getDatesDiff = function(startDate, endDate, diffUnit) {
            if (!diffUnit) {
                diffUnit = 'day';
            }

            var start = moment(startDate).local();
            var end = moment(endDate).local();

            return moment([end.year(), end.month(), end.date()]).diff(
                moment([start.year(), start.month(), start.date()]), diffUnit + 's'
            );
        };

        this.startDateBeforeEndDate = function (startDate, endDate) {
            return this.getDatesDiff(startDate, endDate) < 0 ? false : true;
        };

        this.displayAsDate = function (date) {
            return moment(date).local().format('MM/DD/YYYY');
        }; 

        this.isValidDate = function (date, format) {
            if(!date){
                return false;
            }
            var dateFormat = format || this.inputDateFormat;
            return moment(date, dateFormat).isValid();
        };       
    }]);
})(window, window.angular);
