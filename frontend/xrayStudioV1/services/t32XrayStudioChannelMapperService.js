(function (window, angular, undefined) {
    'use strict';
    angular.module('app').service('t32XrayStudioChannelMapperService', [
        '$q',
        '$timeout',
        'logger',
        '$injector',
        function (
            $q,
            $timeout,
            logger,
            $injector) {

            var channelMap = {
                'tab32 x ray driver channel': 't32XrayDriverService',
                't32XrayStudioAppInstance': 't32XrayStudioAppInstanceService',
                't32XrayStudioAction': 't32XrayStudioActionService',
                't32XrayStudioDb':
                't32XrayStudioDbService'
            };

            this.handleIncomingMsg = function (channel, msg) {
                var deferred = $q.defer();
                //dynamically inject service that is mapped.
                logger.info('handing off to ', channelMap[channel]);
                var channelService = $injector.get(channelMap[channel]);
                // logger.info('channel service ', channelService.handleIncomingMsg);
                channelService.handleIncomingMsg(msg);
                deferred.resolve();

                return deferred.promise;
            }

            this.handleOutgoingMsg = function (channel, msg) {
                var deferred = $q.defer();

                var requestType = "sendOutgoingMsg";

                var requestObject = {
                    channel: channel,
                    msg: msg
                }

                var systemService = $injector.get('t32SystemService');
                systemService.submitSystemRequest(requestType, requestObject)
                .then(function () {
                    deferred.resolve();
                })
                .catch(function (err) {
                    deferred.reject(err);
                })

                return deferred.promise;
            }

        }
    ]);
})(window, window.angular);