(function (window, angular, undefined) {'use strict';
   angular.module('app').service('t32ImageManipulation', [
		'$q',
		'$modal',
		'$timeout',
		'logger',
		'SweetAlert',
		't32SystemService',
      function (
		$q,
		$modal,
		$timeout,
		logger,
		SweetAlert,
		t32SystemService ) {

         /**
         */
		this.rotate = function(imageUrl, originalImageUrl, deg){
			var deferred = $q.defer();
			var requestObject = {
				imageUrl: imageUrl,
				originalImageUrl: originalImageUrl,
				rotationDeg: deg
			}
			t32SystemService.submitSystemRequest('rotateImage', requestObject)
			.then(function(result){
				deferred.resolve(result);
			})
			.catch(function(err){
				logger.debug(err);
				deferred.reject(err);
			})

			return deferred.promise;
		}

		this.flip = function(imageUrl, originalImageUrl, horizontalFlip, verticalFlip){
			var deferred = $q.defer();
			
			var requestObject = {
				imageUrl: imageUrl,
				originalImageUrl, originalImageUrl,
				horizontalFlip: horizontalFlip,
				verticalFlip: verticalFlip
			}
			
			// console.log(requestObject);s
			t32SystemService.submitSystemRequest('flipImage', requestObject)
			.then(function(result){
				deferred.resolve(result);
			})
			.catch(function(err){
				logger.debug(err);
				deferred.reject(err);
			})

			return deferred.promise;
		}

		this.delete = function(isDirectory, filePath, originalFilePath){
			var deferred = $q.defer();

			var requestObject = {
				isDirectory: isDirectory,
				filePath: filePath,
				originalFilePath: originalFilePath
			}

			t32SystemService.submitSystemRequest('deleteXrays', requestObject)
			.then(function(result){
				deferred.resolve(result);
			})
			.catch(function(err){
				logger.debug(err);
				deferred.reject(err)
			})

			return deferred.promise; 
		}

		this.manipulate = function (imageUrls, brightValue, contrastValue) {
			var deferred = $q.defer();

			var requestObject = {
				imageUrls: imageUrls,
				brightValue: brightValue,
				contrastValue: contrastValue
			}

			t32SystemService.submitSystemRequest('manipulateImage', requestObject)
			.then(function (result) {
				deferred.resolve(result);
			})
			.catch(function (err) {
				deferred.reject(err)
			})

			return deferred.promise;
		}
      }
   ]);
})(window, window.angular);