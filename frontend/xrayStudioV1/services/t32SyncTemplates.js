(function (window, angular, undefined){'use strict'; 
    angular.module('app').service('t32SyncTemplate', [
        '$q',
        '$modal',
        '$timeout',
        'logger',
        'SweetAlert',
        't32SystemService',
    function($q,
        $modal,
        $timeout,
        logger,
        SweetAlert,
        t32SystemService){

        this.syncXrayTemplates = function(clinicId){
            var deferred = $q.defer();
            var xraySettings;
            var self = this;

            self.getXrayStudioSetting()
            .then(function(setting){
                //first delete the templates that have been deleted locally
                //alert(setting);
                xraySettings = setting;
                // logger.debug(xraySettings);

                var xrayTemplates = [];
                var deletedTemplates = [];

                if(xraySettings.xrayTemplates){
                    xrayTemplates = JSON.parse(xraySettings.xrayTemplates);
                }

                if(xraySettings.deletedTemplates){
                    deletedTemplates = JSON.parse(xraySettings.deletedTemplates);
                }
            
                // var combinedTemplates = combineTempaltes(xrayTemplates);
                // logger.debug(xrayTemplates);
                // logger.debug(deletedTemplates);

                if(xrayTemplates.length || deletedTemplates.length){
                    logger.info("calling server to sync templates");
                    return self.saveXrayTemplates(clinicId, xrayTemplates, deletedTemplates)
                }
                else{
                    //alert("WE INSIDE OF ELSE")
                    return self.getXrayTemplates(clinicId)
                }
            })
            .then(function(templates){
                logger.info("WE HAVE THE TEMPLATES")
                xraySettings.deletedTemplates = null;
                xraySettings.xrayTemplates = angular.toJson(templates);
                
                return self.saveXrayStudioSetting(xraySettings)
            })
            .then(function(setting){
                //alert("WE ARE INSIDE OF SECOND RETURN")
                // logger.debug(setting)
                deferred.resolve(setting)
            })
            .catch(function(err){
                logger.info("Error syncing templates ", err);
                deferred.reject(err);
            })

            return deferred.promise;
        }

        function add(templateArray, clinicId){
            _.each(templateArray, function(template){
                if(!template.clinic_id){
                    template.clinic_id = clinicId
                }
            })
        }

        var combineTemplates = function(deletedTemplates, xrayTemplates){
            var combinedArray = [];

            _.each(deletedTemplates, function(deletedTemplate){
                combinedArray.push(deletedTemplate);
            })

            _.each(xrayTemplates, function(xrayTemplates){
                combinedArray.push(xrayTemplate)
            })

            return combinedArray;
        }

        this.getXrayTemplates = function(clinicId){

            logger.info("get xray templates");
            var deferred = $q.defer();

            var requestType = "getXrayTemplates";
            var requestObject = {
                clinic_id : clinicId
            }

            t32SystemService.submitSystemRequest(requestType, requestObject).then(function(templates){
                //alert("HERE ARE THE TEMPLATES");
                // logger.debug(templates)
                deferred.resolve(templates);
            })
            .catch(function(err){
                logger.info("error getting xray tempalates", err);
                deferred.reject(err);
            })

            return deferred.promise;
        }

        this.saveXrayTemplates = function(clinicId, templatesToBeSaved, templatesToBeDeleted){
            var deferred = $q.defer();

            var requestType = "saveXrayTemplates";
            var requestObject = {
                clinic_id : clinicId,
                templatesToBeSaved: templatesToBeSaved,
                templatesToBeDeleted: templatesToBeDeleted
            }

            t32SystemService.submitSystemRequest(requestType, requestObject).then(function(templates){

                deferred.resolve(templates);
            })
            .catch(function(err){
                deferred.reject(err);
            })

            return deferred.promise;
        }

        this.getXrayStudioSetting = function(){
            var deferred = $q.defer();

            logger.info("getting xray studio settings");

            t32SystemService.getXrayStudioSetting()
            .then(function(setting){
                //alert("We have the settings");
                // logger.debug(setting);

                deferred.resolve(setting);
            })
            .catch(function(err){
                logger.info('get xray studio setting failed ', err)
                deferred.reject(err);
            })

            return deferred.promise;
        }

        this.saveXrayStudioSetting = function(setting){
            var deferred = $q.defer();

            // var requestType = "saveXrayStudioSetting";
            // var requestObject = setting

            logger.info("WE ARE IN SAVE STUDIO SETTINGS")
            // logger.debug(setting);

            // t32SystemService.submitSystemRequest(requestType, requestObject)
            t32SystemService.saveXrayStudioSetting(setting)
            .then(function(setting){
                deferred.resolve();
            })
            .catch(function(err){
                logger.info("save xray settings failed ", err)
                deferred.reject(err);
            })

            return deferred.promise;
        }

    }
        
    ])
})(window, window.angular);