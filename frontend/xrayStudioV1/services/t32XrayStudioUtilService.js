(function (window, angular, undefined) {
    'use strict';
    angular.module('app').service('t32XrayStudioUtilService', [
        '$rootScope',
        function (
            $rootScope) {
        
            this.formatPatientFolderString = function(patient){
                var folder = replaceSpace(patient.fname, '') + '-' +
                    replaceSpace(patient.lname, '');

                if (patient.clinicData && patient.clinicData.chartId) {
                    folder += '-' + patient.clinicData.chartId;
                }

                return _.capitalize(folder);
            }

            function replaceSpace(str, replacement) {
                return str ? str.replace(/\s/g, replacement) : '';
            }
        }
    ]);
})(window, window.angular);