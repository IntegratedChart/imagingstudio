(function (window, angular, undefined) {
    'use strict';
    angular.module('app').service('t32RotationSettingsService', [
        '$q',
        '$modal',
        '$timeout',
        'logger',
        'SweetAlert',
        't32SystemService',
        function ($q,
            $modal,
            $timeout,
            logger,
            SweetAlert,
            t32SystemService) {

            this.syncRotationSettings = function (clinicId) {
                var deferred = $q.defer();
                var xraySettings;
                logger.info("STARTING ROTATION SETTINGS SYNC");

                if(clinicId){
                    logger.info("WE have the clinic id");
                    getXrayStudioSetting()
                    .then(function (setting) {
                        xraySettings = setting;
    
                        var rotationSettings = [];
                        var deletedRotationSettings = [];
    
                        if (xraySettings.hasOwnProperty('rotationSettings')) {
                            // logger.info("There is a rotation settings prop");
                            // logger.info(xraySettings.rotationSettings);
                            if(xraySettings.rotationSettings){
                                rotationSettings = JSON.parse(xraySettings.rotationSettings);
                            }
                        }
    
                        if (xraySettings.hasOwnProperty('deletedRotationSettings')) {
                            // logger.info("There is a deletedRotation settings prop");
                            // logger.info(xraySettings.deletedRotationSettings);
                            if(xraySettings.deletedRotationSettings){
                                deletedRotationSettings = JSON.parse(xraySettings.deletedRotationSettings);
                            }
                        }
    
                        // logger.info(rotationSettings);
                        // logger.info(deletedRotationSettings);
                
                        if(rotationSettings.length > 0 || deletedRotationSettings.length > 0) {
                            logger.info("about to sync rotation settings with server")
                            return saveRotationSettings(clinicId, rotationSettings, deletedRotationSettings)
                        }
                        else {
                            return getRotationSettings(clinicId)
                        }
                    })
                    .then(function (returnSettings) {
                        logger.info("We have retured from server syncing");
                        if (returnSettings && returnSettings.length > 0){
                            xraySettings.deletedRotationSettings = null;
                            xraySettings.rotationSettings = angular.toJson(returnSettings);
                        }
                        else{
                            xraySettings.deletedRotationSettings = null;
                            xraySettings.rotationSettings = null;
                        }
                        
                        logger.info("about to save rotation settings");
                        // logger.info(returnSettings);
                        // logger.info(xraySettings)
                        return saveXrayStudioSetting(xraySettings)
                    })
                    .then(function () {
                        //alert("WE ARE INSIDE OF SECOND RETURN")
                        deferred.resolve(xraySettings)
                    })
                    .catch(function (err) {
                        logger.error(err);
                        logger.info("The settings did not sync properly, sending back local")
                        t32SystemService.getXrayStudioSetting()
                        .then(function (settings) {
                            logger.info("We have the settings")
                            deferred.resolve(settings);
                        })
                        .catch(function (err) {
                            deferred.reject(err);
                        })
                    })
                }
                else{
                    logger.info("WE did not get a clinic id`")
                    t32SystemService.getXrayStudioSetting()
                    .then(function(settings){
                        deferred.resolve(settings);
                    })
                    .catch(function(err){
                        deferred.reject(err);
                    })
                }

                return deferred.promise;
            }

            this.getDefualtSettings = function(){

                return {
                    name: "Default",
                    setting: defaultSetting
                }

            }

            this.getEmptySettings = function(){
                return {
                    name: "",
                    setting: defaultSetting
                }
            }

            this.getSelectedSubSetting = function(name){
                var subSetting = defaultSetting[name];
                return subSetting ? subSetting : {};
            }

            var defaultSetting = {
                maxilliaryRight: {
                    name: "Maxilliary Right",
                    setting: {
                        "id_max_right_iop_1": "0",
                        "id_max_right_iop_2": "0",
                        "id_max_right_post_pa_3": "0",
                        "id_max_right_post_pa_4": "0"
                    }
                },
                maxillaryAnterior: {
                    name: "Maxilliary Anterior",
                    setting: {
                        "id_max_anterior_iop_1": "0",
                        "id_max_anterior_iop_2": "0",
                        "id_max_anterior_iop_3": "0",
                        "id_max_anterior_pa_4": "0",
                        "id_max_anterior_pa_5": "0",
                        "id_max_anterior_pa_6": "0"
                    }
                },
                maxillaryLeft: {
                    name: "Maxillary Left",
                    setting: {
                        "id_max_left_iop_1": "0",
                        "id_max_left_iop_2": "0",
                        "id_max_left_post_pa_3": "0",
                        "id_max_left_post_pa_4": "0"
                    }
                },
                mandibularLeft: {
                    name: "Mandibular Left",
                    setting: {
                        "id_mandibular_left_post_pa_1": "0",
                        "id_mandibular_left_post_pa_2": "0",
                        "id_mandibular_left_iop_3": "0",
                        "id_mandibular_left_iop_4": "0"
                    }
                },
                mandibularAnterior: {
                    name: "Mandibular Anterior",
                    setting: {
                        "id_mandibular_anterior_pa_1": "0",
                        "id_mandibular_anterior_pa_2": "0",
                        "id_mandibular_anterior_pa_3": "0",
                        "id_mandibular_anterior_iop_4": "0",
                        "id_mandibular_anterior_iop_5": "0",
                        "id_mandibular_anterior_iop_6": "0"
                    }
                },
                mandibularRight: {
                    name: "Mandibular Right",
                    setting: {
                        "id_mandibular_right_post_pa_1": "0",
                        "id_mandibular_right_post_pa_2": "0",
                        "id_mandibular_right_iop_3": "0",
                        "id_mandibular_right_iop_4": "0"
                    }
                },
                biteWingRight: {
                    name: "Bite Wing Right",
                    setting: {
                        "id_right_bw_1": "0",
                        "id_right_bw_2": "0"
                    }
                },
                biteWingLeft: {
                    name: "Bite Wing Left",
                    setting: {
                        "id_left_bw_1": "0",
                        "id_left_bw_2": "0"
                    }
                },
                pano: {
                    name: "Pano",
                    setting: {
                        "id_pano": "0"
                    }
                },
                ceph: {
                    name: "Ceph",
                    setting: {
                        "id_ceph": "0"
                    }
                },
                ortho: {
                    name: "Ortho",
                    setting: {
                        "id_profile_1": "0",
                        "id_profile_2": "0",
                        "id_profile_3": "0",
                        "id_jaw_inside_1": "0",
                        "id_jaw_inside_2": "0",
                        "id_jaw_outside_1": "0",
                        "id_jaw_outside_2": "0",
                        "id_jaw_outside_3": "0"
                    }
                },     
            }

            this.getSettingFromSettingsObj = function(settingObj){
                // logger.info("This is the setting");
                // logger.info(settingObj);
                // var settingObj = setting;
                return {
                    "id_max_right_iop_1": settingObj.maxilliaryRight.setting.id_max_right_iop_1,
                    "id_max_right_iop_2": settingObj.maxilliaryRight.setting.id_max_right_iop_2,
                    "id_max_right_post_pa_3": settingObj.maxilliaryRight.setting.id_max_right_post_pa_3,
                    "id_max_right_post_pa_4": settingObj.maxilliaryRight.setting.id_max_right_post_pa_4,
                    "id_max_anterior_iop_1": settingObj.maxillaryAnterior.setting.id_max_anterior_iop_1,
                    "id_max_anterior_iop_2": settingObj.maxillaryAnterior.setting.id_max_anterior_iop_2,
                    "id_max_anterior_iop_3": settingObj.maxillaryAnterior.setting.id_max_anterior_iop_3,
                    "id_max_anterior_pa_4": settingObj.maxillaryAnterior.setting.id_max_anterior_pa_4,
                    "id_max_anterior_pa_5": settingObj.maxillaryAnterior.setting.id_max_anterior_pa_5,
                    "id_max_anterior_pa_6": settingObj.maxillaryAnterior.setting.id_max_anterior_pa_6,
                    "id_max_left_iop_1": settingObj.maxillaryLeft.setting.id_max_left_iop_1,
                    "id_max_left_iop_2": settingObj.maxillaryLeft.setting.id_max_left_iop_2,
                    "id_max_left_post_pa_3": settingObj.maxillaryLeft.setting.id_max_left_post_pa_3,
                    "id_max_left_post_pa_4": settingObj.maxillaryLeft.setting.id_max_left_post_pa_4,
                    "id_mandibular_left_post_pa_1": settingObj.mandibularLeft.setting.id_mandibular_left_post_pa_1,
                    "id_mandibular_left_post_pa_2": settingObj.mandibularLeft.setting.id_mandibular_left_post_pa_2,
                    "id_mandibular_left_iop_3": settingObj.mandibularLeft.setting.id_mandibular_left_iop_3,
                    "id_mandibular_left_iop_4": settingObj.mandibularLeft.setting.id_mandibular_left_iop_4,
                    "id_mandibular_anterior_pa_1": settingObj.mandibularAnterior.setting.id_mandibular_anterior_pa_1,
                    "id_mandibular_anterior_pa_2": settingObj.mandibularAnterior.setting.id_mandibular_anterior_pa_2,
                    "id_mandibular_anterior_pa_3": settingObj.mandibularAnterior.setting.id_mandibular_anterior_pa_3,
                    "id_mandibular_anterior_iop_4": settingObj.mandibularAnterior.setting.id_mandibular_anterior_iop_4,
                    "id_mandibular_anterior_iop_5": settingObj.mandibularAnterior.setting.id_mandibular_anterior_iop_5,
                    "id_mandibular_anterior_iop_6": settingObj.mandibularAnterior.setting.id_mandibular_anterior_iop_6,
                    "id_mandibular_right_post_pa_1": settingObj.mandibularRight.setting.id_mandibular_right_post_pa_1,
                    "id_mandibular_right_post_pa_2": settingObj.mandibularRight.setting.id_mandibular_right_post_pa_2,
                    "id_mandibular_right_iop_3": settingObj.mandibularRight.setting.id_mandibular_right_iop_3,
                    "id_mandibular_right_iop_4": settingObj.mandibularRight.setting.id_mandibular_right_iop_4,
                    "id_right_bw_1": settingObj.biteWingRight.setting.id_right_bw_1,
                    "id_right_bw_2": settingObj.biteWingRight.setting.id_right_bw_2,
                    "id_left_bw_1": settingObj.biteWingLeft.setting.id_left_bw_1,
                    "id_left_bw_2": settingObj.biteWingLeft.setting.id_left_bw_2,
                    "id_pano": settingObj.pano.setting.id_pano,
                    "id_ceph": settingObj.ceph.setting.id_ceph,
                    "id_profile_1": settingObj.ortho.setting.id_profile_1,
                    "id_profile_2": settingObj.ortho.setting.id_profile_2,
                    "id_profile_3": settingObj.ortho.setting.id_profile_3,
                    "id_jaw_inside_1": settingObj.ortho.setting.id_jaw_inside_1,
                    "id_jaw_inside_2": settingObj.ortho.setting.id_jaw_inside_2,
                    "id_jaw_outside_1": settingObj.ortho.setting.id_jaw_outside_1,
                    "id_jaw_outside_2": settingObj.ortho.setting.id_jaw_outside_2,
                    "id_jaw_outside_3": settingObj.ortho.setting.id_jaw_outside_3
                }
            }

            function getRotationSettings(clinicId) {
                var deferred = $q.defer();

                var requestType = "getRotationSettings";
                var requestObject = {
                    clinic_id: clinicId
                }

                logger.info("getting rotation settings");
                t32SystemService.submitSystemRequest(requestType, requestObject).then(function (settings) {
                    // logger.debug(settings)
                    deferred.resolve(settings);
                })
                .catch(function (err) {
                    deferred.reject(err);
                })

                return deferred.promise;
            }

            function saveRotationSettings(clinicId, settingsToBeSaved, settingsToBeDeleted) {
                var deferred = $q.defer();

                var requestType = "saveRotationSettings";
                var requestObject = {
                    clinic_id: clinicId,
                    settingsToBeSaved: settingsToBeSaved,
                    settingsToBeDeleted: settingsToBeDeleted
                }

                logger.info('saving rotation settings')
                t32SystemService.submitSystemRequest(requestType, requestObject).then(function (settings) {
                    logger.info("We have the settings coming back from saving rotaiton settings.");
                    deferred.resolve(settings);
                })
                .catch(function (err) {
                    deferred.reject(err);
                })

                return deferred.promise;
            }

            function getXrayStudioSetting() {
                var deferred = $q.defer();

                logger.info('getting studio settings')

                t32SystemService.getXrayStudioSetting()
                .then(function (setting) {
                    //alert("We have the settings");
                    // logger.debug(setting);

                    deferred.resolve(setting);
                })
                .catch(function (err) {
                    deferred.reject(err);
                })

                return deferred.promise;
            }

            function saveXrayStudioSetting(setting) {
                var deferred = $q.defer();

                // var requestType = "saveXrayStudioSetting";
                // var requestObject = setting

                // //alert("WE ARE IN SAVE STUDIO SETTINGS")
                // logger.debug(setting);

                // t32SystemService.submitSystemRequest(requestType, requestObject).then(function (setting) {
                t32SystemService.saveXrayStudioSetting(setting).then(function(setting){
                    deferred.resolve();
                })
                .catch(function (err) {
                    deferred.reject(err);
                })

                

                return deferred.promise;
            }

        }

    ])
})(window, window.angular);