const electron = require('electron');
// const {ipcRenderer} = electron;
// const {remote} = electron;
const process = window.require('process');
const path = window.require('path');
const fs = window.require("fs-extra");
const os = window.require('os');
const logger = require('electron-log');
const rootPath = process.cwd().split(path.sep)[0];
const homePath = os.platform() === 'darwin' ? electron.app.getPath('home') : os.platform() === 'win32' ? path.join(rootPath, '/ProgramData/xraystudio/logs/') : null;
const logPath = path.join(homePath, 'XrayStudioFrontendV1.log');
fs.ensureDirSync(homePath);
// const fabric = require('fabric').fabric;

function boot() {
	console.log("the log path ", logPath);

	logger.transports.file.level = 'info'
	logger.transports.file.file = logPath;
	logger.transports.maxSize = 10048576;
	// Get logger instance and inject it in Angular
	angular
		.module('app')
		.value('logger', logger);

	angular.bootstrap(document, ['app'], {
		strictDi: true
	});
}

document.addEventListener('DOMContentLoaded', boot);

// ipcRenderer.on('update-message', function(event, method) {
//     alert(method);
// });

if (fabric.enableGLFiltering && fabric.isWebglSupported && fabric.isWebglSupported(fabric.textureSize)) {
	console.log('max texture size: ' + fabric.maxTextureSize);
	fabric.textureSize = 7000;
}