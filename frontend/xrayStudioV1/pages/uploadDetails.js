(function (window, angular, undefined) {
    'use strict';

    angular.module('app').directive('uploadDetails', [
        '$modal',
        '$timeout',
        '$rootScope',
        function (
            $modal,
            $timeout,
            $rootScope){
        return{
            restrict: 'EA',
            scope: {
                uploads: "="
            },
            link: function(scope, element, attr){
                element.bind('click', function () {
                    var modalInstance = $modal.open({
                        templateUrl: 'pages/uploadDetails.html',
                        controller: 'UploadDetailsCtrl',
                        windowClass: 'md',
                        backdrop: 'static',
                        resolve: {
                            uploads: function(){
                                return scope.uploads;
                            }
                        }
                    });

                    modalInstance.result.then(function (res) {
                    }, function () {
                    });
                })
            }
        }
    }]);

    angular.module('app').controller('UploadDetailsCtrl', [
        '$scope',
        '$q',
        '$location',
        '$modal',
        'SweetAlert',
        'logger',
        't32SystemService',
        't32WorkflowService',
        't32PatientSelectionService',
        '$timeout',
        't32AlertService',
        '$rootScope',
        '$modalInstance',
        'uploads',
        function (
            $scope,
            $q,
            $location,
            $modal,
            SweetAlert,
            logger,
            t32SystemService,
            t32WorkflowService,
            t32PatientSelectionService,
            $timeout,
            t32AlertService,
            $rootScope,
            $modalInstance,
            uploads) {

            function init() {
                $scope.theModalInstance = $modalInstance;
                $scope.uploads = uploads;
                $scope.retryMap = {};
            }

            $scope.getProgress = function(uploaded, total){ 
                var completionPct = Math.floor((uploaded / total) * 100);
                return {
                    "width": completionPct + "%"
                }
            }

            $scope.isFileFinished = function(status, fileUploaded){
                if (status == 'inProgress' || status == 'finished'){
                    if(fileUploaded){
                        return "list-group-item-success"
                    }
                    else{
                        return "list-group-item-info"
                    }
                }
                else{
                    if(fileUploaded){
                        return "list-group-item-success"
                    }
                    else{
                        return "list-group-item-danger"
                    }
                }
            }

            $scope.isCollapsed = function(id){
                var targetId = '#' + id;
                return $(targetId).hasClass('collapse') && $(targetId).hasClass('in');
            }

            $scope.collapse = function(id){
                var targetId = '#' + id;
                $(targetId).toggleClass('in');
                if ($(targetId).hasClass('collapse') && $(targetId).hasClass('in')){
                    // console.log('moving caret up')
                    // console.log($('#caret-up-' + id));
                    $('#caret-up-' + id).show();
                    $('#caret-down-' + id).hide();
                }   
                else{
                    // console.log('moving caret down')
                    $('#caret-up-' + id).hide();
                    $('#caret-down-' + id).show();
                }
            }

            $scope.retryUpload = function(id){
                if($scope.retryMap[id]){
                    return
                }
                else{
                    $scope.retryMap[id] = true;
                    $scope.hideRetryButton(id);
                    $rootScope.$broadcast('retryUpload', id);
                    $scope.retryMap[id] = false;
                }
            }

            $scope.hideRetryButton = function(id){
                console.log("TODO hide retry button")
            }

            $scope.titleImg = "img/upload.svg";
            $scope.titleStyle = {
                "margin-left": "-15px",
                "margin-right": "-15px"
            };
            $scope.titleImgStyle = {
                "width": "100px",
                "position": "absolute",
                "top": "5px",
                "left": "25px"
            }

            init()
        }
    ]);
})(window, window.angular);