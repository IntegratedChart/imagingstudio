(function (window, angular, undefined) {
   'use strict';

    angular.module('app').controller('landingCtrl', [
            '$scope', 
            '$q',
            '$location',
            '$modal',
            '$modalStack',
            'SweetAlert',
            'logger',
            't32SystemService',
            't32WorkflowService',
            't32PatientSelectionService',
            '$timeout',
            't32AlertService',
            '$rootScope',
        function (
            $scope,
            $q,
            $location,
            $modal,
            $modalStack,
            SweetAlert,
            logger,
            t32SystemService,
            t32WorkflowService,
            t32PatientSelectionService,
            $timeout,
            t32AlertService,
            $rootScope) 
        {
            function init()
            {
                logger.info('........landing.js init()........');
                $scope.hasImagingSubscription = false;
                $scope.hideView = false;
                $scope.year = moment().year();
                t32SystemService.initializeListener();
                t32SystemService.getSystemOS();
                // const { ipcRenderer } = require('electron');
                logger.info("....ladning.js...getXrayStudioSetting called....")
                t32SystemService.getXrayStudioSetting()
                .then(function(setting){
                    logger.info("....ladning.js...getXrayStudioSetting results....")
                    $scope.setting = setting;
                    logger.info("has clinic id ", $scope.setting.clinic_id)
                    if ($scope.setting.clinic_id) {
                        logger.info("...getting clinic subscriptions...")
                        $scope.hasOfflineImagingStudioAccess($scope.setting.clinic_id)
                        .then(function(hasAccess){
                            logger.info("clinic has access ", hasAccess);
                            $scope.hasImagingSubscription = hasAccess;
                        })
                    }
                })
            }

            $scope.hasOfflineImagingStudioAccess = function (clinicId) {
                var deferred = $q.defer();
                var requestType = 'offlineClinic';

                var requestObject = {
                    requestType: 'getOfflineImagingStudioAccess',
                    data: {
                        clinicId: clinicId
                    }
                }

                t32SystemService.submitSystemRequest(requestType, requestObject)
                .then(function (hasAccess) {
                    logger.info("THE CLINIC SUBSCRIPTIONS --> ", hasAccess)
                    deferred.resolve(hasAccess);
                })
                .catch(function (err) {
                    logger.info("error getting subscriptions ", err);
                })

                return deferred.promise;
            }

            $rootScope.$on("$routeChangeStart", function(evt, current, prev){
                console.log("-----ROUTE CHANGED----");
                console.log(current);
                console.log(prev);
            })

            $rootScope.$on("clinicUpdate", function(evt, msg) {
                logger.info("got clinic update ", msg);
                var msg = msg.msg;
                $scope.hasImagingSubscription = msg && msg.clinic ? msg.clinic.offlineStudio : false;
            })

            // $scope.startedUploads = [];
            // $scope.failedUploads = [];
            // $scope.finishedUploads = [];

            $scope.uploads = [];

            $scope.onEntry = function() {
                t32WorkflowService.ensureLicense()
                .then(function(){
                    return t32WorkflowService.simulationModePrompt();
                })
                .then(function() {
                    theLoop();
                })
            }

            function theLoop() {
                t32PatientSelectionService.selectPatient()
                .then(function( patientContext ){
                    logger.info("The patient context coming back");
                    logger.info(patientContext);
                    logger.info("The studio context selected folder")
                    logger.info($scope.setting.currentSelectedFolder)

                    if(patientContext.uploadType === 'file'){
                        return openFolderViewer(patientContext);
                    }
                    else{
                        return openXrayTemplate(patientContext);
                    }
                })
                .then(function(){
                    $timeout( function() {
                        theLoop();
                    }, 100 ); 
                })
            }

            function openXrayTemplate(patientContext){
                var deferred = $q.defer();
                var modalInstance = $modal.open({
                    templateUrl: 'widgets/template/xrayTemplate.html',
                    controller: 'XrayTemplateCtrl',
                    windowClass : 'largest-Modal',
                    backdrop: 'static',
                    resolve: {
                        patientContext : function() {
                            return patientContext;
                        },
                    }
                });

                modalInstance.result.then(function (result) {
                    logger.info('after xray template exit logic');
                    logger.info("after xray template exit : ...Getting the studio setting in landing.js ")
                    // logger.info("The scope settings ", $scope.setting);

                    // $scope.setting.currentSelectedFolder = null;
                    // $scope.setting.activeImageCell = "id_max_right_iop_1";
                    // $scope.setting.viewMode = "close";
                    // var currentSetting = null;
                    t32SystemService.getXrayStudioSetting()
                    .then(function (setting) {
                        $scope.setting = setting;
                        $scope.setting.currentSelectedFolder = null;
                        $scope.setting.activeImageCell = "id_max_right_iop_1";
                        $scope.setting.viewMode = "close";
                        return t32SystemService.saveXrayStudioSetting($scope.setting)
                    })
                    .then(function(){
                        return syncDirs()
                    })
                    .then(function (pathsToRemove) {
                        logger.debug("We have the paths to remove in landing page");
                        logger.debug(pathsToRemove);
                        return cleanupDir(pathsToRemove)
                    })
                    .then(function(){
                        deferred.resolve();
                    })
                    .catch(function(err){
                        console.log(err);
                        deferred.resolve();
                    });
                }); 

                return deferred.promise;
            }

            //TODO: The current settings that we are trying to save in this function are not defined and need to be defined 
            /** 
             * 
             * @param {*} patientContext 
             */
            function openFolderViewer(patientContext) {
                var deferred = $q.defer();

                var modalInstance = $modal.open({
                    templateUrl: 'widgets/viewer/t32FolderViewer.html',
                    controller: 't32FolderViewerCtrl',
                    windowClass : 'largest-Modal',
                    // size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        patientContext : function() {
                            return patientContext;
                        },
                    }
                });

                modalInstance.result.then(function (modified) {
                    syncDirs()
                    .then(function (pathsToRemove) {
                        return cleanupDir(pathsToRemove)
                    })
                    .then(function(){                               
                        // return t32SystemService.saveXrayStudioSetting()
                    })
                    .then(function(){
                        deferred.resolve();
                    })
                    .catch(function(err){
                        deferred.resolve();
                    })   
                });  
                
                return deferred.promise;
            }

            /**
             * Ensures that both the original and edited folders are in sync with one another and returns a list of empty dirs that need to be removed.  
             */
            function syncDirs(){
                var deferred = $q.defer();
                var requestType = 'syncFolders';

                t32SystemService.submitSystemRequest(requestType)
                .then(function (pathsToRemove) {
                    deferred.resolve(pathsToRemove);
                })
                .catch(function (err) {
                    deferred.resolve();
                })

                return deferred.promise;             
            }

            /**
             * Cleans up dirs that are empty
             * @param {*} pathsToRemove 
             */
            function cleanupDir(pathsToRemove) {
                var deferred = $q.defer();
                var requestType = 'deleteEmptyFolders';
                var requestObject = {
                    pathsToRemove: pathsToRemove
                }

                logger.info("sending delete request ", requestObject)
                t32SystemService.submitSystemRequest(requestType, requestObject)
                .then(function(){
                    deferred.resolve();
                })
                .catch(function(err){
                    deferred.resolve();
                })

                return deferred.promise;
            }

            // The below section of code are listners for uploading and software update notices that we get form the backend. 
            $scope.$on('uploadStart', function(evt, msg){
                logger.info("*** upload start msg recieved for " + msg.folderName + " ***");

                // $scope.startedUploads.push(msg.fileMap);
                $scope.uploads.push(msg.fileMap);
                logger.info("Pushed into uploads ", $scope.uploads);
                // alert("uploadStart");
                console.log($scope.startedUploads);
                var alertObj = {
                    type: "info",
                    title: "Upload Started!",
                    text: "Uploading files for " + msg.folderName,
                    timeout: 5000,
                    showCloseButton: true
                }
                t32AlertService.showToaster(alertObj);
            })

            $scope.$on('uploadFinish', function(evt, msg){
                logger.info("*** upload finished msg recieved for " + msg.folderName + " ***");
                
                var index = _.findIndex($scope.uploads, function(dir){
                    return dir.id == msg.folderId;
                })

                $scope.uploads[index] = msg.fileMap;
                
                logger.info("The uploads folder after finish ", $scope.uploads);
                // _.remove($scope.startedUploads, function(dir){
                //     return dir.name == msg.folderName;
                // })
                // $scope.finishedUploads.push(msg.fileMap); 
                
                var alertObj = {
                    type: "success",
                    title: "Upload Finished!",
                    text: "Successfully uploaded files for " + msg.folderName,
                    timeout: 5000,
                    showCloseButton: true
                }
                t32AlertService.showToaster(alertObj);
            })

            $scope.$on('uploadsPending', function(){
                logger.info("*** upload pending msg recieved ***");

                var errorObj = {
                    type: "error",
                    title: "Close Failed!",
                    text: "There are still files/offline data being uploaded, please wait for them to finish and try again later.",
                    showCloseButton: true
                }

                // console.log("The current uploads ", $scope.uploads);
                t32AlertService.showToaster(errorObj);
            })

            $scope.$on('uploadFailed', function(evt, msg){
                logger.info("*** upload failed msg recieved for " + msg.folderName + " ***");

                if(msg.folderId){
                    var index = _.findIndex($scope.uploads, function(dir){
                        return dir.id === msg.folderId;
                    })
    
                    $scope.uploads[index] = msg.fileMap
                    logger.info("The uploads folder after failed ", $scope.uploads);
                }

                // _.remove($scope.startedUploads, function (dir) {
                //     return dir.name == msg.folderName;
                // })

                // $scope.failedUploads.push(msg.fileMap);

                var alertObj = {
                    type: "error",
                    title: "Upload Failed!",
                    text: "Failed to upload files for " + msg.folderName,
                    showCloseButton: true
                }

                t32AlertService.showToaster(alertObj);
            })

            $scope.$on('uploadProgressUpdate', function(evt, msg){
                logger.info("*** upload progress msg recieved for " + msg.folderName + " ***")
                var index = _.findIndex($scope.uploads, function (dir) {
                    return dir.id === msg.folderId;
                })

                $scope.uploads[index] = msg.fileMap;

                logger.info("The uploads folder after update ", $scope.uploads);
            })

            $scope.$on('softwareUpdate', function(evt, msg){
                var alertObj = {
                    type: "info",
                    title: "Software Update",
                    text: msg.msg,
                    timeout: 10000,
                    showCloseButton: true
                }
                t32AlertService.showToaster(alertObj);
            })

            $scope.$on('failedDriverCommunication', function(evt, msg){
                var alertObj = {
                    confirmColor: '#ff0000',
                    title: 'Communication Failed With Xray Studio Driver...Please Restart App...',
                    text: msg.error,
                    showCancel: false,
                }
                t32AlertService.showAlert(alertObj)
                .then(function(result){
                    logger.info("Try to close the app now.")
                    t32SystemService.exitAppWindow();
                });

            })
            
            $scope.$on('retryUpload', function(evt, idToRetry){
                console.log("retrying upload for ", idToRetry);
                var uploadToRetry = _.find($scope.uploads, function(dir){
                    return dir.id == idToRetry;
                });

                if(!uploadToRetry){
                    console.log("We cannot find the folder to retry upload on")
                    return 
                }
                else{
                    console.log("Sending to upload ", uploadToRetry.originalRequest);
                    uploadToRetry.originalRequest.retry = true;
                    t32SystemService.submitSystemRequest('uploadPatientXrays', uploadToRetry.originalRequest);

                    _.remove($scope.uploads, function(dir){
                        return dir.id == idToRetry
                    });
                        
                }
            })

            $scope.$on('openXrayTemplate', function (evt, patientContext) {

                logger.info('atemptting to open xray template with patient ', patientContext)
                var deferred = $q.defer();

                openXrayTemplate(patientContext)
                .then(function(){
                    if (!patientContext.secondWindowInstance) {
                        theLoop();
                        return 
                    } else {
                        logger.info("second win ", patientContext.secondWindowInstance)
                        let requestObject = {
                            winName: patientContext.secondWindowInstance
                        }
                        t32SystemService.submitSystemRequest('closeChildWindow', requestObject);
                    }
                })
                .catch(function (err) {
                    deferred.resolve();
                })

                return deferred.promise;
            })

            $scope.$on("clearPatientContext", function() {
                $modalStack.dismissAll('clear');
            })

            $scope.$on("hideView", function(evt, hideViewMsg) {
                logger.info("hideView update", hideViewMsg);
                $scope.hideView = hideViewMsg.msg;
            })
            
            $scope.openChildWindow = function (type) {
                var deferred = $q.defer();

                var path = '';
                var name  = '';
                if (type == 'imagingStudio') {
                    path = "/frontend/imagingStudio/index.html",
                    name = "imagingStudio"
                }

                var requestObject = {
                    path: path,
                    name: name
                }

                t32SystemService.submitSystemRequest('openChildWindow', requestObject)
                .then(function (result) {
                    console.log("done");
                })

                return deferred.promise
            }
            // console.log($scope);
            init();
        }
    ]);
})(window, window.angular);