const builder = require('electron-builder');
const Platform = builder.Platform;
const path = require('path');
var args = process.argv.slice(2);
var buildTarget = args[0] || 'win32';
var arch = args.slice(1, args.length);
arch = arch.length ? arch : ['x64', 'ia32'];


let entryPath = path.resolve("./");
console.log('entry path is ', entryPath);

let files = ["src/**", "backend/**", "e2e/**", "karma.conf.js", "angular.json", "README.md", "tsconfig.json", "tsconfig.app.json", "tsconfig.spec.json", "tslint.json", "main.js", "webpack.config.js"];
let exludedFileList = []

files.forEach((file) => {
  let final = "!" + file;
  exludedFileList.push(final);
})

const SharedConfig = {
  "npmRebuild": true,
  "appId": "com.tab32.imagingStudio",
  "productName": "tab32 Imaging Studio",
  "copyright": "Copyright © 2019 tab32",
  "directories":{
    "app": "./"
  },
  "files": exludedFileList
}
const Config = {
  win32: {
    "win": {
      "target": [{
          "target": "nsis",
          "arch": arch,
        },
        {
          "target": 'zip',
          "arch": arch
        }
      ],
      "icon": "https://images.tab32.com/imagingStudio/assets/imaging-studio.ico",
      // "icon": "build/icons/imaging-studio.ico",
      "verifyUpdateCodeSignature": false,
      "publish": {
        "provider": "generic",
        "url": "https://images.tab32.com/imagingStudio/updates/latest/win/${arch}",
        "publishAutoUpdate": true
      }
    },
    "nsis": {
      "oneClick": false,
      "perMachine": true,
      "warningsAsErrors": false
    },
    //this will bundle any external files, exe's that need to be referenced by the installer, our use case right now is to bundle the driver installer exe here. 
    "extraResources": "./externalResources/**"
  },
  darwin: {
    "dmg": {
      "title": "tab32 Imaging Studio",
      "background": "build/background.png",
      "icon": "build/icon.icns",
      "iconSize": 128,
      "contents": [{
          "x": 355,
          "y": 125,
          "type": "link",
          "path": "/Applications"
        },
        {
          "x": 155,
          "y": 125,
          "type": "file"
        }
      ],
      "publish": {
        "provider": "generic",
        "url": "https://images.tab32.com/imagingStudio/updates/latest/osx",
        "publishAutoUpdate": true
      }
    },
  },
  linux: {}
}

// console.log(Object.assign(Config[buildTarget], SharedConfig));

/**
 * for this build config to work, we must remove the build prop in the package.json. Otherwise the build will build both x64 and ia32 each time. 
 */
builder.build({
    targets: buildTarget == 'win32' ? Platform.WINDOWS.createTarget() : buildTarget == 'darwin' ? Platform.MAC.createTarget() : Platform.LINUX.createTarget(),
    // platform: buildTarget,
    // x64: false,
    // ia32: true,
    config: Object.assign(Config[buildTarget], SharedConfig)
  })
  .then(m => {
    console.log(m)
  })
  .catch(e => {
    console.error(e)
  })



// "build": {
//   "appId": "com.tab32.xrayStudio",
//   "dmg": {
//     "title": "XrayStudio",
//     "background": "build/background.png",
//     "icon": "build/icon.icns",
//     "iconSize": 128,
//     "contents": [
//       {
//         "x": 355,
//         "y": 125,
//         "type": "link",
//         "path": "/Applications"
//       },
//       {
//         "x": 155,
//         "y": 125,
//         "type": "file"
//       }
//     ]
//   },
//   "win": {
//     "target": "squirrel",
//     "icon": "https://images.tab32.com/xrayStudio/icon.ico"
//   }
// }