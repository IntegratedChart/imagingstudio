const glob = require('glob');
const nodeExternals = require('webpack-node-externals');
const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');


let entryFiles = ['./main.js'];
let filePatters = ["./backend/**/*.js"];

console.log("gathering all entry backend files");
filePatters.forEach((pattern) => {
    // console.log("the pattern ", pattern);
    console.log(glob.sync(pattern));
    entryFiles = entryFiles.concat(glob.sync(pattern));
})
console.log('complete file gathering - the entry files ', entryFiles);

console.log("packaging into main.min.js")
module.exports = {
    entry: entryFiles, 
    output:{
        filename: 'main.min.js',
        path: path.resolve('./')
    },
    //webpack tries to overwrite these functions and they will not work unless we tell webpack explicitly not to use them see issue
    //https://github.com/electron/electron/issues/5107
    node:{
        __dirname: false, 
        __filename: false
    },
    target: 'node',
    //for node targets, we need to have 3rd party library that will ignore trying to bundle the node_modules with the minified file.
    externals: [nodeExternals()],
}