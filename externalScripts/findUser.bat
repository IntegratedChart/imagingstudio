
@echo off

SETLOCAL ENABLEEXTENSIONS
REM find if user exits.
set PGUSER=%1
set PGPASSWORD=%2
set DBNAME=%3

FOR /f "usebackq" %%i in (`psql -c \du ^| find "%4" /c`) do set hasUser=%%i

REM psql -c \du | find "%4" /c
echo %hasUser%

exit /B %ERRORlEVEL%