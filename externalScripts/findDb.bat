@echo off

SETLOCAL ENABLEEXTENSIONS

set PGUSER=%1
set PGPASSWORD=%2
set DBNAME=%3
set dbToSearchFor=%4
set currentDir=%cd%

FOR /f "usebackq" %%i in (`psql -l --csv ^| find "%dbToSearchFor%" /c`) do set hasDb=%%i 

echo %hasDb%

exit /B %ERRORlEVEL%

REM /********DO NOT REMOVE BELOW********/

REM set PGUSER=postgres
REM set PGPASSWORD=1234

REM set DBEXEPATH=%1
REM set PGUSER=%2
REM set PGPASSWORD=%3
REM set DBNAME=%4

REM cd %DBEXEPATH%

REM call psql.exe -l --csv | find "%DBNAME%" /c 

REM cd %currentDir%

REM /********DO NOT REMOVE BELOW********/

REM set filepath=%AppData%\postgresql\pgpass.conf

REM if exist %filepath% (
REM     echo "pg pass file already exists"
REM ) else (
REM     echo "creating pg pass file"
REM     cd %AppData%
REM     mkdir postgresql
REM     cd %AppData%\postgresql
REM     echo 127.0.0.1:5432:template1:postgres:1234 > pgpass.conf
REM     cd %currentDir%
REM )

REM set PGPASSFILE=%filepath%

REM echo %PGPASSFILE%