@echo off 

SETLOCAL ENABLEEXTENSIONS

set pgPath=\PostgreSQL\12\bin
REM echo %ProgramW6432%%pgPath%

REM checks to see if env variables have postgresql path set.
for /f "usebackq delims=;" %%i in (` path ^| find "%ProgramW6432%%pgPath%" /c`) do set hasPath=%%i

for /F "usebackq skip=2 tokens=1,2*" %%N in (`%SystemRoot%\System32\reg.exe query HKCU\Environment /v Path 2^>nul`) do if /I "%%N" == "Path" call set "UserPath=%%P"
REM echo %hasPath%
REM echo %UserPath%

if %hasPath% EQU 0 (
  setx path "%UserPath%;%ProgramW6432%%pgPath%"
)

EXIT /B %ERRORLEVEL%
