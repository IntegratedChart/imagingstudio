@echo off
SETLOCAL ENABLEEXTENSIONS


set PGUSER=%1
set PGPASSWORD=%2
set DBNAME=%3
set username=%4
set password=%5

cscript //nologo "%~f0?.wsf" //job:JS
REM cscript //nologo "%~f0?.wsf" //job:VBS
FOR /f "usebackq delims=:" %%i in (`type result.txt ^| find /c "error"`) do set isError=%%i

if %isError% EQU 1 (
	echo %isError%
)

del /f result.txt

EXIT /b %ERRORLEVEL%


----- Begin wsf script --->
<package>
  <job id="JS">
    <script language="JScript">
			var WshShell = WScript.CreateObject("WScript.Shell");
			var password = WshShell.ExpandEnvironmentStrings("%password%")

			var username = WshShell.ExpandEnvironmentStrings("%username%")
    
      function createUser(username, password) {
				WshShell.Run("cmd /c createuser -s -P " + username + " 1> result.txt 2>&1", 2);
				WScript.Sleep(1000);
				WshShell.AppActivate("Windows Command Processor");
				WScript.Sleep(1000);
				WshShell.SendKeys(password + "{Enter}");
				WScript.Sleep(1000);
				WshShell.SendKeys(password + "{Enter}");
				WScript.Sleep(1000);
      }

			createUser(username, password);
    </script>
  </job>
</package>
