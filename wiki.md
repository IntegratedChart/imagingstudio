# Imaging Studio Project Wiki

##Install Steps

**Prerequisites before installing**
 
   * Node V10.13 or higher 
****
1. Pull project form bitbucket [Bitbucket] https://bitbucket.org/IntegratedChart/imagingStudio/src/master/
2. Run `npm install` inside of the root project dir (this should automatically run install for the /app folder as well).
3. There may be errors related to the canvas module on both mac and windows, please see this wiki for help in installing [Github] https://guides.github.com/features/mastering-markdown/ 


## Build and Release
**There are two ways in which we can build our project**

**Option 1 (requires docker)**

1. Go to terminal or cmd 
2. Run `./ build.sh` script 
    * If there are any problems with the build, please try to set npm rebuild to false.

**Option 2**

1. Run `npm run build:win` for windows builds. and `npm run build:mac` for mac builds (mac builds cannot be build on windows machines)

The files will now be inside of **/dist** folder 


**Steps to Release**

1. Locate the following files in the given directories
    * imagingStudio Setup exe inside of /dist
    * latest.yml inside of /dist (for windows builds)
        - if latest.yml is named something else, please rename it to latest.yml
    * app-update.yml inside of win-upacked/resources (x64 build) and win-ia32-unpacked/resources(x86 build).

2. See Below
    * For Windows 
        * Move all three files above to the applicable folder in S3 (you will need to have login for s3, ask for help if you don't have login)
            1. /tab32Images/imagingStudio/updates/latest/win/x64
            2. /tab32Images/imagingStudio/updates/latest/win/ia32
    * For Mac
        * Move the build .dmg file to s3 location 
           /tab32Images/imagingStudio/updates/latest/osx


## Auto-Update
To get updates to work, we need to follow steps for build and release and remove old files and add newly generated ones to the s3 bucket. The built in auto updater(squirell for mac) and (NSIS) for windows, will automatically update. *Note for NSIS there could be some user intervention needed to complete the install. It is not performed in the background. 



## Functionality Breakdown
**Start up logic for app**

* Entry Point `/app/main.js`
    * Fetches studio app settings (stored in json format on machine drive)
    * Initializes the main browser window and calls index.html (frontend angular side)
    * Calls `/app/backend/xrayStudioMain.js`
        * xrayStudioMain.js initiliazes the backend services(redis, express port etc..) and creates studio context object(shared app state within backend and also sent to the front-end).
        * Communication with xray driver is done via Redis, we envoke sajumon's .exe code from within our app and then the rest of communication is done with redis channel. 

**Backend - Frontend Communication**
 
* All communication from front end to backend are done via electron ipc communication
    * fontend code is located `/app/frontend/services/t32SystemService.js`
    * backend code is located `/app/backend/services/xrayStudioIpcMsgHandler.js`


**Driver Communication**

* All driver communication happens via redis `/app/backend/t32XrayStudioRedisHandler.js`
    * `/app/backend/services/channelServices/t32XrayStudioChannelManager.js` acts as middle man to communicate with redis on various channels
        * we have map inside this file that will let us map to the different channel files that have individual actions based on msgs we recieve 
    * The msg is moved forward (if needed) to the frontend using `/app/backend/services/t32XrayStudioDispatchMsg.js`
    * frontend ipc messages are recieved by t32SystemService service. `/app/frontend/services/t32SystemService.js`
        * frontend mirrors approach of backend channel message framework using `/app/frontend/services/t32XrayStudioChannelMapperService.js`
