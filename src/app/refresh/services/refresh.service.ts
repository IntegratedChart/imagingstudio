import { Injectable } from '@angular/core';
import { SystemService } from 'src/app/shared/services/system.service';
import { DataStoreService } from 'src/app/shared/services/data-store.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RefreshService {

  private dataCountSource = new BehaviorSubject<number>(0);
  public dataCount = this.dataCountSource.asObservable();
  // private busySource = new BehaviorSubject<boolean>(false);
  // public busy = this.busySource.asObservable();
  // private allCompleteSource = new BehaviorSubject<boolean>(false);
  // public allComplete = this.allCompleteSource.asObservable();

  constructor(
    private dataStore:DataStoreService,
    private systemService:SystemService
  ) { 
    this.getRefreshDataCount();
  }
  
  /**
   * Gets all the of the patient records that need to be refreshed with EHR.
   */
  async getRefreshDataCount() {
    try {
      let requestMsg = {
        requestType: 'getRefreshDataCount',
        data: {}
      }
      let dataCount = await this.systemService.submitSystemRequest('offlineRefresh', requestMsg);
      this.dataCountSource.next(dataCount);
      return dataCount;
    } catch (e) {
      throw e;
    }
  }

  async refreshOfflineData(patients=null) {
    try {
      // this.busySource.next(true);

      let requestMsg = {
        requestType: 'refreshOfflineData',
        data: {
          clinic_id: this.dataStore.getClinicId(),
          patients: patients
        }
      }

      let refresh = await this.systemService.submitSystemRequest('offlineRefresh', requestMsg);

      // this.busySource.next(false);

      // if (sendAllCompleteMsg) {
      //   this.allCompleteSource.next(true);
      // }

      return 
    } catch (e) {
      // this.busySource.next(false);
      throw e;
    }
  }
}
