import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RefreshButtonComponent } from './components/refresh-button/refresh-button.component';
import { SharedModule } from '../shared/shared.module';



@NgModule({
  declarations: [RefreshButtonComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    RefreshButtonComponent
  ],
  entryComponents: [
    RefreshButtonComponent
  ]
})
export class RefreshModule { }
