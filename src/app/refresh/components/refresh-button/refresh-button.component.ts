import { Component, OnInit, Output, EventEmitter, Input, ChangeDetectorRef } from '@angular/core';
import { RefreshService } from '../../services/refresh.service';
import { AlertService } from 'src/app/shared/services/alert.service';
import { LoggerService } from 'src/app/shared/services/logger.service';
import { DataStoreService } from 'src/app/shared/services/data-store.service';
// import { delay } from 'src/app/shared/utils/commonUtils';

@Component({
  selector: 'app-refresh-button',
  templateUrl: './refresh-button.component.html',
  styleUrls: ['./refresh-button.component.scss']
})
export class RefreshButtonComponent implements OnInit {

  @Input() isDisabled:boolean
  @Input() patient:any
  @Output() refreshing: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() allComplete: EventEmitter<boolean> = new EventEmitter<boolean>();

  public dataCountSubscription:any = null;
  public busyRefreshSubscription:any = null;
  public allCompleteSubscription:any = null;
  // public dataSubscription:any = null;

  public inRefresh:boolean = false;
  public refreshBusy:boolean = false;
  public refreshComplete:boolean = false;
  // public allComplete:boolean = false;

  public refreshCount:number = 0;
  // private delayed = delay(2000);
  // public downloadsInProgress:boolean = false;
  constructor(
    private alertService:AlertService,
    private refreshService:RefreshService,
    private logger: LoggerService,
    private changeDetector: ChangeDetectorRef,
    // private dataStore:DataStoreService
  ) { }

  ngOnInit() {
    this.dataCountSubscription = this.refreshService.dataCount.subscribe((dataCount) => {
      this.refreshCount = dataCount;
      this.changeDetector.markForCheck();
      this.changeDetector.detectChanges();
    })

    // this.busyRefreshSubscription = this.refreshService.busy.subscribe((isBusy) => {
    //   this.refreshBusy = isBusy;
    //   this.changeDetector.markForCheck();
    //   this.changeDetector.detectChanges();
    // })

    // this.dataSubscription = this.dataStore.dataUpdate.subscribe((update) => {
    //   if (update && update.dataType === 'downloadStatus') {
    //     if (this.downloadsInProgress !== update.data.isDownloading) {
    //       this.downloadsInProgress = update.data.isDownloading
    //       this.changeDetector.markForCheck();
    //       this.changeDetector.detectChanges();
    //     }
    //   }
    // })

    // if (this.patient) {
      // this.allCompleteSubscription = this.refreshService.allComplete.subscribe((allComplete) => {
      //   this.allComplete = allComplete;
      //   this.changeDetector.markForCheck();
      //   this.changeDetector.detectChanges();
      // })
    // }
  }

  ngOnDestroy() {
    if (this.dataCountSubscription) {
      this.dataCountSubscription.unsubscribe();
    }

    // if (this.busyRefreshSubscription) {
    //   this.busyRefreshSubscription.unsubscribe();
    // }

    // if (this.allCompleteSubscription) {
    //   this.allCompleteSubscription.unsubscribe();
    // }

    // if (this.dataSubscription) {
    //   this.dataSubscription.unsubscribe();
    // }
  }

  async refreshOfflineData() {
    try {
      if (this.refreshBusy || this.refreshComplete) {
        return 
      }

      this.toggleRefreshState(true);

      // await this.delayed();

      let patient = this.patient ? [this.patient] : null;
      // let allCompleteMsg = this.patient ? false : true;
      await this.refreshService.refreshOfflineData(patient);
      
      await this.refreshService.getRefreshDataCount();

      this.refreshComplete = true;

      this.toggleRefreshState(false);
      return
    } catch (e) {
      
      this.toggleRefreshState(false);
      this.refreshComplete = false;

      this.logger.error('error with refreshing offlinedata ', e);
      let error = {
        text: "Refresh failed! Please try again later.",
        class: "bg-danger text-light"
      }
      this.alertService.showToaster(error)
    }
  }


  toggleRefreshState(state) {
    this.inRefresh = state;
    this.refreshing.emit(state);
  }

}
