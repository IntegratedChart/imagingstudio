import { Injectable } from '@angular/core';
import { SystemService } from 'src/app/shared/services/system.service';
import { DataStoreService } from 'src/app/shared/services/data-store.service';

const MODES = {
  ONLINE: 'online',
  OFFLINE: 'offline',
}

@Injectable({
  providedIn: 'root'
})
export class PatientService {

  constructor(
    private systemService: SystemService,
    private dataStore: DataStoreService,
  ) { }


  async search(searchStr: string, searchMode: string) {
    const clinicId = this.dataStore.getClinicId();

    if (!clinicId || !searchStr) {
      return 'Missing required parameters';
    }

    let requestType;
    
    switch (searchMode) {
      case MODES.ONLINE:
        requestType = 'patientSearch';
        break;
      case MODES.OFFLINE:
        requestType = 'offlinePatientSearch';
        break;
      default:
        requestType = 'patientSearch';
        break;
    }
    
    const searchQuery = {
      clinic_id: clinicId,
      searchInput: searchStr,
    }
    
    const patients = await this.systemService.submitSystemRequest(requestType, searchQuery);
    return patients || [];
  }

  savePatientData(requestType, data) {
    if (requestType === 'savePatient') {
      data.clinic_id = this.dataStore.getClinicId();
    }

    const message = { requestType, data };
    return this.systemService.submitSystemRequest('offlinePatient', message);
  }

  async deletePatientData(type, values) {
    const message = {
      requestType: 'deletePatientData',
      data: { type, values }
    }

    return await this.systemService.submitSystemRequest('offlinePatient', message);
  }

  async getOfflinePatientById(id: string) {
    try {
      const message = {
        requestType: 'getPatient',
        data: id,
      }

      return await this.systemService.submitSystemRequest('offlinePatient', message);
    } catch (error) {
      throw error;
    }
  }
}
