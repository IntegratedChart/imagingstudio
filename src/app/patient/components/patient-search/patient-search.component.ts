import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PatientService } from '../../services/patient.service';
import { delay } from 'src/app/shared/utils/commonUtils';
import { DataStoreService } from 'src/app/shared/services/data-store.service';

const TYPES = {
  SINGLE: 'single',
  ALL: 'all',
}

@Component({
  selector: 'app-patient-search',
  templateUrl: './patient-search.component.html',
  styleUrls: ['./patient-search.component.scss']
})
export class PatientSearchComponent implements OnInit {
  private validModes: string[] = ['single', 'all']
  @Input() public type: string = 'single';
  @Input() public mode: string = 'online';
  @Input() public classList: string = '';
  @Output() onData: EventEmitter<any> = new EventEmitter();

  searchResult = [];
  private delayed = delay();
  private dummyMoreRecordsName: string = 'FOR MORE PATIENTS, CLICK HERE FOR ADVANCED SEARCH';

  constructor(
    private patientService: PatientService,
  ) { }
  
  ngOnInit() {
    
  }

  onSearchInput(event): void {
    const searchStr = event.target.value || '';
    
    this.delayed().then(async () => {
      let patients = await this.patientService.search(searchStr, this.mode);

      if (typeof patients === 'string') {
        patients = [];
      }

      switch (this.type) {
        case TYPES.SINGLE:
          //TODO::
          break;
        case TYPES.ALL:
          this.onData.emit(patients);
          break;
        default:
          break;
      }
    });
  }

  selectPatient(patient_id: string): void {

  }
}
