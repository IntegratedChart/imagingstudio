import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TreatmentQuickKeysComponent } from 'src/app/treatment/components/treatment-quick-keys/treatment-quick-keys.component';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { QuickKey } from 'src/app/treatment/interfaces/quick-key';
import { PatientService } from '../../services/patient.service';
import { ToastService } from 'src/app/shared/services/toast.service';

@Component({
  selector: 'app-patient-treatment',
  templateUrl: './patient-treatment.component.html',
  styleUrls: ['./patient-treatment.component.scss']
})
export class PatientTreatmentComponent implements OnInit {
  treatments = [];
  patient_id: string;
  defaultLocation: string = '';
  
  patientTreatmentsForm: FormGroup;
  treatmentForms: FormArray;
  widgetMode: string = 'Add' // Add or Edit
  // public selectedQuickKey:QuickKey = null;

  constructor(
    public modal: NgbActiveModal,
    public modalService:NgbModal,
    private formBuilder: FormBuilder,
    private patientService: PatientService,
    private toast: ToastService,
  ) { }

  ngOnInit() {
    if (this.treatments.length) {
      this.widgetMode = 'Edit';
    }

    this.patientTreatmentsForm = this.formBuilder.group({
      location: [this.defaultLocation, Validators.required],
      provider: [null, Validators.required],
      patientId: [null, Validators.required],
      treatments: this.formBuilder.array([]),
    })

    // create partial form array
    this.treatmentForms = this.patientTreatmentsForm.get('treatments') as FormArray;

    // patch top level values
    this.patchForm();

    // insert empty treatment if there is no treatment
    if (!this.treatments || !this.treatments.length) {
      this.addTreatment();
    }

  }

  close() {
    this.modal.close('');
  }

  onQuickKeySelect(key: QuickKey) {
    // this.selectedQuickKey = key;
    key.treatments.forEach(treatment => {
      delete treatment._id;
      treatment.completed = true;
      this.treatments.push(treatment);
    })
  }

  onEditQuickKey(key): void {
    const quickKeyModalRef = this.modalService.open(TreatmentQuickKeysComponent, { windowClass: 'mt-4', size: 'xl', backdrop: 'static' });

    if (key) {
      quickKeyModalRef.componentInstance.key = JSON.parse(JSON.stringify(key));
      quickKeyModalRef.componentInstance.isEdit = true;
    } else {
      quickKeyModalRef.componentInstance.isEdit = false;
    }

    quickKeyModalRef.result.then((data) => {
      console.log(data);
    }).catch((error) => console.log(error));
  }

  setTreatmentForm(form: FormGroup): void {
    this.treatmentForms.push(form);
  }

  async handleSubmission(event) {
    try {
      event.preventDefault();
      if (this.patientTreatmentsForm.untouched) {
        this.patientTreatmentsForm.markAllAsTouched();
      }

      if (this.patientTreatmentsForm.valid && this.treatmentForms.length) {
        const result = await this.patientService.savePatientData('saveTreatment', this.patientTreatmentsForm.value);
        this.close()
      } else {
        throw 'Invalid Submission'
      }
    } catch (error) {
      this.toast.show('Invalid Submission', { classname: 'bg-danger text-white' });
      console.log('patient treatments form submission error', error);
    }
  }

  handleLocationSelection(val) {
    this.patientTreatmentsForm.get('location').setValue(val);
    this.patientTreatmentsForm.get('provider').setValue(null);
  }

  handleProviderSelection(val) {
    this.patientTreatmentsForm.get('provider').setValue(val);
  }

  addTreatment() {
    this.treatments.push({ completed: true });
  }

  removeTreatment(index) {
    this.treatments.splice(index, 1);
    this.treatmentForms.removeAt(index);
  }

  patchForm() {
    const patchObj = {
      patientId: this.patient_id,
    }

    if (this.treatments && this.treatments.length) {
      patchObj['location'] = this.treatments[0].locationId || null;
      patchObj['provider'] = this.treatments[0].providerId || null;
    }

    this.patientTreatmentsForm.patchValue(patchObj);
  }
}
