import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientTreatmentComponent } from './patient-treatment.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { TreatmentModule } from 'src/app/treatment/treatment.module';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastService } from 'src/app/shared/services/toast.service';
import { PatientService } from '../../services/patient.service';
import { FormBuilder } from '@angular/forms';

describe('PatientTreatmentComponent', () => {
  let component: PatientTreatmentComponent;
  let fixture: ComponentFixture<PatientTreatmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientTreatmentComponent ],
      imports: [
        SharedModule,
        TreatmentModule
      ],
      providers: [
        NgbModal,
        NgbActiveModal,
        ToastService,
        PatientService,
        FormBuilder,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientTreatmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
