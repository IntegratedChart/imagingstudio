import { Component, OnInit, Input } from '@angular/core';
import { Patient } from '../../interfaces/patient';
import { DatePipe } from '@angular/common';
import { CustomEmailPipe } from '../../../shared/pipes/custom-email.pipe';
import { CustomTelPipe } from '../../../shared/pipes/custom-tel.pipe';

const keyToIconMap = {
  email: 'envelope',
  smsPhone: 'phone',
  phone: 'phone',
}

@Component({
  selector: 'app-patient-detail-view',
  templateUrl: './patient-detail-view.component.html',
  styleUrls: ['./patient-detail-view.component.scss']
})
export class PatientDetailViewComponent implements OnInit {
  detailKeys: string[] = [];
  detailStr: string;
  _patient: Patient;
  
  @Input() header: string; // key from patient obj to use as header
  @Input()
  set details(val: string) { // comma seperated keys from patient obj to build details string
    this.detailKeys = val ? val.split(',') : [];
  }

  @Input()
  set patient (patient: Patient) {
    if (!patient) {
      return;
    }

    this._patient = patient;
    this.detailStr = this.buildDetailString();
  }

  constructor() { }

  ngOnInit() {
  }

  buildDetailString(): string {
    let str = '';
    
    this.detailKeys.forEach(key => {
      const nestedObjKeys = key.split('.').map(str => str.trim());
      let value;

      // get value from nested object
      nestedObjKeys.forEach(key => {
        value = (value) ? value[key] : this._patient[key];
      });

      if (value) {
        const lastKey = nestedObjKeys.pop();
        // generate readable text based on type. eg: dob etc
        value = this.filterValue(lastKey, value);
        const label = this.buildLabel(lastKey);
        str += `<span class="mr-2">${label} ${value}</span>`;
      }
    });

    return str;
  }

  buildLabel(label: string): string {
    const icon = keyToIconMap[label];
    return icon ? `<span class="fa fa-${icon}"></span>` : `<strong class="text-capitalize">${label}:</strong>`;
  }

  filterValue(key: string, value: any): string {
    key = key.toLowerCase();

    switch (key) {
      case 'dob':
        value = new DatePipe('en-US').transform(value, 'shortDate')
        break;
      case 'email':
        value = new CustomEmailPipe().transform(value);
        break;
      case 'smsphone':
      case 'phone':
        value = new CustomTelPipe().transform(value);
        break;
      default:
        break;
    }

    return value;
  }
}
