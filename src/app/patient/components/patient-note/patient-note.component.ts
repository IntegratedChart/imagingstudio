import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PatientService } from '../../services/patient.service';
import NoteTypes from '../../../shared/constants/note-types';
import { Note } from '../../interfaces/note';
import { ToastService } from 'src/app/shared/services/toast.service';

@Component({
  selector: 'app-patient-note',
  templateUrl: './patient-note.component.html',
  styleUrls: ['./patient-note.component.scss']
})
export class PatientNoteComponent implements OnInit {
  note: Note | {[key: string]: any} = {};
  patient_id: string;
  noteForm: FormGroup;
  label: string = '';
  noteTypes; 
  
  constructor(
    public modal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private patientService: PatientService,
    private toast: ToastService,
    ) { }
    
  ngOnInit() {
    this.noteTypes = (this.note.reference_id) ? NoteTypes.treatment : NoteTypes.medical;
    this.label = (this.note._id) ? 'Edit' : 'Add';

    this.noteForm = this.formBuilder.group({
      _id: null,
      patient_id: ['', Validators.required],
      noteType: ['', Validators.required],
      alert: [false],
      notes: ['', Validators.required],
      reference_id: [''],
      reference_desc: [''],
    });

    if (!this.note.patient_id) {
      this.note.patient_id = this.patient_id;
    }

    this.noteForm.patchValue(this.note);

    if (this.note.reference_id) {
      this.noteForm.get('noteType').disable();
    }
  }

  setAlert() {
    const alertValue = !this.noteForm.get('alert').value;
    this.noteForm.get('alert').setValue(alertValue);
  }

  async handleSubmission(event: HTMLFormElement) {
    event.preventDefault();

    if (this.noteForm.untouched) {
      this.noteForm.markAllAsTouched();
    }
    console.log(this.noteForm);

    if (this.noteForm.invalid) {
      this.toast.show('Invalid Submission', { classname: 'bg-danger text-white' });
      return;
    }

    this.noteForm.value.date = Date().toString();
    
    if (this.note.reference_id) {
      this.noteForm.value.noteType = 'treatment';
    }
    
    try {
      const result = await this.patientService.savePatientData('saveNote', this.noteForm.value);
      
      this.close();
    } catch (error) {
      console.log(error);
    }
  }

  close(data = null) {
    this.modal.close(data);
  }

  handleNoteTemplate(template) {
    if (!template || !template.textTemplates) { return; }

    let notes = this.noteForm.get('notes').value || '';

    Object.keys(template.textTemplates).forEach(key => {
      if (notes) {
        notes += '\n';
      }

      notes += template.textTemplates[key].text;
    });

    if (notes) {
      this.noteForm.get('notes').setValue(notes);
    }
  }
}
