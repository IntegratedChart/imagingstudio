import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientNoteComponent } from './patient-note.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormBuilder } from '@angular/forms';
import { TreatmentModule } from 'src/app/treatment/treatment.module';
import { XrayModule } from 'src/app/xray/xray.module';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastService } from 'src/app/shared/services/toast.service';
import { PatientService } from '../../services/patient.service';

describe('PatientNoteComponent', () => {
  let component: PatientNoteComponent;
  let fixture: ComponentFixture<PatientNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientNoteComponent ],
      imports: [
        SharedModule,
      ],
      providers: [
        FormBuilder,
        NgbActiveModal,
        ToastService,
        PatientService,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
