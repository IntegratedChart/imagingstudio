import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPatientComponent } from './new-patient.component';
import { FormBuilder } from '@angular/forms';
import { Location } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('NewPatientComponent', () => {
  let component: NewPatientComponent;
  let fixture: ComponentFixture<NewPatientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPatientComponent ],
      imports: [
        SharedModule,
        RouterTestingModule
      ],
      providers: [
        Location,
        FormBuilder,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
