import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import * as moment from 'moment';
import { PatientService } from '../../services/patient.service';
import { ToastService } from 'src/app/shared/services/toast.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { STATES } from '../../../shared/constants/states';

@Component({
  selector: 'app-new-patient',
  templateUrl: './new-patient.component.html',
  styleUrls: ['./new-patient.component.scss']
})
export class NewPatientComponent implements OnInit {

  @Input() mode: string = 'offline'; // online or offline

  newPatientForm: FormGroup;
  submitted: boolean = false;
  patient;
  states: String[] = STATES;

  constructor(
    private formBuilder: FormBuilder,
    private patientService: PatientService,
    private toast: ToastService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private location: Location,
  ) { }

  ngOnInit() {
    // build form
    this.newPatientForm = this.formBuilder.group({
      fname: ['', Validators.required],
      mname: [''],
      lname: ['', Validators.required],
      dob: ['', Validators.required],
      gender: [''],
      ssn: [''],
      add_line1: [''],
      add_line2: [''],
      city: [''],
      state: [''],
      zipcode: [''],
      phone: [''],
      email: ['', [this.conditionalValidator('emailConsent')]],
      smsPhone: ['', [this.conditionalValidator('smsConsent')]],
      preferEmail: [false],
      preferText: [false],
      preferCall: [false],
      emailConsent: [false],
      smsConsent: [false],
      eStatementConsent: [false],
      notes: [''],
      _id: null,
    });

    this.listenFieldChanges();

    // get patient if passed in and patch form
    this.activeRoute.params.subscribe(params => {
      const patient = params.data ? JSON.parse(params.data) : null;
      
      if (patient) {
        this.patient = patient;
        this.newPatientForm.patchValue(patient);
      }
    });
  }

  get form() {
    return this.newPatientForm.controls;
  }

  listenFieldChanges() {
    this.newPatientForm.get('emailConsent')
      .valueChanges.subscribe((value) => this.updateValue('email'));
    
    this.newPatientForm.get('smsConsent')
      .valueChanges.subscribe((value) => this.updateValue('smsPhone'));
  }

  updateValue(field: string): void {
    this.newPatientForm.get(field).updateValueAndValidity();
  }

  setValue(value: any, fieldName: string) {
    this.newPatientForm.get(fieldName).setValue(value);
  }

  conditionalValidator(targetField: string) {
    return (formControl: AbstractControl) => {
      if (!formControl.parent) { return null; }
  
      if (formControl.parent.get(targetField).value) {
        return Validators.required(formControl);
      }
  
      return null;
    }
  }

  async handleSubmission(event) {
    try {
      event.preventDefault();
      this.submitted = true;
  
      if (this.newPatientForm.invalid) { throw 'Invalid Submission'; }
  
      const patient = this.newPatientForm.value;
      patient.search_string = this.generatePatientSearchString(patient);
      patient.name = `${patient.fname} ${patient.lname}`;
      const result = await this.patientService.savePatientData('savePatient', patient)

      // navigate to details page
      this.router.navigate(['/offline-patient', { data: JSON.stringify(result) }]);
    } catch (error) {
      this.toast.show('Invalid Submission', { classname: 'bg-danger text-white' });
    }
  }

  generatePatientSearchString(patient) {
    let search_string = '';
    search_string += (patient.fname) ? patient.fname + ' ' : '';
    search_string += (patient.lname) ? patient.lname + ' ' : '';
    search_string += (patient.nickname) ? patient.nickname + ' ' : '';
    search_string += (patient.email) ? patient.email + ' ' : '';
    search_string += (patient.smsPhone) ? patient.smsPhone + ' ' : '';
    search_string += (patient.phone) ? patient.phone + ' ' : '';

    if (patient.dob) {
        search_string += moment(patient.dob).format('MM/DD/YY').toString();
    }

    return search_string.toLowerCase();
  }

  back() {
    this.location.back();
  }
}
