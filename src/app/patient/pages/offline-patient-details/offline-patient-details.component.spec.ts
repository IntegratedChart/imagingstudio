import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfflinePatientDetailsComponent } from './offline-patient-details.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { XrayModule } from 'src/app/xray/xray.module';
import { AlertService } from 'src/app/shared/services/alert.service';
import { PatientService } from '../../services/patient.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataStoreService } from 'src/app/shared/services/data-store.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('OfflinePatientDetailsComponent', () => {
  let component: OfflinePatientDetailsComponent;
  let fixture: ComponentFixture<OfflinePatientDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfflinePatientDetailsComponent ],
      imports: [
        SharedModule,
        XrayModule,
        RouterTestingModule,
      ],
      providers: [
        DataStoreService,
        NgbModal,
        PatientService,
        AlertService,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfflinePatientDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
