import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Patient } from '../../interfaces/patient';
import { DataStoreService } from 'src/app/shared/services/data-store.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PatientNoteComponent } from '../../components/patient-note/patient-note.component';
import { PatientTreatmentComponent } from '../../components/patient-treatment/patient-treatment.component';
import { PatientService } from '../../services/patient.service';
import { Note } from '../../interfaces/note';
import { QUADRANTS } from '../../../shared/constants/tooth';
import { AlertService } from 'src/app/shared/services/alert.service';

@Component({
  selector: 'app-offline-patient-details',
  templateUrl: './offline-patient-details.component.html',
  styleUrls: ['./offline-patient-details.component.scss']
})
export class OfflinePatientDetailsComponent implements OnInit {
  patient: Patient;
  medicalNotes: Note[] = [];
  treatments = [];
  clinicId: string;

  constructor(
    private activeRoute: ActivatedRoute,
    private dataStoreService: DataStoreService,
    private modalService: NgbModal,
    private patientService: PatientService,
    private router: Router,
    private alert: AlertService,
  ) { }

  ngOnInit() {
    this.clinicId = this.dataStoreService.getClinicId();

    this.activeRoute.params.subscribe(params => {      
      const patientData = params && params.data ? JSON.parse(params.data) : {};
      this.setPatientData(patientData);
    });
  }

  editPatient() {
    this.router.navigate(['/new-patient', { data: JSON.stringify(this.patient) }]);
  }

  openNoteModal(note: Note = null): void {
    const noteModalRef = this.modalService.open(PatientNoteComponent, { windowClass: 'mt-4', backdrop: 'static' });
    noteModalRef.componentInstance.patient_id = this.patient._id;
    
    if (note) {
      noteModalRef.componentInstance.note = note;
    }

    noteModalRef.result.then((data) => {
      this.getPatient();
    }).catch((error) => {});
  }

  openPreviousXrays() {
    this.router.navigate(['/previous-xrays', { data: JSON.stringify(this.patient) }]);
  }

  hasPreviousXrays() {
    return this.patient && this.patient.xrays && this.patient.xrays.isComplete;
  }

  openTreatmentNote(treatment: any, note = undefined) {
    if (!note) {
      note = {
        reference_id: treatment._id,
        reference_desc: `${treatment.cdtCode} - ${treatment.short_description}`,
        noteType: 'treatment',
      }
    }

    this.openNoteModal(note);
  }

  openTreatmentModal(treatment = null): void {
    const treatmentModalRef = this.modalService.open(PatientTreatmentComponent, { windowClass: 'mt-4', size: 'xl', backdrop: 'static' });
    treatmentModalRef.componentInstance.patient_id = this.patient._id;
    treatmentModalRef.componentInstance.defaultLocation = this.patient.locationId || '';
    
    if (treatment) {
      treatmentModalRef.componentInstance.treatments = [treatment];
    }

    treatmentModalRef.result.then((data) => {
      this.getPatient();
    }).catch(err => {});
  }

  // get patient from backend
  getPatient(): void {
    this.patientService.getOfflinePatientById(this.patient._id)
      .then(patientData => this.setPatientData(patientData[0]));
  }

  // 
  setPatientData(data): void {
    this.patient = data;
    console.log("the patient data ", this.patient)
    // convert object of objects to array of objects
    const allNotes: Note[] = (data.patient_notes) ? Object.values(data.patient_notes) : [];
    this.treatments = (data.treatments) ? Object.values(data.treatments) : [];

    // filter notes
    this.medicalNotes = allNotes.filter((note: Note) => note.noteType != 'treatment'); 
    const treatmentNotes = allNotes.filter((note: Note) => note.noteType === 'treatment');

    for (let treatment of this.treatments) {
      treatment.notes = treatmentNotes.filter(note => note.reference_id == treatment._id);
    }
  }

  getLabelForQuadrant(code) {
    const quadrant = QUADRANTS.filter(quad => quad.code == code)[0];
    return quadrant && quadrant.label || '';
  }

  async delete(type, id) {
    try {
      const alertData = {
        title: 'Are You Sure?',
        text: "You won't be able to recover data once deleted.",
        showCancel: true,
        confirmText: 'Yes',
        confirmColor: '#dc3545',
      }

      const userAction = await this.alert.showAlert(alertData);

      if (userAction.alert === 'cancel') {
        return;
      }

      const deleteValues = [id];
      const deleteResult = await this.patientService.deletePatientData(type, deleteValues);

      if (type === 'patient') {
        this.router.navigate(["/"]);
      } else {
        this.getPatient();
      }
      
    } catch (error) {
      console.log(error);
    }
  }
}