export interface Note {
    _id: string,
    patient_id: string,
    date: Date,
    alert: boolean,
    noteType: string,
    notes: string,
    reference_id?: string,
    reference_desc?: string,
    is_offline: boolean
}
