export interface Patient {
    _id: string,
    name: string,
    fname: string,
    lname: string,
    mname: string,
    smsPhone?: string,
    email?: string,
    clinicData?: {
        chartId: string
    }
    dob: Date,
    gender: number,
    is_offline: boolean,
    //only used by realm if offline record
    chartId?: string,
    locationId?: string,
    xrays?: {
        _id: string,
        patient_id: string, 
        storagePath: string,
        isComplete: string 
    }
}
