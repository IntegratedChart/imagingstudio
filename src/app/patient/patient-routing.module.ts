import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OfflinePatientDetailsComponent } from './pages/offline-patient-details/offline-patient-details.component';
import { NewPatientComponent } from './pages/new-patient/new-patient.component';
import { PatientNoteComponent } from './components/patient-note/patient-note.component';

const patientRoutes: Routes = [
    { 
        path: 'offline-patient', 
        component: OfflinePatientDetailsComponent,
        data: { title: 'Patient'} 
    },
    { 
        path: 'new-patient', 
        component: NewPatientComponent, 
        data: { title: 'New Patient' } 
    },
    // { 
    //     path: 'note', 
    //     component: PatientNoteComponent, 
    //     outlet: 'popup', 
    //     data: { title: 'New Note' } 
    // },
];

@NgModule({
    imports: [RouterModule.forChild(patientRoutes)],
    exports: [RouterModule]
})
export class PatientRoutingModule { }
