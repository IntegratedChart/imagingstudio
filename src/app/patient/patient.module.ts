import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { TreatmentModule } from '../treatment/treatment.module';

import { PatientSearchComponent } from './components/patient-search/patient-search.component';
import { PatientDetailViewComponent } from './components/patient-detail-view/patient-detail-view.component';
import { OfflinePatientDetailsComponent } from './pages/offline-patient-details/offline-patient-details.component';
import { PatientRoutingModule } from './patient-routing.module';
import { NewPatientComponent } from './pages/new-patient/new-patient.component';
import { XrayModule } from '../xray/xray.module';
import { PatientNoteComponent } from './components/patient-note/patient-note.component';
import { PatientTreatmentComponent } from './components/patient-treatment/patient-treatment.component';

@NgModule({
  declarations: [
    PatientSearchComponent,
    PatientDetailViewComponent,
    OfflinePatientDetailsComponent,
    NewPatientComponent,
    PatientNoteComponent,
    PatientTreatmentComponent,
  ],
  imports: [
    SharedModule,
    XrayModule,
    TreatmentModule
  ],
  exports: [
    PatientRoutingModule,
    PatientSearchComponent,
    PatientDetailViewComponent,
  ],
  entryComponents: [
    PatientNoteComponent,
    PatientTreatmentComponent,
  ]
})
export class PatientModule { }
