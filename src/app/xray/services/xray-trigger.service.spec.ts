import { TestBed } from '@angular/core/testing';

import { XrayTriggerService } from './xray-trigger.service';

describe('XrayTriggerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: XrayTriggerService = TestBed.get(XrayTriggerService);
    expect(service).toBeTruthy();
  });
});
