import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class XrayDataService {
  private templateStateSource = new BehaviorSubject<boolean>(false);
  public isTemplateOpen = this.templateStateSource.asObservable();

  constructor() { }

  setTemplateOpen(bolVal) {
    this.templateStateSource.next(bolVal);
  }
}
