import { Injectable } from '@angular/core';
import { SystemService } from 'src/app/shared/services/system.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class XrayTriggerService {

  private templateStateSource = new BehaviorSubject<boolean>(false);
  public isTemplateOpen = this.templateStateSource.asObservable();

  constructor(
    private systemService: SystemService,
  ) {
    this.systemService.incomingMsg.subscribe((msg) => {
      if (msg && msg.msgType === 'windowClose' && msg.winName === 'xrayTemplate') {
        this.setTemplateOpen(false);
      }
    })
  }

  async triggerXrayTemplate(info) {
    try {
      let msg = {
        msg: 'triggerXrayTemplate',
        additionalInfo: info
      }
      await this.systemService.submitSystemRequest('windowCommunication', msg);
      this.setTemplateOpen(true);
    } catch (e) {
      throw e
    }
  }

  setTemplateOpen(bolVal) {
    this.templateStateSource.next(bolVal);
  }
}
