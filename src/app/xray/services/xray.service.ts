import { Injectable } from '@angular/core';
import { SystemService } from 'src/app/shared/services/system.service';


@Injectable({
  providedIn: 'root'
})
export class XrayService {

  constructor(
    private systemService:SystemService
  ) { }

  async getPreviousXrays(storagePath) {
    try {
      let request = 'offlineXrays';
      let requestMsg = {
        requestType: 'getOfflineXrays',
        data: {
          storagePath: storagePath
        }
      }
      return await this.systemService.submitSystemRequest(request, requestMsg);
    } catch (e) {
      throw e;
    }
  }
}
