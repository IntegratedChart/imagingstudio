import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XrayTriggerComponent } from './xray-trigger.component';

describe('XrayTriggerComponent', () => {
  let component: XrayTriggerComponent;
  let fixture: ComponentFixture<XrayTriggerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XrayTriggerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XrayTriggerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
