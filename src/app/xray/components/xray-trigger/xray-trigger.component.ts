import { Component, OnInit, Input } from '@angular/core';
import { AlertService } from 'src/app/shared/services/alert.service';
import { XrayTriggerService } from '../../services/xray-trigger.service';
import { LoggerService } from 'src/app/shared/services/logger.service';

@Component({
  selector: 'app-xray-trigger',
  templateUrl: './xray-trigger.component.html',
  styleUrls: ['./xray-trigger.component.scss']
})
export class XrayTriggerComponent implements OnInit {

  @Input() clinicId:string
  @Input() patientInfo:any

  public templateOpenSubscription:any = null;
  public isTemplateOpen:boolean = null;
  constructor(
    private alertService:AlertService,
    private xrayTriggerService:XrayTriggerService,
    private logger: LoggerService
  ) { }

  ngOnInit() {
    this.templateOpenSubscription = this.xrayTriggerService.isTemplateOpen.subscribe((val) => {
      this.isTemplateOpen = val;
    })
  }

  async openXrayTemplate() {

    try {
      if (!this.clinicId || !this.patientInfo) {
        throw `There is missing information for the patient or the clinic, please try again later . ${this.clinicId}   --- ${this.patientInfo}`
      }
  
      return await this.xrayTriggerService.triggerXrayTemplate( {clinicId: this.clinicId, patientInfo: this.patientInfo, parentWinName:'imagingStudio'} )
    } catch (e) {
      let alert = {
        type: "error",
        title: "Failed to open xray templates!",
        text: e
      }
      this.alertService.showToaster(alert);
      this.logger.error('Failed to open xray templates!', e);
    }
  }

  ngOnDestory(){
    this.templateOpenSubscription.unsubscribe();
  }

}
