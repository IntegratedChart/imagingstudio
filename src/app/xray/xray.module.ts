import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { XrayTriggerComponent } from './components/xray-trigger/xray-trigger.component';
import { SharedModule } from '../shared/shared.module';
import { XrayRoutingModule } from './xray-routing.module';
import { PreviousXraysComponent } from './pages/previous-xrays/previous-xrays.component';


@NgModule({
  declarations: [
    XrayTriggerComponent,
    PreviousXraysComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    XrayRoutingModule,
  ],
  exports: [
    XrayTriggerComponent
  ]
})
export class XrayModule { }
