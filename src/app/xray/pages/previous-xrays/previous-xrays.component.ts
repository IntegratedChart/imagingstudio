import { Component, OnInit, Input } from '@angular/core';
import { XrayDataService } from '../../services/xray-data.service';
import { ToastService } from 'src/app/shared/services/toast.service';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Patient } from 'src/app/patient/interfaces/patient';
import { XrayService } from '../../services/xray.service';
import { LoggerService } from 'src/app/shared/services/logger.service';
import * as moment from 'moment';

@Component({
  selector: 'app-previous-xrays',
  templateUrl: './previous-xrays.component.html',
  styleUrls: ['./previous-xrays.component.scss']
})
export class PreviousXraysComponent implements OnInit {
  @Input() patientId: string;
  patient:Patient = null;
  xrays:any = null;
  xrayDates:any[] = null;
  selectedDate:any = null;
  selectedXray:any = null;

  constructor(
    private xrayService: XrayService,
    private toastService: ToastService,
    public location: Location,
    private activeRoute: ActivatedRoute,
    private logger:LoggerService
  ) { }

  ngOnInit() {

    // get patient if passed in and patch form
    this.activeRoute.params.subscribe(params => {
      const patient = params.data ? JSON.parse(params.data) : null;

      if (patient) {
        this.patient = patient;
        this.getPreviousXrays()
      } else {
        this.logger.info("there was no patient found, going back to patient details page");
        this.location.back();
      }
    });
  }

  getPreviousXrays() {
    if (!this.patient.xrays.storagePath) {
      this.toastService.show('Xrays not found', { classname: 'bg-danger text-white' });
      this.logger.error("storage path for patient previous xrays not found")
    } else {
      this.xrayService.getPreviousXrays(this.patient.xrays.storagePath)
      .then((result) => {
        // console.log("previous xrays ", result);
        this.xrays = result;
        this.xrayDates = Object.keys(this.xrays).sort((a, b) => {
          return <any>new Date(b) - <any>new Date(a);
        });
        // console.log("xray dates ", this.xrayDates);
        this.selectedDate = this.xrayDates && this.xrayDates.length ? this.xrayDates[0] : null;
        this.selectedXray = this.selectedDate ? this.xrays[this.selectedDate][0] : null;
      })
      .catch(err => {
        this.toastService.show('Something went wrong while fetching xrays', { classname: 'bg-danger text-white' });
        this.logger.error("error getting previous xrays ", err);
      });
    }
  }

  selectXray(xrayPath) {
    this.selectedXray = xrayPath;
  }

}
