import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviousXraysComponent } from './previous-xrays.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { Location, LocationStrategy } from '@angular/common';
import { ToastService } from 'src/app/shared/services/toast.service';
import { XrayDataService } from '../../services/xray-data.service';
import { RouterModule } from '@angular/router';

describe('PreviousXraysComponent', () => {
  let component: PreviousXraysComponent;
  let fixture: ComponentFixture<PreviousXraysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviousXraysComponent ],
      imports:[
        SharedModule,
        RouterModule.forRoot([]) // fixes location error
      ],
      providers: [
        ToastService,
        XrayDataService,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviousXraysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
