import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PreviousXraysComponent } from './pages/previous-xrays/previous-xrays.component';

const xrayRoutes: Routes = [
    { 
        path: 'previous-xrays', 
        component: PreviousXraysComponent,
        data: { title: 'X-rays'} 
    },
];

@NgModule({
    imports: [RouterModule.forChild(xrayRoutes)],
    exports: [RouterModule]
})
export class XrayRoutingModule { }
