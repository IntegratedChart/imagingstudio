import { Component, OnInit, Input } from '@angular/core';
import { UploadService } from '../../services/upload.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UploadQueueComponent } from '../upload-queue/upload-queue.component';
import { UploadData } from 'src/app/shared/interfaces/upload-data';


@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {

  @Input() type:string
  @Input() exitingUploadableData:UploadData[]
  constructor(
    private uploadService:UploadService,
    public modal: NgbActiveModal,
    public modalService: NgbModal
  ) { }

  public uploadableRecords:UploadData[] = [];
  public uploadingAll:boolean = false;

  ngOnInit() {

    if (this.exitingUploadableData) {
      this.uploadableRecords = this.exitingUploadableData;
    } else {
      console.log("this type ", this.type)
      this.uploadService.getOfflineUploadData(this.type)
      .then((uploads:UploadData[]) => {
        this.uploadableRecords = uploads;
        console.log("the uploadable records ", this.uploadableRecords)
      }).catch((e) => {
        console.log('error getting uploadable records');
      })
    }

  }

  openPreviousUploads() {
    const previousUploadsModal = this.modalService.open(UploadQueueComponent, { windowClass: 'mt-4', size: 'xl', backdrop: 'static' });

    previousUploadsModal.result.then((data) => {
      console.log(data);
    }).catch((error) => console.log(error));
  }

  async upload(record) {
    try {
      if (record.isUploading || record.isComplete) {
        return
      }
      record.isUploading = true;
      await this.uploadService.uploadData(record);
      console.log("records uploaded ");
      record.isUploading = false;
      record.isComplete = true;
    } catch (e) {
      throw e; 
    }
  }

  async uploadAll() {
    try {
      let uploads = this.uploadableRecords.filter((record) => {
        return !record.isUploading && !record.isComplete
      })

      for (let record of uploads) {
        record.isUploading = true;
      }

      if (uploads && uploads.length) {

        
        this.uploadingAll = true;
        await this.uploadService.uploadData(uploads);
        for (let record of uploads) {
          record.isComplete = true;
        }
      }


    } catch (e) {
      throw e;
    }
  }

  isAllUploaded() {
    if (this.uploadingAll) {
      return true;
    } else if (this.uploadableRecords && this.uploadableRecords.length) {
      let uploadsNeededToBePerformed = this.uploadableRecords.filter((record) => {
        return !record.isUploading && !record.isComplete
      })

      // console.log("uploads needed ", uploadsNeededToBePerformed);
      if(uploadsNeededToBePerformed && uploadsNeededToBePerformed.length) {
        return false;
      } else {
        return true;
      }
    }
  }
}
