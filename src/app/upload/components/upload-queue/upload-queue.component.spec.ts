import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadQueueComponent } from './upload-queue.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

describe('UploadQueueComponent', () => {
  let component: UploadQueueComponent;
  let fixture: ComponentFixture<UploadQueueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadQueueComponent ],
      imports: [ SharedModule ],
      providers: [
        NgbActiveModal,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadQueueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
