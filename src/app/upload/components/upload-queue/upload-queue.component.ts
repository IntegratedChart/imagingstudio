import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UploadService } from '../../services/upload.service';
import { UploadQueue } from 'src/app/shared/interfaces/upload-queue';

@Component({
  selector: 'app-upload-queue',
  templateUrl: './upload-queue.component.html',
  styleUrls: ['./upload-queue.component.scss']
})
export class UploadQueueComponent implements OnInit {
  public currentQueue: UploadQueue = {};
  public currentQueueKeys:any[] = null;
  public queueSubscription: any = null;
  public activeStateMap:Object = {}; 
  constructor(
    private uploadService: UploadService,
    private changeDetector: ChangeDetectorRef,
    public modal:NgbActiveModal,
  ) { }

  ngOnInit() {
    this.queueSubscription = this.uploadService.uploadQueueUpdate.subscribe((queue: UploadQueue) => {
      this.currentQueue = queue || {};
      this.updateActiveState(this.activeStateMap, this.currentQueue);
      this.currentQueueKeys = Object.keys(this.currentQueue);
      console.log("we have a upload queue change ", this.currentQueue);
      //This is to handle situations, where angular doesn't fire the change detection, because the object is not changing but subproperties are changing, to make it run change detection, we can do the below code.
      this.changeDetector.markForCheck(); 
      this.changeDetector.detectChanges();
    });
  }

  ngOnDestroy() {
    if (this.queueSubscription) {
      this.queueSubscription.unsubscribe();
    }
  }

  isComplete(complete) {
    if (complete) {
      return 'text-success'
    } else {
      return 'text-danger'
    }
  }

  updateActiveState(currentActiveMap, queue) {
    for (let key in queue) {
      if (!currentActiveMap[key]) {
        currentActiveMap[key] = true;
      }
    } 
  }

  toggleActiveState(key) {
    this.activeStateMap[key] = !this.activeStateMap[key];
  }

  getStatusClass(key) {
    if (this.activeStateMap[key] == true) {
      return "fa fa-angle-up"
    } else {
      return "fa fa-angle-down"
    }
  }

}
