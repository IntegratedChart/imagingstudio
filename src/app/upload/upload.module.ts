import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { UploadComponent } from './components/upload/upload.component';
import { UploadQueueComponent } from './components/upload-queue/upload-queue.component';
import { SharedModule } from '../shared/shared.module';
import { UploadService } from './services/upload.service';


@NgModule({
  declarations: [
    UploadComponent,
    UploadQueueComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    UploadComponent,
    UploadQueueComponent,
  ],
  entryComponents: [
    UploadComponent,
    UploadQueueComponent
  ],
  providers: [
    DatePipe
  ]
})
export class UploadModule { }
