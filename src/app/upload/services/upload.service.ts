import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { SystemService } from 'src/app/shared/services/system.service';
import { UploadQueue } from 'src/app/shared/interfaces/upload-queue';
import { UploadData } from 'src/app/shared/interfaces/upload-data';
import { LoggerService } from 'src/app/shared/services/logger.service';

@Injectable({
  providedIn: 'root'
})
export class UploadService {
  // private testPatients: Array<UploadData> = [
  //   this.createTestPatients("1", 'Jon Doe', '01/01/2020', true, { noteCount: 2, treatmentCount: 2 }, false),
  //   this.createTestPatients("2", "Jane Doe", "02/02/2020", false, { noteCount: 3, treatmentCount: 3 }, true),
  //   this.createTestPatients("3", "Jake Doe", "03/03/2020", false, { noteCount: 4, treatmentCount: 4 }, false),
  //   this.createTestPatients("4", "James Doe", "04/04/2020", false, { noteCount: 5, treatmentCount: 5 }, true),
  // ];
  // private testPatients2: Array<UploadData> = [
  //   this.createTestPatients("10", 'Jon Dolan', '01/01/2019', true, { noteCount: 2, treatmentCount: 2 }, false),
  //   this.createTestPatients("20", "Jane Dolan", "02/02/2019", false, { noteCount: 3, treatmentCount: 3 }, true),
  //   this.createTestPatients("30", "Jake Dolan", "03/03/2019", false, { noteCount: 4, treatmentCount: 4 }, false),
  //   this.createTestPatients("40", "James Dolan", "04/04/2019", false, { noteCount: 5, treatmentCount: 5 }, true),
  // ];
  // private testPatients3: Array<UploadData> = [
  //   this.createTestPatients("200", 'Jon Jones', '01/01/2017', true, { noteCount: 2, treatmentCount: 2 }, false),
  //   this.createTestPatients("300", "Jane Jones", "02/02/2017", false, { noteCount: 3, treatmentCount: 3 }, false),
  //   this.createTestPatients("400", "Jake Jones", "03/03/2017", false, { noteCount: 4, treatmentCount: 4 }, false),
  //   this.createTestPatients("500", "James Jones", "04/04/2017", false, { noteCount: 5, treatmentCount: 5 }, false),
  // ];

  // private uploadQueue: UploadQueue = {
  //   'test-1': this.testPatients,
  //   'test-2': this.testPatients2
  // };
  private uploadQueue: UploadQueue = {};
  private uploadQueueSource: BehaviorSubject<any> = new BehaviorSubject(this.uploadQueue);
  public uploadQueueUpdate: Observable<any> = this.uploadQueueSource.asObservable();
  public progressConfig: Object = {
    animated: true,
    height: '24px',
    max: 100,
    showValue: true,
    striped: true,
    type: 'primary',
    value: 0,
    error: false
  }
  constructor(
    private systemService: SystemService,
    private logger: LoggerService
  ) {
    this.systemService.incomingMsg.subscribe((msg) => {
      // console.log("We have sys service incoming msg ", msg);
      if (msg && msg.msgType === 'uploadQueue') {
        // console.log("incoming uploadQueue msg");
        this.handleIncomingUploadUpdate(msg.msg);
      }
    })
  }

  // createTestPatients(id: string, name: string, dob: string, isOffline: boolean, uploadData: PatientUploadData
  //   , isComplete: boolean, isStarted = false) {
  //   return {
  //     patientId: id,
  //     patientName: name,
  //     patientDob: dob,
  //     isOffline: isOffline,
  //     uploadData: {
  //       noteCount: uploadData.noteCount,
  //       treatmentCount: uploadData.treatmentCount
  //     },
  //     isComplete: isComplete,
  //     isStarted: isStarted,
  //     progressConfig: {
  //       animated: true,
  //       height: '24px',
  //       max: 100,
  //       showValue: true,
  //       striped: true,
  //       type: 'primary',
  //       value: 50,
  //       error: false
  //     },
  //     isUploading: false
  //   }
  // }

  async getOfflineUploadData(type) : Promise<UploadData[]> {
    try {
      return await this.systemService.getOfflineUploadData(type);
    } catch (e) {
      this.logger.error("error getting patient upload data ", e);
      throw e;
    }
  }

  async uploadData(patients) {
    try {
      let patientsToUpload = patients && patients.length ? patients : [patients];

      return await this.systemService.uploadOfflineData(patientsToUpload);

    } catch (e) {
      this.logger.error("error uploading patient data", e);
      throw e;
    }
  }

  async cancelUpload(id) {
    try {
      let requestMsg = {
        requestType: 'cancelUpload',
        data: {
          id: id
        }
      }
      return await this.systemService.submitSystemRequest('offlineUpload', requestMsg);
    } catch (e) {
      throw e;
    }
  }

  addToQueue(id: string, data: UploadData[]) {
    if (!this.uploadQueue) {
      this.uploadQueue = {};
    }

    if (!this.uploadQueue[id]) {
      this.uploadQueue[id] = [];
    }

    if (data && data.length) {
      for (let patientUpload of data) {
        patientUpload.progressConfig = JSON.parse(JSON.stringify(this.progressConfig));
        this.uploadQueue[id].push(patientUpload);
      }

    }
    this.uploadQueueSource.next(this.uploadQueue);
  }

  removeFromQueue(id: string, patientId: string, ) {
    if (this.uploadQueue[id]) {
      this.uploadQueue[id] = this.uploadQueue[id].filter((patient) => {
        return patient.patientId !== patientId;
      })
    }
    this.uploadQueueSource.next(this.uploadQueue);
  }

  removeQueue(id) {
    if (this.uploadQueue[id]) {
      delete this.uploadQueue[id];
    }
    this.uploadQueueSource.next(this.uploadQueue);
  }

  updateQueue(id: string, patientId: string, data: {}) {
    if (!this.uploadQueue && !this.uploadQueue[id]) {
      // console.log("no queue exist for " + id);
    } else {
      // console.log('about to update upload queue ', this.uploadQueue);

      for (let patientUpload of this.uploadQueue[id]) {
        if (patientUpload.patientId == patientId) {
          if (Object.keys(data)) {
            Object.keys(data).forEach((key) => {
              patientUpload[key] = data[key];
            })
          }

          if (patientUpload.isError) {
            patientUpload.isComplete = false;
            patientUpload.progressConfig.value = 100;
            patientUpload.progressConfig.showValue = false;
            patientUpload.progressConfig.striped = false;
            patientUpload.progressConfig.type = 'danger'
          } else if (patientUpload.isComplete) {
            patientUpload.progressConfig.value = 100;
            patientUpload.progressConfig.striped = false;
            patientUpload.progressConfig.type = 'success'
          } else if (patientUpload.isStarted) {
            patientUpload.progressConfig.value = 50;
          }
        }
      }
      this.uploadQueueSource.next(this.uploadQueue);
    }
  }

  handleIncomingUploadUpdate(msg) {
    if (msg.action === 'addToQueue') {
      this.addToQueue(msg.data._id, msg.data.uploads);
    } else if (msg.action === 'updateQueue') {
      // console.log("seding to upload service updatequeue ", msg);
      this.updateQueue(msg.id, msg.patientId, msg.data);
    } else if (msg.action === 'removeFromQueue') {
      this.removeFromQueue(msg.id, msg.patientId);
    } else if (msg.action === 'removeQueue') {
      this.removeQueue(msg.id);
    }
  }

}
