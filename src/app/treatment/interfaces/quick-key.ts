export interface QuickKey {
    _id?: string,
    clinic_id?:string,
    name:string,
    treatments:Array<any>,
    isNew?:boolean
}
