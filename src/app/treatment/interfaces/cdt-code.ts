export interface CdtCode {
    clinic_id: string,
    cdt_code: string,
    cdt_desc: string,
    short_description: string,
}
