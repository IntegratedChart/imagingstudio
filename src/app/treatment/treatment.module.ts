import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { TreatmentDefinitionComponent } from './components/treatment-definition/treatment-definition.component';
import { TreatmentQuickKeysComponent } from './components/treatment-quick-keys/treatment-quick-keys.component';
import { TreatmentQuickKeysDropdownComponent } from './components/treatment-quick-keys-dropdown/treatment-quick-keys-dropdown.component';
import { XrayModule } from '../xray/xray.module';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [
    TreatmentDefinitionComponent,
    TreatmentQuickKeysComponent,
    TreatmentQuickKeysDropdownComponent,
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  entryComponents: [
    TreatmentQuickKeysComponent
  ],
  exports: [
    TreatmentDefinitionComponent,
    TreatmentQuickKeysComponent,
    TreatmentQuickKeysDropdownComponent,
  ]
})
export class TreatmentModule { }
