import { Injectable } from '@angular/core';
import { SystemService } from 'src/app/shared/services/system.service';
import { DataStoreService } from 'src/app/shared/services/data-store.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { QuickKey } from '../interfaces/quick-key';
import { LoggerService } from 'src/app/shared/services/logger.service';

@Injectable({
  providedIn: 'root'
})
export class TreatmentService {

  private quickKeys:Array<QuickKey> = null;
  private quickKeySource = new BehaviorSubject<any>(JSON.parse(JSON.stringify(this.quickKeys)));
  public quickKeysPub: Observable<any> = this.quickKeySource.asObservable();

  constructor(
    private systemService:SystemService,
    private dataStoreService:DataStoreService,
    private logger:LoggerService
  ) { 
    this.quickKeys = this.dataStoreService.getDataByTypeAndKey('clinicData', 'treatmentQuickKeys');
    this.quickKeySource.next(JSON.parse(JSON.stringify(this.quickKeys)));

    this.dataStoreService.dataUpdate.subscribe((update) => {
      if (update.dataType == 'clinicData') {
        if (update.data && update.data.treatmentQuickKeys) {
          this.quickKeys = update.data.treatmentQuickKeys;
          this.quickKeySource.next(JSON.parse(JSON.stringify(this.quickKeys)));
        }
      }
    })
  }
  
  async getTreatmentQuickKeys() {
    try {
      let clinicId = this.dataStoreService.getClinicId();

      let request = "offlineTreatments";
      let requestMsg = {
        requestType: 'getTreatmentQuickKeys',
        data: {
          clinicId: clinicId
        }
      }
      return await this.systemService.submitSystemRequest(request, requestMsg)
    } catch (e) {
      this.logger.error("error getting treatment quick keys ", e);
      throw e
    }
  }

  async saveTreatmentQuickKeys(keys) {
    try {
      let clinicId = this.dataStoreService.getClinicId();

      let request = "offlineTreatments";
      let requestMsg = {
        requestType: 'saveTreatmentQuickKeys',
        data: {
          clinicId: clinicId,
          keys: keys && keys.length ? keys : [keys]
        }
      }
      let result = await this.systemService.submitSystemRequest(request, requestMsg);

      this.dataStoreService.setDataByTypeAndKey('clinicData', 'treatmentQuickKeys', result);
      return
      console.log("done ", result)
    } catch (e) {
      this.logger.error('error saving treatment quick keys ', e);
      throw e;
    }
  }

  async deleteTreatmentQuickKeys(keys) {
    try {
      let clinicId = this.dataStoreService.getClinicId();

      let request = "offlineTreatments";
      let requestMsg = {
        requestType: 'deleteTreatmentQuickKeys',
        data: {
          clinicId: clinicId,
          keys: keys && keys.length ? keys : [keys]
        }
      }
      let result = await this.systemService.submitSystemRequest(request, requestMsg);

      this.dataStoreService.setDataByTypeAndKey('clinicData', 'treatmentQuickKeys', result);
      return
      console.log("done ", result)
    } catch (e) {
      this.logger.error('error saving treatment quick keys ', e);
      throw e;
    }
  }
}
