import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { TreatmentService } from '../../services/treatment.service';
import { QuickKey } from '../../interfaces/quick-key';
// import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// import { TreatmentQuickKeysComponent } from 'src/app/treatment/components/treatment-quick-keys/treatment-quick-keys.component';

@Component({
  selector: 'treatment-quick-keys-dropdown',
  templateUrl: './treatment-quick-keys-dropdown.component.html',
  styleUrls: ['./treatment-quick-keys-dropdown.component.scss']
})
export class TreatmentQuickKeysDropdownComponent implements OnInit {

  @Input() size:string
  @Input() selectId:string
  @Output() public onKeySelection: EventEmitter<any> = new EventEmitter<any>();
  @Output() public onEdit: EventEmitter<any> = new EventEmitter<any>(); 

  public dropdownSize:string = 'form-control-sm'
  public quickKeys:any = [];
  public selectedKey:QuickKey = null;
  public quickKeysSubscription:any = null;

  constructor(
    public treatmentService:TreatmentService,
    // public modalService: NgbModal,
  ) { }

  ngOnInit() {
    
    if (this.size) {
      this.dropdownSize = "form-control-" + this.size;
    }

    this.quickKeysSubscription = this.treatmentService.quickKeysPub.subscribe((keys) => {
      console.log("new quick keys ", keys);
      this.quickKeys = keys;
      if (!this.quickKeys || !this.quickKeys.length) {
        this.selectedKey = null;
      } else {
        if (!this.selectedKey) {
          this.selectedKey = null;
        } else {
          this.selectedKey = this.quickKeys.filter((key) => {
            return key._id == this.selectedKey._id;
          })
        }
      }
    })

  }

  // async getQuickKeys() {
  //   try {
  //     this.quickKeys = await this.treatmentService.getTreatmentQuickKeys();
  //   } catch (e) {
  //     throw e;
  //   }

  // }

  onSelectionChange() {
    console.log('key change', this.selectedKey)
    this.onKeySelection.emit(this.selectedKey);
  }

  edit() {
    // const quickKeyModalRef = this.modalService.open(TreatmentQuickKeysComponent, { windowClass: 'mt-4', size: 'xl' });

    // if (this.selectedKey) {
    //   quickKeyModalRef.componentInstance.key = this.selectedKey;
    // }

    // quickKeyModalRef.result.then((data) => {
    //   console.log(data);
    // }).catch((error) => console.log(error));
    this.onEdit.emit(this.selectedKey);
  }

  create() {
    this.onEdit.emit(null);
  }

  ngOnDestroy(){
    if (this.quickKeysSubscription) {
      this.quickKeysSubscription.unsubscribe();
    }
  }
}
