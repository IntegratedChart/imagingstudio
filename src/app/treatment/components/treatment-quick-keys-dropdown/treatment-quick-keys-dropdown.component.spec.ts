import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreatmentQuickKeysDropdownComponent } from './treatment-quick-keys-dropdown.component';
import { SharedModule } from 'src/app/shared/shared.module';

describe('TreatmentQuickKeysDropdownComponent', () => {
  let component: TreatmentQuickKeysDropdownComponent;
  let fixture: ComponentFixture<TreatmentQuickKeysDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreatmentQuickKeysDropdownComponent ],
      imports: [
        SharedModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreatmentQuickKeysDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
