import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { CdtCode } from '../../interfaces/cdt-code';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { getUniqueId } from '../../../shared/utils/commonUtils';

@Component({
  selector: 'app-treatment-definition',
  templateUrl: './treatment-definition.component.html',
  styleUrls: ['./treatment-definition.component.scss'],
  host: { class: 'form-row' },
})
export class TreatmentDefinitionComponent implements OnInit {
  treatmentForm: FormGroup;
  uniqueId: string = getUniqueId();
  disabledFields: string[] = []; // widgetType determines disabled fields

  @Input() widgetType: string = 'quick_key'; // quick_key | patient_treatment
  @Input() treatment;
  @Output() form: EventEmitter<FormGroup> = new EventEmitter();

  constructor(
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.treatmentForm = this.formBuilder.group({
      _id: null,
      patient_id: null,
      cdtCode: [null, Validators.required],
      description: null,
      short_description: null,
      tooth_number: null,
      surfaces: null,
      oral_cavity: null,
      isCervicalSurface: false,
      enteredDate: null,
      completed_date: null,
      completed: false,
      is_offline: false,
    });

    if (this.treatment) {
      this.treatmentForm.patchValue(this.treatment);
    }

    this.returnFormToParent();

    // set fields based on widget type
    this.setFields();
  }

  /** returns parital form to parent component */
  returnFormToParent() {
    this.form.emit(this.treatmentForm);
  }

  handleOnChange(field: string, val: any): void {
    switch (field) {
      case 'cdtCode':
        val = <CdtCode> val;
        this.treatmentForm.get('cdtCode').setValue(val.cdt_code);
        this.treatmentForm.get('description').setValue(val.cdt_desc);
        this.treatmentForm.get('short_description').setValue(val.short_description);
        break;

      case 'tooth_number':
      case 'surfaces':
      case 'oral_cavity':
        val = <string> val;
        this.treatmentForm.get(field).setValue(val);
        break;
      
      case 'completed':
        this.treatmentForm.get(field).setValue(val);
        break;

      default:
        break;
    }
  }

  getCurrentValue(field: string): string | void {
    let val = this.treatmentForm.get(field).value;

    if (!val) {
      return;
    }

    if (field != 'cdtCode') {
      return val;
    }

    const description = this.treatmentForm.get('short_description').value;
    val +=  ` - ${description}`;
    return val;
  }

  setFields() {
    if (this.widgetType === 'quick_key') {
      this.disabledFields.push('completed');
    }
  }
}
