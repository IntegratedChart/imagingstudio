import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreatmentDefinitionComponent } from './treatment-definition.component';
import { SharedModule } from 'src/app/shared/shared.module';

describe('TreatmentDefinitionComponent', () => {
  let component: TreatmentDefinitionComponent;
  let fixture: ComponentFixture<TreatmentDefinitionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreatmentDefinitionComponent ],
      imports: [
        SharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreatmentDefinitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
