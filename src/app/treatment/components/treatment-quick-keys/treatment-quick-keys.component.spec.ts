import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreatmentQuickKeysComponent } from './treatment-quick-keys.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { TreatmentDefinitionComponent } from '../treatment-definition/treatment-definition.component';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder } from '@angular/forms';
import { TreatmentService } from '../../services/treatment.service';
import { AlertService } from 'src/app/shared/services/alert.service';

describe('TreatmentQuickKeysComponent', () => {
  let component: TreatmentQuickKeysComponent;
  let fixture: ComponentFixture<TreatmentQuickKeysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreatmentQuickKeysComponent, TreatmentDefinitionComponent ],
      imports: [
        SharedModule,
      ],
      providers: [
        NgbActiveModal,
        FormBuilder,
        TreatmentService,
        AlertService,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreatmentQuickKeysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
