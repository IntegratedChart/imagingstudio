import { Component, OnInit, SimpleChange, SimpleChanges, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TreatmentService } from '../../services/treatment.service';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { QuickKey } from '../../interfaces/quick-key';
import { AlertService } from 'src/app/shared/services/alert.service';
import { Alert } from 'src/app/shared/interfaces/alert';
import { LoggerService } from 'src/app/shared/services/logger.service';



@Component({
  selector: 'treatment-quick-keys',
  templateUrl: './treatment-quick-keys.component.html',
  styleUrls: ['./treatment-quick-keys.component.scss']
})
export class TreatmentQuickKeysComponent implements OnInit {

  @Input() public key:QuickKey
  @Input() public isEdit:boolean;

  public title: string = null;
  public quickKeyForm: FormGroup;
  public treatmentForms: FormArray;
  public submitted:boolean = false;
  constructor(
    public modal: NgbActiveModal,
    private treatmentService: TreatmentService,
    private formBuilder: FormBuilder,
    private alertService:AlertService,
    private logger: LoggerService
  ) { }

  ngOnInit() {
    this.title = this.isEdit ? 'Edit Quick Key' : 'Create Quick Key';
    
    this.quickKeyForm = this.formBuilder.group({
      _id: null,
      name: [null, Validators.required],
      treatments: this.formBuilder.array([])
    })

    if (this.key) {
      // console.log("the key coming in ", this.key);
      this.quickKeyForm.controls['_id'].setValue(this.key._id);
      this.quickKeyForm.controls['name'].setValue(this.key.name);
    } else {
      // console.log('setting up key as new')
      this.key = this.createEmptyQuickKey();
      // console.log("the new key ", this.key);
    }

    this.treatmentForms = this.quickKeyForm.get('treatments') as FormArray;
  }

  createEmptyQuickKey() {
    return {
      name: "",
      treatments: [
        this.createEmptyTreatment()
      ]
    }
  }

  //creates default treatment object to pass into the treatment definition component. 
  createEmptyTreatment() {
    return {
      cdtCode: null,
      description: null,
      short_description: null,
      tooth_number: null,
      surfaces: null,
      isCervicalSurface: null,
      oral_cavity: null,
    }
  }

  addNewTreatment() {
    this.key.treatments.push(this.createEmptyTreatment());
  }

  onTreatmentData(form: FormGroup): void {
    // this.patientTreatmentsForm.setControl(formName, form);
    this.treatmentForms.push(form);
  }

  removeTreatment(index) {
    this.key.treatments.splice(index, 1);
    this.treatmentForms.removeAt(index);
  }

  deleteQuickKey() {
    this.treatmentService.deleteTreatmentQuickKeys(this.key)
    .then(() => {
      this.modal.close('deleted!');
    })
    .catch((e) => {
      //create toaster service and show
      this.logger.error("error submitting request to delete quick key", e);
      let alert: Alert = {
        title: "Operation Failed",
        class: "bg-danger text-light",
        text: "Please try again."
      }

      this.alertService.showToaster(alert);
    })
  }

  close() {
    this.modal.close('send data back to caller')
  }

  save(e) {
    // e.preventDefault();

    this.submitted = true;
    // console.log('run validation logic');
    // this.validateFormData(this.quickKeyForm);
    this.quickKeyForm.markAllAsTouched();

    if (this.quickKeyForm.valid) {
      console.log("run save logic ", this.quickKeyForm);
      this.treatmentService.saveTreatmentQuickKeys(this.quickKeyForm.value)
      .then((result) => {
        this.modal.close('saved!');
      })
      .catch((e) => {
        //create toaster service and show
        this.logger.error("error submitting request to save quick keys", e);
        let alert:Alert = {
          title: "Operation Failed",
          class: "bg-danger text-light",
          text: "Please try again."
        }

        this.alertService.showToaster(alert);
      })
    } else {
      this.logger.info("Invalid form submission for quick keys")
    }
  }

  validateFormData(form) {
    //run vlaidation here
  }

}
