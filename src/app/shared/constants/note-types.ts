const _NoteTypes = [
    
    { value: 'allergies', label: 'Allergies'},
    { value: 'health', label: 'Health Conditions'},
    { value: 'medical', label: 'Medical'},
    { value: 'medications', label: 'Medications'},
    { value: 'treatment', label: 'Treatment'},
];

const types = {
    treatment: _NoteTypes.filter(type => type.value == 'treatment'),
    medical: _NoteTypes.filter(type => type.value != 'treatment'),
    all: _NoteTypes
}

export default types;
