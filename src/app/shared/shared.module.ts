import { NgModule } from '@angular/core';
import { CommonModule, DatePipe} from '@angular/common';
import { LandingComponent } from './components/landing/landing.component';
import { SharedRoutingModule } from './shared-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CustomDatePipe } from './pipes/custom-date.pipe';
import { LabelPipe } from './pipes/label.pipe';
import { CustomTelPipe } from './pipes/custom-tel.pipe';
import { CustomEmailPipe } from './pipes/custom-email.pipe';
import { PopupCloserComponent } from './components/popup-closer/popup-closer.component';
import { GenderPipe } from './pipes/gender.pipe';
import { HomeButtonComponent } from './components/home-button/home-button.component';
import { DatePickerComponent } from './components/date-picker/date-picker.component';
import { ToothPickerComponent } from './components/tooth-picker/tooth-picker.component';
import { SurfacePickerComponent } from './components/surface-picker/surface-picker.component';
import { QuadrantPickerComponent } from './components/quadrant-picker/quadrant-picker.component';
import { CdtcodePickerComponent } from './components/cdtcode-picker/cdtcode-picker.component';
import { ProviderPickerComponent } from './components/provider-picker/provider-picker.component';
import { LocationPickerComponent } from './components/location-picker/location-picker.component';
import { ToastComponent } from './components/toast/toast.component';
import { ProgressBarComponent } from './components/progress-bar/progress-bar.component';
import { ConnectionStatusComponent } from './components/connection-status/connection-status.component';
import { NoteTemplatePickerComponent } from './components/note-template-picker/note-template-picker.component';
import { LoadingViewComponent } from './components/loading-view/loading-view.component';
import { SortByPipe } from './pipes/sort-by.pipe';
import { SafePipe } from './pipes/safe.pipe';
import { DebounceClickDirective } from './directives/debounce-click.directive';

@NgModule({
  declarations: [
    LandingComponent,
    PopupCloserComponent,
    LabelPipe,
    CustomDatePipe,
    CustomTelPipe,
    CustomEmailPipe,
    GenderPipe,
    HomeButtonComponent,
    DatePickerComponent,
    ToothPickerComponent,
    SurfacePickerComponent,
    QuadrantPickerComponent,
    CdtcodePickerComponent,
    ProviderPickerComponent,
    LocationPickerComponent,
    ToastComponent,
    ProgressBarComponent,
    ConnectionStatusComponent,
    NoteTemplatePickerComponent,
    LoadingViewComponent,
    SortByPipe,
    SafePipe,
    DebounceClickDirective,
  ],
  imports: [
    SharedRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DragDropModule,
    NgbModule,
  ],
  entryComponents:[
    LandingComponent
  ],
  exports:[
    SharedRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    LandingComponent,
    CustomTelPipe,
    CustomEmailPipe,
    CustomEmailPipe,
    GenderPipe,
    HomeButtonComponent,
    DatePickerComponent,
    PopupCloserComponent,
    ToothPickerComponent,
    SurfacePickerComponent,
    QuadrantPickerComponent,
    CdtcodePickerComponent,
    ProviderPickerComponent,
    LocationPickerComponent,
    ToastComponent,
    ProgressBarComponent,
    ConnectionStatusComponent,
    NoteTemplatePickerComponent,
    LoadingViewComponent,
    SortByPipe,
    SafePipe,
    DebounceClickDirective
  ],
  providers:[
    LabelPipe,
    CustomDatePipe,
    CustomTelPipe,
    CustomEmailPipe, 
    DatePipe
  ]
})
export class SharedModule { }
