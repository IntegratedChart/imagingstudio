import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { Alert } from '../interfaces/alert';
import { ToastService } from './toast.service';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor( private toastService: ToastService) {}


  async showError(errorObj) {
    try {
      Swal.close();
  
      let isConfirm = await Swal.fire({
        title: errorObj.title || 'Fatal Error',
        text: errorObj.text || "There was a fatal error",
        type: "warning",
        showCancelButton: errorObj.showCancel || false,
        confirmButtonColor: errorObj.confirmColor || "#d9534f",
        confirmButtonText: errorObj.confirmText || "Okay",
        cancelButtonText: errorObj.cancelText || "Cancel",
      });
  
      if(isConfirm.value){
        return {error: 'fatal'}
      } else {
        return {error: 'cancel'}
      }
    } catch (err) {
      throw err;
    }
  }


  async showAlert(alertObj:Alert = {}):Promise<any> {
    try {
      Swal.close();
      
      let isConfirm = await Swal.fire({
        title: alertObj.title || 'Alert',
        text: alertObj.text || 'Something has happened',
        type: 'warning',
        showCancelButton: alertObj.showCancel || false,
        confirmButtonColor: alertObj.confirmColor || "#006400",
        confirmButtonText: alertObj.confirmText || "Okay",
        cancelButtonText: alertObj.cancelText || "Cancel",
        allowOutsideClick: alertObj.outsideClick || false,
      })

      if(isConfirm.value){
        return { alert: 'confirm' }
      }
      
      return { alert: 'cancel' }
    } catch (err) {
      throw err;
    }
  }

  showToaster(alert:Alert = {}) :void {
    let defaultClass = alert.class || "bg-info text-light"
    let title = alert.title || "";
    let text  = alert.text || "";
    let delay = alert.timeout || 5000;

    let options = {
      classname: defaultClass,
      header: title,
      delay: delay
    }

    if (alert.text) {
      this.toastService.show(text, options)
    }
  }

  async showLoadingAlert (alertTitle) {

    try{
      var swalOpts = {
        html: alertTitle,
        title: "<i class='fa fa-spinner fa-pulse fa-3x fa-fw' style='color: #5bc0de'></i>",
        type: null,
        showCancelButton: false,
        showConfirmButton: false,
        allowOutsideClick: false,
      };
      
      return await Swal.fire(swalOpts)
    } catch (err) {
      throw err;
    }
  }

  async closeLoadingAlert(){
    try{
      Swal.close();
  
      setTimeout(function(){
        return 
      }, 1000)

    } catch (err){
      throw err;
    }
  }
}
