import { TestBed } from '@angular/core/testing';

import { CustomTitlebarService } from './custom-titlebar.service';
import { NgZone } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

describe('CustomTitlebarService', () => {
  const mockNgZone = jasmine.createSpyObj('mockNgZone', ['run', 'runOutsideAngular']);
  mockNgZone.run.and.callFake(fn => fn());

  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      RouterTestingModule,
    ],
    providers: [
      // { provide: NgZone, useValue: mockNgZone }
    ]
  }));

  it('should be created', () => {
    const service: CustomTitlebarService = TestBed.get(CustomTitlebarService);
    expect(service).toBeTruthy();
  });
});
