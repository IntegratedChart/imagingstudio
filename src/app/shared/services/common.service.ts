import { Injectable } from '@angular/core';
import Swal from "sweetalert2";

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor() { }

  async askToSave(option:any){
    let isConfirm = await Swal.fire({
      title: "Unsaved Changes",
      text: "Its seems like there are changes made to " + option + " that have not been saved, would you like to save them?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#449d44",
      cancelButtonColor: "#d9534f",
      confirmButtonText: "Save",
      cancelButtonText: "Do Not Save",
      allowOutsideClick: false
    })

    return isConfirm.value;
  }

  toTitleCase(str){
    if (typeof str == "string") {
      return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      });
    }
    else {
      return str;
    }
  }

  /**
   * see if a value exists in a array of objects given a fieldname
   * 
  */
  doesExist(val, arr, field) {

    var exists = false;
    arr.map((obj) => {
      if (val == obj[field]) {
        exists = true;
      }
    })

    return exists
  }

  formatPatientFolderString = function (patient) {
    var folder = this.replaceSpace(patient.fname, '') + '-' + this.replaceSpace(patient.lname, '');

    if (patient.clinicData && patient.clinicData.chartId) {
      folder += '-' + patient.clinicData.chartId;
    }

    return this.capitalizeFirstLetter(folder);
  }

  capitalizeFirstLetter(string) {
    string = string.toLowerCase();
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
  
  replaceSpace(str, replacement) {
    return str ? str.replace(/\s/g, replacement) : '';
  }

}
