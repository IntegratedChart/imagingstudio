
import { Injectable, TemplateRef } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({ 
  providedIn: 'root' 
})
export class ToastService {
  private toasts: any[] = [];
  private toastSource:any = new BehaviorSubject<any>(this.toasts);
  public onNewToast:Observable<any> = this.toastSource.asObservable();

  show(textOrTpl: string | TemplateRef<any>, options: any = {}) {
    this.toasts.push({ textOrTpl, ...options });
    this.toastSource.next(this.toasts);
  }

  remove(toast) {
    this.toasts = this.toasts.filter(t => t !== toast);
    this.toastSource.next(this.toasts);
  }
}