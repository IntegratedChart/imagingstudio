import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Injectable({
  providedIn: 'root'
})
export class ModalService {


  constructor(public modal:NgbModal) { }

  async open(component, modalOpts = {}, componentParams = null){

    try{
      let modalRef = this.modal.open(component, modalOpts);
  
      if(componentParams){
        for(let key in componentParams){
          modalRef.componentInstance[key] = componentParams[key];
        }
      }
      
      try{
        return await modalRef.result;
      } catch (err) {
        return 'dismissed'
      }

    } catch (err) {
      throw err;
    }
  }

  closeAll(){
    this.modal.dismissAll()
  }

  areOpen(){
    return this.modal.hasOpenModals();
  }
}

