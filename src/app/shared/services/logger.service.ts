import { Injectable } from '@angular/core';

declare var window: any;

@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  private logger
  // private path
  // private os
  // private fs
  // private electron
  // private process
  // private rootPath
  // private homePath
  // private logDir

  constructor() { 
    console.log("window logger ", window.logger)
    this.logger = window.logger;
    // if ((<any>window).require) {
    //   try {
    //     this.process = (<any>window).require('process');
    //     this.electron = (<any>window).require('electron');
    //     this.path = (<any>window).require('path');
    //     this.fs = (<any>window).require("fs-extra");
    //     this.os = (<any>window).require('os');

    //     this.logger = (<any>window).require('electron-log');

    //     this.rootPath = this.process.cwd().split(this.path.sep)[0];
    //     this.homePath = this._getHomePath();

    //     this.logDir = this.path.join(this.homePath, 'XrayStudioFrontendV2.log');

    //     this.logger.transports.file.level = 'info'
    //     this.logger.transports.file.file = this.logDir;
    //     this.logger.transports.maxSize = 10048576;

    //     this.fs.ensureDirSync(this.logDir);
    //     this.logger.info("logger ", this.logger)
    //   } catch (error) {
    //     console.log("we have problem with setting up system service")
    //     throw error
    //   }
    // } else {
    //   console.log('Could not load electron ipc');
    // }
  }

  // private _getHomePath() {
  //   //adjust code here to get the 
  //   if (this.os.platform() === 'darwin') {
  //     return this.electron.app.getPath('home')
  //   } else if (this.os.platform() === 'win32') {
  //     return this.path.join(this.rootPath, '/ProgramData/xraystudio');
  //   }

  // }
  object(msg) {
    this.logger ? this.logger.info(msg) : null;
  }

  info(...msg) {
    this.logger ? this.logger.info(this.parseMsg('INFO', ...msg)) : null;
  }

  warn(...msg) {
    this.logger ? this.logger.warn(this.parseMsg('WARN', ...msg)) : null;
  }

  debug(...msg) {
    this.logger ? this.logger.debug(this.parseMsg('DEBUG', ...msg)) : null;
  }

  error(...msg) {
    this.logger ? this.logger.error(this.parseMsg('ERROR', ...msg)) : null;
  }

  private parseMsg(msgType, ...msg) {

    let finalMsg = ``;
    msg.map((msgPart) => {
      if (msgPart == '[object Object]'){
        msgPart = JSON.stringify(msgPart);
      }
      finalMsg += msgPart;
    });

    return finalMsg;
  }
}
