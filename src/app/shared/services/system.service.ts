import { Injectable } from '@angular/core';
import { IpcRenderer } from 'electron'
import { DataStoreService } from './data-store.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { UploadData } from '../interfaces/upload-data';
import { LoggerService } from './logger.service';

@Injectable({
  providedIn: 'root'
})
export class SystemService {

  private incomingMsgSource = new BehaviorSubject<any>(null);
  public incomingMsg: Observable<any> = this.incomingMsgSource.asObservable();

  public ipc: IpcRenderer;
  public backendPushNotificationListener:any = null;
  private myRequestId = 10;
  private deferredMapByRequestId = {};
  constructor(
    private dataStore: DataStoreService,
    private logger:LoggerService
    // private backendMsgService: BackendMsgService
  ) {

    console.log(<any>window)
    if ((<any>window).require) {
      try {
        this.ipc = (<any>window).require('electron').ipcRenderer   
        console.log("ipc", this.ipc);
        this.setup();
      } catch (error) {
        console.log("we have problem with setting up system service")
        if (this.logger) {
          this.logger.error(`error setting up system service, ${error}`);
        }
        throw error
      }
    } else {
      console.log('Could not load electron ipc');
    }

  }

  async setup(){
    try {
      this.backendPushNotificationListener = this.backendPushNotificationListenerInit();
      this.initializeListener();

      await this.getXrayStudioSetting();
      
      let userAuthCode = await this.getAuthStatus();
      this.dataStore.setData("authentication", {userAuthCode: userAuthCode});

      let clinicId = this.dataStore.getClinicId();

      if (clinicId) {
        // this.logger.info("the clinicId ", clinicId);
        await this.getCurrentClinicData(clinicId.toString());
      }

      this.logger.info("------ setup complete -------->");
      return
    } catch (e) {
      throw e
    }
  }

  async getCurrentClinicData(clinicId) {
    try {
      let clinicData = await this.submitSystemRequest('getClinicData', {clinicId: clinicId})
      this.dataStore.setData('clinicData', clinicData);
      return
    } catch (e) {
      throw e;
    }
  }

  async getAuthStatus() {
    try {
      return await this.submitSystemRequest('getAuthStatus', {});
    } catch (e) {
      console.log("failed to get auth status ", e);
      return null;
    }  
  }

  async getXrayStudioSetting() {
    try {
      let settings = await this.submitSystemRequest('getXrayStudioSetting', {});
      this.dataStore.setData('settings', settings);
    } catch (e) {
      throw e
    }
  }


  async exitAppWindow() {
    try {
      await this.submitSystemRequest('exitAppWindow', {})
    } catch (err) {
      throw err;
    }
  }

  //essentially same method as above, only it will not save any settings.
  async exitApp() {
    try {
      await this.submitSystemRequest('exitApp', {})
    } catch (err) {
      throw err;
    }
  }

  /**
   * function to submit a request from frontend to backend.
   */
  submitSystemRequest(requestType, requestMsg) {

    var nextId = this.myRequestId++;

    var t32Request = {
      requestType: requestType,
      requestMsg: requestMsg,
      requestId: nextId
    }

    var promise: any = {};

    var deferred = new Promise<any>(function (resolve, reject) {
      promise.resolve = resolve;
      promise.reject = reject;
    })

    this.deferredMapByRequestId[nextId] = { deferred: deferred, promise: promise };

    console.log('the deferred map', this.deferredMapByRequestId)

    this.ipc.send('asynchronous-message', t32Request);

    return deferred;
  };

  /**
  * initialize the listener for backend notification
  */
  initializeListener() {
    var self = this;

    this.logger.info("-------Listener registered------------");
    this.ipc.on('asynchronous-reply', (event, wrapper) => {
      this.logger.info('.....V2 System Service Listener active.....')
      // console.log("reply ", wrapper);
      var requestId = wrapper.requestId;
      // console.log("we recieved a reply back from backend");
      // console.log(wrapper);

      if (requestId && !wrapper.error) {
        var theDeferred = self.deferredMapByRequestId[requestId];
        if (theDeferred) {
          // console.log("about to resolve for ", wrapper.requestId);
          // console.log('the result is', wrapper.result);
          theDeferred.promise.resolve(wrapper.result);
          delete self.deferredMapByRequestId[requestId];
        }
      } else if (requestId && wrapper.error) {
        // this.logger.info("error coming in to system service ", wrapper)
        var theDeferred = self.deferredMapByRequestId[requestId];
        if (theDeferred) {
          theDeferred.promise.reject(wrapper.error);
          delete self.deferredMapByRequestId[requestId];
        }
      }
    });

    this.ipc.removeListener('BackendPushNotification', this.backendPushNotificationListener);
    
    this.ipc.on('BackendPushNotification', this.backendPushNotificationListener);

    // this.ipc.send('clientInitComplete');
  };

  backendPushNotificationListenerInit() {
    let self = this;
    return function (evt, msg) {
      self.logger.info("backend message recieved on V2 ", msg);

      if (msg.msgType === 'Authentication') {
        self.dataStore.setDataByTypeAndKey('authentication', 'userAuthCode', msg.authCode);
        if (msg.authCode) {
          self.getLicencedClinic().then((clinic) => {
            if (clinic && clinic._id) {
              self.getCurrentClinicData(clinic._id.toString());
            }
          })
        }
      } else if (msg.msgType === 'offlineDownloadUpdate') {
        self.logger.info("adding to data store dowload status ", msg.msg.isDownloading)
        self.dataStore.setDataByTypeAndKey('downloadStatus', 'isDownloading', msg.msg.isDownloading);
      }

      self.incomingMsgSource.next(msg);
    }
  }

  async getLicencedClinic() {
    try {
      let requestType = 'getXrayStudioLicencedClinic';
      return await this.submitSystemRequest(requestType, null);
    } catch (e) {
      throw e;
    }
  }

  async getOfflineUploadData(type): Promise<UploadData[]> {
    try {
      let requestMsg = {
        requestType: 'getOfflineUploadData',
        data: {
          type: type
        }
      }
      return await this.submitSystemRequest('offlineUpload', requestMsg);
    } catch (e) {
      console.log("error getting patients ", e);
      throw e;
    }
  }

  async uploadOfflineData(data) {
    try {
      let requestMsg = {
        requestType: 'uploadOfflineData',
        data: {
          uploadData: data
        }
      }
      return await this.submitSystemRequest('offlineUpload', requestMsg);
    } catch (e) {
      console.log("error uploading ", e);
      throw e;
    }
  }
}
