import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
/**
 * Please Read::
 * custom-title-bar package has some issue with angular change detection
 * if we require this package inside of angular then on each click it re-renders view
 * for now we are requiring package in index.html and defining created titlebar on window context
 */
declare var window: any;

export interface CustomTitleBarConfig {
  backgroundColor?: string,
  titleHorizontalAlignment?: string,
  minimizable?: boolean,
  maximizable?: boolean,
  enableMnemonics?: boolean,
}

@Injectable({
  providedIn: 'root'
})
export class CustomTitlebarService {
  private customTitlebar;
  private defaultConfig: CustomTitleBarConfig = {
    backgroundColor: '#33b5e5',
    titleHorizontalAlignment: 'left',
    minimizable: true,
    maximizable: true,
    enableMnemonics: false
  }

  constructor(
    private router: Router,
    private _ngZone: NgZone,
  ) {
  }
  
  create(config:CustomTitleBarConfig = {}) {
    // temporary fix to handle unwanted change detection caused by package
    return this.getTitlebarFromWindowContext();
    
    if (this.customTitlebar) {
      // don't do anything if custom titlebar already exists
      return;
    }

    const { Titlebar, Color } = window.require('custom-electron-titlebar');

    // configuration for custom titlebar
    const titlebarConfig = {
      backgroundColor: Color.fromHex(config.backgroundColor || this.defaultConfig.backgroundColor),
      titleHorizontalAlignment: config.titleHorizontalAlignment || this.defaultConfig.titleHorizontalAlignment,
    }

     // create custom titlebar
     this.customTitlebar = new Titlebar(titlebarConfig);

    this.updateMenu();
  }

  getTitlebarFromWindowContext() {
    if (this.customTitlebar) {
      return;
    }
    
    this.customTitlebar = window.titlebar;
    this.updateMenu();
  }

  updateMenu() {
    if (!this.customTitlebar) {
      return;
    }
    
    const { Menu, MenuItem} = window.require('electron').remote;
    const menu = new Menu();
    menu.append(new MenuItem({ 
      label: '<span class="text-white" style="font-size: 1.2em;"><span class="fa fa-home"></span> Home</span>',
      click: () => this._ngZone.run(() => this.router.navigateByUrl('/')) 
    }));

    this.customTitlebar.updateMenu(menu);
  }

  updateTitle(title: string): void {
    if (!this.customTitlebar) {
      return;
    }

    this.customTitlebar.updateTitle(title);
  }
}
