import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { LoggerService } from './logger.service';

@Injectable({
  providedIn: 'root'
})
export class DataStoreService {
  private data:any = {
    // clinic_id: null
  }
  // private dataStoreSource = new BehaviorSubject<any>(this.data);
  // public dataStorePub: Observable<any> = this.dataSource.asObservable();
  private dataUpdateSource = new BehaviorSubject<any>(null);
  public dataUpdate: Observable<any> = this.dataUpdateSource.asObservable(); 
  constructor(
    private logger:LoggerService
  ) {}

  setData(dataType, data) {
    this.data[dataType] = data;
    // this.dataSource.next(this.data);
    this.dataUpdateSource.next({ dataType: dataType, data: this.data[dataType] });
  }

  setDataByTypeAndKey(dataType, key, data) {
    if (!this.data[dataType]) {
      this.data[dataType] = {};
    } 

    this.data[dataType][key] = data;

    console.log("setting data by type and key ", dataType, key, data);
    // this.dataSource.next(this.data);
    this.dataUpdateSource.next({dataType: dataType, data: this.data[dataType]})
  }

  getClinicId() {
    return this.data.settings && this.data.settings.clinic_id ? this.data.settings.clinic_id : null;
  }

  getDataByTypeAndKey(dataType:string, key: string) {
    return this.data && this.data[dataType] && this.data[dataType][key] ? this.data[dataType][key] : undefined;
  }
}
 