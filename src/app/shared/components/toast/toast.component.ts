import { Component, OnInit, TemplateRef } from '@angular/core';
import { ToastService } from '../../services/toast.service';

@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss']
})
export class ToastComponent implements OnInit {
  public toasts:any[] = [];
  private toastSubscription:any = null;
  constructor(public toastService:ToastService) { }

  ngOnInit() {
    this.toastSubscription = this.toastService.onNewToast.subscribe((toasts) => {
      this.toasts = toasts;
    });
  }

  remove(toast) {
    this.toastService.remove(toast);
  }

  isTemplate(toast) { 
    return toast.textOrTpl instanceof TemplateRef; 
  }



}
