import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderPickerComponent } from './provider-picker.component';
import { FormsModule } from '@angular/forms';

describe('ProviderPickerComponent', () => {
  let component: ProviderPickerComponent;
  let fixture: ComponentFixture<ProviderPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderPickerComponent ],
      imports: [
        FormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
