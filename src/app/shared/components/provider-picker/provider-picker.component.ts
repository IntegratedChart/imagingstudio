import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { DataStoreService } from '../../services/data-store.service';

@Component({
  selector: 'app-provider-picker',
  templateUrl: './provider-picker.component.html',
  styleUrls: ['./provider-picker.component.scss']
})
export class ProviderPickerComponent implements OnInit, OnChanges {
  providers;
  model: string;
  @Input() locationId: string;
  @Input() currentProvider: string;
  @Output() onSelect: EventEmitter<string> = new EventEmitter();

  constructor(
    private dataStore: DataStoreService,
  ) { }

  ngOnInit() {
  }
  
  ngOnChanges(changes: SimpleChanges) {
    if (changes.locationId && changes.locationId.currentValue != changes.locationId.previousValue) {
      this.getProviders();
      this.setDefaultProvider();
    }

    if (changes.currentProvider) {
      this.model = this.currentProvider;
    }
  }
  
  getProviders() {
    const clinic = this.dataStore.getDataByTypeAndKey('clinicData', 'clinic');

    if (!clinic) {
      this.providers = [];
      return;
    }

    const location = clinic.locations.filter(loc => loc._id == this.locationId);
    this.providers = location.length && location[0].clinicProviders || [];
  }

  setDefaultProvider() {
    if (this.currentProvider) {
      this.model = this.currentProvider;
    } else {
      this.model = this.providers.length ? this.providers[0].provider_id : null;
       
      // Delay emit change to avoid error related to change in value after initial check
      // FIXME: look into changeDetectionStrategy instead of timeout (because it is expensive operation)
      setTimeout(() => this.handleChange(), 50);
    }
  }

  handleChange() {
    this.onSelect.emit(this.model);
  }

}
