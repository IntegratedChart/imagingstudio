import { Component, OnInit, Output, EventEmitter, Input, ChangeDetectionStrategy } from '@angular/core';
import { DataStoreService } from '../../services/data-store.service';

@Component({
  selector: 'app-location-picker',
  templateUrl: './location-picker.component.html',
  styleUrls: ['./location-picker.component.scss'],
})
export class LocationPickerComponent implements OnInit {
  locations;
  model: string;
  @Input() currentLocation: string;
  @Output() onSelect: EventEmitter<any> = new EventEmitter();

  constructor(
    private dataStore: DataStoreService,
  ) { }

  ngOnInit() {
    const clinic = this.dataStore.getDataByTypeAndKey('clinicData', 'clinic');
    this.locations = clinic && clinic.locations || [];

    if (this.currentLocation) {
      this.model = this.currentLocation;
    } else {
      this.model = this.locations.length ? this.locations[0]._id : '';

      // Delay emit change to avoid error related to change in value after initial check
      // FIXME: look into changeDetectionStrategy instead of timeout (because it is expensive operation)
      setTimeout(() => this.handleChange(), 50);
    }
  }

  handleChange() {
    this.onSelect.emit(this.model);
  }

}
