import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-popup-closer',
  templateUrl: './popup-closer.component.html',
  styleUrls: ['./popup-closer.component.scss']
})
export class PopupCloserComponent implements OnInit {
  @Input() modalInstance;
  
  constructor(
  ) { }

  ngOnInit() {
  }

  close(){
    if (this.modalInstance) {
      this.modalInstance.dismiss();
    }
  }

}
