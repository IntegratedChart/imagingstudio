import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupCloserComponent } from './popup-closer.component';

describe('PopupCloserComponent', () => {
  let component: PopupCloserComponent;
  let fixture: ComponentFixture<PopupCloserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupCloserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupCloserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
