import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurfacePickerComponent } from './surface-picker.component';
import { FormsModule } from '@angular/forms';

describe('SurfacePickerComponent', () => {
  let component: SurfacePickerComponent;
  let fixture: ComponentFixture<SurfacePickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurfacePickerComponent ],
      imports: [
        FormsModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurfacePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
