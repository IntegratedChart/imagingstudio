import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { SURFACES } from '../../constants/tooth';

@Component({
  selector: 'app-surface-picker',
  templateUrl: './surface-picker.component.html',
  styleUrls: ['./surface-picker.component.scss']
})
export class SurfacePickerComponent implements OnInit {
  currentOptions = SURFACES;
  model: string;

  @Input()
  set selectedValue(val: string) {
    if (val) {
      this.model = val;
    }  
  }

  @Output() onChange: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  handleChange() {
    this.onChange.emit(this.model);
  }

}
