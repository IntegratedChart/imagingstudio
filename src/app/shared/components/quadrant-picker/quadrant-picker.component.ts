import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { QUADRANTS } from '../../constants/tooth';

@Component({
  selector: 'app-quadrant-picker',
  templateUrl: './quadrant-picker.component.html',
  styleUrls: ['./quadrant-picker.component.scss']
})
export class QuadrantPickerComponent implements OnInit {
  currentOptions = QUADRANTS;
  model: string;

  @Input()
  set selectedValue(val: string) {
    if (val) {
      this.model = val;
    }  
  }

  @Output() onChange: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  handleChange() {
    this.onChange.emit(this.model);
  }

}
