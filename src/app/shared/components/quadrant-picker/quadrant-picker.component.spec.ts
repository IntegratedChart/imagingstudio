import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuadrantPickerComponent } from './quadrant-picker.component';
import { FormsModule } from '@angular/forms';

describe('QuadrantPickerComponent', () => {
  let component: QuadrantPickerComponent;
  let fixture: ComponentFixture<QuadrantPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuadrantPickerComponent ],
      imports: [
        FormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuadrantPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
