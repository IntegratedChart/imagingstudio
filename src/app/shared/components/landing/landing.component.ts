import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  private logger:any;
  
  constructor(  ) { 

    var self = this;
    if ((<any>window).require) {
      try {
        self.logger = (<any>window).require('electron-log');
      } catch (error) {
        throw error
      }
    } else {
      console.warn('Could not load electron ipc');
    }

  }

  ngOnInit() {
    console.log('Inside of landing')
  }

   
}
