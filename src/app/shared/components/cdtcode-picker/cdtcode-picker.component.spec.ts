import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CdtcodePickerComponent } from './cdtcode-picker.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

describe('CdtcodePickerComponent', () => {
  let component: CdtcodePickerComponent;
  let fixture: ComponentFixture<CdtcodePickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CdtcodePickerComponent ],
      imports: [
        FormsModule,
        NgbModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CdtcodePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
