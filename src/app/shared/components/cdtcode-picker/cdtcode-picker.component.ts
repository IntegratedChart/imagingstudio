import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { DataStoreService } from '../../services/data-store.service';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
  selector: 'app-cdtcode-picker',
  templateUrl: './cdtcode-picker.component.html',
  styleUrls: ['./cdtcode-picker.component.scss']
})
export class CdtcodePickerComponent implements OnInit {
  cdtCodes = [];
  searchedCode = '';
  
  @Input()
  set selectedValue(val: string) {
    if (val) {
      this.searchedCode = val;
    }  
  }

  @Output() onChange: EventEmitter<any> = new EventEmitter();

  // search method for typeahead
  search = function (searchInput: Observable<string>) {
    return searchInput.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      map(input => {
        if (input.length < 1) {
          return [];
        }

        return this.cdtCodes.filter(item => {
          return item.cdt_code.search(input) > -1;
        }).map(item => `${item.cdt_code} - ${item.short_description}` ).splice(0, 10);
      })
    );
  }.bind(this);

  constructor(
    private dataStore: DataStoreService,
  ) { }

  ngOnInit() {
    this.cdtCodes = this.dataStore.getDataByTypeAndKey('clinicData', 'cdtCodes') || [];
  }

  handleChange() {
    if (this.searchedCode) {
      const codeArr = this.searchedCode.split('-');
      const cdtCode = codeArr[0].trim();
      
      // find and emit cdt code object from cdt codes array
      const selectedCodeObject = this.cdtCodes.filter(code => code.cdt_code === cdtCode);
      this.onChange.emit(selectedCodeObject[0]);
    }
  }
  
}
