import { Component, OnInit } from '@angular/core';
import { SystemService } from '../../services/system.service';
import { AuthComponent } from 'src/app/auth/components/auth/auth.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'src/app/auth/services/auth.service';


@Component({
  selector: 'app-connection-status',
  templateUrl: './connection-status.component.html',
  styleUrls: ['./connection-status.component.scss']
})
export class ConnectionStatusComponent implements OnInit {

  public isConnected:boolean = false;
  public statusSubscription:any = null;
  constructor(
    private authService:AuthService,
    private modalService:NgbModal
  ) { }

  ngOnInit() {
    this.statusSubscription = this.authService.authStatus.subscribe((status) => {
      console.log("connection status update from auth service ", status)
      this.isConnected = status;
    })
  }

  ngOnDestroy(){
    if (this.statusSubscription) {

      this.statusSubscription.unsubscribe();
    }
  }

  logIn() {
    if (!this.isConnected) {
      const uplaodModalRef = this.modalService.open(AuthComponent, { windowClass: 'mt-4', size: 'xs', backdrop: 'static' });

      uplaodModalRef.componentInstance.isModal = true;
      
      uplaodModalRef.result.then((data) => {
        console.log(data);
      }).catch((error) => console.log(error));
    }
  }

  // setStatusClass() {
  //   if (this.isConnected) {
  //     return 'text-success';
  //   } else {
  //     return 'text-danger';
  //   }
  // }
}
