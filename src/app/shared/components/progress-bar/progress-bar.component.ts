import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { ProgressBarConfig } from '../../interfaces/progressBarConfig';

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss']
})
export class ProgressBarComponent implements OnInit {
  @Input() config:ProgressBarConfig
  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes:SimpleChanges) {
    if(changes.config && changes.config.currentValue !== null) {
      console.log("we have changes to the config.")
    }
  }

}
