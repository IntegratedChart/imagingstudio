import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToothPickerComponent } from './tooth-picker.component';
import { FormsModule } from '@angular/forms';

describe('ToothPickerComponent', () => {
  let component: ToothPickerComponent;
  let fixture: ComponentFixture<ToothPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToothPickerComponent ],
      imports: [
        FormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToothPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
