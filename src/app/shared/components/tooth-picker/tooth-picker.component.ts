import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { TOOTH_NUMBERS } from '../../constants/tooth';
import { getUniqueId } from '../../utils/commonUtils';

@Component({
  selector: 'app-tooth-picker',
  templateUrl: './tooth-picker.component.html',
  styleUrls: ['./tooth-picker.component.scss']
})
export class ToothPickerComponent implements OnInit {
  primary: boolean = false;
  supernumerary: boolean = false;
  model: string;
  uniqueId: string = getUniqueId();
  
  @Input()
  set selectedValue(val: string) {
    if (val) {
      this.model = val;
      this.setInitialFlags();
      this.setCurrentOptions();
    }  
  }

  @Output() onChange: EventEmitter<string> = new EventEmitter();

  currentOptions = TOOTH_NUMBERS.ALL_ADULT_REGULAR_TEETH;

  constructor() { }

  ngOnInit(): void {
  }

  handleModelChange(): void {
    this.setCurrentOptions();
  }
  
  setCurrentOptions(): void {
    if (this.primary) {
      this.currentOptions = (this.supernumerary) ? 
          TOOTH_NUMBERS.ALL_PRIME_SUPER_TEETH : TOOTH_NUMBERS.ALL_PRIME_REGULAR_TEETH;
    } else if (this.supernumerary) {
      this.currentOptions = TOOTH_NUMBERS.ALL_ADULT_SUPER_TEETH;
    } else {
      this.currentOptions = TOOTH_NUMBERS.ALL_ADULT_REGULAR_TEETH
    }
  }

  handleChange() {
    this.onChange.emit(this.model);
  }

  setInitialFlags() {
    if (!this.model) {
      return;
    }

    // search prime super teeh
    const primeSuper = TOOTH_NUMBERS.ALL_PRIME_SUPER_TEETH.filter(tooth => tooth == this.model);

    if (primeSuper.length) {
      this.primary = true;
      this.supernumerary = true;
      return;
    } 
    
    // search all prime regular teeth if not found in all prime super teeth
    const primeRegular = TOOTH_NUMBERS.ALL_PRIME_REGULAR_TEETH.filter(tooth => tooth == this.model);

    if (primeRegular.length) {
      this.primary = true;
      return;
    }
    
    // search all adult super teeth if not found in above both cases
    const superNum = TOOTH_NUMBERS.ALL_ADULT_SUPER_TEETH.filter(tooth => tooth == this.model);

    if (superNum.length) {
      this.supernumerary = true;
    }

    return;
  }

}
