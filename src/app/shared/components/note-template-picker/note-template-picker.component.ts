import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DataStoreService } from '../../services/data-store.service';

@Component({
  selector: 'app-note-template-picker',
  templateUrl: './note-template-picker.component.html',
  styleUrls: ['./note-template-picker.component.scss']
})
export class NoteTemplatePickerComponent implements OnInit {
  templateList;
  model = '';

  @Output() onSelect: EventEmitter<any> = new EventEmitter();

  constructor(
    private dataStore: DataStoreService,
  ) { }

  ngOnInit() {
    this.templateList = this.dataStore.getDataByTypeAndKey('clinicData', 'noteTemplates');
  }

  handleChange() {
    if (!this.model) { return; }

    const template = this.templateList.filter(template => template._id == this.model)[0];

    if (!template) { return; }

    this.onSelect.emit(template);
  }

}
