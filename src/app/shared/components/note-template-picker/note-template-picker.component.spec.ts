import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoteTemplatePickerComponent } from './note-template-picker.component';
import { DataStoreService } from '../../services/data-store.service';
import { FormsModule } from '@angular/forms';

describe('NoteTemplatePickerComponent', () => {
  let component: NoteTemplatePickerComponent;
  let fixture: ComponentFixture<NoteTemplatePickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoteTemplatePickerComponent ],
      providers: [
        DataStoreService
      ],
      imports: [
        FormsModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoteTemplatePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
