import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss']
})
export class DatePickerComponent implements OnInit {
  dateModel;
  
  @Input()
  set initialVal(val) {
    if (val && !this.dateModel) {
      console.log('setting date')
      const dateArr = val.split('-');
      console.log(dateArr)
      this.dateModel = {
        year: Number(dateArr[0]),
        month: Number(dateArr[1]),
        day: Number(dateArr[2]),
      }
    }
  }

  @Output() onSelect: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  handleChange() {
    if (!this.dateModel) {
      return;
    }

    const { year, day, month } = this.dateModel;

    if (!year || !day || !month) {
      return;
    }

    const dateString = `${year}-${ ('0' + month).slice(-2) }-${ ('0' + day).slice(-2) }`;
    console.log('out', dateString);
    this.onSelect.emit(dateString);
  }

}
