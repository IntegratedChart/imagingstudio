import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customEmail'
})
export class CustomEmailPipe implements PipeTransform {

  transform(email:any): any {
    if (!email || email == "noemail@tab32.com") {
      return "";
    } else {
      return email;
    }
    
  }

}
