import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import { DatePipe } from "@angular/common";

@Pipe({
  name: 'customDate'
})
export class CustomDatePipe implements PipeTransform {

  constructor(private datePipe: DatePipe){}

  transform(dateToFormat: any, formatString:any): any {
    var self = this;
    if (!dateToFormat) {
      return "";
    }
    if (moment(dateToFormat).isSame("1776-07-04", "year")) {
      return "";
    } else {
      return this.datePipe.transform(dateToFormat, formatString)
    }
  }

}
