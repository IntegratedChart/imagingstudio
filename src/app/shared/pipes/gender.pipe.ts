import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'gender'
})
export class GenderPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if (value == '0' || value == 'female') {
      return 'Female'
    } else if (value == '1' || value == 'male') {
      return 'Male'
    }

    return '';
  }

}
