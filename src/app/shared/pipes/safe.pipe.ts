import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'safe'
})
export class SafePipe implements PipeTransform {

  constructor(
    private sanitizer: DomSanitizer
  ) {
  }

  transform(value: any, type?: any): any {
    switch (type) {
      case 'url':
        return this.sanitizer.bypassSecurityTrustResourceUrl(value);

      case 'html':
        return this.sanitizer.bypassSecurityTrustHtml(value);

      default:
        return null;
    }
  }

}
