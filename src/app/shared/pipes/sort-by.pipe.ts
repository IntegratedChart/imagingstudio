import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortBy'
})
export class SortByPipe implements PipeTransform {

  transform(value: any, key: string, order: string = 'asc', ...args: any[]): any {
    if (!value || !Array.isArray(value) || !key) {
      return value;
    }

    value.sort((a, b) => {
      if (order === 'desc') {
        return a[key] > b[key] ? -1 : 1
      }

      return a[key] > b[key]? 1 : -1
    });

    return value;
  }

}
