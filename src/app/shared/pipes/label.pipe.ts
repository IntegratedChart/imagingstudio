import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'label'
})
export class LabelPipe implements PipeTransform {

  transform(wordsStr: any, capitalized:any = null): any {
    if(!wordsStr){
      return wordsStr;
    }

    var words = wordsStr.split(/[ \-_]/);
    var finalWords = '';

    for (var i = 0; i < words.length; i++) {
      words[i] = words[i].trim();
      if (!capitalized) {
        finalWords += words[i] + ' ';
      } else if (capitalized == 'firstWord') {
        if (i > 0) {
          finalWords += words[i] + ' ';
        } else {
          finalWords += words[i].substring(0, 1).toUpperCase() + words[i].substring(1) + ' ';
        }
      } else if (capitalized == 'initial') {
        finalWords += words[i].substring(0, 1).toUpperCase() + ' ';
      } else {
        finalWords += words[i].substring(0, 1).toUpperCase() + words[i].substring(1) + ' ';
      }
    }

    return finalWords.trim();
  }

}
