import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './components/landing/landing.component';

const sharedRoutes: Routes = [
    { path: 'landing', component: LandingComponent, data: { title: ''} }
];

@NgModule({
    imports: [RouterModule.forChild(sharedRoutes)],
    exports: [RouterModule]
})
export class SharedRoutingModule { }
