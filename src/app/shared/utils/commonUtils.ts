// Some useful function to have :)

export const getPatientInfoByFolder = function (selectedFolder) {
    return selectedFolder.replace(/-/g, ' ');
}

export const delay = function (wait: number = 600) {
    const delayDuration: number = wait;
    let timeout: ReturnType<typeof setTimeout>;
    let delayedPromise: Promise<any>;

    return function (): Promise<null> {
        // clear previous timer
        if (timeout) {
            clearTimeout(timeout);
        }

        delayedPromise =  new Promise((resolve, reject) => {
            // set new timer
            timeout = setTimeout(() => {
                clearTimeout(timeout);
                resolve();
            }, delayDuration);

        });
        
        return delayedPromise;
    }
}

export const getUniqueId = function (prefix: string = ''): string {
   return prefix + '_' + Math.random().toString(36).substr(2, 9);
}