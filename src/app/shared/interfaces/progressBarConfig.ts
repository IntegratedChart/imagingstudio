export interface ProgressBarConfig {
    animated?:boolean,
    height?:string, 
    max?:number, 
    showValue?:boolean, 
    striped?:boolean, 
    textType?:string, 
    type?:string, 
    value?:number,
    error?:boolean,
}