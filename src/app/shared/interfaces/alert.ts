export interface Alert {
    title?: string,
    type?: string,
    class?: any,
    text?: any
    confirmColor?: string,
    confirmText?: string,
    cancelText?: string,
    closeOnConfirm?: boolean,
    outsideClick?: boolean,
    closeOnCancel?: boolean,
    showCancel?:boolean,
    timeout?: number,
    showCloseButton?: boolean,
    closeHtml?:boolean
}