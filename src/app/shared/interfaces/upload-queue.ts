import { UploadData } from './upload-data';

export interface UploadQueue {
    [key: string]: UploadData[]
}
