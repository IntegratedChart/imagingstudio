export interface UploadData {
    patientId: string,
    patientName: string,
    patientDob: string,
    isOffline: boolean,
    uploadData: uploadDataDetails,
    isStarted: boolean,
    isComplete: boolean,
    isError: boolean,
    progressConfig?: any,
    isUploading?: boolean
}

export interface uploadDataDetails {
    noteCount: number,
    treatmentCount: number
}
