import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../services/auth.service';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {  

  public authenticationSubscription = null;
  public signInAttempt = false;

  @Input() isModal:boolean
  constructor(
    public modal: NgbActiveModal,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.authenticationSubscription = this.authService.authStatus.subscribe((isAuthenticated) => {
      if (isAuthenticated) {
        this.signInAttempt = false;
        this.modal.close();
      }
    })
  }

  ngOnDestroy() {
    if (this.authenticationSubscription) {
      this.authenticationSubscription.unsubscribe();
    }
  }

  async signIn() {
    try {
      this.signInAttempt = true;
      await this.authService.signIn();
      this.modal.close();
    } catch (e) {
      console.log("error logging in", e);
    }
  }

}
