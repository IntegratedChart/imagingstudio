import { Injectable } from '@angular/core';
import { SystemService } from 'src/app/shared/services/system.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { DataStoreService } from 'src/app/shared/services/data-store.service';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private userAuthCode = null;
  private isAuthenticatedSource = new BehaviorSubject<any>(this.isAuthenticated());
  public authStatus: Observable<any> = this.isAuthenticatedSource.asObservable();

  constructor(
    private systemService:SystemService,
    private dataStore: DataStoreService
  ) {
    this.userAuthCode = this.dataStore.getDataByTypeAndKey('authentication', 'userAuthCode');
    this.isAuthenticatedSource.next(this.isAuthenticated());

    this.dataStore.dataUpdate.subscribe((update) => {
      console.log("we have a data store update ", update);
      if (update && update.dataType === 'authentication') {
        this.userAuthCode = update.data.userAuthCode;
        console.log('user auth ', this.userAuthCode)
        this.isAuthenticatedSource.next(this.isAuthenticated());
      }
    })
  }

  isAuthenticated() {
    return this.userAuthCode ? true : false;
  }

  async signIn() {
    try {
      let requestType = 'getGoogleAuthUrl';
      return await this.systemService.submitSystemRequest(requestType, null);
    } catch (e) {
      throw e;
    }
  } 
}
