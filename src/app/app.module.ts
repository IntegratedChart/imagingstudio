import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
// import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from "./shared/shared.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { DragDropModule } from '@angular/cdk/drag-drop'
import { DownloadModule } from './download/download.module';
import { PatientModule } from './patient/patient.module';

import { AppComponent } from './app.component';
import { SystemService } from './shared/services/system.service';
import { HomePageComponent } from './home-page/home-page.component';
import { XrayModule } from './xray/xray.module';
import { UploadModule } from './upload/upload.module';
import { AuthModule } from './auth/auth.module';
import { RefreshModule } from './refresh/refresh.module';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    // NgbModule,
    SharedModule,
    DownloadModule,
    PatientModule,
    XrayModule,
    UploadModule,
    AuthModule,
    RefreshModule
  ],
  entryComponents: [
    AppComponent,
  ],
  providers: [
    SystemService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
