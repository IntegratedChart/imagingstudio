import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Patient } from '../patient/interfaces/patient';
import { Router } from '@angular/router';
import { AlertService } from '../shared/services/alert.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UploadComponent } from '../upload/components/upload/upload.component';
import { AuthService } from '../auth/services/auth.service';
import { DataStoreService } from '../shared/services/data-store.service';
import { SystemService } from '../shared/services/system.service';
import * as moment from 'moment';
import { RefreshService } from '../refresh/services/refresh.service';
import { LoggerService } from '../shared/services/logger.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  public searchedPatients: Patient[] = [];
  public authSubscription:any = null;
  public dataSubscription:any = null;
  public isConnected:boolean = false;
  public expiredData:any[] = [];
  public isDownloading:boolean = false;
  public isRefreshing:boolean = false;

  constructor(
    private router: Router,
    private authService: AuthService,
    private systemService:SystemService,
    private dataStore: DataStoreService,
    private alertService:AlertService,
    public modalService: NgbModal,
    private changeDetector: ChangeDetectorRef,
    private logger:LoggerService
  ) { }

  ngOnInit() {
    this.logger.info("init home page ");

    this.authSubscription = this.authService.authStatus.subscribe((isConnected) => {
      this.isConnected = isConnected;
    })

    this.isDownloading = this.dataStore.getDataByTypeAndKey('downloadStatus', 'isDownloading') || false;
    
    this.dataSubscription = this.dataStore.dataUpdate.subscribe((update) => {
      this.logger.info("we have a data update inside of home component");

      if (update && update.dataType === 'downloadStatus') {
        this.logger.info("we have a dowload status update")
        this.logger.object(update);
        this.logger.info("current downloading status ", this.isDownloading);
        if (this.isDownloading !== update.data.isDownloading) {
          this.logger.info("we have a download status change")
          this.isDownloading = update.data.isDownloading;
          this.logger.info("marking for change");
          this.changeDetector.markForCheck();
          this.logger.info("detecting for change");
          this.changeDetector.detectChanges();
          if (!this.isDownloading) {
            let toast = {
              text: "Patient download complete!",
              class: "bg-success text-white"
            }
            this.alertService.showToaster(toast)
          }
        }
      }
    })
  }

  ngOnDestroy() {
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }

    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }

  }

  handleSearchResult(data) {
    this.searchedPatients = data || [];
  }

  selectPatient(patientData) {
    this.router.navigate(['/offline-patient', { data: JSON.stringify(patientData) }]);
  }

  async openDownloadPage():Promise<void> {
    try {
      this.expiredData = await this.systemService.getOfflineUploadData('expiredOnly');

      if (this.expiredData && this.expiredData.length) {
        let prompt = {
          title: "Expired Downloads",
          text: "There are some dowloaded records that are expired and need to be uploaded before downloading new data. Please upload data before continuing.",
          showCancel: true,
          confirmText: "Upload"
        }
        let answer = await this.alertService.showAlert(prompt)

        if (answer && answer.alert == 'confirm') {
          this.openUploadPage('expiredOnly', this.expiredData);
        } else {
          console.log("cancelled operation");
        }
      } else {
        this.router.navigate(['/download'])
      }
      return
    } catch (e) {
      console.log("error opening the download page")
    }
  }

  openUploadPage(type:string="includeAll", data:any=null): void {
    const uplaodModalRef = this.modalService.open(UploadComponent, { windowClass: 'mt-4', size: 'xl', backdrop: 'static' });

    uplaodModalRef.componentInstance.type = type;
    uplaodModalRef.componentInstance.existingUploadableData = data;

    uplaodModalRef.result.then((data) => {
      console.log(data);
    }).catch((error) => console.log(error));
  }

  refreshNeededClass(patient) {
    return this.refreshNeeded(patient) ? 'border-left-danger border-left-1' : '';
  }

  refreshNeeded(patient) {
    let today = moment().utc();
    today.hour(0);
    today.minute(0);
    today.second(0);
    today.millisecond(0);
    let refreshDate = moment(patient.refreshDate);

    // console.log("today", today)
    // console.log("refreshDate", refreshDate)
    return moment(today).isSameOrAfter(refreshDate) ? true : false;
  }

  onRefreshChange(status) {
    this.isRefreshing = status;
  }
}
