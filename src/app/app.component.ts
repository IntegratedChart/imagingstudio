import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { CustomTitlebarService } from './shared/services/custom-titlebar.service';
import { LoggerService } from './shared/services/logger.service';
// import { UploadQueueService } from './upload/services/upload-queue.service';
// import { AlertService } from '../app/shared/services/alert.service';
// import { HelloWorldComponent } from './xray/hello-world/hello-world.component';
// import { ModalService } from "./shared/services/modal.service";
// import { PushNotificationService } from "./shared/services/push-notification.service"

declare var window: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private customTitleBar: CustomTitlebarService,
    private logger:LoggerService
    // private uploadQueueService: UploadQueueService
  ){}

  ngOnInit(){
    this.customTitleBar.create();

    this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd),
      map(() => this.getTitleFromRoute())
    ).subscribe(this.setTitle.bind(this));
  }
    
  getTitleFromRoute() {
    const child = this.activatedRoute.firstChild;

    if (child.snapshot.data['title']) {
      return child.snapshot.data['title'];
    }

    return null;
  }

  setTitle(title: string) {
    const baseTitle = 'T32 Imaging Studio | Offline EHR';
    const appTitle = `${baseTitle} ${(title) ? '| ' + title : '' }`;
    this.customTitleBar.updateTitle(appTitle);
  }
}
