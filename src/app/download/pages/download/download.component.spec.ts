import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadComponent } from './download.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { QuickDownloadFormComponent } from '../../components/quick-download-form/quick-download-form.component';
import { DownloadService } from '../../services/download.service';
import { ToastService } from 'src/app/shared/services/toast.service';
import { PatientModule } from 'src/app/patient/patient.module';
import { RouterTestingModule } from '@angular/router/testing';

class MockRouter {
  navigate = jasmine.createSpy('navigate');
}

describe('DownloadComponent', () => {
  let component: DownloadComponent;
  let fixture: ComponentFixture<DownloadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DownloadComponent, QuickDownloadFormComponent ],
      imports: [
        SharedModule,
        PatientModule,
        RouterTestingModule,
      ],
      providers: [
        DownloadService,
        ToastService,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
