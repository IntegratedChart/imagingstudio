import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DownloadService } from '../../services/download.service';
import { Patient } from 'src/app/patient/interfaces/patient';
import { ToastService } from 'src/app/shared/services/toast.service';
import { DataStoreService } from 'src/app/shared/services/data-store.service';
import { LoggerService } from 'src/app/shared/services/logger.service';

@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.scss']
})
export class DownloadComponent implements OnInit {
  searchResult: Patient[] = [];
  selectedForDownload: Patient[] = [];
  public dataSubscription:any = null;
  public isDownloading:boolean = false;

  constructor(
    private downloadService: DownloadService,
    private toast: ToastService,
    private dataStore: DataStoreService,
    private changeDetector: ChangeDetectorRef,
    private logger:LoggerService,
  ) { }

  ngOnInit() {
    this.isDownloading = this.dataStore.getDataByTypeAndKey('downloadStatus', 'isDownloading');
    
    this.dataSubscription = this.dataStore.dataUpdate.subscribe((update) => {
      if (update && update.dataType === 'downloadStatus') {
        this.logger.info("the downlaod status update inside of dowload component ", update);
        this.isDownloading = update.data.isDownloading;
        this.changeDetector.markForCheck();
        this.logger.info("detecting changes");
        this.changeDetector.detectChanges();
      }
    })
  }

  ngOnDestroy() {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  handleSearchedPatients(patients: Patient[]): void {
    console.log('patients searched::', patients);
    this.searchResult = patients || [];
  }

  // selects all patients from search result
  selectAll(): void {
    const currentSelectedIds = this.selectedForDownload.map(patient => patient._id);
    const patients = this.searchResult.filter(patient => currentSelectedIds.indexOf(patient._id) == -1);
    this.selectedForDownload = [...this.selectedForDownload, ...patients];
  }

  selectSinglePatient(patient: Patient): void {
    if (!this.isSelected(patient._id)) {
      this.selectedForDownload.push(patient);
    }
  }

  isSelected(patientId: string): boolean {
    const patient = this.selectedForDownload.filter(patient => patient._id == patientId);
    return patient.length > 0;
  }

  removeSelected(patientId: string): void {
    for (let i in this.selectedForDownload) {
      if (this.selectedForDownload[i]._id == patientId) {
        this.selectedForDownload.splice(Number(i), 1);
        break;
      }
    }
  }

  async download() {
    try {
      const patientIds = this.selectedForDownload.map(patient => patient._id);
  
      if (patientIds.length) {
        await this.downloadService.download({ patientIds }, 'offlineDownload')
        this.selectedForDownload.length = 0;
        this.toast.show('Request Successful', { classname: 'bg-success text-white' });
        await this.downloadService.download({}, 'offlineDownloadCdtCodes');
        await this.downloadService.download({}, 'offlineDownloadNoteTemplates');
        return await this.downloadService.getClinicData();
      }
    } catch (e) {
      this.toast.show('Download Failed', { classname: 'bg-danger text-white' });
      this.isDownloading = false;
    }
  }

}
