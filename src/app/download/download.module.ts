import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { PatientModule } from '../patient/patient.module';

import { DownloadRoutingModule } from './download-routing.module';
import { QuickDownloadFormComponent } from './components/quick-download-form/quick-download-form.component';
import { DownloadComponent } from './pages/download/download.component';

@NgModule({
  declarations: [
    QuickDownloadFormComponent,
    DownloadComponent,
  ],
  imports: [
    SharedModule,
    DownloadRoutingModule,
    PatientModule,
  ],
  entryComponents:[
  ],
  exports:[
    DownloadRoutingModule,
    DownloadComponent,
  ],
  providers:[
  ]
})
export class DownloadModule { }
