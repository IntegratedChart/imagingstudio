import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DownloadComponent } from './pages/download/download.component';

const downloadRoutes: Routes = [
    { path: 'download', component: DownloadComponent, data: { title: 'Download'} }
];

@NgModule({
    imports: [RouterModule.forChild(downloadRoutes)],
    exports: [RouterModule]
})
export class DownloadRoutingModule { }
