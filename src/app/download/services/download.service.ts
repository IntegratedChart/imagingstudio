import { Injectable } from '@angular/core';
import { SystemService } from 'src/app/shared/services/system.service';
import { DataStoreService } from 'src/app/shared/services/data-store.service';
import { LoggerService } from 'src/app/shared/services/logger.service';

@Injectable({
  providedIn: 'root'
})
export class DownloadService {
  constructor(
    private systemService: SystemService,
    private dataStore: DataStoreService,
    private logger:LoggerService
  ) { }

  async download(query: object, dataType) {
    try {
      query['clinic_id'] = this.dataStore.getClinicId() || null;
      await this.systemService.submitSystemRequest(dataType, query);
    } catch (e) {
      throw e;
    }
  }

  async getClinicData() {
    try {
      let clinicId = this.dataStore.getClinicId() || null;

      if (!clinicId) {
        this.logger.info("failed to get clinic data in download service: no clinicId found")
        return 
      }

      return await this.systemService.getCurrentClinicData(clinicId);
    } catch (e) {

    }
  }
}
