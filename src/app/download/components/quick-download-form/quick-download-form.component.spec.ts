import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuickDownloadFormComponent } from './quick-download-form.component';
import { SharedModule } from 'src/app/shared/shared.module';

describe('QuickDownloadFormComponent', () => {
  let component: QuickDownloadFormComponent;
  let fixture: ComponentFixture<QuickDownloadFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuickDownloadFormComponent ],
      imports: [
        SharedModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuickDownloadFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
