import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DownloadService } from '../../services/download.service';
import { DataStoreService } from 'src/app/shared/services/data-store.service';
import { NgbDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { ToastService } from 'src/app/shared/services/toast.service';
import { LoggerService } from 'src/app/shared/services/logger.service';

@Component({
  selector: 'app-quick-download-form',
  templateUrl: './quick-download-form.component.html',
  styleUrls: ['./quick-download-form.component.scss']
})
export class QuickDownloadFormComponent implements OnInit {

  clinicLocations: any = [];
  quickSearchForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private downloadService: DownloadService,
    private dataStore: DataStoreService,
    private toast: ToastService,
    private logger:LoggerService
  ) { }

  ngOnInit() {
    // get clinic locations
    const clinic = this.dataStore.getDataByTypeAndKey('settings', 'clinic') || {}
    this.clinicLocations = clinic.locations || [];

    //[Important] quick download will only supports download by appointment date right now
    // remove all and age filters

    this.quickSearchForm = this.formBuilder.group({
      location: [null, Validators.required],
      selectionType: ['aptDate', Validators.required],
      selectionCriteria: ['', Validators.required],
    });

    // initialize custom validation
    this.customValidation();
  }

  customValidation(): void {
    this.quickSearchForm.get('selectionType').valueChanges.subscribe(
      (selectionType: string) => {
        if (selectionType === 'age' || selectionType === 'aptDate') {
          this.quickSearchForm.get('selectionCriteria').setValidators([Validators.required]);
        } else {
          this.quickSearchForm.get('selectionCriteria').clearValidators();
        }

        this.quickSearchForm.get('selectionCriteria').setValue('');
      }
    );
  }

  setValue(value, formField) {
    this.quickSearchForm.get(formField).setValue(value || null);
  }

  async handleSubmission() {
    try {
      if (this.quickSearchForm.invalid) {
        this.toast.show('Invalid Submission', { classname: 'bg-danger text-white' });
        return;
      }
      
      const query = {
        location: this.quickSearchForm.value.location,
        // selectionType: this.quickSearchForm.value.selectionType,
        selectionType: 'aptDate',
        selectionCriteria: this.quickSearchForm.value.selectionCriteria,
      }
  
      await this.downloadService.download(query, 'offlineDownload');
      this.toast.show('Request Successful', { classname: 'bg-success text-white' });

      await this.downloadService.download({}, 'offlineDownloadCdtCodes');
      await this.downloadService.download({}, 'offlineDownloadNoteTemplates');
      return await this.downloadService.getClinicData();
    } catch (e) {
      console.log('error quick downloads', e);
      this.logger.info('error quick doqnload ', e);
      if (e == 'Patient(s) not found') {
        this.toast.show('Patients with given criteria not found, please try again with a different criteria.', {classname: 'bg-warning text-white'})
      } else {
        this.toast.show('Download Failed', { classname: 'bg-danger text-white' });
      }
    }
  }
}
